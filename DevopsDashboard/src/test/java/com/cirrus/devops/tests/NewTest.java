package com.cirrus.devops.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class NewTest {

	@Test
	public void test() {
		 WebDriverManager.chromedriver().setup();
		//System.setProperty("webdriver.chrome.driver", this.getClass().getClassLoader().getResource("chromedriver").getPath());
		ChromeOptions options=new ChromeOptions();
		options.addArguments("--headless");
		options.addArguments("--no-sandbox");
		  options.addArguments("disable-setuid-sandbox");
		    options.addArguments("no-sandbox");
		    options.addArguments("allow-insecure-localhost");
		    WebDriver driver = new ChromeDriver(options);
		
		driver.get("https:google.com");
		System.out.println(driver.getTitle());
		String title = driver.getTitle();				 
		Assert.assertTrue(title.contains("Google")); 		
		driver.close();
		driver.quit();
	}
}
