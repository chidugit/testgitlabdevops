package com.cirrus.devops.tests;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;

import com.cirrus.devops.util.PropertyUtility;
@RunWith(JUnit4.class)
public class PropertyUtilityTest {
	String git_url ="http://192.168.137.100";
	String	urlSuffix="/oauth/token?";
	String urlparameters="grant_type=password&username=@username@&password=@password@";
	String loginUrl="/LoginService/Login?name=@username@&password=@password@";
	String projectUrl="/GitService/projects?accessToken=@accessToken@";
	String jenkinProjectUrl = "/JenkinsService/projects/all?name=@username@&password=@password@";
	String gitProjectUrl="/api/v4/projects?access_token=@accessToken@";
	String appUrl ="/rest";
	
	PropertyUtility object = PropertyUtility.getPropertyUtility();
	
	public PropertyUtilityTest() {}
	@Test
	public void Testgit_url() {
		assertEquals(git_url,object.getProperty("git_url"));
	}
	@Test
	public void TesturlSuffix() {
		assertEquals(urlSuffix,object.getProperty("urlSuffix"));
	}
	@Test
	public void Testurlparameters() {
		assertEquals(urlparameters,object.getProperty("urlparameters"));
	}
	
	@Test
	public void testloginUrl() {
		assertEquals(loginUrl,object.getProperty("loginUrl"));
	}
	
	@Test
	public void testprojectUrl() {
		assertEquals(projectUrl,object.getProperty("projectUrl"));
	}
	@Test
	public void testjenkinProjectUrl() {
		System.out.println("--->"+jenkinProjectUrl);
		assertEquals(jenkinProjectUrl,object.getProperty("jenkinProjectUrl"));
	}
	
	@Test
	public void testgitProjectUrl() {
		assertEquals(gitProjectUrl,object.getProperty("gitProjectUrl"));
	}
	
	@Test
	public void testappUrl() {
		assertEquals(appUrl,object.getProperty("appUrl"));
	}
	

	@Test
	public void test() {
		System.setProperty("webdriver.chrome.driver", this.getClass().getClassLoader().getResource("chromedriver").getPath());
		ChromeOptions options=new ChromeOptions();
		options.addArguments("--headless");
		options.addArguments("--no-sandbox");
		 /* options.addArguments("disable-setuid-sandbox");
		    options.addArguments("no-sandbox");
		    options.addArguments("allow-insecure-localhost");*/
		    WebDriver driver = new ChromeDriver(options);
		
		driver.get("https:google.com");
		System.out.println(driver.getTitle());
		String title = driver.getTitle();				 
		Assert.assertTrue(title.contains("Google")); 		
		driver.close();
		driver.quit();
	}
}
