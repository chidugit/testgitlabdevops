package com.cirrus.devops.service;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Properties;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.log4j.BasicConfigurator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.cirrus.devops.ControllerServlet;
import com.cirrus.devops.util.PropertyUtility;
import org.apache.log4j.Logger;
@Path("/GitService") 
public class GitService {
	
		static final Logger logger = Logger.getLogger(GitService.class);
	 	private String gitBaseUrl;
	 	private String projectUrl;
		private String accessToken;
		private String gitProjectCreation;
		private String gitgroupurl;
		private String gitSingleGroupUrl;
		private String gitSingleProjectUrl;
		private String commitsUrl;
		 private String name;
		 private String path;
		 private String tagUrl;
		 private String apiSuffix = "api/v4/";
		 private String projectApiSuffix = "projects";
		 private String parameters = "?access_token= ";
		
		 String projectPath = null; 
		 String projectName = null; 
		 String groupID=null;
		 private HttpURLConnection con;
		 String inputLine;
		 JSONObject pair;
		 String projectCreationMsg;
		 BufferedReader in;
		 JSONArray array = new JSONArray();
		 String url;
		public  JSONObject jobj3;
	 public GitService() {}
	 
	   @SuppressWarnings({ "unchecked", "unused" })
	@GET 
	   @Path("/projects")
	   @Produces(MediaType.APPLICATION_JSON)
	   public Response getprojects( @NotNull @QueryParam("accessToken") String accessToken, @QueryParam("owned") String owned){
		init(accessToken);
		logger.info("getprojects function started");
		HashMap<String,String> pair = new HashMap<String,String>() ;
		
		
		try{
			if(owned==null){
				url=gitBaseUrl+projectUrl+"&statistics=true";
			}
			else{
				url=gitBaseUrl+projectUrl+"&statistics=true"+"&owned="+owned;
			}
				
		
			HttpURLConnection con = (HttpURLConnection) ((new URL(url).openConnection()));
			System.out.println("projectUrl"+con.getURL());
		 System.out.println(url);
		 con.setDoOutput(true);
		 con.setRequestMethod("GET");
		 con.connect();
			
		 System.out.println(con.getResponseCode());
        BufferedReader in = new BufferedReader(
      		  new InputStreamReader(con.getInputStream()));
        logger.debug("Connection was executed");
      String inputLine;
      StringBuffer content = new StringBuffer();
      
      System.out.println("in !! "+in);
      while ((inputLine = in.readLine()) != null) {
    	  System.out.println("inputLine !! "+inputLine);
    	  logger.debug("inputLine !! "+inputLine);
     	    content.append(inputLine);
      }
      
    
      JSONParser parser = new JSONParser(); 
      JSONArray jobj = (JSONArray)parser.parse(content.toString());
      
      for(int i=0;i<jobj.size();i++) 
      {
    	  	pair = new HashMap<String,String>();
   	    	JSONObject jobj1 =(JSONObject)jobj.get(i);
   	    	System.out.println(jobj1.toJSONString());
   	     System.out.println("name and url "+jobj1.get("name").toString()+ " ::: "+jobj1.get("web_url").toString());
      		pair.put("name", jobj1.get("name").toString());
      		pair.put("web_url", jobj1.get("web_url").toString());
      		JSONObject stat=(JSONObject)jobj1.get("statistics");
      		pair.put("commit_count", stat.get("commit_count").toString());
      		try{
      		JSONObject owner=(JSONObject)jobj1.get("owner");
      		pair.put("ownerName",owner.get("name").toString());
      		}catch(Exception e){
      			pair.put("ownerName", "No Owner");
      		}
      		pair.put("requestEnabled", jobj1.get("request_access_enabled").toString());
      		pair.put("visibility", jobj1.get("visibility").toString());
      		pair.put("created_at", jobj1.get("created_at").toString());
      		pair.put("id", jobj1.get("id").toString());
      		JSONArray tags=(JSONArray)jobj1.get("tag_list");
//      		for (int j = 0; j < tags.size(); j++) {
//				
//			}
      		
      		array.add(pair);
      		logger.debug("Getting the Details");
      		System.out.println("Response Array:"+array);
      }
      
      con.disconnect();
		}catch(Exception e){
			logger.error("There was some error with the collection");
			pair.put("Status", "Error gettign the projects");
			   return Response.status(500).entity(pair).build();
		}
		System.out.println("response 200 sent");
		return Response.status(200).entity(array).build();
		
	}
	
	public void init(String accessToken){
		
		   BasicConfigurator.configure();
		   this.accessToken = accessToken;
		   PropertyUtility props = PropertyUtility.getPropertyUtility();
		   this.gitBaseUrl = props.getProperty("git_url");
		   this.gitgroupurl=props.getProperty("gitgroupurl");
		   this.projectUrl =  props.getProperty("gitProjectUrl");
		   this.projectUrl = projectUrl.replaceAll("@accessToken@", accessToken);
		   this.gitProjectCreation = props.getProperty("gitProjectCreation");
		   this.gitProjectCreation = gitProjectCreation.replaceAll("@accessToken@", accessToken);
		   this.gitSingleProjectUrl = props.getProperty("gitSingleProjectUrl");
		   this.gitSingleGroupUrl = props.getProperty("gitSingleGroupUrl");
		   this.commitsUrl=props.getProperty("commits_url");
		   this.tagUrl=props.getProperty("tagUrl");
	   }

	public void start(){
		 PropertyUtility props = PropertyUtility.getPropertyUtility();
		this.gitBaseUrl = props.getProperty("git_url");
		this.gitProjectCreation = props.getProperty("gitProjectCreation");
		this.gitgroupurl= props.getProperty("gitgroupurl");
//	this.projectUrl =  props.getProperty("gitProjectUrl");
//		   this.gitProjectCreation = projectUrl.replaceAll("@accessToken@", accessToken);
	}
	
	@SuppressWarnings({ "deprecation", "unused", "unchecked" })
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/createProject")

	public Response postprojects( @QueryParam("access_token") String access_token, @QueryParam("project_name") String project_name,
			@QueryParam("group_name") String groupName, @QueryParam("group_path") String groupPath, @QueryParam("industry") String industry,
				@QueryParam("description") String description,@QueryParam("visibility") String visibility 
			) throws ParseException, MalformedURLException, IOException, URISyntaxException{
		
		String description1;
		description1 = URLEncoder.encode(description).replaceAll("\\+", "%20");
		String name= URLEncoder.encode(project_name).replaceAll("\\+", "%20");
		init(access_token);
		//description=description1;
		Integer statusCode;
//		ControllerServlet obj = new ControllerServlet();
		 logger.info("Inside create project service");
	
		 if(groupName!=null&&groupPath!=null){
			 String gitgroup_name= URLEncoder.encode(groupName).replaceAll("\\+", "%20");
				String gitgroup_path=URLEncoder.encode(groupPath).replaceAll("\\+", "%20");
			 
			 con = (HttpURLConnection) ((new URL(this.gitBaseUrl+this.gitgroupurl+"access_token="+access_token+"&search="+gitgroup_name+"&search="+gitgroup_path).openConnection()));
			con.setRequestMethod("GET");
			 
			 
				BufferedReader in2 = new BufferedReader(new InputStreamReader(con.getInputStream()));
				   StringBuffer content2 = new StringBuffer();
				   while ((inputLine = in2.readLine()) != null) {
					    content2.append(inputLine);
					    }
				   System.out.println("Response Message after GET call for Group "+content2.toString());
				   
				  JSONParser parsers = new JSONParser(); 
				  JSONArray jobj2 = (JSONArray)parsers.parse(content2.toString());		  
				   
				  for(int i=0;i<jobj2.size();i++) 
				  {
					  pair= new JSONObject();
				   jobj3=(JSONObject) jobj2.get(i);
				 
				 
				  }
				
				 if(con.getResponseCode()==200&&!jobj2.isEmpty()){
					 System.out.println(con.getResponseMessage());
			//Git Project creation under Group
					 con=(HttpURLConnection) ((new URL(this.gitBaseUrl+this.gitProjectCreation+"&access_token="+access_token+"&name="+name+"_"+industry+"&namespace_id="+jobj3.get("id")+"&description="+description1).openConnection()));
			con.setRequestMethod("POST");
			
			 BufferedReader in211 = new BufferedReader(new InputStreamReader(con.getInputStream()));
			   StringBuffer content211 = new StringBuffer();
			   while ((inputLine = in211.readLine()) != null) {
				    content211.append(inputLine);
				    }
			   System.out.println("Response Message after GET call for Group "+content211.toString());
			   
			  JSONParser parsers11 = new JSONParser(); 
			  JSONObject jobj211 = (JSONObject)parsers11.parse(content211.toString());		  
			   
			  jobj211.get("http_url_to_repo");

			   
			  FileInputStream in = new FileInputStream(this.getClass().getClassLoader().getResource("config.properties").getFile());
			   Properties props = new Properties();
			   props.load(in);
			   in.close();

			   FileOutputStream out = new FileOutputStream(this.getClass().getClassLoader().getResource("config.properties").getFile());
			   props.setProperty("newGitProjectUrl", jobj211.get("http_url_to_repo").toString());
			   props.store(out, null);
			   out.close();
			  System.out.println(props.getProperty("newGitProjectUrl"));
			//  ControllerServlet.session.setAttribute("newGitProjectUrl", jobj211.get("http_url_to_repo").toString());
				
		}
				 else{
					 con = (HttpURLConnection) ((new URL(this.gitBaseUrl+this.gitgroupurl+"access_token="+access_token+"&name="+gitgroup_name+"&description="+description1+
								"&visibility="+visibility+"&path="+gitgroup_path).openConnection()));
					 con.setDoOutput(true);
					con.setRequestMethod("POST");
					if(con.getResponseCode()==201){
					
					 con = (HttpURLConnection) ((new URL(this.gitBaseUrl+this.gitgroupurl+"access_token="+access_token+"&search="+gitgroup_name+"&search="+gitgroup_path).openConnection()));
						con.setRequestMethod("GET");
					}else{
						return Response.serverError().build();
					}
						 
							BufferedReader in21 = new BufferedReader(new InputStreamReader(con.getInputStream()));
							   StringBuffer content21 = new StringBuffer();
							   while ((inputLine = in21.readLine()) != null) {
								    content21.append(inputLine);
								    }
							   System.out.println("Response Message after GET call for Group "+content21.toString());
							   
							  JSONParser parsers1 = new JSONParser(); 
							  JSONArray jobj21 = (JSONArray)parsers1.parse(content21.toString());		  
							   
							  for(int i=0;i<jobj21.size();i++) 
							  {
								  pair= new JSONObject();
							   jobj3=(JSONObject) jobj21.get(i);
							  	
							  System.out.println(jobj3.get("id").toString());
							 
							  }
							
							 if(con.getResponseCode()==200&&!jobj21.isEmpty()){
								
						//Git Project creation under Group
							
							con=(HttpURLConnection) ((new URL(this.gitBaseUrl+this.gitProjectCreation+"&access_token="+access_token+"&name="+name+"_"+industry+"&namespace_id="+jobj3.get("id")+"&description="+description1).openConnection()));
							con.setRequestMethod("POST");}
							 BufferedReader in211 = new BufferedReader(new InputStreamReader(con.getInputStream()));
							   StringBuffer content211 = new StringBuffer();
							   while ((inputLine = in211.readLine()) != null) {
								    content211.append(inputLine);
								    }
							   System.out.println("Response Message after GET call for Group "+content211.toString());
							   
							  JSONParser parsers11 = new JSONParser(); 
							  JSONObject jobj211 = (JSONObject)parsers11.parse(content211.toString());		  
							   
							  jobj211.get("http_url_to_repo");

						
							   FileInputStream in = new FileInputStream(this.getClass().getClassLoader().getResource("config.properties").getFile());
							   Properties props = new Properties();
							   props.load(in);
							   in.close();

							   FileOutputStream out = new FileOutputStream(this.getClass().getClassLoader().getResource("config.properties").getFile());
							   props.setProperty("newGitProjectUrl", jobj211.get("http_url_to_repo").toString());
							   props.store(out, null);
							   out.close();
							  System.out.println(props.getProperty("newGitProjectUrl"));
							//  ControllerServlet.session.setAttribute("newGitProjectUrl", jobj211.get("http_url_to_repo").toString());
				 }
				
		 } else {
				
		 con = (HttpURLConnection) ((new URL(this.gitBaseUrl+this.gitProjectCreation+"&access_token="+access_token+"&name="+name+"_"+industry+"&description="+description1+
		"&visibility="+visibility).openConnection()));
			 con.setDoOutput(true);
			 con.setRequestMethod("POST");
			 BufferedReader in2 = new BufferedReader(new InputStreamReader(con.getInputStream()));
			   StringBuffer content2 = new StringBuffer();
			   while ((inputLine = in2.readLine()) != null) {
				    content2.append(inputLine);
				    }
			   System.out.println("Response Message after GET call for Group "+content2.toString());
			   
			  JSONParser parsers = new JSONParser(); 
			  JSONObject jobj2 = (JSONObject) parsers.parse(content2.toString());		  
			   jobj2.get("http_url_to_repo");

		  
			   FileInputStream in = new FileInputStream(this.getClass().getClassLoader().getResource("config.properties").getFile());
			   Properties props = new Properties();
			   props.load(in);
			   in.close();

			   FileOutputStream out = new FileOutputStream(this.getClass().getClassLoader().getResource("config.properties").getFile());
			   props.setProperty("newGitProjectUrl", jobj2.get("http_url_to_repo").toString());
			   props.store(out, null);
			   out.close();
			  System.out.println(props.getProperty("newGitProjectUrl"));
			   //ControllerServlet.session.setAttribute("newGitProjectUrl", jobj2.get("http_url_to_repo").toString());

		 } 

		System.out.println(con.getResponseCode());

		JSONParser parser = new JSONParser();
		JSONObject obj;
		
		if (con.getResponseCode() == 201) {
			logger.info("Project created");
			HashMap<String,String> pair = new HashMap<String,String>();
			pair.put("ProjectCreated", "Success");
			pair.put("ProjectNme",project_name );
			//array.add(pair);
			 
			 obj = (JSONObject)parser.parse(JSONObject.toJSONString(pair));
			return Response.status(201).entity(obj).build();
			//return Response.temporaryRedirect(location).build();
		}else if (con.getResponseCode() == 400) {
			logger.info("Project Not created");
			BufferedReader br = new BufferedReader( new InputStreamReader(con.getErrorStream()));
			
			StringBuilder sb = new StringBuilder();
			String output;
			while ((output = br.readLine()) != null) {
				sb.append(output);
			}
		
			 pair.put("Status", "Error creating the projects");
			 obj = (JSONObject)parser.parse(JSONObject.toJSONString(pair));
			   return Response.status(400).entity(obj).build();	
		}
			
		
			obj = (JSONObject)parser.parse(JSONObject.toJSONString(pair));
			return Response.status(500).entity(obj).build();	
		
		
}
	
	
	
	
	
	@SuppressWarnings({ "deprecation", "unused", "unchecked" })
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/createProject1")

	public Response createProject( @QueryParam("access_token") String access_token, InputStream json
			) throws ParseException, MalformedURLException, IOException, URISyntaxException{
		
		StringBuffer sb = new StringBuffer();
		StringBuilder sb2=new StringBuilder();
		BufferedReader br;
		br = new BufferedReader(new InputStreamReader(json));
		String content;
		
		while((content=br.readLine())!=null){
			sb2.append(content);
		}
		
		init(access_token);
		JSONObject jobj211 =new JSONObject();
	
		Integer statusCode;
		 JSONObject obj = new JSONObject();
		 logger.info("Inside create project service");
	
		 JSONParser parser = new JSONParser();
		  obj= (JSONObject)parser.parse(sb2.toString());   
		String gitgroup_name= (String) obj.get("groupName");
		String gitgroup_path = (String) obj.get("groupPath");
		String description1 = (String)obj.get("description");
		description1= URLEncoder.encode(description1).replaceAll("\\+", " ");
		 
		 if(!gitgroup_name.isEmpty()&&!gitgroup_path.isEmpty()){
			 
	con = (HttpURLConnection) ((new URL(this.gitBaseUrl+this.gitgroupurl+"access_token="+access_token+"&search="+gitgroup_name+"&search="+gitgroup_path).openConnection()));
	
	con.setRequestMethod("GET");
			 
				br = new BufferedReader(new InputStreamReader(con.getInputStream()));
				  
				   while ((content = br.readLine()) != null) {
					    sb.append(content);
					    }
				   
				  JSONParser parsers = new JSONParser(); 
				  JSONArray jobj2 = (JSONArray)parsers.parse(sb.toString());		  
				   
				  for(int i=0;i<jobj2.size();i++) 
				  {
					  pair= new JSONObject();
				   jobj3=(JSONObject) jobj2.get(i);
				 
				 
				  }
				
				 if(con.getResponseCode()==200&&!jobj2.isEmpty()){
					 System.out.println(con.getResponseMessage());
			//Git Project creation under Group
try {
			con=(HttpURLConnection) ((new URL(this.gitBaseUrl+this.gitProjectCreation+"&namespace_id="+jobj3.get("id")).openConnection()));
			obj.remove("groupName");
			obj.remove("groupPath");
			con.setRequestMethod("POST");
			
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setRequestProperty("Content-Type", "application/json");
			OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
			
			writer.write(obj.toJSONString());
			writer.close();
			
			  br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			 StringBuffer ssb1=new StringBuffer();
			  
			   while ((content = br.readLine()) != null) {
				    ssb1.append(content);
				    }   
			   
			 		
			 
			  jobj211 = (JSONObject)parser.parse(ssb1.toString());
			  jobj211.get("http_url_to_repo");

			   
			   FileInputStream in = new FileInputStream(this.getClass().getClassLoader().getResource("config.properties").getFile());
			   Properties props = new Properties();
			   props.load(in);
			   in.close();

			   FileOutputStream out = new FileOutputStream(this.getClass().getClassLoader().getResource("config.properties").getFile());
			   props.setProperty("newGitProjectUrl", jobj211.get("http_url_to_repo").toString());
			   props.store(out, null);
			   out.close();
			  System.out.println(props.getProperty("newGitProjectUrl"));
			  //ControllerServlet.session.setAttribute("newGitProjectUrl", jobj211.get("http_url_to_repo").toString());
}catch(IOException e){
	logger.info("Project Not created");
	 br = new BufferedReader( new InputStreamReader(con.getErrorStream()));
	
	StringBuilder sb3 = new StringBuilder();
	String output;
	while ((output = br.readLine()) != null) {
		sb3.append(output);
	}

	 pair.put("Status", "Error creating the projects");
	 obj = (JSONObject)parser.parse(sb3.toString());
	   return Response.status(con.getResponseCode()).entity(obj).build();
}
				
		}
				 
		else{
			String url = this.gitBaseUrl+this.gitgroupurl+"access_token="+access_token+"&name="+gitgroup_name+"&description="+description1+
					"&path="+gitgroup_path;
			url= url.replaceAll(" ", "%20");
			 con = (HttpURLConnection) ((new URL(url).openConnection()));
					
					con.setRequestMethod("POST");
					con.setRequestProperty("Content-Type","application/json");
					
//					OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream(), "UTF-8");
//					writer.write(sb2.toString());
//					writer.close();
					System.out.println(con.getResponseCode());
					if(con.getResponseCode()==201){
						con = (HttpURLConnection) ((new URL(this.gitBaseUrl+this.gitgroupurl+"access_token="+access_token+"&search="+gitgroup_name+"&search="+gitgroup_path).openConnection()));
						con.setRequestMethod("GET");
						br = new BufferedReader(new InputStreamReader(con.getInputStream()));
					}else{
						br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
					}
						 
							
							   StringBuffer ssb2 = new StringBuffer();
							   while ((content = br.readLine()) != null) {
								    ssb2.append(content);
								    }
							 
					JSONArray jobj21 = (JSONArray)parser.parse(ssb2.toString());		  
							   
							  for(int i=0;i<jobj21.size();i++) 
							  {
								  pair= new JSONObject();
							   jobj3=(JSONObject) jobj21.get(i);
							  	
							  System.out.println(jobj3.get("id").toString());
							 
							  }
							
			if(con.getResponseCode()==200&&!jobj21.isEmpty()){
								
						//Git Project creation under Group
							
							con=(HttpURLConnection) ((new URL(this.gitBaseUrl+this.gitProjectCreation+"&namespace_id="+jobj3.get("id")).openConnection()));
							obj.remove("groupName");
							obj.remove("groupPath");
							String description=(String) obj.get("description");
							description= description.replaceAll(" ", "%20");
							obj.put("description", description);
							
							System.out.println(obj.toJSONString());
							con.setRequestMethod("POST");
							con.setRequestProperty("Content-Type", "application/json");
							con.setDoInput(true);
							con.setDoOutput(true);
							OutputStreamWriter writer2 = new OutputStreamWriter(con.getOutputStream());
							writer2.write(obj.toJSONString());
							writer2.close();
							 
							 }
							 br = new BufferedReader(new InputStreamReader(con.getInputStream()));
							   StringBuffer ssb3 = new StringBuffer();
							   while ((content = br.readLine()) != null) {
								    ssb3.append(content);
								    }
							    
							  
							   jobj211 = (JSONObject)parser.parse(ssb3.toString());		  
							   
							  jobj211.get("http_url_to_repo");

							   
							   FileInputStream in = new FileInputStream(this.getClass().getClassLoader().getResource("config.properties").getFile());
							   Properties props = new Properties();
							   props.load(in);
							   in.close();

							   FileOutputStream out = new FileOutputStream(this.getClass().getClassLoader().getResource("config.properties").getFile());
							   props.setProperty("newGitProjectUrl", jobj211.get("http_url_to_repo").toString());
							   props.store(out, null);
							   out.close();
							  System.out.println(props.getProperty("newGitProjectUrl"));
							//  ControllerServlet.session.setAttribute("newGitProjectUrl", jobj211.get("http_url_to_repo").toString());
				 }
				
		 } else {
				
		 con = (HttpURLConnection) ((new URL(this.gitBaseUrl+this.gitProjectCreation).openConnection()));
			 obj.remove("groupName");
			 obj.remove("groupPath");
			 con.setRequestMethod("POST");
			 con.setDoOutput(true);
			 con.setDoInput(true);
			 con.setRequestProperty("Content-Type", "application/json");
			 OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
			 writer.write(obj.toJSONString());
			 writer.close();
			 
			 br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			 StringBuffer ssb4 = new StringBuffer();
			   while ((inputLine = br.readLine()) != null) {
				    ssb4.append(inputLine);
				    }
	   
			  JSONParser parsers = new JSONParser(); 
			  jobj211 = (JSONObject) parsers.parse(ssb4.toString());		  
			  jobj211.get("http_url_to_repo");

			   
			   FileInputStream in = new FileInputStream(this.getClass().getClassLoader().getResource("config.properties").getFile());
			   Properties props = new Properties();
			   props.load(in);
			   in.close();

			   FileOutputStream out = new FileOutputStream(this.getClass().getClassLoader().getResource("config.properties").getFile());
			   props.setProperty("newGitProjectUrl", jobj211.get("http_url_to_repo").toString());
			   props.store(out, null);
			   out.close();
			  System.out.println(props.getProperty("newGitProjectUrl"));
			 // ControllerServlet.session.setAttribute("newGitProjectUrl", jobj211.get("http_url_to_repo").toString());

		 } 


		 parser = new JSONParser();
		
		
		if (con.getResponseCode() == 201) {
			logger.info("Project created");
			HashMap<String,String> pair = new HashMap<String,String>();
			pair.put("ProjectCreated", "Success");
			 
			 obj = (JSONObject)parser.parse(JSONObject.toJSONString(pair));
			return Response.status(con.getResponseCode()).entity(jobj211).build();
			
		}else if (con.getResponseCode() == 400) {
			logger.info("Project Not created");
			 br = new BufferedReader( new InputStreamReader(con.getErrorStream()));
			
			StringBuilder sb3 = new StringBuilder();
			String output;
			while ((output = br.readLine()) != null) {
				sb3.append(output);
			}
		
			 pair.put("Status", "Error creating the projects");
			 obj = (JSONObject)parser.parse(JSONObject.toJSONString(pair));
			   return Response.status(con.getResponseCode()).entity(jobj211).build();	
		}
			
		
			obj = (JSONObject)parser.parse(JSONObject.toJSONString(pair));
			return Response.status(con.getResponseCode()).entity(jobj211).build();	
		
		
}
	
	@SuppressWarnings("unchecked")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/groups")

	public Response getgroups( @QueryParam("access_token") String accessToken, @QueryParam("search") String search
			) throws ParseException, MalformedURLException, IOException, URISyntaxException{
				
		init(accessToken);
		logger.info("getprojects function started");
		HashMap<String,String> pair = new HashMap<String,String>() ;
		
		if(search!=null){
			String url = this.gitBaseUrl+this.gitgroupurl+"access_token="+accessToken+"&min_access_level=40&search="+search;
			this.url=url;
		}else{
		String url = this.gitBaseUrl+this.gitgroupurl+"access_token="+accessToken+"&min_access_level=40";
		this.url=url;
		}
		try{
		HttpURLConnection con = (HttpURLConnection) ((new URL(url).openConnection()));
		
		con.setDoOutput(true);
		con.setRequestMethod("GET");
		con.connect();
			
		 System.out.println(con.getResponseCode());
        BufferedReader in = new BufferedReader(
      		  new InputStreamReader(con.getInputStream()));
        logger.debug("Connection was executed");
      String inputLine;
      StringBuffer content = new StringBuffer();
      
      while ((inputLine = in.readLine()) != null) {
     	    content.append(inputLine);
      }
      
    
      JSONParser parser = new JSONParser(); 
      JSONArray jobj = (JSONArray)parser.parse(content.toString());
      
      for(int i=0;i<jobj.size();i++) 
      {
    	  	pair = new HashMap<String,String>();
   	    	JSONObject jobj1 =(JSONObject)jobj.get(i);
   	    	System.out.println(jobj1.toJSONString());
      		pair.put("name", jobj1.get("name").toString());
      		pair.put("path", jobj1.get("path").toString()); 
      		array.add(pair);
      		logger.debug("Getting the Details");
      		
      }
      
      con.disconnect();
		}catch(Exception e){
			logger.error("There was some error with the collection");
			pair.put("Status", "Error gettign the projects");
			   return Response.status(500).entity(pair).build();
		}
		return Response.status(200).entity(array).build();

	}
	
	
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/projects/all")
	
	public Response getAllProjects(@QueryParam("access_token") String accessToken){
		System.out.println(accessToken);
		HttpURLConnection con;
		 String inputLine;
		 StringBuffer content;
		init(accessToken);
		JSONArray jobj;
		
			String url = this.gitBaseUrl+this.projectUrl+"&per_page=50";
	
		HashMap<String,String> pair = new HashMap<String,String>() ;
		
						
		try{
		con = (HttpURLConnection) ((new URL(url).openConnection()));
		System.out.println(url); 
		con.setDoOutput(true);
		con.setRequestMethod("GET");
		con.connect();
			
		 System.out.println(con.getResponseCode());
        BufferedReader in = new BufferedReader(
      		  new InputStreamReader(con.getInputStream()));
        logger.debug("Connection was executed");
     
      content = new StringBuffer();
      
      while ((inputLine = in.readLine()) != null) {
     	    content.append(inputLine);
      }
      
      
      JSONParser parser = new JSONParser(); 
   
      jobj = (JSONArray)parser.parse(content.toString());   
    
      con.disconnect();
		}catch(Exception e){
			System.out.println(e);
			logger.error("There was some error with the collection");
			pair.put("Status", "Error gettign the projects");
			   return Response.status(500).entity(pair).build();
		}
		return Response.ok(jobj, MediaType.APPLICATION_JSON).build();
	
	}
	
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/projects/{project_id}")
	
	public Response getSingleProjects(@QueryParam("access_token") String accessToken, @PathParam("project_id") int project_id){
		
		HttpURLConnection con;
		 String inputLine;
		 StringBuffer content;
		init(accessToken);
		JSONObject jobj;
		
			String url = this.gitBaseUrl+this.gitSingleProjectUrl+project_id+"?access_token="+accessToken;
	
		HashMap<String,String> pair = new HashMap<String,String>() ;
		
						
		try{
		con = (HttpURLConnection) ((new URL(url).openConnection()));
		System.out.println(url); 
		con.setDoOutput(true);
		con.setRequestMethod("GET");
		con.connect();
			
		 System.out.println(con.getResponseCode());
        BufferedReader in = new BufferedReader(
      		  new InputStreamReader(con.getInputStream()));
        logger.debug("Connection was executed");
     
      content = new StringBuffer();
      
      while ((inputLine = in.readLine()) != null) {
     	    content.append(inputLine);
      }
      
      
      JSONParser parser = new JSONParser(); 
   
      jobj = (JSONObject) parser.parse(content.toString());   
    
      con.disconnect();
		}catch(Exception e){
			System.out.println(e);
			logger.error("There was some error with the collection");
			pair.put("Status", "Error gettign the projects");
			   return Response.status(500).entity(pair).build();
		}
		return Response.ok(jobj, MediaType.APPLICATION_JSON).build();
	
	}
	
	
	@SuppressWarnings("unused")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/groups/{group_id}")
	
	public Response getSingleGroups(@QueryParam("access_token") String accessToken, @PathParam("group_id") int group_id){
		
		HttpURLConnection con;
		 String inputLine;
		 StringBuffer content;
		init(accessToken);
		JSONObject jobj;
		
			String url = this.gitBaseUrl+this.gitSingleGroupUrl+group_id+"?access_token="+accessToken;
	
		HashMap<String,String> pair = new HashMap<String,String>() ;
		
						
		try{
		con = (HttpURLConnection) ((new URL(url).openConnection()));
		System.out.println(url); 
		con.setDoOutput(true);
		con.setRequestMethod("GET");
		con.connect();
			
		 System.out.println(con.getResponseCode());
        BufferedReader in = new BufferedReader(
      		  new InputStreamReader(con.getInputStream()));
        logger.debug("Connection was executed");
     
      content = new StringBuffer();
      
      while ((inputLine = in.readLine()) != null) {
     	    content.append(inputLine);
      }

      con.disconnect();
		}catch(Exception e){
			System.out.println(e);
			logger.error("There was some error with the collection");
			pair.put("Status", "Error gettign the projects");
			   return Response.status(500).entity(pair).build();
		}
		return Response.ok(content.toString(), MediaType.APPLICATION_JSON).build();
	
	}
	
	@SuppressWarnings("unused")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/groups/{group_id}/members")
	
	public Response getGroupMembers(@QueryParam("access_token") String accessToken, @PathParam("group_id") int group_id){
		
		HttpURLConnection con;
		 String inputLine;
		 StringBuffer content;
		init(accessToken);
		JSONObject jobj;
		
			String url = this.gitBaseUrl+this.gitSingleGroupUrl+group_id+"/members?access_token="+accessToken;
	
		HashMap<String,String> pair = new HashMap<String,String>() ;
		
						
		try{
		con = (HttpURLConnection) ((new URL(url).openConnection()));
		System.out.println(url); 
		con.setDoOutput(true);
		con.setRequestMethod("GET");
		con.connect();
			
		 System.out.println(con.getResponseCode());
        BufferedReader in = new BufferedReader(
      		  new InputStreamReader(con.getInputStream()));
        logger.debug("Connection was executed");
     
      content = new StringBuffer();
      
      while ((inputLine = in.readLine()) != null) {
     	    content.append(inputLine);
      }

      con.disconnect();
		}catch(Exception e){
			System.out.println(e);
			logger.error("There was some error with the collection");
			pair.put("Status", "Error gettign the projects");
			   return Response.status(500).entity(pair).build();
		}
		return Response.ok(content.toString(), MediaType.APPLICATION_JSON).build();
	
	}
	
	@SuppressWarnings("unused")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{path}/{id}/access_requests")
	
	public Response requestAccess(@QueryParam("access_token") String accessToken, @PathParam("path") String path, @PathParam("id") int id) throws IOException{
		
		HttpURLConnection con = null;
		 String inputLine;
		 StringBuffer content;
		init(accessToken);
		JSONObject jobj;
		
			String url = this.gitBaseUrl+"/api/v4/"+path+"/"+id+"/access_requests?access_token="+accessToken;
	
		HashMap<String,String> pair = new HashMap<String,String>() ;
		
						
		try{
		con = (HttpURLConnection) ((new URL(url).openConnection()));
		System.out.println(url); 
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		con.connect();
			
		 System.out.println(con.getResponseCode());
        BufferedReader in = new BufferedReader(
      		  new InputStreamReader(con.getInputStream()));
        logger.debug("Connection was executed");
     
      content = new StringBuffer();
      
      while ((inputLine = in.readLine()) != null) {
     	    content.append(inputLine);
      }

      con.disconnect();
		}catch(Exception e){
			System.out.println(e);
			logger.error("There was some error with the collection");
			//pair.put("Status", "Error gettign the projects");
			   return Response.status(con.getResponseCode()).build();
		}
		return Response.ok(content.toString(), MediaType.APPLICATION_JSON).build();
	
	}
	
	@SuppressWarnings("unused")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{path}/{id}/access_requests")
	
	public Response listofrequestAccess(@QueryParam("access_token") String accessToken, @PathParam("path") String path, @PathParam("id") int id) throws IOException{
		
		HttpURLConnection con = null;
		 String inputLine;
		 StringBuffer content;
		init(accessToken);
		JSONObject jobj = null;
		
		String url = this.gitBaseUrl+"/api/v4/"+path+"/"+id+"/access_requests?access_token="+accessToken;
		HashMap<String,String> pair = new HashMap<String,String>() ;
						
		try{
		con = (HttpURLConnection) ((new URL(url).openConnection()));
		con.setDoOutput(true);
		con.setRequestMethod("GET");
		con.connect();
			
		 System.out.println(con.getResponseCode());
        BufferedReader in = new BufferedReader(
      		  new InputStreamReader(con.getInputStream()));
        logger.debug("Connection was executed");
     
      content = new StringBuffer();
      
      while ((inputLine = in.readLine()) != null) {
     	    content.append(inputLine);
      }

      con.disconnect();
      JSONParser parser = new JSONParser();
      jobj = (JSONObject) parser.parse(content.toString());
		}catch(Exception e){
			System.out.println(e);
			logger.error("There was some error with the collection");
			pair.put("Status", "Error gettign the projects");
			   return Response.status(con.getResponseCode()).entity(pair).build();
		}
		return Response.ok(content.toString(), MediaType.APPLICATION_JSON).build();
	
	}
	
	
	@SuppressWarnings("unused")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{path}/{id}/access_requests/{user_id}/approve")
	
	public Response approveRequestAccess(@QueryParam("access_token") String accessToken, @PathParam("path") String path,
			@PathParam("id") int id,@PathParam("user_id") Integer user_id,
			@QueryParam("access_level") Integer access_level) throws IOException{
		
		HttpURLConnection con = null;
		 String inputLine;
		 StringBuffer content;
		init(accessToken);
		JSONObject jobj;
		String url;
		if(access_level==null){
			 url = this.gitBaseUrl+"/api/v4/"+path+"/"+id+"/access_requests/"+user_id+"/approve?access_token="+accessToken;
		}else{
			 url = this.gitBaseUrl+"/api/v4/"+path+"/"+id+"/access_requests/"+user_id+"/approve?access_token="+accessToken+"&access_level="+access_level;
		}
		
		HashMap<String,String> pair = new HashMap<String,String>() ;
						
		try{
		con = (HttpURLConnection) ((new URL(url).openConnection()));
		con.setRequestMethod("PUT");
		con.connect();
			
		
        BufferedReader in = new BufferedReader(
      		  new InputStreamReader(con.getInputStream()));
        logger.debug("Connection was executed");
     
      content = new StringBuffer();
      
      while ((inputLine = in.readLine()) != null) {
     	    content.append(inputLine);
      }
      con.disconnect();
      
      JSONParser parser = new JSONParser();
       jobj = (JSONObject) parser.parse(content.toString());
      
		}catch(Exception e){
			System.out.println(e);
			logger.error("There was some error with the collection");
			//pair.put("Status", "Error gettign the projects");
			   return Response.status(con.getResponseCode()).build();
		}
		return Response.status(con.getResponseCode()).entity(jobj).build();
	
	}
	
	@SuppressWarnings("unused")
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{path}/{id}/access_requests/{user_id}")
	
	public Response disApproveRequestAccess(@QueryParam("access_token") String accessToken, @PathParam("path") String path, @PathParam("id") int id,@PathParam("user_id") Integer user_id) throws IOException{
		
		HttpURLConnection con = null;
		 String inputLine;
		 StringBuffer content = null;
		init(accessToken);
		JSONObject jobj;
		String url;
		
			 url = this.gitBaseUrl+"/api/v4/"+path+"/"+id+"/access_requests/"+user_id+"?access_token="+accessToken;
		
		HashMap<String,String> pair = new HashMap<String,String>() ;
						
		try{
		con = (HttpURLConnection) ((new URL(url).openConnection()));
		con.setRequestMethod("DELETE");
		con.connect();
			
		
//        BufferedReader in = new BufferedReader(
//      		  new InputStreamReader(con.getInputStream()));
//        logger.debug("Connection was executed");
//     
//      content = new StringBuffer();
//      
//      while ((inputLine = in.readLine()) != null) {
//     	    content.append(inputLine);
//      }
      con.disconnect();
      
		}catch(Exception e){
			
			logger.error("There was some error with the collection");
			//pair.put("Status", "Error gettign the projects");
			   return Response.status(con.getResponseCode()).entity(con.getResponseMessage()).build();
		}
		return Response.status(con.getResponseCode()).entity(con.getResponseMessage()).build();
	
	}
	
	
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("update/projects/{project_id}")
	
	public Response updateProject(@QueryParam("access_token") String accessToken, @PathParam("project_id") int project_id, InputStream json) throws Exception{
		
		
		init(accessToken);
		StringBuilder sb = new StringBuilder();
		BufferedReader in = new BufferedReader(new InputStreamReader(json));
        String line = null;
        while ((line = in.readLine()) != null) {
            sb.append(line);
        }
         
        try {
        	
        	 
        	String url = this.gitBaseUrl+this.gitSingleProjectUrl+project_id+"?access_token="+accessToken;
        	con = (HttpURLConnection) ((new URL(url).openConnection()));
        	con.setDoInput(true);
            con.setDoOutput(true);
            con.setRequestMethod("PUT");
            con.setRequestProperty("Content-Type", "application/json");
            OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
            writer.write(sb.toString());
            writer.close();
            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuffer jsonString = new StringBuffer();
            String line1;
            while ((line1 = br.readLine()) != null) {
                    jsonString.append(line1);
            }
            br.close();
            con.disconnect();
            JSONParser parser = new JSONParser(); 
            JSONObject jobj = (JSONObject) parser.parse(jsonString.toString());  
             return Response.ok(jobj, MediaType.APPLICATION_JSON).build();
        	
            }
         catch (Exception e) {
            System.out.println("Error Parsing: - ");
        }
       
        
        return Response.status(con.getResponseCode()).build();
    }
		
	
	@SuppressWarnings("unused")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/users/{type}")
	public Response getUsers(@QueryParam("access_token") String access_token, @PathParam("type") String type) throws Exception{
		
		String url=null;
		init(access_token);
		if(type.equals("all")){
		 url = this.gitBaseUrl+"/api/v4/users?access_token="+access_token;
		}else if(type.equals("active")){
			 url = this.gitBaseUrl+"/api/v4/users?active=true&access_token="+access_token;
		}else if(type.equals("blocked")){
			 url = this.gitBaseUrl+"/api/v4/users?blocked=true&access_token="+access_token;
		}else{
			url = this.gitBaseUrl+"/api/v4/users/"+type+"?access_token="+access_token;
		}
		
		JSONObject jobj;
	      StringBuffer content ;

		try{
		con = (HttpURLConnection) ((new URL(url).openConnection()));
		con.setRequestMethod("GET");
		con.connect();
	
	    BufferedReader in = new BufferedReader(
	      		  new InputStreamReader(con.getInputStream()));
	        logger.debug("Connection was executed");
	     
	       content = new StringBuffer();
	      
	      while ((inputLine = in.readLine()) != null) {
	    	  content.append(inputLine);
	      }
//	      JSONParser parser = new JSONParser();
//	      JSONArray jb1 = (JSONArray) parser.parse(content.toString());
//	      for(int i=0;i<jb1.size();i++){
//	    	  Map<String,String> pair = new HashMap<String,String>();
//	    	  JSONObject jobject = (JSONObject) jb1.get(i);
//	    	  pair.put("name", jobject.get("name").toString());
//	    	  pair.put("username", jobject.get("username").toString());
//	    	  pair.put("web_url", jobject.get("web_url").toString());
//	    	  array.add(pair);
//	      }
	      
	      con.disconnect();
	      
          
		}catch(Exception e){
			return Response.status(con.getResponseCode()).build();
			
		}
		return Response.ok(content.toString(), MediaType.APPLICATION_JSON).build();
//		return Response.ok().entity(array).build();
	      
		
	}
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/users")
	public Response search_Users(@NotNull @QueryParam("username") String username, @QueryParam("access_token") String access_token) throws Exception{
		StringBuffer sb = null ;
		init(access_token);
		try{
			String url=this.gitBaseUrl+"/api/v4/users?username="+username+"&access_token="+access_token;
			con= (HttpURLConnection) new URL(url).openConnection();
			con.setRequestMethod("GET");
			con.connect();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			sb = new StringBuffer();
			String line;
			while((line=br.readLine())!=null){
				sb.append(line);
			}
			con.disconnect();
			
		}catch(Exception e){
			
		}
		
		return Response.status(con.getResponseCode()).entity(sb.toString()).build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{type}/{id}/members")
	/**
	 * type: Group or Project
	 * id: Group ID or Project ID
	 */
	public Response addUsersToGroupOrProject(@QueryParam("access_token") String access_token,@PathParam("type") String type, @PathParam("id") int group_id, InputStream jsondata) throws Exception, IOException{
		
		init(access_token);
		BufferedReader br2 ;
		StringBuffer sb = new StringBuffer();
		BufferedReader br = new BufferedReader(new InputStreamReader(jsondata));
		String line=null;
		while((line=br.readLine())!=null){
			sb.append(line);
		}
		
		
		String url = this.gitBaseUrl+"/api/v4/"+type+"/"+group_id+"/members?access_token="+access_token;
		con = (HttpURLConnection) (new URL(url).openConnection());
		con.setRequestMethod("POST");
		con.setDoOutput(true);
		con.setDoInput(true);
		con.setRequestProperty("Content-Type", "application/json");
		OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
		writer.write(sb.toString());
		writer.close();
		try{
		 br2 = new BufferedReader(new InputStreamReader(con.getInputStream()));
		}catch(Exception e){
		 br2 = new BufferedReader(new InputStreamReader(con.getErrorStream()));
		}
		String content;
		StringBuffer json = new StringBuffer();
		while((content=br2.readLine())!=null){
			json.append(content);
		}
		br2.close();
		con.disconnect();
		JSONParser parser = new JSONParser();
		JSONObject jobj= (JSONObject) parser.parse(json.toString());
		return Response.status(con.getResponseCode()).entity(jobj).build();
		
//		}catch(Exception e){
//			br2 = new BufferedReader(new InputStreamReader(con.getErrorStream()));
//			String content;
//			StringBuffer json = new StringBuffer();
//			while((content=br2.readLine())!=null){
//				json.append(content);
//			}
//			br2.close();
//			con.disconnect();
//			JSONParser parser = new JSONParser();
//			JSONObject jobj= (JSONObject) parser.parse(json.toString());
//			return Response.status(con.getResponseCode()).entity(jobj).build();
//
//		}

	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{type}/{id}/members/{userid}")
public Response getMembersofGroupOrProject(@QueryParam("access_token") String access_token,@PathParam("type") String type, @PathParam("id") int type_id, @PathParam("userid") String userid) throws Exception, IOException{
		
		init(access_token);
		BufferedReader br;
		String url;
		if(userid.equals("all")){
			url = this.gitBaseUrl+"/api/v4/"+type+"/"+type_id+"/members/all?access_token="+access_token;
		}else{
			url = this.gitBaseUrl+"/api/v4/"+type+"/"+type_id+"/members/"+userid+"?access_token="+access_token;
		}
		
		
		con = (HttpURLConnection) (new URL(url).openConnection());
		con.setRequestMethod("GET");
		try{
		 br = new BufferedReader(new InputStreamReader(con.getInputStream()));
		}catch(Exception e){
		 br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
		}
		String content;
		StringBuffer json = new StringBuffer();
		while((content=br.readLine())!=null){
			json.append(content);
		}
		br.close();
		con.disconnect();
		JSONParser parser = new JSONParser();
		try{
			JSONObject jobj= (JSONObject) parser.parse(json.toString());
			return Response.status(con.getResponseCode()).entity(jobj).build();
		}catch(Exception e){
			JSONArray jarr = (JSONArray) parser.parse(json.toString());
			return Response.status(con.getResponseCode()).entity(jarr).build();
		}
		

	}
	
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{type}/{id}/members/{userid}")
	/**
	 * type: group/project
	 * id: group/project ID
	 */
public Response updateMembersofGroupOrProject(@QueryParam("access_token") String access_token,@PathParam("type") String type, @PathParam("id") int type_id, @PathParam("userid") String userid,
		@QueryParam("access_level") Integer access_level, @QueryParam("expires_at") String expires_at) throws Exception, IOException{
		
		init(access_token);
		BufferedReader br;
		String url;
		
		if(expires_at!=null){
			url = this.gitBaseUrl+"/api/v4/"+type+"/"+type_id+"/members/"+userid+"?access_token="+access_token+"&access_level="+access_level+"&expires_at="+expires_at;
		}else{		
			
			url = this.gitBaseUrl+"/api/v4/"+type+"/"+type_id+"/members/"+userid+"?access_token="+access_token+"&access_level="+access_level;
			
		}
		con = (HttpURLConnection) (new URL(url).openConnection());
		con.setRequestMethod("PUT");
		try{
		 br = new BufferedReader(new InputStreamReader(con.getInputStream()));
		}catch(Exception e){
		 br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
		}
		String content;
		StringBuffer json = new StringBuffer();
		while((content=br.readLine())!=null){
			json.append(content);
		}
		br.close();
		con.disconnect();
		JSONParser parser = new JSONParser();
		
			JSONObject jobj= (JSONObject) parser.parse(json.toString());
			return Response.status(con.getResponseCode()).entity(jobj).build();
	}
	
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{type}/{id}/members/{userid}")
public Response removeMembersFromGroupOrProject(@QueryParam("access_token") String access_token,@PathParam("type") String type, @PathParam("id") int type_id, @PathParam("userid") String userid) throws Exception, IOException{
		
		BufferedReader br;
		String url;
		init(access_token);
		url = this.gitBaseUrl+"/api/v4/"+type+"/"+type_id+"/members/"+userid+"?access_token="+access_token;
		
		con = (HttpURLConnection) (new URL(url).openConnection());
		con.setRequestMethod("DELETE");
		try{
		 br = new BufferedReader(new InputStreamReader(con.getInputStream()));
		}catch(Exception e){
		 br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
		}
		String content;
		StringBuffer json = new StringBuffer();
		while((content=br.readLine())!=null){
			json.append(content);
		}
		br.close();
		con.disconnect();
		
		return Response.status(con.getResponseCode()).build();
	}
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/projects/{id}/hooks")
	public Response getListOfProjectHooks(@QueryParam("access_token") String access_token,@PathParam("id") Integer id) throws Exception, IOException{
		
		init(access_token);
		BufferedReader br;
		String url;
		url = this.gitBaseUrl+"/api/v4/projects/"+id+"/hooks?access_token="+access_token;
		con = (HttpURLConnection) (new URL(url).openConnection());
		con.setRequestMethod("GET");
		try{
		 br = new BufferedReader(new InputStreamReader(con.getInputStream()));
		}catch(Exception e){
		 br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
		}
		String content;
		StringBuffer json = new StringBuffer();
		while((content=br.readLine())!=null){
			json.append(content);
		}
		br.close();
		con.disconnect();
		JSONParser parser = new JSONParser();
		try{
			JSONObject jobj= (JSONObject) parser.parse(json.toString());
			return Response.status(con.getResponseCode()).entity(jobj).build();
		}catch(Exception e){
			JSONArray jarr = (JSONArray) parser.parse(json.toString());
			return Response.status(con.getResponseCode()).entity(jarr).build();
		}
		

	}
	
	
	@SuppressWarnings("unchecked")
	@POST
	@Consumes({MediaType.APPLICATION_JSON,MediaType.APPLICATION_FORM_URLENCODED})
	@Path("/projects/{id}/hooks")
	/**
	 * id: Hook ID
	 */
	public Response addProjectHooks(@QueryParam("access_token") String access_token,@PathParam("id") Integer id, InputStream jsondata) throws Exception, IOException{
		
		init(access_token);
		
		
		StringBuffer json = null;
		BufferedReader br2 ;
		StringBuffer sb = new StringBuffer();
		BufferedReader br = new BufferedReader(new InputStreamReader(jsondata));
		String line=null;
		
		while((line=br.readLine())!=null){
			sb.append(line);
		}
		JSONParser parser = new JSONParser();
		JSONObject jsonobject = (JSONObject) parser.parse(sb.toString());

		String project=(String) jsonobject.get("projectName");
		
		System.out.println("project Name for webhook: "+project);
		String token = JenkinsService.getToken();
		String username=(String) ControllerServlet.session.getAttribute("username");
		String projectName=project;
		PropertyUtility props = PropertyUtility.getPropertyUtility();
		String jurl= props.getProperty("jenkins_url");
		String jenkinsurl;
		if(jurl.contains("http")){
			jenkinsurl= jurl.replaceAll("http://", "");
		}else{
			jenkinsurl=jurl.replaceAll("https://", "");
		}
		
		String webhookUrl= "http://"+username+":"+token+"@"+jenkinsurl+"/project/"+projectName;
		
		jsonobject.put("url", webhookUrl);
		System.out.println("hooks json object::"+jsonobject);
		
		String url = this.gitBaseUrl+"/api/v4/projects/"+id+"/hooks?access_token="+access_token;
		System.out.println(url);
		con = (HttpURLConnection) (new URL(url).openConnection());
		con.setRequestMethod("POST");
		con.setDoOutput(true);
		con.setDoInput(true);
		con.setRequestProperty("Content-Type", "application/json");
		OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
		writer.write(jsonobject.toString().replace("\\",""));
		writer.close();
		System.out.println("check::"+jsonobject);
		try{
		 br2 = new BufferedReader(new InputStreamReader(con.getInputStream()));
		 
		}catch(Exception e){
			System.out.println(con.getResponseCode()+" : "+con.getResponseMessage());
			
			con.disconnect();
			con = (HttpURLConnection) (new URL(url).openConnection());
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			con.setDoInput(true);
		 con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		 OutputStreamWriter writer2 = new OutputStreamWriter(con.getOutputStream(),"UTF-8");
		 writer2.write(sb.toString());
			writer2.close();
			try{
				br2 = new BufferedReader(new InputStreamReader(con.getInputStream()));
			}catch(Exception e1){
				br2 = new BufferedReader(new InputStreamReader(con.getErrorStream()));
			}
		}
		String content;
		 json = new StringBuffer();
		while((content=br2.readLine())!=null){
			json.append(content);
		}
		br2.close();
	
		con.disconnect();
		
		JSONObject jobj= (JSONObject) parser.parse(json.toString());
		return Response.status(con.getResponseCode()).entity(jobj).build();
	}
	
	@PUT
	@Consumes({MediaType.APPLICATION_JSON,MediaType.APPLICATION_FORM_URLENCODED})
	@Path("/projects/{id}/hooks/{webhook_id}")
	public Response updateProjectHooks(@QueryParam("access_token") String access_token,@PathParam("id") Integer id,@PathParam("webhook_id") Integer webhook_id, InputStream jsondata) throws Exception, IOException{
		
		init(access_token);
		StringBuffer json = null;
		BufferedReader br2 ;
		StringBuffer sb = new StringBuffer();
		BufferedReader br = new BufferedReader(new InputStreamReader(jsondata));
		
		String line=null;
		
		while((line=br.readLine())!=null){
			sb.append(line);
		}
		
	
		
		String url = this.gitBaseUrl+"/api/v4/projects/"+id+"/hooks/"+webhook_id+"?access_token="+access_token;
		System.out.println(url);
		con = (HttpURLConnection) (new URL(url).openConnection());
		con.setRequestMethod("PUT");
		con.setDoOutput(true);
		con.setDoInput(true);
		con.setRequestProperty("Content-Type", "application/json");
		OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
		writer.write(sb.toString());
		writer.close();
		
		try{
		 br2 = new BufferedReader(new InputStreamReader(con.getInputStream()));
		}catch(Exception e){
			con.disconnect();
			con = (HttpURLConnection) (new URL(url).openConnection());
			con.setRequestMethod("PUT");
			con.setDoOutput(true);
			con.setDoInput(true);
		 con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		 OutputStreamWriter writer2 = new OutputStreamWriter(con.getOutputStream());
		 writer2.write(sb.toString());
			writer2.close();
			try{
				br2 = new BufferedReader(new InputStreamReader(con.getInputStream()));
			}catch(Exception e1){
				br2 = new BufferedReader(new InputStreamReader(con.getErrorStream()));
			}
		}
		String content;
		 json = new StringBuffer();
		while((content=br2.readLine())!=null){
			json.append(content);
		}
		br2.close();
	
		con.disconnect();
		JSONParser parser = new JSONParser();
		JSONObject jobj= (JSONObject) parser.parse(json.toString());
		return Response.status(con.getResponseCode()).entity(jobj).build();
	}
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/projects/{id}/hooks/{webhook_id}")
	public Response deleteProjectHooks(@QueryParam("access_token") String access_token,@PathParam("id") Integer id,@PathParam("webhook_id") Integer webhook_id, InputStream jsondata) throws Exception, IOException{
		
		init(access_token);
		StringBuffer json = null;
		BufferedReader br2 ;
		StringBuffer sb = new StringBuffer();
		BufferedReader br = new BufferedReader(new InputStreamReader(jsondata));
		
		String line=null;
		
		while((line=br.readLine())!=null){
			sb.append(line);
		}
		
	
		String url = this.gitBaseUrl+"/api/v4/projects/"+id+"/hooks/"+webhook_id+"?access_token="+access_token;
		System.out.println(url);
		con = (HttpURLConnection) (new URL(url).openConnection());
		con.setRequestMethod("DELETE");
		con.setDoOutput(true);
		con.setDoInput(true);
		con.setRequestProperty("Content-Type", "application/json");
		OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
		writer.write(sb.toString());
		writer.close();
		
		try{
		 br2 = new BufferedReader(new InputStreamReader(con.getInputStream()));
		}catch(Exception e){
			br2 = new BufferedReader(new InputStreamReader(con.getErrorStream()));
		}
		
		String content;
		 json = new StringBuffer();
		while((content=br2.readLine())!=null){
			json.append(content);
		}
		br2.close();
	
		con.disconnect();
		
		return Response.status(con.getResponseCode()).entity(con.getResponseMessage()).build();
	}
	
	
//Git commits
	@SuppressWarnings("unchecked")
	@GET 
	   @Path("/commits")
	   @Produces(MediaType.APPLICATION_JSON)
	   public Response getcommits( @NotNull @QueryParam("access_token") String access_token, @NotNull @QueryParam("id") String id){
		init(access_token);
		logger.info("getprojects function started");
		HashMap<String,String> pair1 = new HashMap<String,String>() ;
		int page=1;
		for (int k = 0; k < page; k++) {
			logger.info(page);
			System.out.println("page number:"+page);
		try{
				url=gitBaseUrl+gitSingleProjectUrl+id+commitsUrl+"?access_token="+access_token+"&per_page=100&page="+page;
			HttpURLConnection con = (HttpURLConnection) ((new URL(url).openConnection()));
			System.out.println("projectUrl"+con.getURL());
		 System.out.println(url);
		 con.setDoOutput(true);
		 con.setRequestMethod("GET");
		 con.connect();
			
		 System.out.println(con.getResponseCode());
     BufferedReader in = new BufferedReader(
   		  new InputStreamReader(con.getInputStream()));
     logger.debug("Connection was executed");
   String inputLine;
   StringBuffer content = new StringBuffer();
   
   System.out.println("in !! "+in);
   while ((inputLine = in.readLine()) != null) {
 	  System.out.println("inputLine !! "+inputLine);
 	  logger.debug("inputLine !! "+inputLine);
  	    content.append(inputLine);
   }
   
 
   JSONParser parser = new JSONParser(); 
   JSONArray jobj = (JSONArray)parser.parse(content.toString());
   System.out.println("response size:"+jobj.size());
   if(jobj.size()<=100&&jobj.size()>0) {
	   page=page+1;
   }else if(jobj.size()==0) {
	   break;
   }else {
	   break;
   }
   System.out.println("page number:"+page);
   logger.info(page);
   for(int i=0;i<jobj.size();i++) 
   {
 	  	pair1 = new HashMap<String,String>();
	    	JSONObject jobj1 =(JSONObject)jobj.get(i);
	    	System.out.println(jobj1.toJSONString());
   		pair1.put("id", jobj1.get("short_id").toString());
   		pair1.put("title", jobj1.get("title").toString());
   		pair1.put("committed_date", jobj1.get("committed_date").toString());
   		pair1.put("message", jobj1.get("message").toString());
   		logger.info(pair1);
   		array.add(pair1);
   		logger.debug("Getting the Details");
   		System.out.println("Response Array:"+array);
   }
   
   con.disconnect();
		}catch(Exception e){
			logger.error("There was some error with the collection");
			pair1.put("Status", "Error gettign the projects");
			   return Response.status(500).entity(pair1).build();
		}
		logger.info(page);
		}
		System.out.println("final response array:"+array.size());
		System.out.println("response 200 sent");
		return Response.status(200).entity(array).build();
		
	}
	
	@GET 
	   @Path("/tags")
	   @Produces(MediaType.APPLICATION_JSON)
	   public Response getTags( @NotNull @QueryParam("access_token") String access_token, @NotNull @QueryParam("id") String id){
		init(access_token);
		logger.info("getprojects function started");
		HashMap<String,String> pair1 = new HashMap<String,String>() ;
		JSONArray tagsArray=new JSONArray();
		int page=1;
		for (int k = 0; k < page; k++) {
			logger.info(page);
			System.out.println("page number:"+page);
		try{
				url=gitBaseUrl+gitSingleProjectUrl+id+tagUrl+"?access_token="+access_token+"&scope=merge_requests&search=file";
			HttpURLConnection con = (HttpURLConnection) ((new URL(url).openConnection()));
			System.out.println("projectUrl"+con.getURL());
		 System.out.println(url);
		 con.setDoOutput(true);
		 con.setRequestMethod("GET");
		 con.connect();
			
		 System.out.println(con.getResponseCode());
  BufferedReader in = new BufferedReader(
		  new InputStreamReader(con.getInputStream()));
  logger.debug("Connection was executed");
String inputLine;
StringBuffer content = new StringBuffer();

System.out.println("in !! "+in);
while ((inputLine = in.readLine()) != null) {
	  System.out.println("inputLine !! "+inputLine);
	  logger.debug("inputLine !! "+inputLine);
	    content.append(inputLine);
}


JSONParser parser = new JSONParser(); 
tagsArray = (JSONArray)parser.parse(content.toString());
con.disconnect();
		}catch(Exception e){
			logger.error("There was some error with the collection");
			pair1.put("Status", "Error gettign the projects");
			   return Response.status(500).entity(pair1).build();
		}
		logger.info(page);
		}
		System.out.println("final response array:"+array.size());
		System.out.println("response 200 sent");
		return Response.status(200).entity(tagsArray).build();
		
	}	
	
}
