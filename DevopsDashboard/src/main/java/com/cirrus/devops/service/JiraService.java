package com.cirrus.devops.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;
import java.util.HashMap;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.cirrus.devops.ControllerServlet;
import com.cirrus.devops.service.database.Licensing;
import com.cirrus.devops.util.PropertyUtility;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

@Path("/JiraService")
public class JiraService {
	static final Logger logger = Logger.getLogger(JiraService.class);
	 HashMap<String,String> message=new HashMap<String,String>();
	
	@SuppressWarnings("unchecked")
	@GET 
	   @Path("/projects")
	   @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getprojects(@QueryParam("name") String username, @QueryParam("password") String pass) throws IOException{		
		BasicConfigurator.configure();
		HttpURLConnection con = null;
	PropertyUtility props=PropertyUtility.getPropertyUtility();
		
		JSONObject jiraObjects = new JSONObject();
		JSONArray array = new JSONArray();
		JSONObject jenkinsBuildpair = new JSONObject();
		JSONArray array2 = new JSONArray();
		String url=props.getProperty("jira_url");
	
			try{
			        URL urlObject = new URL(url+"/rest/api/2/project");
			    	con = (HttpURLConnection) urlObject.openConnection();
			    	String name = (String) ControllerServlet.session.getAttribute("username");	;				
			        String password = (String) ControllerServlet.session.getAttribute("userpassword");
			        System.out.println("JIRA service: "+name+password);
			        String authString = name + ":" + password;	
			        String authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes());	
			        con.setRequestMethod("GET");
			        con.setDoOutput(true);
			        con.setRequestProperty  ("Authorization", "Basic " + authStringEnc);	
			        
			        System.out.println(urlObject.toString());
			BufferedReader in = new BufferedReader(
		      		  new InputStreamReader(con.getInputStream()));	        
		      String inputLine;
		      StringBuffer content = new StringBuffer();		      
		      while ((inputLine = in.readLine()) != null) {
		     	    content.append(inputLine);
		      }	
     JSONParser parser = new JSONParser(); 
     array = (JSONArray)parser.parse(content.toString());
     con.disconnect();
		}catch(Exception e){
			e.printStackTrace();
			if(con.getResponseCode()==401){
				jiraObjects.put("Message", "Un-Authorized");
			}else{
			jiraObjects.put("Status", "Error getting the Projects");
			}
			return Response.status(con.getResponseCode()).entity(jiraObjects).build();
		}
	return Response.status(con.getResponseCode()).entity(array).build();
	}
	
	
	@POST 
	   @Path("/create")
	   @Consumes({MediaType.APPLICATION_JSON,MediaType.APPLICATION_FORM_URLENCODED})

	public Response createProjects(@QueryParam("name") String username,
			   @QueryParam("password") String pass, InputStream json) throws IOException, ParseException{	
		
		JSONObject jobj;
		BufferedReader br;
		StringBuffer sb;
		HttpURLConnection con = null;
		sb= new StringBuffer();
		br = new BufferedReader(new InputStreamReader(json));
		String line;
		while((line=br.readLine())!=null){
			sb.append(line);
		}
		
		BasicConfigurator.configure();
		
	PropertyUtility props=PropertyUtility.getPropertyUtility();
	String name = (String) ControllerServlet.session.getAttribute("username");					
    String password = (String) ControllerServlet.session.getAttribute("userpassword");
    System.out.println("JIRA service: "+name+password);	        
    String authString = name + ":" + password;	
    String authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes());	
		
		JSONObject jenkinsJobPair = new JSONObject();
		String url=props.getProperty("jira_url");
		try{
			        URL urlObject = new URL(url+"/rest/api/2/project");
			    	con = (HttpURLConnection) urlObject.openConnection();
			    		        
			        con.setRequestMethod("POST");
			        con.setDoOutput(true);
			        con.setDoInput(true);
			        con.setRequestProperty("Content-Type", "application/json");
			        
			        con.setRequestProperty  ("Authorization", "Basic " + authStringEnc);	
			        OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
			        writer.write(sb.toString());
			        writer.close();
			       
			   		 br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			   		  
			        
			        System.out.println(br.toString());
		      String inputLine;
		      StringBuffer content = new StringBuffer();		      
		      while ((inputLine = br.readLine()) != null) {
		     	    content.append(inputLine);
		      }	
  JSONParser parser = new JSONParser(); 
   jobj = (JSONObject)parser.parse(content.toString());
		  	    		
   		con.disconnect();
		}catch(Exception e){
			/*e.printStackTrace();
			jenkinsJobPair.put("Status", "Error getting the Jobs");
			return Response.status(con.getResponseCode()).entity(jenkinsJobPair).build();*/
			br = new BufferedReader( new InputStreamReader(con.getErrorStream()));
			
			StringBuilder sb3 = new StringBuilder();
			String output;
			while ((output = br.readLine()) != null) {
				sb3.append(output);
			}
			JSONParser parser = new JSONParser();
			 //pair.put("Status", "Error creating the projects");
			jenkinsJobPair =(JSONObject) parser.parse(sb3.toString());
			   return Response.status(con.getResponseCode()).entity(jenkinsJobPair).build();
		}
	return Response.status(con.getResponseCode()).entity(jobj).build();
	}
	
	@GET 
	@Path("/users")
	@Consumes(MediaType.APPLICATION_JSON)

	public Response getUsersAssignableToProjects() throws IOException{	
		
		JSONObject jobj;
		BufferedReader br;
		StringBuffer sb;
		HttpURLConnection con = null;
		JSONArray array;
		
		BasicConfigurator.configure();
		
	PropertyUtility props=PropertyUtility.getPropertyUtility();
	String name = (String) ControllerServlet.session.getAttribute("username");				
    String password = (String) ControllerServlet.session.getAttribute("userpassword");
    System.out.println("JIRA service '/users': "+name+password);		        
	String authString = name + ":" + password;	
	String authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes());	
		
		JSONObject jenkinsJobPair = new JSONObject();
		String url=props.getProperty("jira_url");
		try{
			        URL urlObject = new URL(url+"/rest/api/2/user/search?username=.");
			    	con = (HttpURLConnection) urlObject.openConnection();
			    		        
			        con.setRequestMethod("GET");
			        con.setDoOutput(true);
			        con.setDoInput(true);
			        con.setRequestProperty("Content-Type", "application/json");
			        
			        con.setRequestProperty  ("Authorization", "Basic " + authStringEnc);	
			       
			   		 br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			   		
		      String inputLine;
		      StringBuffer content = new StringBuffer();		      
		      while ((inputLine = br.readLine()) != null) {
		     	    content.append(inputLine);
		      }	
	JSONParser parser = new JSONParser(); 
	 array = (JSONArray)parser.parse(content.toString());
		  	    		
		con.disconnect();
		}catch(Exception e){
			e.printStackTrace();
			jenkinsJobPair.put("Status", "Error getting the Jobs");
			return Response.status(con.getResponseCode()).entity(jenkinsJobPair).build();
		}
	return Response.status(con.getResponseCode()).entity(array).build();
	}
	
	
	@SuppressWarnings("unchecked")
	@GET 
	@Path("/project/{projectKey}")
	@Consumes(MediaType.APPLICATION_JSON)

	public Response searchProject(@QueryParam("name") String username,
			   @QueryParam("password") String pass, @PathParam("projectKey") String projectKey) throws IOException{	
		
		JSONObject jobj;
		BufferedReader br;
		StringBuffer sb;
		HttpURLConnection con = null;
		JSONArray array;
		
		BasicConfigurator.configure();
		
	PropertyUtility props=PropertyUtility.getPropertyUtility();
	String name = (String) ControllerServlet.session.getAttribute("username");	;				
    String password = (String) ControllerServlet.session.getAttribute("userpassword");
    System.out.println("JIRA service projectKey: "+name+password);        
	String authString = name + ":" + password;	
	String authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes());	
		
		JSONObject jenkinsJobPair = new JSONObject();
		String url=props.getProperty("jira_url");
		try{
			        URL urlObject = new URL(url+"/rest/api/2/project/"+projectKey);
			    	con = (HttpURLConnection) urlObject.openConnection();
			    		        
			        con.setRequestMethod("GET");
			        con.setDoOutput(true);
			        con.setDoInput(true);
			        con.setRequestProperty("Content-Type", "application/json");
			        
			        con.setRequestProperty  ("Authorization", "Basic " + authStringEnc);	
			       
			   		 br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			   		
		      String inputLine;
		      StringBuffer content = new StringBuffer();		      
		      while ((inputLine = br.readLine()) != null) {
		     	    content.append(inputLine);
		      }	
	JSONParser parser = new JSONParser(); 
	 jobj = (JSONObject)parser.parse(content.toString());
		  	    		
		con.disconnect();
		}catch(Exception e){
			e.printStackTrace();
			jenkinsJobPair.put("Status", "Error getting the Jobs");
			return Response.status(con.getResponseCode()).entity(jenkinsJobPair).build();
		}
	return Response.status(con.getResponseCode()).entity(jobj).build();
	}
	
	
	@SuppressWarnings("unchecked")
	@GET 
	@Path("/jiraIssues")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response issuesData(@NotNull @PathParam("projectKey") String projectKey) throws IOException{	
		
		JSONObject jobj;
		BufferedReader br;
		StringBuffer sb;
		HttpURLConnection con = null;
		JSONArray array;
		
		BasicConfigurator.configure();
		
	PropertyUtility props=PropertyUtility.getPropertyUtility();
	String name = (String) ControllerServlet.session.getAttribute("username");	;				
    String password = (String) ControllerServlet.session.getAttribute("userpassword");
    System.out.println("JIRA service issues: "+name+password);        
	String authString = name + ":" + password;	
	String authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes());	
		
		JSONObject jenkinsJobPair = new JSONObject();
		String url=props.getProperty("jira_url");
		try{
			System.out.println(url+"/rest/api/2/search?projectkey="+projectKey);
			        URL urlObject = new URL(url+"/rest/api/2/search?projectkey="+projectKey);
			    	con = (HttpURLConnection) urlObject.openConnection();
			    		        
			        con.setRequestMethod("GET");
			        con.setDoOutput(true);
			        con.setDoInput(true);
			        con.setRequestProperty("Content-Type", "application/json");
			        
			        con.setRequestProperty  ("Authorization", "Basic " + authStringEnc);	
			       
			   		 br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			   		
		      String inputLine;
		      StringBuffer content = new StringBuffer();		      
		      while ((inputLine = br.readLine()) != null) {
		     	    content.append(inputLine);
		      }	
	JSONParser parser = new JSONParser(); 
	 jobj = (JSONObject)parser.parse(content.toString());
		  	    		
		con.disconnect();
		}catch(Exception e){
			e.printStackTrace();
			jenkinsJobPair.put("Status", "Error getting the issues");
			return Response.status(con.getResponseCode()).entity(jenkinsJobPair).build();
		}
	return Response.status(con.getResponseCode()).entity(jobj).build();
	}
	
}




