package com.cirrus.devops.service.license;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Base64;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import com.cirrus.devops.service.database.Licensing;
@SuppressWarnings("unchecked")
@Path("/SystemAddress")
public class SystemAddress {
	static final Logger logger = Logger.getLogger(SystemAddress.class);
	@SuppressWarnings({ "deprecation", "unchecked" })
	@GET 
	@Path("/macAddress")
	@Produces(MediaType.APPLICATION_JSON)
	public Response macAddress() throws ClassNotFoundException, ParseException, IOException{
	JSONArray macadrress=new JSONArray();
	HashMap<String, String> autherization = new HashMap<String,String>();
		 try {
			// searchForMac();
			    InetAddress ip = InetAddress.getLocalHost();
			    //System.out.println("Current IP address : " + ip.getHostAddress());

			    Enumeration<NetworkInterface> networks = NetworkInterface.getNetworkInterfaces();
			    while(networks.hasMoreElements()) {
			      NetworkInterface network = networks.nextElement();
			      byte[] mac = network.getHardwareAddress();

			      if(mac != null) {
			       // System.out.print("Current MAC address : ");

			        StringBuilder sb = new StringBuilder();
			        for (int i = 0; i < mac.length; i++) {
			          sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
			        }
			       // System.out.println(sb.toString());
			        macadrress.add(sb.toString());
			      }
			    }
			  } catch (UnknownHostException ex) {
					logger.error("There was Problems while getting system id");
		        	autherization.put("status", "Unable to get system address");
					   return Response.status(500).entity(autherization).build();
			  } catch (SocketException e){
					logger.error("There was Problems while getting system id");
		        	autherization.put("status", "Unable to get system address");
					   return Response.status(500).entity(autherization).build();
			  }
	//}
         logger.info("All the functions worked properly, exiting the function with an array object");
 		return Response.status(200).entity(macadrress).build();	
	
	/*public static String searchForMac() throws SocketException {
	    String firstInterface = null;        
	    Map<String, String> addressByNetwork = new HashMap<>();
	    Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();

	    while(networkInterfaces.hasMoreElements()){
	        NetworkInterface network = networkInterfaces.nextElement();

	        byte[] bmac = network.getHardwareAddress();
	        if(bmac != null){
	            StringBuilder sb = new StringBuilder();
	            for (int i = 0; i < bmac.length; i++){
	                sb.append(String.format("%02X%s", bmac[i], (i < bmac.length - 1) ? "-" : ""));        
	            }

	            if(sb.toString().isEmpty()==false){
	                addressByNetwork.put(network.getName(), sb.toString());
	                System.out.println("Address = "+sb.toString()+" @ ["+network.getName()+"] "+network.getDisplayName());
	            }

	            if(sb.toString().isEmpty()==false && firstInterface == null){
	                firstInterface = network.getName();
	            }
	        }
	    }

	    if(firstInterface != null){
	        return addressByNetwork.get(firstInterface);
	    }

	    return null;
	}*/
	}
}
