package com.cirrus.devops.service.database.createTable;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Base64;
import java.util.Properties;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.postgresql.util.PSQLException;

import com.cirrus.devops.service.database.SSHTunnel;
import com.cirrus.devops.util.PropertyUtility;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class CreateTable {
	
	static final Logger logger = Logger.getLogger(CreateTable.class);
	 private String authString;
	 private String url;
	
	/*private static void doSshTunnel(String strSshUser, String strSshPassword, String strSshHost, int nSshPort,
	            String strRemoteHost, int nLocalPort, int nRemotePort) throws JSchException {
	        final JSch jsch = new JSch();
	        Session session = jsch.getSession(strSshUser, strSshHost, 22);
	        session.setPassword(strSshPassword);

	        final Properties config = new Properties();
	        config.put("StrictHostKeyChecking", "no");
	        session.setConfig(config);

	        session.connect();
	        session.setPortForwardingL(nLocalPort, strRemoteHost, nRemotePort);
	    }*/
	 
    public void createTables() throws ClassNotFoundException, ParseException, IOException, JSchException{
		init();
		 Base64.Decoder decoder1 = Base64.getMimeDecoder(); 
		   String keydecoded1=new String(decoder1.decode(authString));
		   String[] arrOfStr1 = keydecoded1.split("!q4Ev");
		   String pass=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-1]));
		   String user=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-2]));
	        
	       try {
	        	Class.forName("org.postgresql.Driver");
	        	System.out.println("Driver version: " + org.postgresql.Driver.getVersion());
	        	    Connection conn = null;
	        	    //CreateTable.doSshTunnel("root", "VMwar3!!", "192.168.137.121", 22 , "127.0.0.1", 5433,5432);
	        	    SSHTunnel sshTunnel=new SSHTunnel();
	        	    sshTunnel.doSshTunnel();
	        	    Properties properties = new Properties();
	    			properties.setProperty("user", user);
	    			properties.setProperty("password", pass);
	        	     conn = DriverManager.getConnection(url,properties);
	        	        System.out.println("Connected to the PostgreSQL server successfully.");
	        	   
	        	        Statement stmt = conn.createStatement();
	        	        String sql = "CREATE TABLE iqusers " +
	        	                "(USER_NAME TEXT PRIMARY KEY     NOT NULL," +
	        	                " PASSWORD           TEXT    NOT NULL, " +
	        	                " ACCESS_LEVEL       TEXT      NOT NULL, " +
	        	                " FIRST_NAME        TEXT NOT NULL, " +
	        	                " LAST_NAME         TEXT NOT NULL,"
	        	                + "EMAIL_ID         TEXT NOT NULL);"
	        	                + "CREATE TABLE licensedetails"
	        	                + "(NAME TEXT PRIMARY KEY     NOT NULL,"
	        	                + "VALUE          TEXT    NOT NULL);"
	        	                + "INSERT INTO iqusers(USER_NAME,PASSWORD,ACCESS_LEVEL,FIRST_NAME,LAST_NAME,EMAIL_ID)" + 
	        	                "VALUES('admin','Welcome123','Admin','firstuser','asAdmin','admin@cirrusdigitals.com');";
	        	             stmt.executeUpdate(sql);
	        	             stmt.close();
	        	             conn.close();
		                logger.info("All the functions worked properly, exiting the function");
	      }catch(PSQLException ex) {
	    	  ex.getMessage();
	        	ex.getErrorCode();
	        	logger.error("There was Problems while adding user"+ex.getMessage());
	      }
	       catch (SQLException ex) {
	        	ex.printStackTrace();
	        	ex.getMessage();
	        	ex.getErrorCode();
	        	logger.error("There was Problems while adding user");
	        } 
	}  
    
    public void init(){
		
		   BasicConfigurator.configure();
		   PropertyUtility props = PropertyUtility.getPropertyUtility();
		  /* this.authString=props.getProperty("privatToken");
		   this.url=props.getProperty("dburl");*/
		   this.authString=props.getProperty("remotedbToken");
		   this.url=props.getProperty("remotedbUrl");
		   
	   }	
  
}
