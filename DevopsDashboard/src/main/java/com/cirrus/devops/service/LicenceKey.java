package com.cirrus.devops.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.HashMap;
import java.util.Properties;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import com.cirrus.devops.util.PropertyUtility;
@Path("/LicenceKey")
public class LicenceKey {
	static final Logger logger = Logger.getLogger(LicenceKey.class);
	@POST 
	   @Path("/licenceKey")
	   @Produces(MediaType.APPLICATION_JSON)
	   public Response generateLicenceKey() throws IOException{	
		HashMap<String,String> pair = new HashMap<String,String>() ;
		BasicConfigurator.configure();
		//@QueryParam("name") String username,@QueryParam("password") String pass
			try{
				   LocalDateTime today =  LocalDateTime.now();
				  // System.out.println(today);
				   LocalDateTime sameDayNextMonth = today.plusMonths(1);
				  // System.out.println(sameDayNextMonth);
				   LocalDateTime sameDayNextYear = today.plusYears(1); 
				   long daysBetween = today.until(sameDayNextYear, ChronoUnit.DAYS );
				   System.out.println(daysBetween);
				   //System.out.println(sameDayNextYear);
				   Base64.Encoder encoder = Base64.getMimeEncoder();  
				   String todayEncoder = encoder.encodeToString(today.toString().getBytes());
				   String sameDayNextMonthEncoder = encoder.encodeToString(sameDayNextMonth.toString().getBytes());
				   String sameDayNextYearEncoder = encoder.encodeToString(sameDayNextYear.toString().getBytes());
				   String inserter="iqdevlicenceKey";
				   String licencekey=inserter+"!q4Ev"+todayEncoder+"!q4Ev"+sameDayNextMonthEncoder;
				  // System.out.println("licence key:"+licencekey);
				   //String licencekeyencoded = encoder.encodeToString(licencekey.getBytes());
				  // System.out.println("licence key encoded:"+licencekeyencoded);
				   //System.out.println(this.getClass().getClassLoader().getResource("Licence.properties").getFile());
				   String deploymod="Deployment";
				   String secmod="ServerSecurity";
				   String sysId="58-8A-5A-0E-B4-8E";
				   String sysIdencoded=encoder.encodeToString(sysId.getBytes());
				   String DeployModule=encoder.encodeToString(deploymod.getBytes());
				   LocalDateTime sameDaypreviousmonth=today.minusMonths(1);
				   String sameDayPreviosMonthEncoder = encoder.encodeToString(sameDaypreviousmonth.toString().getBytes());
				   String securitymod=encoder.encodeToString(secmod.getBytes());
				   String modulelicencekey=sysIdencoded+"!q4Ev"+DeployModule+"!q4Ev"+securitymod+"!q4Ev"+sameDayNextMonthEncoder+"!q4Ev"+todayEncoder;
				  //String modulelicencekey=DeployModule+"!q4Ev"+securitymod+"!q4Ev"+sameDayPreviosMonthEncoder;
				   String licencekeyencoded = encoder.encodeToString(modulelicencekey.getBytes());
				   System.out.println("licence key encoded:"+licencekeyencoded.toString());
				/*FileWriter fw=new FileWriter(this.getClass().getClassLoader().getResource("Licence.properties").getFile());  
				   fw.write("licenceKey="+licencekey);    
		           fw.close(); */
				   
				   /*FileInputStream in = new FileInputStream(this.getClass().getClassLoader().getResource("Licence.properties").getFile());
				   Properties props = new Properties();
				   props.load(in);
				   in.close();

				   FileOutputStream out = new FileOutputStream(this.getClass().getClassLoader().getResource("Licence.properties").getFile());
				   //props.setProperty("licenceKey", licencekeyencoded);
				   props.setProperty("licenceKey", licencekeyencoded);
				   props.setProperty("key1", todayEncoder);
				   props.setProperty("key2", sameDayNextMonthEncoder);
				   props.setProperty("key3", sameDayNextYearEncoder);
				   props.store(out, null);
				   out.close();*/
				   String[] keys = licencekeyencoded.split("\r\n");
				   pair.put("License_Key",keys[keys.length-3]+keys[keys.length-2]+keys[keys.length-1] );
				   pair.put("Status", "Success");  
				  /*PropertyUtility props1=PropertyUtility.getPropertyUtility();
				   System.out.println("licencekey:"+props1.getProperty("licenceKey").toString());
				   String key= props1.getProperty("licenceKey");
				   Base64.Decoder decoder = Base64.getMimeDecoder(); 
				   String keydecoded=new String(decoder.decode(key));
				   System.out.println("keydecoded:"+keydecoded);
				   //String subkey=keydecoded.substring(0,key.indexOf("!q4Ev"));
				   String[] arrOfStr = keydecoded.split("!q4Ev"); 
				   System.out.println("key:"+keydecoded);
				   System.out.println("arrOfStr:"+arrOfStr[arrOfStr.length-1]);
				   String endDateDecoded=new String(decoder.decode(arrOfStr[arrOfStr.length-1]));
				   System.out.println(endDateDecoded);
				   LocalDateTime end=LocalDateTime.parse(endDateDecoded);
				   System.out.println("end:"+end);
				   int ret=LocalDateTime.now().compareTo(end);
				   System.out.println("ret:"+ret);*/
		}catch(Exception e){
			e.printStackTrace();
			logger.error("There was Problems while generating licence key");
			pair.put("Status", "Error getting the Jobs");
			   return Response.status(500).entity(pair).build();
		}
		logger.info("All the functions worked properly, exiting the function with an array object");
		return Response.status(200).entity(pair).build();
	}
	
	
	@POST 
	   @Path("/licenceKeyUpdate")
	   @Produces(MediaType.APPLICATION_JSON)
	   public Response updateLicenceKey(@QueryParam("licencekey") String key) throws IOException{	
		HashMap<String,String> pair = new HashMap<String,String>() ;
		BasicConfigurator.configure();
			try{
				System.out.println("key:"+key);
				   FileInputStream in = new FileInputStream(this.getClass().getClassLoader().getResource("Licence.properties").getFile());
				   Properties props = new Properties();
				   props.load(in);
				   in.close();

				   FileOutputStream out = new FileOutputStream(this.getClass().getClassLoader().getResource("Licence.properties").getFile());
				   props.setProperty("licenceKey", key);
				   props.store(out, null);
				   out.close();
				   pair.put("Status", "Success"); 
				   //pair.put("Status", "fail"); 
				 /* PropertyUtility props1=PropertyUtility.getPropertyUtility();
				   System.out.println("licencekey:"+props1.getProperty("licenceKey").toString());
				   String key1= props1.getProperty("licenceKey");
				   Base64.Decoder decoder = Base64.getMimeDecoder(); 
				   String keydecoded=new String(decoder.decode(key1));
				   System.out.println("keydecoded:"+keydecoded);
				   String[] arrOfStr = keydecoded.split("!q4Ev"); 
				   System.out.println("key:"+keydecoded);
				   System.out.println("arrOfStr:"+arrOfStr[arrOfStr.length-1]);
				   String endDateDecoded=new String(decoder.decode(arrOfStr[arrOfStr.length-1]));
				   System.out.println(endDateDecoded);
				   LocalDateTime end=LocalDateTime.parse(endDateDecoded);
				   System.out.println("end:"+end);*/
		}catch(Exception e){
			e.printStackTrace();
			logger.error("There was Problems while updating licence key");
			pair.put("Status", "Error getting the Jobs");
			   return Response.status(500).entity(pair).build();
		}
		logger.info("All the functions worked properly, exiting the function with an array object");
		return Response.status(200).entity(pair).build();
	}
	
	@GET 
	   @Path("/getlicenceKey")
	   @Produces(MediaType.APPLICATION_JSON)
	   public Response getLicenceKey() throws IOException{	
		HashMap<String,String> pair = new HashMap<String,String>();
		JSONObject licenseObject=new JSONObject();
		BasicConfigurator.configure();
			try{
				 PropertyUtility props1=PropertyUtility.getPropertyUtility();
				   System.out.println("licencekey:"+props1.getProperty("licenceKey").toString());
				   String key1= props1.getProperty("licenceKey");
				   Base64.Decoder decoder = Base64.getMimeDecoder(); 
				   String keydecoded=new String(decoder.decode(key1));
				   System.out.println("keydecoded:"+keydecoded);
				   String[] arrOfStr = keydecoded.split("!q4Ev"); 
				   System.out.println("key:"+keydecoded);
				   System.out.println("arrOfStr:"+arrOfStr[arrOfStr.length-1]);
				   String creationDateDecoded=new String(decoder.decode(arrOfStr[arrOfStr.length-1]));
				   System.out.println("arrOfStr:"+arrOfStr[arrOfStr.length-2]);
				   String endDateDecoded=new String(decoder.decode(arrOfStr[arrOfStr.length-2]));
				   System.out.println("arrOfStr:"+arrOfStr[arrOfStr.length-2]);
				   String securityDecoded=new String(decoder.decode(arrOfStr[arrOfStr.length-3]));
				   System.out.println("arrOfStr:"+arrOfStr[arrOfStr.length-2]);
				   String deployDecoded=new String(decoder.decode(arrOfStr[arrOfStr.length-4]));
				   System.out.println(endDateDecoded);
				   System.out.println(creationDateDecoded);
				   System.out.println(securityDecoded);
				   System.out.println(deployDecoded);
				   LocalDateTime end=LocalDateTime.parse(endDateDecoded);
				   LocalDateTime start=LocalDateTime.parse(creationDateDecoded);
				   System.out.println("end:"+end);
				   System.out.println("start:"+start);
				   if(deployDecoded.equals("Deployment")){
					   pair.put("deploy", "true");   
				   }else {
					   pair.put("deploy", "false");
				   }
				   if(securityDecoded.equals("ServerSecurity")){
					   pair.put("security", "true");   
				   }else {
					   pair.put("security", "false");
				   }
				   pair.put("purchaseDate",start.toString());
				   pair.put("expireDate",end.toString());
		}catch(Exception e){
			e.printStackTrace();
			logger.error("There was Problems while updating licence key");
			pair.put("Status", "Error getting the Jobs");
			   return Response.status(500).entity(pair).build();
		}
		logger.info("All the functions worked properly, exiting the function with an array object");
		return Response.status(200).entity(pair).build();
	}	


}
