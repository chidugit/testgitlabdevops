package com.cirrus.devops.service;


import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.bson.Document;
import org.json.simple.JSONArray;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import com.cirrus.devops.ControllerServlet;
import com.mongodb.MongoClient;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

@Path("/TestService") 
public class TestService {
	static final Logger logger = Logger.getLogger(TestService.class);
	private MongoClient connect2Mongo(String node, Integer Num) {
		logger.info("starting the connect2Mongo function");
		MongoClient mongo = new MongoClient( node , Num );
		return mongo;
	}
	
	private MongoDatabase getDatabase(String databaseName) {
		logger.info("starting the getDatabase function");
		MongoClient mongo = this.connect2Mongo("localhost", 27017);
		MongoDatabase database = mongo.getDatabase(databaseName);
		return database;
	}
	 
	
 @GET
 @Path("/Projects")
 @Produces(MediaType.APPLICATION_JSON)
 public Response GetTestResults() {
	 BasicConfigurator.configure();
	 logger.info("Started with GetTestResults function");
	 
	 logger.debug("making the call to get the database");
	 MongoDatabase database2write = this.getDatabase("exampleDB");
	 
	 logger.debug("making the call to get the collections in that database");
	MongoCollection<Document> collection = database2write.getCollection("TestReports");
	JSONArray returnList = new JSONArray();
	List<Document> documents = (List<Document>) collection.find().into(
			new ArrayList<Document>());
	 for(Document document : documents){
         
         returnList.add(document);
     }
	
	 logger.info("getting the collections and sending it back");
	 logger.info("exiting the GetTestResults function");
	 return Response.status(200).entity(returnList).build();
 }
 

}
