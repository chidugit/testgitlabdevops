package com.cirrus.devops.service.database;

import java.util.Base64;
import java.util.Properties;

import org.apache.log4j.BasicConfigurator;

import com.cirrus.devops.util.PropertyUtility;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class SSHTunnel {
	String authString;
	String sshUser;
	String sshPassword;
	String sshHost;
	int nSshPort;
    String remoteHost;
    int nLocalPort;
    int nRemotePort;
	public void doSshTunnel() throws JSchException {
		init();
		 Base64.Decoder decoder1 = Base64.getMimeDecoder(); 
		   String keydecoded1=new String(decoder1.decode(authString));
		   String[] arrOfStr1 = keydecoded1.split("!q4Ev");
		   sshPassword=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-1]));
		   sshUser=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-2]));
		  System.out.println(sshUser); 
		  System.out.println(sshPassword);
        final JSch jsch = new JSch();
        Session session = jsch.getSession(sshUser, sshHost, nSshPort);
        session.setPassword(sshPassword);

        final Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);

        session.connect();
        session.setPortForwardingL(nLocalPort, remoteHost, nRemotePort);
    }
	
	 public void init(){
			
		   BasicConfigurator.configure();
		   PropertyUtility props = PropertyUtility.getPropertyUtility();
		   this.authString=props.getProperty("sshAuthToken");
		  // this.url=props.getProperty("dburl");*/
		   this.sshHost=props.getProperty("sshHost");
		   this.nSshPort=Integer.valueOf(props.getProperty("nSshPort"));
		   this.remoteHost=props.getProperty("remoteHost");
		   this.nLocalPort=Integer.valueOf(props.getProperty("nLocalPort"));
		   this.nRemotePort=Integer.valueOf(props.getProperty("nRemotePort"));
	   }	
}
