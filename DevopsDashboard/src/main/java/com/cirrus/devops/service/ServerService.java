package com.cirrus.devops.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.cirrus.devops.util.PropertyUtility;

@Path("/ServerService") 
public class ServerService{
	
		static final Logger logger = Logger.getLogger(ServerService.class);
		 private HttpURLConnection con;
		 private String authString;
		 private String vcUrl;
		 private String appUrl;
		 private String vcsessionid;
		 private String vmdetails;
		 private String vcmonitor;
		 String inputLine;
		 JSONObject pair;
		 String projectCreationMsg;
		 BufferedReader in;
		 JSONArray array = new JSONArray();
		 String url;
		 String url1;
		 String sessionId;
		public  JSONObject jobj3;
		
	   @SuppressWarnings("unchecked")
	@GET 
	   @Path("/vmdetails")
	   @Produces(MediaType.APPLICATION_JSON)
	   public Response getSessionId(){
		   init();
		logger.info("get vm details function started");
		HashMap<String,String> pair = new HashMap<String,String>() ;
		HttpsTrustManager.allowAllSSL();
		
		try{
				url=vcUrl+appUrl+vcsessionid;
			HttpsURLConnection con = (HttpsURLConnection) ((new URL(url).openConnection()));
			
		 System.out.println(url);
		 con.setDoOutput(true);
		 con.setRequestMethod("POST");
		 con.setRequestProperty("Content-Type", "application/json");
		 con.setRequestProperty("Authorization", "Basic "+authString);
		 con.setRequestProperty("vmware-use-header-authn", "Administrator@vsphere.local");
		 con.connect();
			
		 System.out.println(con.getResponseCode());
        BufferedReader in = new BufferedReader(
      		  new InputStreamReader(con.getInputStream()));
        logger.debug("Connection was executed");
      String inputLine;
      StringBuffer content = new StringBuffer();
      
      System.out.println("in !! "+in);
      while ((inputLine = in.readLine()) != null) {
    	  System.out.println("inputLine !! "+inputLine);
    	  logger.debug("inputLine !! "+inputLine);
     	    content.append(inputLine);
      }
      
    
      JSONParser parser = new JSONParser(); 
      JSONObject jobj = (JSONObject)parser.parse(content.toString());
      System.out.println(jobj.toJSONString());
      sessionId=jobj.get("value").toString();
		logger.info("get vm details function started");
		HttpsTrustManager.allowAllSSL();
				url1=vcUrl+appUrl+vmdetails;
			HttpsURLConnection con1 = (HttpsURLConnection) ((new URL(url1).openConnection()));
		 System.out.println(url1);
		 con1.setDoOutput(true);
		 con1.setRequestMethod("GET");
		 con1.setRequestProperty("Content-Type", "application/json");
		 con1.setRequestProperty("Authorization", "Basic "+authString);
		 con1.setRequestProperty("vmware-api-session-id", sessionId);
		 con1.connect();
			
		 System.out.println(con1.getResponseCode());
        BufferedReader in1 = new BufferedReader(
      		  new InputStreamReader(con1.getInputStream()));
        logger.debug("Connection was executed");
      String inputLine1;
      StringBuffer content1 = new StringBuffer();
      
      System.out.println("in1 !! "+in1);
      while ((inputLine1 = in1.readLine()) != null) {
    	  System.out.println("inputLine1 !! "+inputLine1);
    	  logger.debug("inputLine1 !! "+inputLine1);
     	    content1.append(inputLine1);
      }
      JSONParser parser1 = new JSONParser(); 
      JSONObject jobjj = (JSONObject)parser1.parse(content1.toString());
      logger.info("jobjj:"+jobjj);
      JSONArray jobjo = (JSONArray)jobjj.get("value");
      logger.info("jobo:"+jobjo);
      for(int i=0;i<jobjo.size();i++) 
      {
    	  	pair = new HashMap<String,String>();
   	    	JSONObject jobjo1 =(JSONObject)jobjo.get(i);
   	    	System.out.println(jobjo1.toJSONString());
      		pair.put("name", jobjo1.get("name").toString());
      		pair.put("vm", jobjo1.get("vm").toString());
      		pair.put("power_state", jobjo1.get("power_state").toString());
      		pair.put("cpu_count", jobjo1.get("cpu_count").toString());
      		int memorysize=( Integer.valueOf((jobjo1.get("memory_size_MiB").toString())))/1024;
      		pair.put("memory_size_MiB", String.valueOf(memorysize));
      		array.add(pair);
      		logger.debug("Getting the Details");
      		System.out.println("Response Array:"+array);
      }
      
      con1.disconnect();
      con.disconnect();
		}catch(Exception e){
			e.printStackTrace();
			logger.error("There was some error with the collection");
			pair.put("Status", "Error gettign the vm details");
			   return Response.status(500).entity(pair).build();
		}
		System.out.println("response 200 sent");
		return Response.status(200).entity(array).build();
		
	}

	   public void init(){
			
		   BasicConfigurator.configure();
		   PropertyUtility props = PropertyUtility.getPropertyUtility();
		   this.authString=props.getProperty("vcenter");
		   this.vcUrl=props.getProperty("vcbaseuri");
		   this.appUrl=props.getProperty("appUrl");
		   this.vcsessionid=props.getProperty("vcsessionid");
		   this.vmdetails=props.getProperty("vmdetails");
		   this.vcmonitor=props.getProperty("vcmonitor");
	   }

	   	@GET 
		   @Path("/utilitydata")
		   @Produces(MediaType.APPLICATION_JSON)
		   public Response getUtility(){
	   		init();
	   JSONArray jobjo;
			logger.info("get vm details function started");
			HashMap<String,String> pair = new HashMap<String,String>() ;
			
			HttpsTrustManager.allowAllSSL();
			try{
				url=vcUrl+appUrl+vcsessionid;
				HttpsURLConnection con = (HttpsURLConnection) ((new URL(url).openConnection()));
				
			 System.out.println(url);
			 con.setDoOutput(true);
			 con.setRequestMethod("POST");
			 con.setRequestProperty("Content-Type", "application/json");
			 con.setRequestProperty("Authorization", "Basic "+authString);
			 con.setRequestProperty("vmware-use-header-authn", "Administrator@vsphere.local");
			 con.connect();
				
			 System.out.println(con.getResponseCode());
	        BufferedReader in = new BufferedReader(
	      		  new InputStreamReader(con.getInputStream()));
	        logger.debug("Connection was executed");
	      String inputLine;
	      StringBuffer content = new StringBuffer();
	      
	      System.out.println("in !! "+in);
	      while ((inputLine = in.readLine()) != null) {
	    	  System.out.println("inputLine !! "+inputLine);
	    	  logger.debug("inputLine !! "+inputLine);
	     	    content.append(inputLine);
	      }
	      
	    
	      JSONParser parser = new JSONParser(); 
	      JSONObject jobj = (JSONObject)parser.parse(content.toString());
	      System.out.println(jobj.toJSONString());
	      sessionId=jobj.get("value").toString();
	    
	      LocalDateTime endDate =  LocalDateTime.now();
	      LocalDateTime startDate = endDate.minusMinutes(30);
	      System.out.println("minus 30 minutes start:"+startDate);
	      startDate = startDate.minusHours(6);
	      System.out.println("minus 6 hours 30 min start: "+startDate);
	      endDate = endDate.minusMinutes(35);
	      System.out.println("minus 30 minutes end:"+startDate);
	      endDate = endDate.minusHours(5);
	      System.out.println("minus 6 hours 30 min end:"+startDate);
	      //LocalDateTime startDate = endDate.minusDays(1);
		   System.out.println(endDate);
		   String subendDate=endDate.toString().substring(0,endDate.toString().indexOf(".")+4);
		   System.out.println(endDate.toString().indexOf(".")+4);
		   System.out.println(subendDate);
		   System.out.println(startDate);
		   String substartDate=startDate.toString().substring(0,startDate.toString().indexOf(".")+4);
		   System.out.println(startDate.toString().indexOf(".")+4);
		   System.out.println(substartDate);
	      HttpsTrustManager.allowAllSSL();
			url1=vcUrl+appUrl+vcmonitor+"?item.interval=MINUTES5&item.end_time="+subendDate+"Z&item.names.1=cpu.util&item.names.2=mem.util&item.start_time="+substartDate+"Z&item.function=AVG";
			HttpsURLConnection con1 = (HttpsURLConnection) ((new URL(url1).openConnection()));
				
			 System.out.println(url1);
			 con1.setDoOutput(true);
			 con1.setRequestMethod("GET");
			 con1.setRequestProperty("Content-Type", "application/json");
			 con1.setRequestProperty("Authorization", "Basic "+authString);
			 con1.setRequestProperty("vmware-api-session-id", sessionId);
			 con1.connect();
				
			 System.out.println(con1.getResponseCode());
	        BufferedReader in1 = new BufferedReader(
	      		  new InputStreamReader(con1.getInputStream()));
	        logger.debug("Connection was executed");
	      String inputLine1;
	      StringBuffer content1 = new StringBuffer();
	      
	      System.out.println("in1 !! "+in1);
	      while ((inputLine1 = in1.readLine()) != null) {
	    	  System.out.println("inputLine1 !! "+inputLine1);
	    	  logger.debug("inputLine1 !! "+inputLine1);
	     	    content1.append(inputLine1);
	      }
	      JSONParser parser1 = new JSONParser(); 
	      JSONObject jobjj = (JSONObject)parser1.parse(content1.toString());
	      logger.info("jobjj:"+jobjj);
	      jobjo = (JSONArray)jobjj.get("value");
	      logger.info("jobo:"+jobjo);
	      
	      
	      con1.disconnect();
			}catch(Exception e){
				e.printStackTrace();
				logger.error("There was some error with the collection");
				pair.put("Status", "Error gettign the vm details");
				   return Response.status(500).entity(pair).build();
			}
			System.out.println("response 200 sent");
			return Response.status(200).entity(jobjo).build();
			
		}
}