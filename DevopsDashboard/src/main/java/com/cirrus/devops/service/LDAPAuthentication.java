package com.cirrus.devops.service;

import java.util.HashMap;
import java.util.Hashtable;

import javax.naming.CommunicationException;
import javax.naming.Context;
import javax.naming.directory.InitialDirContext;
import javax.validation.constraints.NotNull;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import com.cirrus.devops.util.PropertyUtility;

@Path("/ldaputhentication")
public class LDAPAuthentication {
	static final Logger logger = Logger.getLogger(LDAPAuthentication.class);
	@POST 
	   @Path("/authenticate")
	   @Produces(MediaType.APPLICATION_JSON)
	   public Response authenticate(@NotNull @QueryParam("username") String userName,@NotNull @QueryParam("password") String password){
		HashMap<String,String> pair = new HashMap<String,String>() ;
		BasicConfigurator.configure();
		PropertyUtility props=PropertyUtility.getPropertyUtility();
		String ldapUrl = props.getProperty("ldapUrl");
			// Set up the environment for creating the initial context
			  /*	String userName = "adityacv";
			    String password = "VMwar3!!";*/
			    String base ="OU=IQusers,DC=cdlab,DC=com";//OU=IQusers,
			   // String dn = "cn=" + userName + "," + "CN=Users," + base;  
			    Hashtable<String, String> env = new Hashtable<String, String>();
			    env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
			    env.put(Context.PROVIDER_URL, ldapUrl);
			    env.put(Context.SECURITY_AUTHENTICATION, "simple");
			    //env.put(Context.SECURITY_PRINCIPAL, dn);
			    env.put(Context.SECURITY_PRINCIPAL, userName+"@cdlab.com");
			    env.put(Context.SECURITY_CREDENTIALS, password);
			    new LDAPAuthentication();
			   try {
			        new InitialDirContext(env);
			  System.out.println("Authenticated Successfully");
			  
		  }catch (CommunicationException e) {
			  e.printStackTrace();
			  logger.error("Not able to connect to AD");
				pair.put("Status", "connection time out");
				   return Response.status(500).entity(pair).build();
		}catch (Exception e) {
			  e.printStackTrace();
			  logger.error("Inavalid username/password");
				pair.put("Status", "fail");
				   return Response.status(401).entity(pair).build();
		}
			   logger.info("All the functions worked properly, exiting the function with an array object");
			   pair.put("Status", "success");
				return Response.status(200).entity(pair).build();
	  }
}
