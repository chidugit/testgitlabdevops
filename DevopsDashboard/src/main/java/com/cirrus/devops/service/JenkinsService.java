package com.cirrus.devops.service;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Document;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Jsoup;
import org.xml.sax.SAXException;
import com.cirrus.devops.ControllerServlet;
import com.cirrus.devops.util.PropertyUtility;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import org.jsoup.nodes.Element;

@Path("/JenkinsService") 
public class JenkinsService {
	private String jenkinsBaseUrl;
	 private String projectUrl;
	 private String jenkinsAuthurl;
	 private String cred_url;
	 private static URL obj;
	 private static HttpURLConnection con;
	 JSONObject gitUrl;
	 Client restClient;
	 WebResource webResource;
	 ClientResponse resp;
	 static String authString;
	 static String authStringEnc;
	 static JSONObject jobj;
	 static StringBuffer content;
	 static Map<String,String> message=new HashMap<String,String>();;
	 static final Logger logger = Logger.getLogger(JenkinsService.class);
	 static Map<String,String> map = new HashMap<String,String>();
	
	@SuppressWarnings("unchecked")
	@GET 
	   @Path("/projects/all")
	   @Produces(MediaType.APPLICATION_JSON)
	   public Response getprojects(@QueryParam("name") String username,
			   @QueryParam("password") String pass) throws IOException{		
		BasicConfigurator.configure();
		logger.info("Starting the getprojects function");
		PropertyUtility props=PropertyUtility.getPropertyUtility();
		try {
		if (username == null|| pass == null||
				username.isEmpty() || pass.isEmpty()) {
			logger.error("you have to enter username and password for now");
			message.put("Message", "The username and password must be given.");
			return Response.status(400).entity(message).build();
		}
		}catch(Exception e) {
			logger.error("you have to enter username and password for now",e);			
			message.put("Message", "The username and password must be given.");
			return Response.status(400).entity(message).build();			
		}
		JSONObject jenkinsJobPair = new JSONObject();
		JSONArray array = new JSONArray();
		JSONObject jenkinsBuildpair = new JSONObject();
		JSONArray array2 = new JSONArray();
		String url=props.getProperty("jenkins_url");
		String suffix = props.getProperty("jenkinSuffix");
		String buildData = props.getProperty("jenkinsBuildData");
			try{
			        URL urlObject = new URL(url+suffix);
			    	HttpURLConnection con = (HttpURLConnection) urlObject.openConnection();
			    	String name = username;				
			        String password = pass;		        
			        String authString = name + ":" + password;	
			        String authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes());		        
			        con.setDoOutput(true);
			        con.setRequestProperty  ("Authorization", "Basic " + authStringEnc);		        
			        Client restClient = Client.create();
			        WebResource webResource = restClient.resource(urlObject.toString());
			        ClientResponse resp = webResource.accept("application/json")
			                                         .header("Authorization", "Basic " + authStringEnc)
			                                         .get(ClientResponse.class);
			      
			        message.put("username", username);
			        message.put("password", pass);
			        message.put("auth", authStringEnc);
			        
			if (resp.getStatus() != 200) {
				logger.debug("Check for the Credentials");
				message.put("Message", "The username and password must be correct.");
				return Response.status(400).entity(message).build();
			}
			BufferedReader in = new BufferedReader(
		      		  new InputStreamReader(con.getInputStream()));	        
		      String inputLine;
		      StringBuffer content = new StringBuffer();		      
		      while ((inputLine = in.readLine()) != null) {
		     	    content.append(inputLine);
		      }	
      JSONParser parser = new JSONParser(); 
      JSONObject jobj = (JSONObject)parser.parse(content.toString());
      logger.info("jobj:"+jobj);
      JSONArray arr = (JSONArray) jobj.get("jobs");
      logger.info("arr:"+arr);
      logger.debug("Getting the jobs from jenkins");
      for(int i=0;i<arr.size();i++) 
      {
    	  	jenkinsJobPair = new JSONObject();
    	  	jenkinsBuildpair = new JSONObject();
  	    	JSONObject jobj1 =(JSONObject)arr.get(i);   	
      		jenkinsJobPair.put("name", jobj1.get("name").toString());
      		jenkinsJobPair.put("url", jobj1.get("url").toString());
      		jenkinsJobPair.put("color", jobj1.get("color").toString());
      		array.add(jenkinsJobPair);     		
      		jenkinsBuildpair.put("url", jobj1.get("url").toString());
      		logger.info(jobj1.get("url").toString());
      		array2.add(jenkinsBuildpair);    		
      		jenkinsBuildpair.get("url");	
      		String JobName = jenkinsJobPair.get("name").toString();
      		logger.debug(JobName);
      		logger.debug("Making a call to the respective job to get more information"); 		   
      		   logger.debug("Making the call to get the details from the jenkins");
      		   URL obj2 = new URL(jenkinsBuildpair.get("url").toString()+"api/json"+buildData);
      		 	HttpURLConnection con2 = (HttpURLConnection) obj2.openConnection();
      		     con2.setDoOutput(true);
      		     con2.setRequestProperty  ("Authorization", "Basic " + authStringEnc);
      		     Client restClient2 = Client.create();
      		     /*String url1=jenkinsBuildpair.get("url").toString();
      		     String fir=url1.substring(0,url1.indexOf(":8080"));
      		   String sec=url1.substring(url1.indexOf("/job"),url1.length());
      		     url1=fir+sec;
      		     System.out.println("url1:"+url1);*/
      		     WebResource webResource2 = restClient2.resource(jenkinsBuildpair.get("url").toString()+"api/json"+buildData);
      		     logger.info("web:"+webResource2);
      		     ClientResponse resp2 = webResource2.accept("application/json")
      		                                      .header("Authorization", "Basic " + authStringEnc)
      		                                      .get(ClientResponse.class); 
      		     logger.info("resp2:"+resp2.toString());
      		     if (resp2.getStatus() !=200) {
      		    	logger.debug("The Username and password might be the issue here");
      		    	message.put("Message", "The username and password must be given.");
      				return Response.status(400).entity(message).build();
      		     }
      		     BufferedReader in2 = new BufferedReader(
      		     		  new InputStreamReader(con2.getInputStream()));
      		
      		     String inputLine2;
      		     StringBuffer content2 = new StringBuffer();
      		     
      		     while ((inputLine2 = in2.readLine()) != null) {
      		    	    content2.append(inputLine2);
      		     }
      		     logger.info("content2:"+content2);
      		JSONParser parser2 = new JSONParser(); 
      		JSONObject jobj2 = (JSONObject)parser2.parse(content2.toString());
      		logger.info("jobj2:"+jobj2);
      		JSONObject lastbuild=(JSONObject)jobj2.get("lastBuild");
      		if(lastbuild!=null) {
      		if(lastbuild.get("number")!=null) {
      		jenkinsJobPair.put("lastBuildNumber", lastbuild.get("number"));
      		}
      		logger.info("lastbuild:"+lastbuild);
      		JSONArray action = (JSONArray)lastbuild.get("actions");
		      logger.info("Array data:"+action);
		      logger.debug("Getting the job details");
		      logger.info(action.size());
		      if(action!=null) {
		      for(int l=0;l<action.size();l++) 
		      {
		  	    	JSONObject node =(JSONObject)action.get(l);
		  	    	if(node!=null) {
		  	    		if(node.get("totalCount")!=null) {
		  	    			jenkinsJobPair.put("totalcount", node.get("totalCount"));
		  	    		}
		  	    		if(node.get("skipCount")!=null) {	
		  	    			jenkinsJobPair.put("skipcount", node.get("skipCount"));
		  	    		}
		  	    		if(node.get("failCount")!=null) {
		  	    			jenkinsJobPair.put("failcount", node.get("failCount"));
		  	    		}
		  	    		
		  	    	JSONArray nodesArr=(JSONArray)node.get("nodes");
		  	    	if(nodesArr!=null) {
		  	    	for (int j = 0; j < nodesArr.size(); j++) {
		  	    			//JSONObject nodesinfo=new JSONObject();
		  	    			JSONObject node1 =(JSONObject)nodesArr.get(j);
		  	    			if(node1.get("displayName").toString().equalsIgnoreCase("Deploy to Testing Environment")) {
		  	    				jenkinsJobPair.put("testName", node1.get("displayName"));
		  	    				jenkinsJobPair.put("testiconColor", node1.get("iconColor"));
		  	    			}else if(node1.get("displayName").toString().equalsIgnoreCase("Deploy to Staging Environment")){
		  	    				jenkinsJobPair.put("stageName", node1.get("displayName"));
		  	    				jenkinsJobPair.put("stageiconColor", node1.get("iconColor"));
		  	    			}else if(node1.get("displayName").toString().equalsIgnoreCase("Deploy to Production Environment")){
		  	    				jenkinsJobPair.put("prodName", node1.get("displayName"));
		  	    				jenkinsJobPair.put("prodiconColor", node1.get("iconColor"));
		  	    			}
				      		//array.add(jenkinsJobPair);
		  	    			}
		  	    		}
		  	    	}
		      }
		      HttpURLConnection con1 = (HttpURLConnection) new URL(url+"/job/"+jobj1.get("name").toString()+"/config.xml").openConnection();
		      	con1.setRequestMethod("GET");
		    	con1.setRequestProperty("Authorization", "Basic " + authStringEnc);

		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		   documentBuilderFactory.setNamespaceAware(true);
		   DocumentBuilder builder = documentBuilderFactory.newDocumentBuilder();
		   Document doc = builder.parse(con1.getInputStream()); //input stream of response.

		   XPathFactory xPathFactory = XPathFactory.newInstance();
		   XPath xpath = xPathFactory.newXPath();

		   XPathExpression expr = xpath.compile("//script"); // Look for status tag value.
		  // String status =  expr.evaluate(doc);
		      if(expr.evaluate(doc).contains("scp")==true){
		    	  jenkinsJobPair.put("server", "scp");
		      }else if(expr.evaluate(doc).contains("AWSEBDeploymentBuilder")==true){
		    	  jenkinsJobPair.put("server", "aws");
		      }else if(expr.evaluate(doc).contains("ibmcloud")==true){
		    	  jenkinsJobPair.put("server", "ibm");
		      	}
		      }
      }
      		JSONObject jobj3 =(JSONObject) jobj2.get("lastSuccessfulBuild");
      		if((JSONObject) jobj2.get("lastSuccessfulBuild")==null){
      		}else{
      			jenkinsJobPair.put("lastSuccessfulBuildNumber", jobj3.get("number"));
      			jenkinsJobPair.put("lastSuccessfulBuildUrl", jobj3.get("url"));
      		}
      			JSONObject jobj4 =(JSONObject)  jobj2.get("lastFailedBuild");
      		try{
      			jenkinsJobPair.put("lastFailedBuildNumber", jobj4.get("number"));
      			jenkinsJobPair.put("lastFailedBuildUrl", jobj4.get("url"));
      		}catch (Exception e) {
			 logger.error("Something went wrong while putting the details of jenkins into an Object");
			}
      }
      con.disconnect();
		}catch(Exception e){
			e.printStackTrace();
			logger.error("There was Problems while getting the details from jenkins");
			jenkinsJobPair.put("Status", "Error getting the Jobs");
			   return Response.status(500).entity(jenkinsJobPair).build();
		}
		logger.info("All the functions worked properly, exiting the function with an array object");
		return Response.status(200).entity(array).build();
	}


	@SuppressWarnings("unchecked")
	@POST 
	   @Path("/createjob")
	   @Produces(MediaType.APPLICATION_JSON)
	   public Response getprojects(InputStream json) 
			   throws ParseException, ParserConfigurationException, SAXException, TransformerException, IOException{
		
		BufferedReader br1 = new BufferedReader(new InputStreamReader(json));
		StringBuffer sb1 = new StringBuffer();
		String content;
			while((content=br1.readLine())!=null){
				sb1.append(content);
			}
		
		JSONParser parser = new JSONParser();
		JSONObject jobj1;
			jobj1 = (JSONObject) parser.parse(sb1.toString());
		
		String JobName=(String) jobj1.get("name");
		String buildTriggerType = (String) jobj1.get("buildTriggerType");
		String schedule = (String) jobj1.get("schedule");
		JobName = URLEncoder.encode(JobName).replaceAll("\\+", "%20");
		
		String username = (String) ControllerServlet.session.getAttribute("username");				
        String password = (String) ControllerServlet.session.getAttribute("userpassword");	
		/*String username = "root";				
        String password = "VMwar3!!";*/	
		String authString = username + ":" + password;
	    String authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes());
	  PropertyUtility props=PropertyUtility.getPropertyUtility();
				   String jenkinsURL = props.getProperty("jenkins_url");
		obj = new URL(jenkinsURL+"/crumbIssuer/api/json");
		con = (HttpURLConnection) obj.openConnection();
	   con.setRequestProperty  ("Authorization", "Basic " + authStringEnc);
		con.setRequestMethod("GET");
		readResponse();
		String JenkinsCrumb = jobj.get("crumbRequestField").toString();
		String crumbID= jobj.get("crumb").toString();
		message.put("JenkinsCrumb", jobj.get("crumbRequestField").toString());
		message.put("crumbID", jobj.get("crumb").toString());
		
		try {
			obj = new URL(jenkinsURL+"/createItem?name="+JobName);
			con = (HttpURLConnection) obj.openConnection();
		con.setRequestProperty  ("Authorization", "Basic " + authStringEnc);
		con.setDoOutput(true);
		con.setRequestProperty("Content-Type", "application/xml");
		con.setRequestProperty(JenkinsCrumb, crumbID);
		} catch (MalformedURLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		InputStream reader = null;
		ClassLoader classLoader = getClass().getClassLoader();
		
		if(buildTriggerType.equals("Tag Push")){
		
		System.out.println(classLoader.getResourceAsStream("createJob/pushEventsconfig.xml"));
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(classLoader.getResourceAsStream("createJob/pushEventsconfig.xml"));
		
		Node flowdefinition = doc.getFirstChild();
		
		Node staff = doc.getElementsByTagName("definition").item(0);
		
		Node jobName= doc.getElementsByTagName("description").item(0);
		String description = JobName.replaceAll("%20", " ");
		jobName.setTextContent(description);
		
		
		NodeList list = staff.getChildNodes();
		
		for (int i = 0; i < list.getLength(); i++) {
		           Node node = list.item(i);
		   if ("script".equals(node.getNodeName())) {
			   	   PropertyUtility prop=PropertyUtility.getPropertyUtility();
			   	 FileInputStream in = new FileInputStream(this.getClass().getClassLoader().getResource("config.properties").getFile());
				   Properties prop1 = new Properties();
				   prop1.load(in);
				   in.close();
				// String gitUrl = prop.getProperty("git_url");
				   String tomUrl = prop1.getProperty("tomUrl");
				   //URL neturl = new URL("http://localhost:8080/");
				   System.out.println("tomurl:"+tomUrl);
				   //URL neturl = new URL(tomUrl);
				   String projectUrl = prop1.getProperty("newGitProjectUrl");
				   System.out.println(JobName);
				   String projNameSubString;
			   if(JobName.contains("_")){
			    projNameSubString=JobName.substring(0, JobName.indexOf("_"));
			   }else{
				   projNameSubString=JobName;
			   }
			   if(jobj1.get("selenium").toString().equalsIgnoreCase("true")) {
			   //String tomcatUrl = neturl.getHost();
			   node.setTextContent("stage 'Code Build' node { git credentialsId: 'f4155b0d-9719-41bd-b6df-d91433d3f161', url:'"+projectUrl+"' sh 'mvn -f "+projNameSubString+"/pom.xml clean package install' } stage 'JUnit Report' node { step([$class: 'JUnitResultArchiver', testResults: '**/*.xml']) }  stage 'Selenium Report' node { step([$class: 'SeleniumResultArchiver', testResults: '**/*.xml']) } stage 'SonarQube Analysis' node{ env.sonarHome=tool name: 'SonarQubeScanner', type: 'hudson.plugins.sonar.SonarRunnerInstallation' withSonarQubeEnv { sh '${sonarHome}/bin/sonar-scanner -Dsonar.projectKey=sonar1234 -Dsonar.projectVersion=1.0 -Dsonar.sources="+projNameSubString+"/src/main/java -Dsonar.language=java -Dsonar.java.binaries="+projNameSubString+"/target/classes ' } } stage 'Deploy to Testing Environment' node { sshagent(['tomcatroot']) { sh 'sudo -u jenkins scp "+projNameSubString+"/target/"+projNameSubString+".war root@192.168.137.118:/opt/tomcat/webapps/' } } mail (to: 'projects@cirrusdigitals.com', from: 'Devops@cirrusdigitals.com', subject: 'Dashboard application with Jenkins Job '${env.JOB_BASE_NAME}' (${env.BUILD_NUMBER}) is waiting for input', body:'''Dear project manager, A Succesfull Deployment of the build '${env.JOB_BASE_NAME}' has been done in the Test Enviroment and now seeking your consent for Staging. Please click on the link ${env.BUILD_URL}input to Approve or Reject.The Build will be automatically aborted if there is no response within 60 minutes.''') timeout (time:3600, unit: 'SECONDS'){ input message: 'Do you want to proceed with deployment to Staging Environment?' , submitter: 'admin' } stage 'Deploy to Staging Environment' node { sshagent(['tomcatroot'])"
			   + "{ sh 'sudo -u jenkins scp "+projNameSubString+"/target/"+projNameSubString+".war root@192.168.137.116:/opt/tomcat/webapps/' } } "
			   + "	mail (to: 'projects@cirrusdigitals.com', from: 'Devops@cirrusdigitals.com', subject: 'Dashboard application with Jenkins Job '${env.JOB_BASE_NAME}' (${env.BUILD_NUMBER}) is waiting for input', body:'''Dear project manager, A Succesfull Deployment of the build '${env.JOB_BASE_NAME}' has been done in the Staging Enviroment and now seeking your consent for Production. Please click on the link ${env.BUILD_URL}input to Approve or Reject.The Build will be automatically aborted if there is no response within 60 minutes.''') timeout (time:3600, unit: 'SECONDS'){ input message: 'Do you want to proceed with deployment to Production Environment?' , submitter: 'admin' } stage 'Deploy to Production Environment' node { sshagent(['tomcatroot']) "
				+	"{ sh 'sudo -u jenkins scp "+projNameSubString+"/target/"+projNameSubString+".war root@192.168.137.117:/opt/tomcat/webapps/' } }");
		   	}else if(jobj1.get("selenium").toString().equalsIgnoreCase("false")) {
		   	 node.setTextContent("stage 'Code Build' node { git credentialsId: 'f4155b0d-9719-41bd-b6df-d91433d3f161', url:'"+projectUrl+"' sh 'mvn -f "+projNameSubString+"/pom.xml clean package install' } stage 'JUnit Report' node { step([$class: 'JUnitResultArchiver', testResults: '**/*.xml']) } stage 'SonarQube Analysis' node{ env.sonarHome=tool name: 'SonarQubeScanner', type: 'hudson.plugins.sonar.SonarRunnerInstallation' withSonarQubeEnv { sh '${sonarHome}/bin/sonar-scanner -Dsonar.projectKey=sonar1234 -Dsonar.projectVersion=1.0 -Dsonar.sources="+projNameSubString+"/src/main/java -Dsonar.language=java -Dsonar.java.binaries="+projNameSubString+"/target/classes ' } } stage 'Deploy to Testing Environment' node { sshagent(['tomcatroot']) { sh 'sudo -u jenkins scp "+projNameSubString+"/target/"+projNameSubString+".war root@192.168.137.118:/opt/tomcat/webapps/' } } mail (to: 'projects@cirrusdigitals.com', from: 'Devops@cirrusdigitals.com', subject: 'Dashboard application with Jenkins Job '${env.JOB_BASE_NAME}' (${env.BUILD_NUMBER}) is waiting for input', body:'''Dear project manager, A Succesfull Deployment of the build '${env.JOB_BASE_NAME}' has been done in the Test Enviroment and now seeking your consent for Staging. Please click on the link ${env.BUILD_URL}input to Approve or Reject.The Build will be automatically aborted if there is no response within 60 minutes.''') timeout (time:3600, unit: 'SECONDS'){ input message: 'Do you want to proceed with deployment to Staging Environment?' , submitter: 'admin' } stage 'Deploy to Staging Environment' node { sshagent(['tomcatroot'])"
					   + "{ sh 'sudo -u jenkins scp "+projNameSubString+"/target/"+projNameSubString+".war root@192.168.137.116:/opt/tomcat/webapps/' } } "
					   + "	mail (to: 'projects@cirrusdigitals.com', from: 'Devops@cirrusdigitals.com', subject: 'Dashboard application with Jenkins Job '${env.JOB_BASE_NAME}' (${env.BUILD_NUMBER}) is waiting for input', body:'''Dear project manager, A Succesfull Deployment of the build '${env.JOB_BASE_NAME}' has been done in the Staging Enviroment and now seeking your consent for Production. Please click on the link ${env.BUILD_URL}input to Approve or Reject.The Build will be automatically aborted if there is no response within 60 minutes.''') timeout (time:3600, unit: 'SECONDS'){ input message: 'Do you want to proceed with deployment to Production Environment?' , submitter: 'admin' } stage 'Deploy to Production Environment' node { sshagent(['tomcatroot']) "
						+	"{ sh 'sudo -u jenkins scp "+projNameSubString+"/target/"+projNameSubString+".war root@192.168.137.117:/opt/tomcat/webapps/' } }");
		   	}
		   }

}

// write the content into xml file

TransformerFactory transformerFactory = TransformerFactory.newInstance();
Transformer transformer = transformerFactory.newTransformer();
DOMSource source = new DOMSource(doc);

File file = new File(classLoader.getResource("createJob/pushEventsconfig.xml").getFile());
StreamResult result = new StreamResult(new File(classLoader.getResource("createJob/pushEventsconfig.xml").getFile()).getAbsolutePath());

transformer.transform(source, result);

 reader =new FileInputStream(classLoader.getResource("createJob/pushEventsconfig.xml").getFile());
	}
else if(buildTriggerType!=null&&buildTriggerType.equals("Build Periodically")){
	
	DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	Document doc = docBuilder.parse(classLoader.getResourceAsStream("createJob/buildPeriodicConfig.xml"));

	  Node staff = doc.getElementsByTagName("hudson.triggers.TimerTrigger").item(0);
        
       NodeList list = staff.getChildNodes();
           
        for (int i = 0; i < list.getLength(); i++) {
        	Node node = list.item(i);
           if ("spec".equals(node.getNodeName())) {
        	   
        	   node.setTextContent(schedule);
        	   
           }
          
        }
	Node staff1 = doc.getElementsByTagName("definition").item(0);
		
		Node jobName= doc.getElementsByTagName("description").item(0);
		String description = JobName.replaceAll("%20", " ");
		jobName.setTextContent(description);
		
		
		NodeList list1 = staff1.getChildNodes();
		
		for (int i = 0; i < list1.getLength(); i++) {
		           Node node = list1.item(i);
		   if ("script".equals(node.getNodeName())) {
			   	   PropertyUtility prop=PropertyUtility.getPropertyUtility();
			   	 FileInputStream in = new FileInputStream(this.getClass().getClassLoader().getResource("config.properties").getFile());
				   Properties prop1 = new Properties();
				   prop1.load(in);
				   in.close();
				// String gitUrl = prop.getProperty("git_url");
				   String tomUrl = prop1.getProperty("tomUrl");
				   //URL neturl = new URL("http://localhost:8080/");
				   System.out.println("tomurl:"+tomUrl);
				   //URL neturl = new URL(tomUrl);
				   String projectUrl = prop1.getProperty("newGitProjectUrl");
				   System.out.println(JobName);
				   String projNameSubString;
			   if(JobName.contains("_")){
			    projNameSubString=JobName.substring(0, JobName.indexOf("_"));
			   }else{
				   projNameSubString=JobName;
			   }
			   if(jobj1.get("selenium").toString().equalsIgnoreCase("true")) {
			   //String tomcatUrl = neturl.getHost();
			   node.setTextContent("stage 'Code Build' node { git credentialsId: 'f4155b0d-9719-41bd-b6df-d91433d3f161', url:'"+projectUrl+"' sh 'mvn -f "+projNameSubString+"/pom.xml clean package install' } stage 'JUnit Report' node { step([$class: 'JUnitResultArchiver', testResults: '**/*.xml']) }  stage 'Selenium Report' node { step([$class: 'SeleniumResultArchiver', testResults: '**/*.xml']) } stage 'SonarQube Analysis' node{ env.sonarHome=tool name: 'SonarQubeScanner', type: 'hudson.plugins.sonar.SonarRunnerInstallation' withSonarQubeEnv { sh '${sonarHome}/bin/sonar-scanner -Dsonar.projectKey=sonar1234 -Dsonar.projectVersion=1.0 -Dsonar.sources="+projNameSubString+"/src/main/java -Dsonar.language=java -Dsonar.java.binaries="+projNameSubString+"/target/classes ' } } stage 'Deploy to Testing Environment' node { sshagent(['tomcatroot']) { sh 'sudo -u jenkins scp "+projNameSubString+"/target/"+projNameSubString+".war root@192.168.137.118:/opt/tomcat/webapps/' } } mail (to: 'projects@cirrusdigitals.com', from: 'Devops@cirrusdigitals.com', subject: 'Dashboard application with Jenkins Job '${env.JOB_BASE_NAME}' (${env.BUILD_NUMBER}) is waiting for input', body:'''Dear project manager, A Succesfull Deployment of the build '${env.JOB_BASE_NAME}' has been done in the Test Enviroment and now seeking your consent for Staging. Please click on the link ${env.BUILD_URL}input to Approve or Reject.The Build will be automatically aborted if there is no response within 60 minutes.''') timeout (time:3600, unit: 'SECONDS'){ input message: 'Do you want to proceed with deployment to Staging Environment?' , submitter: 'admin' } stage 'Deploy to Staging Environment' node { sshagent(['tomcatroot'])"
			   + "{ sh 'sudo -u jenkins scp "+projNameSubString+"/target/"+projNameSubString+".war root@192.168.137.116:/opt/tomcat/webapps/' } } "
			   + "	mail (to: 'projects@cirrusdigitals.com', from: 'Devops@cirrusdigitals.com', subject: 'Dashboard application with Jenkins Job '${env.JOB_BASE_NAME}' (${env.BUILD_NUMBER}) is waiting for input', body:'''Dear project manager, A Succesfull Deployment of the build '${env.JOB_BASE_NAME}' has been done in the Staging Enviroment and now seeking your consent for Production. Please click on the link ${env.BUILD_URL}input to Approve or Reject.The Build will be automatically aborted if there is no response within 60 minutes.''') timeout (time:3600, unit: 'SECONDS'){ input message: 'Do you want to proceed with deployment to Production Environment?' , submitter: 'admin' } stage 'Deploy to Production Environment' node { sshagent(['tomcatroot']) "
				+	"{ sh 'sudo -u jenkins scp "+projNameSubString+"/target/"+projNameSubString+".war root@192.168.137.117:/opt/tomcat/webapps/' } }");
		   	}else if(jobj1.get("selenium").toString().equalsIgnoreCase("false")) {
		   	 node.setTextContent("stage 'Code Build' node { git credentialsId: 'f4155b0d-9719-41bd-b6df-d91433d3f161', url:'"+projectUrl+"' sh 'mvn -f "+projNameSubString+"/pom.xml clean package install' } stage 'JUnit Report' node { step([$class: 'JUnitResultArchiver', testResults: '**/*.xml']) } stage 'SonarQube Analysis' node{ env.sonarHome=tool name: 'SonarQubeScanner', type: 'hudson.plugins.sonar.SonarRunnerInstallation' withSonarQubeEnv { sh '${sonarHome}/bin/sonar-scanner -Dsonar.projectKey=sonar1234 -Dsonar.projectVersion=1.0 -Dsonar.sources="+projNameSubString+"/src/main/java -Dsonar.language=java -Dsonar.java.binaries="+projNameSubString+"/target/classes ' } } stage 'Deploy to Testing Environment' node { sshagent(['tomcatroot']) { sh 'sudo -u jenkins scp "+projNameSubString+"/target/"+projNameSubString+".war root@192.168.137.118:/opt/tomcat/webapps/' } } mail (to: 'projects@cirrusdigitals.com', from: 'Devops@cirrusdigitals.com', subject: 'Dashboard application with Jenkins Job '${env.JOB_BASE_NAME}' (${env.BUILD_NUMBER}) is waiting for input', body:'''Dear project manager, A Succesfull Deployment of the build '${env.JOB_BASE_NAME}' has been done in the Test Enviroment and now seeking your consent for Staging. Please click on the link ${env.BUILD_URL}input to Approve or Reject.The Build will be automatically aborted if there is no response within 60 minutes.''') timeout (time:3600, unit: 'SECONDS'){ input message: 'Do you want to proceed with deployment to Staging Environment?' , submitter: 'admin' } stage 'Deploy to Staging Environment' node { sshagent(['tomcatroot'])"
					   + "{ sh 'sudo -u jenkins scp "+projNameSubString+"/target/"+projNameSubString+".war root@192.168.137.116:/opt/tomcat/webapps/' } } "
					   + "	mail (to: 'projects@cirrusdigitals.com', from: 'Devops@cirrusdigitals.com', subject: 'Dashboard application with Jenkins Job '${env.JOB_BASE_NAME}' (${env.BUILD_NUMBER}) is waiting for input', body:'''Dear project manager, A Succesfull Deployment of the build '${env.JOB_BASE_NAME}' has been done in the Staging Enviroment and now seeking your consent for Production. Please click on the link ${env.BUILD_URL}input to Approve or Reject.The Build will be automatically aborted if there is no response within 60 minutes.''') timeout (time:3600, unit: 'SECONDS'){ input message: 'Do you want to proceed with deployment to Production Environment?' , submitter: 'admin' } stage 'Deploy to Production Environment' node { sshagent(['tomcatroot']) "
						+	"{ sh 'sudo -u jenkins scp "+projNameSubString+"/target/"+projNameSubString+".war root@192.168.137.117:/opt/tomcat/webapps/' } }");
		   	}
		   }

}
	// write the content into xml file

	TransformerFactory transformerFactory = TransformerFactory.newInstance();
	Transformer transformer = transformerFactory.newTransformer();
	DOMSource source = new DOMSource(doc);

	File file = new File(classLoader.getResource("createJob/buildPeriodicConfig.xml").getFile());
	
	StreamResult result = new StreamResult(new File(classLoader.getResource("createJob/buildPeriodicConfig.xml").getFile()).getAbsolutePath());
	transformer.transform(source, result);
	reader =new FileInputStream(classLoader.getResource("createJob/buildPeriodicConfig.xml").getFile());
}
BufferedReader br = new BufferedReader(new InputStreamReader(reader));
String line;
StringBuilder sb = new StringBuilder();

while((line=br.readLine())!= null){
 sb.append(line.trim());
}
String body = sb.toString();

OutputStream output = new BufferedOutputStream(con.getOutputStream());
output.write(body.getBytes());
output.flush();
output.close();
try {
return Response.status(con.getResponseCode()).build();
}catch(IOException e) {
	e.printStackTrace();
	JSONObject obj = new JSONObject();
	obj.put("error", "project name already exist in monitoring level");
	return Response.status(con.getResponseCode()).entity(obj).build();
}
}

	
	public static JSONObject readResponse() throws IOException, ParseException{
		BufferedReader in = new BufferedReader(
		  		  new InputStreamReader(con.getInputStream()));
		    System.out.println("A connection was made.");
		  String inputLine;
		  content = new StringBuffer();
		  
		  while ((inputLine = in.readLine()) != null) {
		 	    content.append(inputLine);
		  }
	
		JSONParser parser = new JSONParser(); 
		jobj = (JSONObject)parser.parse(content.toString());
		return jobj;
	}

//SERVICE TO GET JENKINS JOB DETAILS LIKE BUILD NAME, NUMBER, STATUS, TIME, RESULT
		@SuppressWarnings("unchecked")
		@GET
		@Path("/project_details")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getProjectDetails(@QueryParam("projectname") String projName ,@QueryParam("name") String username,
				   @QueryParam("password") String password){
			BasicConfigurator.configure();
			logger.info("Starting the getprojects function");
			PropertyUtility props=PropertyUtility.getPropertyUtility();
			
			JSONObject jenkinsJobPair = new JSONObject();
			JSONArray array = new JSONArray();

			String url=props.getProperty("jenkins_url");
			String suffix = props.getProperty("jenkinSuffix");
			String jenkinsProjectDetails=props.getProperty("jenkinsProjectDetails");	
			try{
				
				String actualUrl=url+"/job/"+projName+suffix+jenkinsProjectDetails;
				logger.info("Requesting to jenkins to get project details using URL: " + actualUrl);
				 URL urlObject = new URL(url+"/job/"+projName+suffix+jenkinsProjectDetails);
				//URL urlObject = new URL("http://192.168.137.101:8080/job/Devops-Dashboard-Tomcat/api/json?tree=builds[number,status,timestamp,id,result]");
				    	//REQUEST TO GET DETAILS FROM JENKINS AND CHECKING CREDENTIALS
				 HttpURLConnection con = (HttpURLConnection) urlObject.openConnection();
				    	con.setRequestMethod("GET");
				    	String authString = username + ":" + password;
				    	String authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes());

				    	con.setRequestProperty("Authorization", "Basic " + authStringEnc);
				BufferedReader in = new BufferedReader(
			      		  new InputStreamReader(con.getInputStream()));
				 StringBuffer response = new StringBuffer();
				 String inputLine;
			     while ((inputLine = in.readLine()) != null) {
			     	response.append(inputLine);
			     	
			     } 
			     logger.info("Data Received:" + response);
               in.close();		
	      JSONParser parser = new JSONParser(); 
	      JSONObject jobj = (JSONObject)parser.parse(response.toString());
	     logger.info("Json Object is " + jobj);
	      JSONArray arr = (JSONArray)jobj.get("allBuilds");
	      logger.debug("Getting the job details from jenkins");
	      for(int i=0;i<arr.size();i++) 
	      {
	    	  	jenkinsJobPair = new JSONObject();
	  	    	JSONObject jobj1 =(JSONObject)arr.get(i);
	  	    	try {
	  	    	Instant instant = Instant.ofEpochMilli((long)jobj1.get("timestamp"));
	  	    	 TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	  	    	jenkinsJobPair.put("buildTime", jobj1.get("timestamp").toString());
	      		jenkinsJobPair.put("buildTimeformated", instant.toString());
	      		jenkinsJobPair.put("buildId", jobj1.get("id").toString());
	      		jenkinsJobPair.put("result", jobj1.get("result").toString());
	      		jenkinsJobPair.put("duration", jobj1.get("duration").toString());
	      		jenkinsJobPair.put("estimatedDuration", jobj1.get("estimatedDuration").toString());
	      		array.add(jenkinsJobPair);
	  	    	}catch(NullPointerException e){
	  	    		continue;
	  	    	}
	      		//logger.info("Time got is:" + time);
	      		//logger.info("Build ID"+id);
	      		//String JobName = jenkinsJobPair.get("name").toString();
	      		//logger.debug(JobName);
	   
			}
	      con.disconnect();
		}
			
	      catch(Exception e){
	    	  e.printStackTrace();
				logger.error("There was Problems while getting the details from jenkins");
				jenkinsJobPair.put("Status", "Error getting the Jobs");
			
				   return Response.status(500).entity(jenkinsJobPair).build();
			}
		
			logger.info("All the functions worked properly, exiting the function with an array object");
			return Response.status(200).entity(array).build();	
		}
		
		
		//SERVICE TO GET JUNIT TEST RESULTS OF A PROJECT FROM JENKINS
		@SuppressWarnings("unchecked")
		@GET
		@Path("/JUnit_details")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getJUnitDetails(@QueryParam("projectname") String projName,@QueryParam("name") String username,
				   @QueryParam("password") String password) {
			BasicConfigurator.configure();
			logger.info("Starting the getprojects function");
			PropertyUtility props=PropertyUtility.getPropertyUtility();
			
			JSONObject jenkinsJobPair = new JSONObject();
			JSONArray array = new JSONArray();

			String url=props.getProperty("jenkins_url");
			String suffix = props.getProperty("jenkinSuffix");
			String JUnit=props.getProperty("JUnit");	
			try{
				String actualUrl=url+"/job/"+projName+suffix+JUnit;
				logger.info("Requesting to jenkins to get project details of JUnit testing  " );
				 URL urlObject = new URL(url+"/job/"+projName+"/"+suffix+JUnit);
				//URL urlObject = new URL("http://192.168.137.101:8080/job/Devops-Dashboard-Tomcat/api/json?tree=builds[number,status,timestamp,id,result]");
				 //ESTABLISHING CONNECTION TO GET JUNIT RESULTS FROM JENKINS TO GET TEST RESULTS LIKE PASSED,SKIPPED AND FAILED TEST COUNT
				    	HttpURLConnection con = (HttpURLConnection) urlObject.openConnection();
				    	con.setRequestMethod("GET");
				    	String authString = username + ":" + password;
				    	String authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes());

				    	con.setRequestProperty("Authorization", "Basic " + authStringEnc);
				BufferedReader in = new BufferedReader(
			      		  new InputStreamReader(con.getInputStream()));
				 StringBuffer response = new StringBuffer();
				 String inputLine;
			     while ((inputLine = in.readLine()) != null) {
			     	response.append(inputLine);
			     	
			     } 
			     logger.info("Data Received:" + response);
               in.close();		
	      JSONParser parser = new JSONParser(); 
	      JSONObject jobj = (JSONObject)parser.parse(response.toString());
	      JSONArray arr = (JSONArray)jobj.get("allBuilds");
	      logger.debug("Getting the job details from JUnit");
	      for(int i=0;i<arr.size();i++) 
	      {
	    	  	jenkinsJobPair = new JSONObject();
	  	    	JSONObject jobj1 =(JSONObject)arr.get(i);
	  	    	jenkinsJobPair.put("buildnumber", jobj1.get("number").toString());
	  	    	
	  	    	JSONArray arr1=(JSONArray)jobj1.get("actions");
	  	    	logger.info("jobj1:"+jobj1);
	  	    	logger.info("arr1:"+arr1);
	  	    	for (int j = 0; j < arr1.size(); j++) {
	  	    		JSONObject jobj2 =(JSONObject)arr1.get(j);
	  	    		/*if(jobj2.get("totalCount")==null) {
	  	    			jenkinsJobPair.put("totalcount","0");
	  	    			jenkinsJobPair.put("skipcount","0");
	  		      		jenkinsJobPair.put("failcount", "0");
	  	    		}*/
	  	    		try {
	  		      		jenkinsJobPair.put("totalcount", jobj2.get("totalCount").toString());
	  		      		jenkinsJobPair.put("skipcount", jobj2.get("skipCount").toString());
	  		      		jenkinsJobPair.put("failcount", jobj2.get("failCount").toString());
	  		      	array.add(jenkinsJobPair);
	  		      		logger.info("Array response:"+array);
	  		  	    }catch(Exception e) {
	  		  	    	continue;
	  		  	    }	
				}
	  	    	
	  	    
	      }
	      con.disconnect();
		}
			
	      catch(Exception e){
	    	  e.printStackTrace();
				logger.error("There was Problems while getting the details from JUnit");
				jenkinsJobPair.put("Status", "Error getting the Jobs");
				   return Response.status(500).entity(jenkinsJobPair).build();
			}
			logger.info("All the functions worked properly, exiting the function with an array object");
			return Response.status(200).entity(array).build();	
		}	

		
		//SERVICE TO GET Staging information of PROJECT FROM JENKINS
				@SuppressWarnings("unchecked")
				@GET
				@Path("/Staging_info")
				@Produces(MediaType.APPLICATION_JSON)
				public Response getStagingInfo(@QueryParam("projectname") String projName,@QueryParam("name") String username,
						   @QueryParam("password") String password) {
					BasicConfigurator.configure();
					logger.info("Starting the getprojects function");
					PropertyUtility props=PropertyUtility.getPropertyUtility();
					
					JSONObject jenkinsJobPair = new JSONObject();
					JSONArray array = new JSONArray();

					String url=props.getProperty("jenkins_url");
					String suffix = props.getProperty("stageSuffixe");
					try{
						String actualUrl=url+"/job/"+projName+suffix;
						logger.info("Requesting to jenkins to get project details of JUnit testing ");
						 URL urlObject = new URL(url+"/job/"+projName+suffix);
						 //ESTABLISHING CONNECTION TO GET JUNIT RESULTS FROM JENKINS TO GET TEST RESULTS LIKE PASSED,SKIPPED AND FAILED TEST COUNT
						    	HttpURLConnection con = (HttpURLConnection) urlObject.openConnection();
						    	con.setRequestMethod("GET");
						    	String authString = username + ":" + password;
						    	String authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes());

						    	con.setRequestProperty("Authorization", "Basic " + authStringEnc);
						BufferedReader in = new BufferedReader(
					      		  new InputStreamReader(con.getInputStream()));
						 StringBuffer response = new StringBuffer();
						 String inputLine;
					     while ((inputLine = in.readLine()) != null) {
					     	response.append(inputLine);
					     	
					     } 
					     logger.info("Data Received:" + response);
		             in.close();		
			      JSONParser parser = new JSONParser(); 
			      JSONArray jobj = (JSONArray)parser.parse(response.toString());
			     logger.info("Json array is " + jobj);
			      logger.debug("Getting the job details from JUnit");
			      for(int i=0;i<jobj.size();i++) 
			      {
			    	  JSONObject nodesinfo=new JSONObject();
			  	    	JSONObject jobj1 =(JSONObject)jobj.get(i);
			  	    	JSONArray stage=(JSONArray)jobj1.get("stages");
			  	    	logger.info("jobj1:"+jobj1);
			  	    	logger.info("stage:"+stage);
			  	    	if(stage!=null) {
			  	    		
			  	    		System.out.println(stage.size());
			  	    	for (int j = 0; j <stage.size(); j++) {
			  	    		try {
			  	    			//JSONObject nodesinfo=new JSONObject();
			  	    			JSONObject jobj2 =(JSONObject)stage.get(j);
			  	    			nodesinfo.put("buildNumber", jobj1.get("id"));
			  	    			nodesinfo.put("overallStatus", jobj1.get("status"));
			  	    			if(jobj2.get("name")!=null) {
			  	    			if(jobj2.get("name").toString().equalsIgnoreCase("Code Build")) {
			  	    			nodesinfo.put("buildName", jobj2.get("name").toString());
			  	    			nodesinfo.put("buildstatus", jobj2.get("status").toString());
			  	    			nodesinfo.put("buildduration", jobj2.get("durationMillis").toString());
			  	    			nodesinfo.put("buildpauseDuration", jobj2.get("pauseDurationMillis").toString());
			  	    			System.out.println(jobj2.get("name")+"::"+jobj2.get("status"));
					      		//array.add(nodesinfo);
			  	    			}else if(jobj2.get("name").toString().equalsIgnoreCase("JUnit Report")) {
			  	    				nodesinfo.put("junitName", jobj2.get("name").toString());
				  	    			nodesinfo.put("junitstatus", jobj2.get("status").toString());
				  	    			nodesinfo.put("junitduration", jobj2.get("durationMillis").toString());
				  	    			nodesinfo.put("junitpauseDuration", jobj2.get("pauseDurationMillis").toString());
				  	    			System.out.println(jobj2.get("name")+"::"+jobj2.get("status"));
						      		//array.add(nodesinfo);
			  	    			}else if(jobj2.get("name").toString().equalsIgnoreCase("SonarQube Analysis")) {
			  	    				nodesinfo.put("sonarName", jobj2.get("name").toString());
				  	    			nodesinfo.put("sonarstatus", jobj2.get("status").toString());
				  	    			nodesinfo.put("sonarduration", jobj2.get("durationMillis").toString());
				  	    			nodesinfo.put("sonarpauseDuration", jobj2.get("pauseDurationMillis").toString());
				  	    			System.out.println(jobj2.get("name")+"::"+jobj2.get("status"));
						      		//array.add(nodesinfo);
			  	    			}else if(jobj2.get("name").toString().equalsIgnoreCase("Deploy to Testing Environment")) {
			  	    				nodesinfo.put("testName", jobj2.get("name").toString());
				  	    			nodesinfo.put("teststatus", jobj2.get("status").toString());
				  	    			nodesinfo.put("testduration", jobj2.get("durationMillis").toString());
				  	    			nodesinfo.put("testpauseDuration", jobj2.get("pauseDurationMillis").toString());
				  	    			System.out.println(jobj2.get("name")+"::"+jobj2.get("status"));
						      		//array.add(nodesinfo);
			  	    			}else if(jobj2.get("name").toString().equalsIgnoreCase("Deploy to Staging Environment")) {
			  	    				nodesinfo.put("stageName", jobj2.get("name").toString());
				  	    			nodesinfo.put("stagestatus", jobj2.get("status").toString());
				  	    			nodesinfo.put("stageduration", jobj2.get("durationMillis").toString());
				  	    			nodesinfo.put("stagepauseDuration", jobj2.get("pauseDurationMillis").toString());
				  	    			System.out.println(jobj2.get("name")+"::"+jobj2.get("status"));
						      		//array.add(nodesinfo);
			  	    			}else if(jobj2.get("name").toString().equalsIgnoreCase("Deploy to Production Environment")) {
			  	    				nodesinfo.put("prodName", jobj2.get("name").toString());
				  	    			nodesinfo.put("prodstatus", jobj2.get("status").toString());
				  	    			nodesinfo.put("prodduration", jobj2.get("durationMillis").toString());
				  	    			nodesinfo.put("prodpauseDuration", jobj2.get("pauseDurationMillis").toString());
				  	    			System.out.println(jobj2.get("name")+"::"+jobj2.get("status"));
						      		//array.add(nodesinfo);
			  	    			}
			  	    		}
					      		logger.info("Array response:"+array);
			  	    		}catch(Exception e) {
					  	    	continue;
					  	    }
			  	    		
						}
			  	    	array.add(nodesinfo);
			  	    }
			  	    	//array.add(nodesinfo);
			      }
			      con.disconnect();
				}
					
			      catch(Exception e){
			    	  e.printStackTrace();
						logger.error("There was Problems while getting the details from JUnit");
						jenkinsJobPair.put("Status", "Error getting the Jobs");
					
						   return Response.status(500).entity(jenkinsJobPair).build();
					}
				
					logger.info("All the functions worked properly, exiting the function with an array object");
					return Response.status(200).entity(array).build();	
				}	
				

				@GET 
				   @Path("/update")
				   @Produces(MediaType.APPLICATION_JSON)
				   public Response updateJob(@QueryParam("oldJobName") String oldJobName, @QueryParam("newJobName") String newJobName
						   ) 
						   throws Exception{
					try{
					oldJobName = URLEncoder.encode(oldJobName).replaceAll("\\+", "%20");
					newJobName = URLEncoder.encode(newJobName).replaceAll("\\+", "%20");
					System.out.println(newJobName);
//					 authString = username + ":" + password;
//					   System.out.println("Base64 encoded auth string: " + authString);
//					   authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes());
//					   System.out.println("Base64 encoded auth string: " + authStringEnc);
//					   
				  PropertyUtility props=PropertyUtility.getPropertyUtility();
							   String jenkinsURL = props.getProperty("jenkins_url");
//				   obj = new URL(jenkinsURL+"/crumbIssuer/api/json");
//				   con = (HttpURLConnection) obj.openConnection();
//				   con.setRequestProperty  ("Authorization", "Basic " + message.get("auth"));
//				   con.setRequestMethod("GET");
//				   readResponse();
//			String JenkinsCrumb = jobj.get("crumbRequestField").toString();
//			String crumbID= jobj.get("crumb").toString();
//			System.out.println(JenkinsCrumb);
//			System.out.println(crumbID);
							 
							getJenskinsCrumb();
			System.out.println("Jenkins update:"+jenkinsURL+"/job/"+oldJobName+"/doRename/?newName="+newJobName);
			obj = new URL(jenkinsURL+"/job/"+oldJobName+"/doRename/?newName="+newJobName);
			con = (HttpURLConnection) obj.openConnection();
			con.setRequestProperty  ("Authorization", "Basic " +  message.get("auth"));
			con.setRequestMethod("POST");
//			con.setDoOutput(true);
			con.setRequestProperty(message.get("JenkinsCrumb"), message.get("crumbID"));

			System.out.println("After connection");
			con.disconnect();
					}catch(Exception e){
						e.printStackTrace();
						return Response.status(con.getResponseCode()).build();
					}
					
					return Response.status(con.getResponseCode()).build();
			}
	
				
				
	public static void getJenskinsCrumb() throws Exception{
		
//		 authString = username + ":" + password;
//		   System.out.println("Base64 encoded auth string: " + authString);
//		   authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes());
//		   System.out.println("Base64 encoded auth string: " + authStringEnc);
//		   
		
	  PropertyUtility props=PropertyUtility.getPropertyUtility();
				   String jenkinsURL = props.getProperty("jenkins_url");
				   
	   obj = new URL(jenkinsURL+"/crumbIssuer/api/json");
	   con = (HttpURLConnection) obj.openConnection();
	   con.setRequestProperty  ("Authorization", "Basic " + message.get("auth"));
	   con.setRequestMethod("GET");

	   readResponse();
	   message.put("JenkinsCrumb", jobj.get("crumbRequestField").toString());
	   message.put("crumbID", jobj.get("crumb").toString());
	   System.out.println(jobj.get("crumbRequestField").toString());
	   System.out.println(jobj.get("crumb").toString());
	   
	}
	
	@GET
	@Path("/token")
	@Consumes(MediaType.TEXT_HTML)
	public static String getToken() throws Exception{
		HttpURLConnection con = null;
		PropertyUtility props=PropertyUtility.getPropertyUtility();
			String jenkinsURL= props.getProperty("jenkins_url");
			JSONObject jiraObjects = new JSONObject();
			JSONArray array = new JSONArray();
			JSONObject jenkinsBuildpair = new JSONObject();
			JSONArray array2 = new JSONArray();
			String token = null;
		
				try{
				        URL urlObject = new URL(jenkinsURL+"/me/configure");
				    	con = (HttpURLConnection) urlObject.openConnection();
				    	String name = (String) ControllerServlet.session.getAttribute("username");				
				        String password = (String) ControllerServlet.session.getAttribute("userpassword");			
				        String authString = name + ":" + password;	
				        String authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes());	
				        con.setRequestMethod("GET");
				        con.setDoOutput(true);
				        con.setRequestProperty  ("Authorization", "Basic " + authStringEnc);	
				   
				BufferedReader in = new BufferedReader(
			      		  new InputStreamReader(con.getInputStream()));	        
			      String inputLine;
			      StringBuffer content = new StringBuffer();		      
			      while ((inputLine = in.readLine()) != null) {
			     	    content.append(inputLine);
			      }	
			      
	    org.jsoup.nodes.Document html = (org.jsoup.nodes.Document) Jsoup.parse(content.toString());
	   token= html.getElementById("apiToken").attr("value");
	   
	     con.disconnect();
			}catch(Exception e){
				e.printStackTrace();
			}
		
		return token;
	}
	
	@SuppressWarnings("unchecked")
	@GET
	@Path("/testReport")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTestReport(@QueryParam("projectname") String projName,@QueryParam("name") String username,
			   @QueryParam("password") String password) {
		BasicConfigurator.configure();
		logger.info("Starting the getprojects function");
		PropertyUtility props=PropertyUtility.getPropertyUtility();
		
		JSONObject jenkinsJobPair = new JSONObject();
		JSONArray array = new JSONArray();

		String url=props.getProperty("jenkins_url");
		String suffix = props.getProperty("jenkinSuffix");
		String testReport = props.getProperty("testReport");
		try{
			String actualUrl=url+"/job/"+projName+testReport+suffix;
			logger.info("Requesting to jenkins to get project details of JUnit testing using URL: " + actualUrl);
			 URL urlObject = new URL(actualUrl);
			 //ESTABLISHING CONNECTION TO GET JUNIT RESULTS FROM JENKINS TO GET TEST RESULTS LIKE PASSED,SKIPPED AND FAILED TEST COUNT
			    	HttpURLConnection con = (HttpURLConnection) urlObject.openConnection();
			    	con.setRequestMethod("GET");
			    	String authString = username + ":" + password;
			    	String authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes());

			    	con.setRequestProperty("Authorization", "Basic " + authStringEnc);
			BufferedReader in = new BufferedReader(
		      		  new InputStreamReader(con.getInputStream()));
			 StringBuffer response = new StringBuffer();
			 String inputLine;
		     while ((inputLine = in.readLine()) != null) {
		     	response.append(inputLine);
		     	
		     } 
		     logger.info("Data Received:" + response);
         in.close();		
      JSONParser parser = new JSONParser(); 
      JSONObject jobjo=(JSONObject) parser.parse(response.toString());
      logger.info("Json array is " + jobjo);
      JSONArray jobj = (JSONArray) jobjo.get("suites");
      logger.info("Json array is " + jobj);
      for (int i = 0; i < jobj.size(); i++) {
		JSONObject jobj1=(JSONObject) jobj.get(i);
		logger.info("Json array is " + jobj1);
		JSONArray jobj2=(JSONArray) jobj1.get("cases");
		logger.info("Json array is " + jobj2);
		for (int j = 0; j < jobj2.size(); j++) {
			JSONObject jobj3=(JSONObject) jobj2.get(j);
			logger.info("Json array is " + jobj3);
			array.add(jobj3);
		}
	}
      logger.debug("Getting the job details from JUnit");
      con.disconnect();
	}
		
      catch(Exception e){
    	  e.printStackTrace();
			logger.error("There was Problems while getting the details from JUnit");
			jenkinsJobPair.put("Status", "Error getting the Jobs");
		
			   return Response.status(500).entity(jenkinsJobPair).build();
		}
	
		logger.info("All the functions worked properly, exiting the function with an array object");
		return Response.status(200).entity(array).build();	
	}	
	
}	
