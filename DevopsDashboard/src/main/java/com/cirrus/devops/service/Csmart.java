package com.cirrus.devops.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;
import java.util.TimeZone;

import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.cirrus.devops.util.PropertyUtility;
@Path("/csmart") 
public class Csmart {
	static final Logger logger = Logger.getLogger(Csmart.class);
	private String username;
	private String password;
	private String csmartUrl;
	private String csmartAuth;
	private String authToken;
	private String enrol;
	private String reports;
	private String audit;
	private String hostname;
	private String assetId;
	private String remidiat;
	private String auditId;
	private String remidiatDet;
	private String authString;
	String url;
	String url1;
	JSONArray array = new JSONArray();
	JSONObject jsonObject=new JSONObject();
	@POST 
	@Path("/csmartToken")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSecurityDetails(InputStream json){
		init();
		logger.info("get security function started");
		HashMap<String,String> pair = new HashMap<String,String>() ;
		
		/*Base64.Encoder encoder = Base64.getMimeEncoder();
		String user="cdlocal";
		String userencode=encoder.encodeToString(user.getBytes());
		String pass="VMwar3!!";
		String passencode=encoder.encodeToString(pass.getBytes());
		System.out.println(userencode+"::"+passencode);
		String userpass=userencode+"!q4Ev"+passencode;
		String auth=encoder.encodeToString(userpass.getBytes());
		System.out.println(auth);*/
		Base64.Decoder decoder = Base64.getMimeDecoder();
		String usepassDecod=new String(decoder.decode(authString));
		String[] arrOfStr = usepassDecod.split("!q4Ev");
		String userdecod=new String(decoder.decode(arrOfStr[arrOfStr.length-2]));
		String passdecod=new String(decoder.decode(arrOfStr[arrOfStr.length-1]));
		this.username=userdecod;
		this.password=passdecod;
		try{
			BufferedReader br;
			StringBuffer sb;
			sb= new StringBuffer();
			br = new BufferedReader(new InputStreamReader(json));
			String line;
			while((line=br.readLine())!=null){
				sb.append(line);
			}
			String payload="{\"username\":\""+username+"\",\"password\":\""+password+"\"}";
			System.out.println(payload);
			url=csmartUrl+csmartAuth;
			HttpURLConnection con = (HttpURLConnection) ((new URL(url).openConnection()));
			System.out.println("projectUrl"+con.getURL());
			 con.setRequestMethod("POST");
			con.setDoOutput(true);
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Accept", "application/json");
		 System.out.println(url);
		 OutputStreamWriter output=new OutputStreamWriter(con.getOutputStream());
		 System.out.println(payload);
		 output.write(payload);
		 output.flush();
		 output.close();
		 con.connect();
		 
		 System.out.println(con.getResponseCode());
     BufferedReader in = new BufferedReader(
   		  new InputStreamReader(con.getInputStream()));
     logger.debug("Connection was executed");
   String inputLine;
   StringBuffer content = new StringBuffer();
   
   System.out.println("in !! "+in);
   while ((inputLine = in.readLine()) != null) {
 	  System.out.println("inputLine !! "+inputLine);
 	  logger.debug("inputLine !! "+inputLine);
  	    content.append(inputLine);
   }
   
 
   JSONParser parser = new JSONParser(); 
   JSONObject jobj = (JSONObject)parser.parse(content.toString());
   authToken=jobj.get("token").toString();
   
   url1=csmartUrl+enrol;
	HttpURLConnection con1 = (HttpURLConnection) ((new URL(url1).openConnection()));
	System.out.println("projectUrl"+con1.getURL());
	 con1.setRequestMethod("POST");
	con1.setDoOutput(true);
	con1.setRequestProperty("Content-Type", "application/json");
	con1.setRequestProperty("Accept", "application/json");
	System.out.println("Bearer "+authToken);
	con1.setRequestProperty("Authorization", "Bearer "+authToken);
System.out.println(url1);
OutputStreamWriter output1=new OutputStreamWriter(con1.getOutputStream());
System.out.println(sb.toString());
output1.write(sb.toString());
output1.flush();
output1.close();
con1.connect();
System.out.println(con1.getResponseCode());
BufferedReader in1 = new BufferedReader(new InputStreamReader(con1.getInputStream()));
logger.debug("Connection was executed");
String inputLine1;
StringBuffer content1 = new StringBuffer();

System.out.println("in1 !! "+in1);
while ((inputLine1 = in1.readLine()) != null) {
  System.out.println("inputLine1 !! "+inputLine1);
  logger.debug("inputLine1 !! "+inputLine1);
	    content1.append(inputLine1);
}

JSONParser parser1 = new JSONParser(); 
jsonObject= (JSONObject)parser1.parse(content1.toString());

   con.disconnect();
		}catch(Exception e){
			e.printStackTrace();
			logger.error("There was some error with the collection");
			pair.put("Status", "Error gettign the token");
			   return Response.status(500).entity(pair).build();
		}
		System.out.println("response 200 sent");
		return Response.status(200).entity(jsonObject).build();
		
	}
	
	public void init(){
		
		   BasicConfigurator.configure();
		   PropertyUtility props = PropertyUtility.getPropertyUtility();
		   //this.username = props.getProperty("csmartUser");
		   //this.password=props.getProperty("csmartPass");
		   this.authString=props.getProperty("csmart");
		   this.csmartUrl=props.getProperty("csmartUrl");
		   this.csmartAuth=props.getProperty("csmartAuth");
		   this.enrol=props.getProperty("enrol");
		   this.reports=props.getProperty("reports");
		   this.audit=props.getProperty("audit");
		   this.hostname=props.getProperty("hostname");
		   this.remidiat=props.getProperty("remidiat");
		   this.remidiatDet=props.getProperty("remidiatDet");
		   
	   }
	
	@SuppressWarnings("unchecked")
	@GET
	   @Path("/csmartassets")
	   @Produces(MediaType.APPLICATION_JSON)
	   public Response getAssetsDetails(){
		init();
		logger.info("get security function started");
		HashMap<String,String> pair = new HashMap<String,String>() ;
		Base64.Decoder decoder = Base64.getMimeDecoder();
		String usepassDecod=new String(decoder.decode(authString));
		 String[] arrOfStr = usepassDecod.split("!q4Ev");
		String userdecod=new String(decoder.decode(arrOfStr[arrOfStr.length-2]));
		String passdecod=new String(decoder.decode(arrOfStr[arrOfStr.length-1]));
		this.username=userdecod;
		this.password=passdecod;
		
		try{
			String payload="{\"username\":\""+username+"\",\"password\":\""+password+"\"}";
			System.out.println(payload);
					url=csmartUrl+csmartAuth;
						HttpURLConnection con = (HttpURLConnection) ((new URL(url).openConnection()));
						System.out.println("projectUrl"+con.getURL());
						 con.setRequestMethod("POST");
						con.setDoOutput(true);
						con.setRequestProperty("Content-Type", "application/json");
						con.setRequestProperty("Accept", "application/json");
					 System.out.println(url);
					 OutputStreamWriter output=new OutputStreamWriter(con.getOutputStream());
					 System.out.println(payload);
					 output.write(payload);
					 output.flush();
					 output.close();
					 con.connect();
					 
					 System.out.println(con.getResponseCode());
			     BufferedReader in = new BufferedReader(
			   		  new InputStreamReader(con.getInputStream()));
			     logger.debug("Connection was executed");
			   String inputLine;
			   StringBuffer content = new StringBuffer();
			   
			   System.out.println("in !! "+in);
			   while ((inputLine = in.readLine()) != null) {
			 	  System.out.println("inputLine !! "+inputLine);
			 	  logger.debug("inputLine !! "+inputLine);
			  	    content.append(inputLine);
			   }
			   
 
   JSONParser parser = new JSONParser(); 
   JSONObject jobj = (JSONObject)parser.parse(content.toString());
   authToken=jobj.get("token").toString();
   
   url1=csmartUrl+enrol;
  	HttpURLConnection con1 = (HttpURLConnection) ((new URL(url1).openConnection()));
  	System.out.println("projectUrl"+con1.getURL());
  	 con1.setRequestMethod("GET");
  	con1.setDoOutput(true);
  	con1.setRequestProperty("Content-Type", "application/json");
  	con1.setRequestProperty("Accept", "application/json");
  	System.out.println("Bearer "+authToken);
  	con1.setRequestProperty("Authorization", "Bearer "+authToken);
  System.out.println(url1);
  con1.connect();
  System.out.println(con1.getResponseCode());
  BufferedReader in1 = new BufferedReader(new InputStreamReader(con1.getInputStream()));
  logger.debug("Connection was executed");
  String inputLine1;
  StringBuffer content1 = new StringBuffer();

  System.out.println("in1 !! "+in1);
  while ((inputLine1 = in1.readLine()) != null) {
    System.out.println("inputLine1 !! "+inputLine1);
    logger.debug("inputLine1 !! "+inputLine1);
  	    content1.append(inputLine1);
  }

  JSONParser parser1 = new JSONParser(); 
 JSONArray jobjj= (JSONArray)parser1.parse(content1.toString());
   
   
   for(int i=0;i<jobjj.size();i++) 
   {
 	  	pair = new HashMap<String,String>();
	    	JSONObject jobj1 =(JSONObject)jobjj.get(i);
	    	System.out.println(jobj1.toJSONString());
	    	if(jobj1.get("id")!=null) {
   		pair.put("id", jobj1.get("id").toString());
	    	}
	    	if(jobj1.get("ip")!=null) {
   		pair.put("ip", jobj1.get("ip").toString());
	    	}
   		JSONArray policies=(JSONArray)jobj1.get("policies");
   		if(policies!=null) {
   		for (int j = 0; j < policies.size(); j++) {
   			if(jobj1.get(j)!=null) {
			pair.put("policies"+j, policies.get(j).toString());
   			}
		}
   		}
   		JSONArray groups=(JSONArray)jobj1.get("groups");
   		if(groups!=null) {
   	   		for (int j = 0; j < groups.size(); j++) {
   	   			if(jobj1.get(j)!=null) {
   				pair.put("groups"+j, groups.get(j).toString());
   	   			}
   			}
   	   		}
   		if(jobj1.get("hostname")!=null) {
   		pair.put("hostname", jobj1.get("hostname").toString());
   		}if(jobj1.get("username")!=null) {
   		pair.put("username", jobj1.get("username").toString());
   		}if(jobj1.get("online")!=null) {
   			if(jobj1.get("online").toString().equalsIgnoreCase("true")) {
   				pair.put("online", "Online");
   			}else {
   				pair.put("online", "Offline");
   			}
   		
   		}if(jobj1.get("os")!=null) {
   		pair.put("os", jobj1.get("os").toString());
   		}if(jobj1.get("compliance")!=null) {
   		pair.put("compliance", jobj1.get("compliance").toString());
   		}if(jobj1.get("lastChecked")!=null) {
   		pair.put("lastChecked", jobj1.get("lastChecked").toString());
   		}
   		array.add(pair);
   		logger.debug("Getting the Details");
   		System.out.println("Response Array:"+array);
   }
   
   con.disconnect();
		}catch(Exception e){
			e.printStackTrace();
			logger.error("There was some error with the collection");
			pair.put("Status", "Error gettign the token");
			   return Response.status(500).entity(pair).build();
		}
		System.out.println("response 200 sent");
		return Response.status(200).entity(array).build();
		
	}
	
	
	@GET
	   @Path("/hostname")
	   @Produces(MediaType.APPLICATION_JSON)
	   public Response getHostname(@NotNull @QueryParam("ip") String ipaddress){
		init();
		JSONObject jobjj;
		logger.info("get security function started");
		HashMap<String,String> pair = new HashMap<String,String>() ;
		Base64.Decoder decoder = Base64.getMimeDecoder();
		String usepassDecod=new String(decoder.decode(authString));
		 String[] arrOfStr = usepassDecod.split("!q4Ev");
		String userdecod=new String(decoder.decode(arrOfStr[arrOfStr.length-2]));
		String passdecod=new String(decoder.decode(arrOfStr[arrOfStr.length-1]));
		this.username=userdecod;
		this.password=passdecod;
		
		try{
			String payload="{\"username\":\""+username+"\",\"password\":\""+password+"\"}";
			System.out.println(payload);
					url=csmartUrl+csmartAuth;
						HttpURLConnection con = (HttpURLConnection) ((new URL(url).openConnection()));
						System.out.println("projectUrl"+con.getURL());
						 con.setRequestMethod("POST");
						con.setDoOutput(true);
						con.setRequestProperty("Content-Type", "application/json");
						con.setRequestProperty("Accept", "application/json");
					 System.out.println(url);
					 OutputStreamWriter output=new OutputStreamWriter(con.getOutputStream());
					 System.out.println(payload);
					 output.write(payload);
					 output.flush();
					 output.close();
					 con.connect();
					 
					 System.out.println(con.getResponseCode());
			     BufferedReader in = new BufferedReader(
			   		  new InputStreamReader(con.getInputStream()));
			     logger.debug("Connection was executed");
			   String inputLine;
			   StringBuffer content = new StringBuffer();
			   
			   System.out.println("in !! "+in);
			   while ((inputLine = in.readLine()) != null) {
			 	  System.out.println("inputLine !! "+inputLine);
			 	  logger.debug("inputLine !! "+inputLine);
			  	    content.append(inputLine);
			   }
			   
 
   JSONParser parser = new JSONParser(); 
   JSONObject jobj = (JSONObject)parser.parse(content.toString());
   authToken=jobj.get("token").toString();
   
   url1=csmartUrl+hostname+ipaddress;
  	HttpURLConnection con1 = (HttpURLConnection) ((new URL(url1).openConnection()));
  	System.out.println("projectUrl"+con1.getURL());
  	 con1.setRequestMethod("GET");
  	con1.setDoOutput(true);
  	con1.setRequestProperty("Content-Type", "application/json");
  	con1.setRequestProperty("Accept", "application/json");
  	System.out.println("Bearer "+authToken);
  	con1.setRequestProperty("Authorization", "Bearer "+authToken);
  System.out.println(url1);
  con1.connect();
  System.out.println(con1.getResponseCode());
  BufferedReader in1 = new BufferedReader(new InputStreamReader(con1.getInputStream()));
  logger.debug("Connection was executed");
  String inputLine1;
  StringBuffer content1 = new StringBuffer();

  System.out.println("in1 !! "+in1);
  while ((inputLine1 = in1.readLine()) != null) {
    System.out.println("inputLine1 !! "+inputLine1);
    logger.debug("inputLine1 !! "+inputLine1);
  	    content1.append(inputLine1);
  }

  JSONParser parser1 = new JSONParser(); 
 jobjj= (JSONObject)parser1.parse(content1.toString());
   con.disconnect();
		}catch(Exception e){
			e.printStackTrace();
			logger.error("There was some error with the collection");
			pair.put("Status", "Error gettign the token");
			   return Response.status(500).entity(pair).build();
		}
		System.out.println("response 200 sent");
		return Response.status(200).entity(jobjj).build();
		
	}
	
	
	@SuppressWarnings({ "unchecked" })
	@GET
	   @Path("/csmartreports")
	   @Produces(MediaType.APPLICATION_JSON)
	   public Response getAuditDetails(){
		init();
		logger.info("get security function started");
		HashMap<String,String> pair = new HashMap<String,String>() ;
		
		Base64.Decoder decoder = Base64.getMimeDecoder();
		String usepassDecod=new String(decoder.decode(authString));
		 String[] arrOfStr = usepassDecod.split("!q4Ev");
		String userdecod=new String(decoder.decode(arrOfStr[arrOfStr.length-2]));
		String passdecod=new String(decoder.decode(arrOfStr[arrOfStr.length-1]));
		this.username=userdecod;
		this.password=passdecod;
		try{
			String payload="{\"username\":\""+username+"\",\"password\":\""+password+"\"}";
			System.out.println(payload);
					url=csmartUrl+csmartAuth;
						HttpURLConnection con = (HttpURLConnection) ((new URL(url).openConnection()));
						System.out.println("projectUrl"+con.getURL());
						 con.setRequestMethod("POST");
						con.setDoOutput(true);
						con.setRequestProperty("Content-Type", "application/json");
						con.setRequestProperty("Accept", "application/json");
					 System.out.println(url);
					 OutputStreamWriter output=new OutputStreamWriter(con.getOutputStream());
					 System.out.println(payload);
					 output.write(payload);
					 output.flush();
					 output.close();
					 con.connect();
					 
					 System.out.println(con.getResponseCode());
			     BufferedReader in = new BufferedReader(
			   		  new InputStreamReader(con.getInputStream()));
			     logger.debug("Connection was executed");
			   String inputLine;
			   StringBuffer content = new StringBuffer();
			   
			   System.out.println("in !! "+in);
			   while ((inputLine = in.readLine()) != null) {
			 	  System.out.println("inputLine !! "+inputLine);
			 	  logger.debug("inputLine !! "+inputLine);
			  	    content.append(inputLine);
			   }
			   
 
   JSONParser parser = new JSONParser(); 
   JSONObject jobj = (JSONObject)parser.parse(content.toString());
   authToken=jobj.get("token").toString();
   
   url1=csmartUrl+reports;
  	HttpURLConnection con1 = (HttpURLConnection) ((new URL(url1).openConnection()));
  	System.out.println("projectUrl"+con1.getURL());
  	 con1.setRequestMethod("GET");
  	con1.setDoOutput(true);
  	con1.setRequestProperty("Content-Type", "application/json");
  	con1.setRequestProperty("Accept", "application/json");
  	System.out.println("Bearer "+authToken);
  	con1.setRequestProperty("Authorization", "Bearer "+authToken);
  System.out.println(url1);
  con1.connect();
  System.out.println(con1.getResponseCode());
  BufferedReader in1 = new BufferedReader(new InputStreamReader(con1.getInputStream()));
  logger.debug("Connection was executed");
  String inputLine1;
  StringBuffer content1 = new StringBuffer();

  System.out.println("in1 !! "+in1);
  while ((inputLine1 = in1.readLine()) != null) {
    System.out.println("inputLine1 !! "+inputLine1);
    logger.debug("inputLine1 !! "+inputLine1);
  	    content1.append(inputLine1);
  }

  JSONParser parser1 = new JSONParser(); 
 JSONArray jobjj= (JSONArray)parser1.parse(content1.toString());
   
   System.out.println("audit details:"+jobjj);
   for(int i=0;i<jobjj.size();i++) 
   {
 	  	pair = new HashMap<String,String>();
	    	JSONObject jobj1 =(JSONObject)jobjj.get(i);
	    	System.out.println(jobj1.toJSONString());
	    	if(jobj1.get("id")!=null) {
   		pair.put("id", jobj1.get("id").toString());
	    	}
	    	if(jobj1.get("assetId")!=null) {
   		pair.put("assetId", jobj1.get("assetId").toString());
	    	}
   		if(jobj1.get("hostname")!=null) {
   		pair.put("hostname", jobj1.get("hostname").toString());
   		}if(jobj1.get("policy")!=null) {
   		pair.put("policy", jobj1.get("policy").toString());
   		}if(jobj1.get("status")!=null) {
   		pair.put("status", jobj1.get("status").toString());
   		}if(jobj1.get("os")!=null) {
   		pair.put("os", jobj1.get("os").toString());
   		}if(jobj1.get("startTime")!=null) {
   			
   			//Long gmtTime =1317951113613L; // 2.32pm NZDT
   			Calendar calendar = Calendar.getInstance();
   			calendar.setTime(new Date((long) jobj1.get("startTime")));
   			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

   			//Here you say to java the initial timezone. This is the secret
   			sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
   			//Will print in UTC
   			System.out.println(sdf.format(calendar.getTime()));    

   			//Here you set to your timezone
   			sdf.setTimeZone(TimeZone.getDefault());
   			//Will print on your default Timezone
   			
   			System.out.println(sdf.format(calendar.getTime()));
   		pair.put("startTime", sdf.format(calendar.getTime()));
   		}if(jobj1.get("endTime")!=null) {
   			Calendar calendar = Calendar.getInstance();
   			calendar.setTime(new Date((long) jobj1.get("endTime")));
   			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

   			//Here you say to java the initial timezone. This is the secret
   			sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
   			//Will print in UTC
   			System.out.println(sdf.format(calendar.getTime()));    

   			//Here you set to your timezone
   			sdf.setTimeZone(TimeZone.getDefault());
   			//Will print on your default Timezone
   			
   			System.out.println(sdf.format(calendar.getTime()));
   		pair.put("endTime",sdf.format(calendar.getTime()));
   		}if(jobj1.get("user")!=null) {
   	   		pair.put("user", jobj1.get("user").toString());
   	   		}if(jobj1.get("totalTasks")!=null) {
   	    		pair.put("totalTasks", jobj1.get("totalTasks").toString());
   	   		}if(jobj1.get("score")!=null) {
   	    		pair.put("score", jobj1.get("score").toString());
   	   		}if(jobj1.get("logFilePath")!=null) {
   	    		pair.put("logFilePath", jobj1.get("logFilePath").toString());
   	   		}
   		
   		array.add(pair);
   		logger.debug("Getting the Details");
   		System.out.println("Response Array:"+array);
   }
   
   con.disconnect();
		}catch(Exception e){
			e.printStackTrace();
			logger.error("There was some error with the collection");
			pair.put("Status", "Error gettign the token");
			   return Response.status(500).entity(pair).build();
		}
		System.out.println("response 200 sent");
		return Response.status(200).entity(array).build();
		
	}
	
	
	@POST 
	   @Path("/csmartAudit")
	   @Produces(MediaType.APPLICATION_JSON)
	   public Response doAudit(@NotNull @QueryParam("id") String assetIdl){
		init();
		JSONObject jsonobject1=new JSONObject();
		logger.info("get security function started");
		HashMap<String,String> pair = new HashMap<String,String>() ;
		
		Base64.Decoder decoder = Base64.getMimeDecoder();
		String usepassDecod=new String(decoder.decode(authString));
		 String[] arrOfStr = usepassDecod.split("!q4Ev");
		String userdecod=new String(decoder.decode(arrOfStr[arrOfStr.length-2]));
		String passdecod=new String(decoder.decode(arrOfStr[arrOfStr.length-1]));
		this.username=userdecod;
		this.password=passdecod;
		try{
	this.assetId=assetIdl;
	String payload="{\"username\":\""+username+"\",\"password\":\""+password+"\"}";
System.out.println(payload);
		url=csmartUrl+csmartAuth;
			HttpURLConnection con = (HttpURLConnection) ((new URL(url).openConnection()));
			System.out.println("projectUrl"+con.getURL());
			 con.setRequestMethod("POST");
			con.setDoOutput(true);
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Accept", "application/json");
		 System.out.println(url);
		 OutputStreamWriter output=new OutputStreamWriter(con.getOutputStream());
		 System.out.println(payload);
		 output.write(payload);
		 output.flush();
		 output.close();
		 con.connect();
		 
		 System.out.println(con.getResponseCode());
     BufferedReader in = new BufferedReader(
   		  new InputStreamReader(con.getInputStream()));
     logger.debug("Connection was executed");
   String inputLine;
   StringBuffer content = new StringBuffer();
   
   System.out.println("in !! "+in);
   while ((inputLine = in.readLine()) != null) {
 	  System.out.println("inputLine !! "+inputLine);
 	  logger.debug("inputLine !! "+inputLine);
  	    content.append(inputLine);
   }
   
 
   JSONParser parser = new JSONParser(); 
   JSONObject jobj = (JSONObject)parser.parse(content.toString());
   authToken=jobj.get("token").toString();
   String assetIdpayload="{\"assetId\":\""+assetId+"\"}"; 
   url1=csmartUrl+audit;
	HttpURLConnection con1 = (HttpURLConnection) ((new URL(url1).openConnection()));
	System.out.println("projectUrl"+con1.getURL());
	 con1.setRequestMethod("POST");
	con1.setDoOutput(true);
	con1.setRequestProperty("Content-Type", "application/json");
	con1.setRequestProperty("Accept", "application/json");
	System.out.println("Bearer "+authToken);
	con1.setRequestProperty("Authorization", "Bearer "+authToken);
System.out.println(url1);
OutputStreamWriter output1=new OutputStreamWriter(con1.getOutputStream());
System.out.println(assetIdpayload);
output1.write(assetIdpayload);
output1.flush();
output1.close();
con1.connect();
System.out.println(con1.getResponseCode());
BufferedReader in1 = new BufferedReader(new InputStreamReader(con1.getInputStream()));
logger.debug("Connection was executed");
String inputLine1;
StringBuffer content1 = new StringBuffer();

System.out.println("in1 !! "+in1);
while ((inputLine1 = in1.readLine()) != null) {
  System.out.println("inputLine1 !! "+inputLine1);
  logger.debug("inputLine1 !! "+inputLine1);
	    content1.append(inputLine1);
}

JSONParser parser1 = new JSONParser(); 
jsonobject1= (JSONObject)parser1.parse(content1.toString());
   con1.disconnect();
   con.disconnect();
		}catch(Exception e){
			e.printStackTrace();
			logger.error("There was some error with the collection");
			pair.put("Status", "Error gettign the token");
			   return Response.status(500).entity(pair).build();
		}
		System.out.println("response 200 sent");
		return Response.status(200).entity(jsonobject1).build();
		
	}
	

//http://localhost:8080/api/reports/remediation/run/
@POST 
   @Path("/csmartRemidiat")
   @Produces(MediaType.APPLICATION_JSON)
   public Response doRemidiat(@NotNull @QueryParam("id") String auditId1){
	init();
	JSONObject jsonobject1=new JSONObject();
	logger.info("get security function started");
	HashMap<String,String> pair = new HashMap<String,String>() ;
	this.auditId=auditId1;
	Base64.Decoder decoder = Base64.getMimeDecoder();
	String usepassDecod=new String(decoder.decode(authString));
	 String[] arrOfStr = usepassDecod.split("!q4Ev");
	String userdecod=new String(decoder.decode(arrOfStr[arrOfStr.length-2]));
	String passdecod=new String(decoder.decode(arrOfStr[arrOfStr.length-1]));
	this.username=userdecod;
	this.password=passdecod;
	try{

		String payload="{\"username\":\""+username+"\",\"password\":\""+password+"\"}";
System.out.println(payload);
	url=csmartUrl+csmartAuth;
		HttpURLConnection con = (HttpURLConnection) ((new URL(url).openConnection()));
		System.out.println("projectUrl"+con.getURL());
		 con.setRequestMethod("POST");
		con.setDoOutput(true);
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Accept", "application/json");
	 System.out.println(url);
	 OutputStreamWriter output=new OutputStreamWriter(con.getOutputStream());
	 System.out.println(payload);
	 output.write(payload);
	 output.flush();
	 output.close();
	 con.connect();
	 
	 System.out.println(con.getResponseCode());
 BufferedReader in = new BufferedReader(
		  new InputStreamReader(con.getInputStream()));
 logger.debug("Connection was executed");
String inputLine;
StringBuffer content = new StringBuffer();

System.out.println("in !! "+in);
while ((inputLine = in.readLine()) != null) {
	  System.out.println("inputLine !! "+inputLine);
	  logger.debug("inputLine !! "+inputLine);
	    content.append(inputLine);
}


JSONParser parser = new JSONParser(); 
JSONObject jobj = (JSONObject)parser.parse(content.toString());
authToken=jobj.get("token").toString();
String assetIdpayload="{\"auditId\":\""+auditId+"\"}"; 
url1=csmartUrl+remidiat;
HttpURLConnection con1 = (HttpURLConnection) ((new URL(url1).openConnection()));
System.out.println("projectUrl"+con1.getURL());
 con1.setRequestMethod("POST");
con1.setDoOutput(true);
con1.setRequestProperty("Content-Type", "application/json");
con1.setRequestProperty("Accept", "application/json");
System.out.println("Bearer "+authToken);
con1.setRequestProperty("Authorization", "Bearer "+authToken);
System.out.println(url1);
OutputStreamWriter output1=new OutputStreamWriter(con1.getOutputStream());
System.out.println(assetIdpayload);
output1.write(assetIdpayload);
output1.flush();
output1.close();
con1.connect();
System.out.println(con1.getResponseCode());
BufferedReader in1 = new BufferedReader(new InputStreamReader(con1.getInputStream()));
logger.debug("Connection was executed");
String inputLine1;
StringBuffer content1 = new StringBuffer();

System.out.println("in1 !! "+in1);
while ((inputLine1 = in1.readLine()) != null) {
System.out.println("inputLine1 !! "+inputLine1);
logger.debug("inputLine1 !! "+inputLine1);
    content1.append(inputLine1);
}

JSONParser parser1 = new JSONParser(); 
jsonobject1= (JSONObject)parser1.parse(content1.toString());
con1.disconnect();
con.disconnect();
	}catch(Exception e){
		e.printStackTrace();
		logger.error("There was some error with the collection");
		pair.put("Status", "Error gettign the token");
		   return Response.status(500).entity(pair).build();
	}
	System.out.println("response 200 sent");
	return Response.status(200).entity(jsonobject1).build();
	
}

@SuppressWarnings({ "unchecked" })
@GET
   @Path("/csmartRemidiation")
   @Produces(MediaType.APPLICATION_JSON)
   public Response getRemidiationDetails(){
	init();
	logger.info("get security function started");
	HashMap<String,String> pair = new HashMap<String,String>() ;
	
	Base64.Decoder decoder = Base64.getMimeDecoder();
	String usepassDecod=new String(decoder.decode(authString));
	 String[] arrOfStr = usepassDecod.split("!q4Ev");
	String userdecod=new String(decoder.decode(arrOfStr[arrOfStr.length-2]));
	String passdecod=new String(decoder.decode(arrOfStr[arrOfStr.length-1]));
	this.username=userdecod;
	this.password=passdecod;
	try{
		String payload="{\"username\":\""+username+"\",\"password\":\""+password+"\"}";
		System.out.println(payload);
				url=csmartUrl+csmartAuth;
					HttpURLConnection con = (HttpURLConnection) ((new URL(url).openConnection()));
					System.out.println("projectUrl"+con.getURL());
					 con.setRequestMethod("POST");
					con.setDoOutput(true);
					con.setRequestProperty("Content-Type", "application/json");
					con.setRequestProperty("Accept", "application/json");
				 System.out.println(url);
				 OutputStreamWriter output=new OutputStreamWriter(con.getOutputStream());
				 System.out.println(payload);
				 output.write(payload);
				 output.flush();
				 output.close();
				 con.connect();
				 
				 System.out.println(con.getResponseCode());
		     BufferedReader in = new BufferedReader(
		   		  new InputStreamReader(con.getInputStream()));
		     logger.debug("Connection was executed");
		   String inputLine;
		   StringBuffer content = new StringBuffer();
		   
		   System.out.println("in !! "+in);
		   while ((inputLine = in.readLine()) != null) {
		 	  System.out.println("inputLine !! "+inputLine);
		 	  logger.debug("inputLine !! "+inputLine);
		  	    content.append(inputLine);
		   }
		   

JSONParser parser = new JSONParser(); 
JSONObject jobj = (JSONObject)parser.parse(content.toString());
authToken=jobj.get("token").toString();

url1=csmartUrl+remidiatDet;
	HttpURLConnection con1 = (HttpURLConnection) ((new URL(url1).openConnection()));
	System.out.println("projectUrl"+con1.getURL());
	 con1.setRequestMethod("GET");
	con1.setDoOutput(true);
	con1.setRequestProperty("Content-Type", "application/json");
	con1.setRequestProperty("Accept", "application/json");
	System.out.println("Bearer "+authToken);
	con1.setRequestProperty("Authorization", "Bearer "+authToken);
System.out.println(url1);
con1.connect();
System.out.println(con1.getResponseCode());
BufferedReader in1 = new BufferedReader(new InputStreamReader(con1.getInputStream()));
logger.debug("Connection was executed");
String inputLine1;
StringBuffer content1 = new StringBuffer();

System.out.println("in1 !! "+in1);
while ((inputLine1 = in1.readLine()) != null) {
System.out.println("inputLine1 !! "+inputLine1);
logger.debug("inputLine1 !! "+inputLine1);
	    content1.append(inputLine1);
}

JSONParser parser1 = new JSONParser(); 
JSONArray jobjj= (JSONArray)parser1.parse(content1.toString());


for(int i=0;i<jobjj.size();i++) 
{
	  	pair = new HashMap<String,String>();
    	JSONObject jobj1 =(JSONObject)jobjj.get(i);
    	System.out.println(jobj1.toJSONString());
    	if(jobj1.get("id")!=null) {
		pair.put("id", jobj1.get("id").toString());
    	}
    	if(jobj1.get("assetId")!=null) {
		pair.put("assetId", jobj1.get("assetId").toString());
    	}if(jobj1.get("auditId")!=null) {
		pair.put("auditId", jobj1.get("auditId").toString());
    	}
		if(jobj1.get("hostname")!=null) {
		pair.put("hostname", jobj1.get("hostname").toString());
		}if(jobj1.get("policy")!=null) {
		pair.put("policy", jobj1.get("policy").toString());
		}if(jobj1.get("status")!=null) {
		pair.put("status", jobj1.get("status").toString());
		}if(jobj1.get("os")!=null) {
		pair.put("os", jobj1.get("os").toString());
		}if(jobj1.get("startTime")!=null) {
			
			//Long gmtTime =1317951113613L; // 2.32pm NZDT 
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date((long) jobj1.get("startTime")));
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

			//Here you say to java the initial timezone. This is the secret
			sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
			//Will print in UTC
			System.out.println(sdf.format(calendar.getTime()));    

			//Here you set to your timezone
			sdf.setTimeZone(TimeZone.getDefault());
			//Will print on your default Timezone
			
			System.out.println(sdf.format(calendar.getTime()));
		pair.put("startTime", sdf.format(calendar.getTime()));
		}if(jobj1.get("endTime")!=null) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date((long) jobj1.get("endTime")));
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

			//Here you say to java the initial timezone. This is the secret
			sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
			//Will print in UTC
			System.out.println(sdf.format(calendar.getTime()));    

			//Here you set to your timezone
			sdf.setTimeZone(TimeZone.getDefault());
			//Will print on your default Timezone
			
			System.out.println(sdf.format(calendar.getTime()));
		pair.put("endTime",sdf.format(calendar.getTime()));
		}if(jobj1.get("user")!=null) {
	   		pair.put("user", jobj1.get("user").toString());
	   		}
		
		array.add(pair);
		logger.debug("Getting the Details");
		System.out.println("Response Array:"+array);
}

con.disconnect();
	}catch(Exception e){
		e.printStackTrace();
		logger.error("There was some error with the collection");
		pair.put("Status", "Error gettign the token");
		   return Response.status(500).entity(pair).build();
	}
	System.out.println("response 200 sent");
	return Response.status(200).entity(array).build();
	
}



@DELETE
   @Path("/deleteassets")
   @Produces(MediaType.APPLICATION_JSON)
   public Response deleteAssets(@NotNull @QueryParam("id") String id){
	init();
	logger.info("get security function started");
	HashMap<String,String> pair = new HashMap<String,String>() ;
	String jobjj;
	Base64.Decoder decoder = Base64.getMimeDecoder();
	String usepassDecod=new String(decoder.decode(authString));
	 String[] arrOfStr = usepassDecod.split("!q4Ev");
	String userdecod=new String(decoder.decode(arrOfStr[arrOfStr.length-2]));
	String passdecod=new String(decoder.decode(arrOfStr[arrOfStr.length-1]));
	this.username=userdecod;
	this.password=passdecod;
	
	try{
		String payload="{\"username\":\""+username+"\",\"password\":\""+password+"\"}";
		System.out.println(payload);
				url=csmartUrl+csmartAuth;
					HttpURLConnection con = (HttpURLConnection) ((new URL(url).openConnection()));
					System.out.println("projectUrl"+con.getURL());
					 con.setRequestMethod("POST");
					con.setDoOutput(true);
					con.setRequestProperty("Content-Type", "application/json");
					con.setRequestProperty("Accept", "application/json");
				 System.out.println(url);
				 OutputStreamWriter output=new OutputStreamWriter(con.getOutputStream());
				 System.out.println(payload);
				 output.write(payload);
				 output.flush();
				 output.close();
				 con.connect();
				 
				 System.out.println(con.getResponseCode());
		     BufferedReader in = new BufferedReader(
		   		  new InputStreamReader(con.getInputStream()));
		     logger.debug("Connection was executed");
		   String inputLine;
		   StringBuffer content = new StringBuffer();
		   
		   System.out.println("in !! "+in);
		   while ((inputLine = in.readLine()) != null) {
		 	  System.out.println("inputLine !! "+inputLine);
		 	  logger.debug("inputLine !! "+inputLine);
		  	    content.append(inputLine);
		   }
		   

JSONParser parser = new JSONParser(); 
JSONObject jobj = (JSONObject)parser.parse(content.toString());
authToken=jobj.get("token").toString();
//http://localhost:8080/api/assets/5caf0aed817a1e09a4f85463
url1=csmartUrl+enrol+"/"+id;
	HttpURLConnection con1 = (HttpURLConnection) ((new URL(url1).openConnection()));
	System.out.println("projectUrl"+con1.getURL());
	 con1.setRequestMethod("DELETE");
	con1.setDoOutput(true);
	con1.setRequestProperty("Content-Type", "application/json");
	con1.setRequestProperty("Accept", "application/json");
	System.out.println("Bearer "+authToken);
	con1.setRequestProperty("Authorization", "Bearer "+authToken);
System.out.println(url1);
con1.connect();
System.out.println(con1.getResponseCode());
BufferedReader in1 = new BufferedReader(new InputStreamReader(con1.getInputStream()));
logger.debug("Connection was executed");
String inputLine1;
StringBuffer content1 = new StringBuffer();

System.out.println("in1 !! "+in1);
while ((inputLine1 = in1.readLine()) != null) {
System.out.println("inputLine1 !! "+inputLine1);
logger.debug("inputLine1 !! "+inputLine1);
	    content1.append(inputLine1);
}

jobjj= inputLine1;
con.disconnect();
	}catch(Exception e){
		e.printStackTrace();
		logger.error("There was some error with the collection");
		pair.put("Status", "Error deleting the asset");
		   return Response.status(500).entity(pair).build();
	}
	System.out.println("response 200 sent");
	return Response.status(200).entity(jobjj).build();
	
}


}
