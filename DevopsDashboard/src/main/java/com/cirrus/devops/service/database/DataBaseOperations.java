package com.cirrus.devops.service.database;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Base64;
import java.util.HashMap;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.postgresql.util.PSQLException;

import com.cirrus.devops.util.PropertyUtility;
import com.jcraft.jsch.JSchException;

@Path("/DataBaseOperations") 
public class DataBaseOperations {
	static final Logger logger = Logger.getLogger(DataBaseOperations.class);
	 private String authString;
		private String url;
	@SuppressWarnings({ "deprecation", "unchecked" })
	@POST 
	@Path("/writeData")
	@Produces(MediaType.APPLICATION_JSON)
	//public Response writeData(@QueryParam("username") String userName , @QueryParam("password") String password, @QueryParam("roll") String roll) throws ClassNotFoundException{
	public Response writeData(InputStream json) throws ClassNotFoundException, ParseException, IOException{
		init();
		JSONObject autherization=new JSONObject();
		// String url = "jdbc:postgresql://localhost:5432/postgres";
		 Base64.Decoder decoder1 = Base64.getMimeDecoder(); 
		   String keydecoded1=new String(decoder1.decode(authString));
		   System.out.println("keydecoded1:"+keydecoded1);
		   String[] arrOfStr1 = keydecoded1.split("!q4Ev");
		   System.out.println("arrOfStr:"+arrOfStr1[arrOfStr1.length-1]);
		   String pass=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-1]));
		   System.out.println("arrOfStr:"+arrOfStr1[arrOfStr1.length-2]);
		   String user=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-2]));
	        
	       try {
	    	   BufferedReader br;
				StringBuffer sb;
				sb= new StringBuffer();
				br = new BufferedReader(new InputStreamReader(json));
				String line;
				while((line=br.readLine())!=null){
					sb.append(line);
				}
				JSONParser parser = new JSONParser(); 
				JSONObject json1 = (JSONObject) parser.parse(sb.toString());
				System.out.println(json1);
	        	Class.forName("org.postgresql.Driver");
	        	System.out.println("Driver version: " + org.postgresql.Driver.getVersion());
	        	    Connection conn = null;
	        	     conn = DriverManager.getConnection(url, user, pass);
	        	        System.out.println("Connected to the PostgreSQL server successfully.");
	        	   
	        	   	PreparedStatement st = conn.prepareStatement("INSERT INTO iqusers (user_name,password,access_level,first_name,last_name,email_address) VALUES (?, ?, ?, ?, ?, ?)");
	        	    	st.setString(1, json1.get("username").toString());
	        	    	st.setString(2, json1.get("password").toString());
	        	    	st.setString(3, json1.get("access").toString());
	        	    	st.setString(4, json1.get("firstname").toString());
	        	    	st.setString(5, json1.get("lastname").toString());
	        	    	st.setString(6, json1.get("emailid").toString());
	        	    	st.executeUpdate();
	        	    	st.close();
	        	    	conn.close();
	        	    	autherization.put("Status","success");
		                logger.info("All the functions worked properly, exiting the function with an array object");
		        		return Response.status(200).entity(autherization).build();	
	      }catch(PSQLException ex) {
	    	  ex.getMessage();
	        	ex.getErrorCode();
	        	logger.error("There was Problems while adding user"+ex.getMessage());
	        	autherization.put("Status", "usernameexist");
				   return Response.status(409).entity(autherization).build();
	      }
	       catch (SQLException ex) {
	        	ex.printStackTrace();
	        	ex.getMessage();
	        	ex.getErrorCode();
	        	logger.error("There was Problems while adding user");
	        	autherization.put("Status", "Unable to add user");
				   return Response.status(500).entity(autherization).build();
	        } 
	}  
	
	public void init(){
		
		   BasicConfigurator.configure();
		   PropertyUtility props = PropertyUtility.getPropertyUtility();
		  /* this.authString=props.getProperty("privatToken");
		   this.url=props.getProperty("dburl");*/
		   this.authString=props.getProperty("remotedbToken");
		   this.url=props.getProperty("remotedbUrl");
		   
	   }	
	@SuppressWarnings({ "unchecked", "deprecation" })
	@DELETE 
	@Path("/deleteData")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteData(@QueryParam("key") String key ) throws ClassNotFoundException{
		init();
		JSONObject autherization=new JSONObject();
		 Base64.Decoder decoder1 = Base64.getMimeDecoder(); 
		   String keydecoded1=new String(decoder1.decode(authString));
		   System.out.println("keydecoded1:"+keydecoded1);
		   String[] arrOfStr1 = keydecoded1.split("!q4Ev");
		   System.out.println("arrOfStr:"+arrOfStr1[arrOfStr1.length-1]);
		   String pass=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-1]));
		   System.out.println("arrOfStr:"+arrOfStr1[arrOfStr1.length-2]);
		   String user=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-2]));

	        try {
	        	Class.forName("org.postgresql.Driver");
	        	System.out.println("Driver version: " + org.postgresql.Driver.getVersion());
	        	    Connection conn = null;
	        	        conn = DriverManager.getConnection(url, user, pass);
	        	        System.out.println("Connected to the PostgreSQL server successfully.");
	        	   
	        	    	PreparedStatement st = conn.prepareStatement("DELETE FROM iqusers WHERE user_name= ?");
	        	    	st.setString(1, key);
	        	    	st.executeUpdate();
	        	    	st.close();
	        	    	conn.close();
	        	    	autherization.put("Status",key+" deleted successfully");
		                logger.info("All the functions worked properly, exiting the function with an array object");
		        		return Response.status(200).entity(autherization).build();	
		        		
	        } catch (SQLException ex) {
	        	ex.printStackTrace();
	        	ex.getMessage();
	        	((SQLException) ex).getErrorCode();
	        	logger.error("There was Problems while generating licence key");
	        	autherization.put("Status", "Unable to delete user");
				   return Response.status(500).entity(autherization).build();
	        }
	}
	
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	@GET 
	@Path("/AllData")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllData() throws ClassNotFoundException{
		init();
		JSONArray array=new JSONArray();
		JSONObject autherization=new JSONObject();
		 Base64.Decoder decoder1 = Base64.getMimeDecoder(); 
		   String keydecoded1=new String(decoder1.decode(authString));
		   System.out.println("keydecoded1:"+keydecoded1);
		   String[] arrOfStr1 = keydecoded1.split("!q4Ev");
		   System.out.println("arrOfStr:"+arrOfStr1[arrOfStr1.length-1]);
		   String pass=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-1]));
		   System.out.println("arrOfStr:"+arrOfStr1[arrOfStr1.length-2]);
		   String user=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-2]));

	        try {
	        	HashMap<String, String> pair;
	        	Class.forName("org.postgresql.Driver");
	        	System.out.println("Driver version: " + org.postgresql.Driver.getVersion());
	        	    Connection conn = null;
	        	        conn = DriverManager.getConnection(url, user, pass);
	        	        System.out.println("Connected to the PostgreSQL server successfully.");
	        	   
	        	    	PreparedStatement pst = conn.prepareStatement("SELECT * FROM iqusers ");
	        	    	 ResultSet rs = pst.executeQuery();
	     	            while (rs.next()) {
	     	            	pair=new HashMap<String, String>();
	     	            	pair.put("name",rs.getString(1));
	     	            	pair.put("roll",rs.getString(3));
	     	            	array.add(pair);
	     	            	}
	     	           conn.close();
		                logger.info("All the functions worked properly, exiting the function with an array object");
		        		return Response.status(200).entity(array).build();	
		        		
	        } catch (SQLException ex) {
	        	ex.printStackTrace();
	        	ex.getMessage();
	        	((SQLException) ex).getErrorCode();
	        	logger.error("There was Problems while generating licence key");
	        	autherization.put("Status", "Unable to get users information");
				   return Response.status(500).entity(autherization).build();
	        }
	}	
	
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	@GET 
	@Path("/SingleData")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSingleData(@QueryParam("key") String key ) throws ClassNotFoundException{
		init();
		JSONObject autherization=new JSONObject();
		 Base64.Decoder decoder1 = Base64.getMimeDecoder(); 
		   String keydecoded1=new String(decoder1.decode(authString));
		   System.out.println("keydecoded1:"+keydecoded1);
		   String[] arrOfStr1 = keydecoded1.split("!q4Ev");
		   System.out.println("arrOfStr:"+arrOfStr1[arrOfStr1.length-1]);
		   String pass=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-1]));
		   System.out.println("arrOfStr:"+arrOfStr1[arrOfStr1.length-2]);
		   String user=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-2]));
	        try {
	        	HashMap<String, String> pair=new HashMap<String, String>();
	        	Class.forName("org.postgresql.Driver");
	        	System.out.println("Driver version: " + org.postgresql.Driver.getVersion());
	        	    Connection conn = null;
	        	        conn = DriverManager.getConnection(url, user, pass);
	        	        System.out.println("Connected to the PostgreSQL server successfully.");
	        	   
	        	    	PreparedStatement pst = conn.prepareStatement("SELECT * FROM iqusers WHERE user_name= ? ");
	        	    	pst.setString(1, key);
	        	    	 ResultSet rs = pst.executeQuery();
	        	    	 //if(rs) {
	     	            while (rs.next()) {
	     	            	pair.put("username",rs.getString(1));
	     	            	pair.put("password",rs.getString(2));
	     	            	pair.put("access",rs.getString(3));
	     	            	pair.put("firstname",rs.getString(4));
	     	            	pair.put("lastname",rs.getString(5));
	     	            	pair.put("emailid",rs.getString(6));
	     	            	}
	     	            System.out.println(pair.size());
	     	            conn.close();
	     	            if(pair.size()>0) {
	     	           logger.info("All the functions worked properly, exiting the function with an array object");
		        		return Response.status(200).entity(pair).build();
	        	    	 }else {
	        	    		 pair.put("Status", "No data found for "+key);
	        	    		 logger.info("All the functions worked properly, exiting the function with an array object");
	 		        		return Response.status(200).entity(pair).build();
	        	    	 }
	        } catch (SQLException ex) {
	        	ex.printStackTrace();
	        	ex.getMessage();
	        	((SQLException) ex).getErrorCode();
	        	logger.error("There was Problems while generating licence key");
	        	autherization.put("Status", "Unable to get user information");
				   return Response.status(500).entity(autherization).build();
	        }
	}	
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	@PUT
	@Path("/updateUser")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateUser(InputStream json ) throws ClassNotFoundException, IOException, ParseException{
		init();
		JSONObject autherization=new JSONObject();
		 Base64.Decoder decoder1 = Base64.getMimeDecoder(); 
		   String keydecoded1=new String(decoder1.decode(authString));
		   System.out.println("keydecoded1:"+keydecoded1);
		   String[] arrOfStr1 = keydecoded1.split("!q4Ev");
		   System.out.println("arrOfStr:"+arrOfStr1[arrOfStr1.length-1]);
		   String pass=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-1]));
		   System.out.println("arrOfStr:"+arrOfStr1[arrOfStr1.length-2]);
		   String user=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-2]));

	        try {
	        	 BufferedReader br;
					StringBuffer sb;
					sb= new StringBuffer();
					br = new BufferedReader(new InputStreamReader(json));
					String line;
					while((line=br.readLine())!=null){
						sb.append(line);
					}
					JSONParser parser = new JSONParser(); 
					JSONObject json1 = (JSONObject) parser.parse(sb.toString());
					System.out.println(json1);
	        	//HashMap<String, String> pair=new HashMap<String, String>();
	        	Class.forName("org.postgresql.Driver");
	        	System.out.println("Driver version: " + org.postgresql.Driver.getVersion());
	        	    Connection conn = null;
	        	        conn = DriverManager.getConnection(url, user, pass);
	        	        System.out.println("Connected to the PostgreSQL server successfully.");
	        	   
	        	    	PreparedStatement st = conn.prepareStatement("Update iqusers SET user_name=?,password=?,access_level=?,first_name=?,last_name=?,email_id=?  WHERE user_name=? ");
	        	    	st.setString(1, json1.get("username").toString());
	        	    	st.setString(2, json1.get("password").toString());
	        	    	st.setString(3, json1.get("access").toString());
	        	    	st.setString(4, json1.get("firstname").toString());
	        	    	st.setString(5, json1.get("lastname").toString());
	        	    	st.setString(6, json1.get("emailid").toString());
	        	    	st.setString(7, json1.get("oldpmr").toString());
	        	    	st.executeUpdate();
	        	    	st.close();
	        	    	conn.close();
	        	    	autherization.put("Status","success");
		                logger.info("All the functions worked properly, exiting the function with an array object");
		        		return Response.status(200).entity(autherization).build();	
	      }catch(PSQLException ex) {
	    	  ex.printStackTrace();
	    	  ex.getMessage();
	        	ex.getErrorCode();
	        	logger.error("There was Problems while adding user"+ex.getMessage());
	        	autherization.put("Status", "usernameexist");
				   return Response.status(409).entity(autherization).build();
	      }
	       catch (SQLException ex) {
	        	ex.printStackTrace();
	        	ex.getMessage();
	        	ex.getErrorCode();
	        	logger.error("There was Problems while adding user");
	        	autherization.put("Status", "Unable to update user information");
				   return Response.status(500).entity(autherization).build();
	        } 
	}		
	
}
