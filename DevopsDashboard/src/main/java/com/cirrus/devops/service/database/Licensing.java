package com.cirrus.devops.service.database;

import java.io.IOException;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.postgresql.util.PSQLException;

import com.cirrus.devops.util.PropertyUtility;
import com.jcraft.jsch.JSchException;

@Path("/Licence")
public class Licensing {
	static final Logger logger = Logger.getLogger(Licensing.class);
	private String authString;
	private String url;
	int count=3;
	public static String expiredatevalid=null;
	@SuppressWarnings({ "deprecation", "unchecked" })
	@POST 
	@Path("/writeLicense")
	@Produces(MediaType.APPLICATION_JSON)
	public Response writeData(@QueryParam("licencekey") String key) throws ClassNotFoundException, ParseException, IOException{
		init();
		JSONObject autherization=new JSONObject();
	        Base64.Decoder decoder1 = Base64.getMimeDecoder(); 
			   String keydecoded1=new String(decoder1.decode(authString));
			   String[] arrOfStr1 = keydecoded1.split("!q4Ev");
			   String pass=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-1]));
			   String user=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-2]));
	       try {
	    	   
			   Base64.Decoder decoder = Base64.getMimeDecoder(); 
			   String keydecoded=new String(decoder.decode(key));
			   System.out.println("keydecoded:"+keydecoded);
			   String[] arrOfStr = keydecoded.split("!q4Ev"); 
			   System.out.println("key:"+keydecoded);
			   System.out.println("arrOfStr:"+arrOfStr[arrOfStr.length-1]);
			   String creationDateDecoded=new String(decoder.decode(arrOfStr[arrOfStr.length-1]));
			   System.out.println("arrOfStr:"+arrOfStr[arrOfStr.length-2]);
			   String endDateDecoded=new String(decoder.decode(arrOfStr[arrOfStr.length-2]));
			   System.out.println("arrOfStr:"+arrOfStr[arrOfStr.length-3]);
			   String securityDecoded=new String(decoder.decode(arrOfStr[arrOfStr.length-3]));
			   System.out.println("arrOfStr:"+arrOfStr[arrOfStr.length-4]);
			   String deployDecoded=new String(decoder.decode(arrOfStr[arrOfStr.length-4]));
			   System.out.println("arrOfStr:"+arrOfStr[arrOfStr.length-5]);
			   String sysId=new String(decoder.decode(arrOfStr[arrOfStr.length-5]));
			   System.out.println(endDateDecoded);
			   System.out.println(creationDateDecoded);
			   System.out.println(securityDecoded);
			   System.out.println(deployDecoded);
			   System.out.println(sysId);
			   LocalDateTime end=LocalDateTime.parse(endDateDecoded);
			   LocalDateTime start=LocalDateTime.parse(creationDateDecoded);
			   ArrayList<String> licensevalue=new ArrayList<String>(arrOfStr.length+1);
			   ArrayList<String> licensename=new ArrayList<String>(arrOfStr.length+1);
			   licensevalue.add(key);
			   licensevalue.add(deployDecoded);
			   licensevalue.add(securityDecoded);
			   licensevalue.add(creationDateDecoded);
			   licensevalue.add(endDateDecoded);
			   licensevalue.add(sysId);
			   licensevalue.add("true");
			   licensename.add("licensekey");
			   licensename.add("deployment");
			   licensename.add("security");
			   licensename.add("purchasedate");
			   licensename.add("expiredate");
			   licensename.add("systemId");
			   licensename.add("validity");
			   System.out.println(licensevalue);
			   System.out.println(licensename);
	        	Class.forName("org.postgresql.Driver");
	        	System.out.println("Driver version: " + org.postgresql.Driver.getVersion());
	        	    Connection conn = null;
	        	     conn = DriverManager.getConnection(url, user, pass);
	        	        System.out.println("Connected to the PostgreSQL server successfully.");
	        	   
	        	   	PreparedStatement st = conn.prepareStatement("INSERT INTO licensedetails (name,value) VALUES (?, ?)");
					for (int i = 0; i < licensename.size(); i++) {
						
						st.setString(1,licensename.get(i));
	        	    	st.setString(2,licensevalue.get(i));
						st.addBatch();
	                    	//st.executeBatch();
	                }
					
	        	    	 int[] affectedRows =st.executeBatch();
	        	            if (affectedRows.length > 0) {
	        	            	st.close();
	    	        	    	autherization.put("status","success");
	    		                logger.info("All the functions worked properly, exiting the function with an array object");
	    		                this.expiredatevalid=endDateDecoded;
	    		                //Start Scheduled Run
	    		                /*new ScheduledExecutor().scheduleRegularly(() -> {
			        	    		  LocalDateTime expireCheck=LocalDateTime.parse(endDateDecoded);
			   					   System.out.println("end:"+expireCheck);
			   					   int ret=LocalDateTime.now().compareTo(expireCheck);
			   					   try {
			   						 if(ret>0) {
									updateLicenseValidity();
			   						 }
								} catch (ClassNotFoundException e) {
									e.printStackTrace();
								}
			        	    		}, LocalDateTime.now(), thisTime -> thisTime.plusHours(10));*/
	    		        		return Response.status(200).entity(autherization).build();	
	        	            }else {
	        	            	logger.error("There was Problems while adding license");
	        		        	autherization.put("status", "Unable to add license");
	        					   return Response.status(500).entity(autherization).build();
	        	            }
	      }catch(BatchUpdateException ex) {
	    	  ex.printStackTrace();
	    	  ex.getMessage();
	        	ex.getErrorCode();
	        	logger.error("There was Problems while adding license"+ex.getMessage());
	        	autherization.put("status", "exist");
				   return Response.status(409).entity(autherization).build();
	      }catch(PSQLException ex) {
	    	  ex.printStackTrace();
	    	  ex.getMessage();
	        	ex.getErrorCode();
	        	logger.error("There was Problems while adding license"+ex.getMessage());
	        	autherization.put("status", "exist");
				   return Response.status(409).entity(autherization).build();
	      }
	       catch (SQLException ex) {
	        	ex.printStackTrace();
	        	ex.getMessage();
	        	ex.getErrorCode();
	        	logger.error("There was Problems while adding license");
	        	autherization.put("status", "Unable to add license");
				   return Response.status(500).entity(autherization).build();
	        } 
	}
	
	public void init(){
		   BasicConfigurator.configure();
		   PropertyUtility props = PropertyUtility.getPropertyUtility();
		 /*  this.authString=props.getProperty("privatToken");
		   this.url=props.getProperty("dburl");*/
		   this.authString=props.getProperty("remotedbToken");
		   this.url=props.getProperty("remotedbUrl");
		   
	   }
	@SuppressWarnings("unchecked")
	@GET 
	@Path("/AllData")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllData() throws ClassNotFoundException{
		init();
		JSONArray array=new JSONArray();
		JSONObject autherization=new JSONObject();
		 //String url = "jdbc:postgresql://localhost:5432/postgres";
		  Base64.Decoder decoder1 = Base64.getMimeDecoder(); 
		   String keydecoded1=new String(decoder1.decode(authString));
		   String[] arrOfStr1 = keydecoded1.split("!q4Ev");
		   String pass=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-1]));
		   String user=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-2]));
		   
	        try {
	        	HashMap<String, String> pair=new HashMap<String, String>();
	        	Class.forName("org.postgresql.Driver");
	        	System.out.println("Driver version: " + org.postgresql.Driver.getVersion());
	        	    Connection conn = null;
	        	        conn = DriverManager.getConnection(url, user, pass);
	        	        System.out.println("Connected to the PostgreSQL server successfully.");
	        	   
	        	    	PreparedStatement pst = conn.prepareStatement("SELECT * FROM licensedetails ");
	        	    	 ResultSet rs = pst.executeQuery();
	        	    	 //if(rs.getRow()>0) {
	     	            while (rs.next()) {
	     	            	pair.put(rs.getString(1),rs.getString(2));
	     	            	}
	        	    	if(pair.isEmpty()) {
		        		pair.put("status","License key not found");
       	    		 	logger.info("No data found");
		        		return Response.status(204).entity(pair).build();	
	        	    	}else {
	        	    		this.expiredatevalid=pair.get("expiredate").toString();
	        	    		/*new ScheduledExecutor().scheduleRegularly(() -> {
	        	    		  LocalDateTime end=LocalDateTime.parse(pair.get("expiredate"));
	   					   System.out.println("end:"+end);
	   					   int ret=LocalDateTime.now().compareTo(end);
	   					   System.out.println(ret);
	        	    		}, LocalDateTime.now().truncatedTo(ChronoUnit.DAYS).withHour(14), thisTime -> thisTime.plusDays(1));*/
	        	    		logger.info("All the functions worked properly, exiting the function with an array object");
			        		return Response.status(200).entity(pair).build();
	        	    	 }
	        } catch (SQLException ex) {
	        	ex.printStackTrace();
	        	ex.getMessage();
	        	((SQLException) ex).getErrorCode();
	        	logger.error("There was Problems while generating licence key");
	        	autherization.put("Status", "Unable to add user");
				   return Response.status(500).entity(autherization).build();
	        }
	}
	
	@SuppressWarnings("unchecked")
	@PUT
	@Path("/updateLicense")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateLicense(@QueryParam("licencekey") String key) throws ClassNotFoundException, IOException, ParseException{
		init();
		JSONObject autherization=new JSONObject();
		 Base64.Decoder decoder1 = Base64.getMimeDecoder(); 
		   String keydecoded1=new String(decoder1.decode(authString));
		   //System.out.println("keydecoded1:"+keydecoded1);
		   String[] arrOfStr1 = keydecoded1.split("!q4Ev");
		   //System.out.println("arrOfStr:"+arrOfStr1[arrOfStr1.length-1]);
		   String pass=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-1]));
		   //System.out.println("arrOfStr:"+arrOfStr1[arrOfStr1.length-2]);
		   String user=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-2]));

	        try {
	        	 Base64.Decoder decoder = Base64.getMimeDecoder(); 
				   String keydecoded=new String(decoder.decode(key));
				   System.out.println("keydecoded:"+keydecoded);
				   String[] arrOfStr = keydecoded.split("!q4Ev"); 
				   System.out.println("key:"+keydecoded);
				   System.out.println("arrOfStr:"+arrOfStr[arrOfStr.length-1]);
				   String creationDateDecoded=new String(decoder.decode(arrOfStr[arrOfStr.length-1]));
				   System.out.println("arrOfStr:"+arrOfStr[arrOfStr.length-2]);
				   String endDateDecoded=new String(decoder.decode(arrOfStr[arrOfStr.length-2]));
				   System.out.println("arrOfStr:"+arrOfStr[arrOfStr.length-3]);
				   String securityDecoded=new String(decoder.decode(arrOfStr[arrOfStr.length-3]));
				   System.out.println("arrOfStr:"+arrOfStr[arrOfStr.length-4]);
				   String deployDecoded=new String(decoder.decode(arrOfStr[arrOfStr.length-4]));
				   System.out.println("arrOfStr:"+arrOfStr[arrOfStr.length-5]);
				   String sysId=new String(decoder.decode(arrOfStr[arrOfStr.length-5]));
				   System.out.println(endDateDecoded);
				   System.out.println(creationDateDecoded);
				   System.out.println(securityDecoded);
				   System.out.println(deployDecoded);
				   System.out.println(sysId);
				   LocalDateTime end=LocalDateTime.parse(endDateDecoded);
				   LocalDateTime start=LocalDateTime.parse(creationDateDecoded);
				   ArrayList<String> licensevalue=new ArrayList<String>(arrOfStr.length+1);
				   ArrayList<String> licensename=new ArrayList<String>(arrOfStr.length+1);
				   licensevalue.add(key);
				   licensevalue.add(deployDecoded);
				   licensevalue.add(securityDecoded);
				   licensevalue.add(creationDateDecoded);
				   licensevalue.add(endDateDecoded);
				   licensevalue.add(sysId);
				   licensevalue.add("true");
				   licensename.add("licensekey");
				   licensename.add("deployment");
				   licensename.add("security");
				   licensename.add("purchasedate");
				   licensename.add("expiredate");
				   licensename.add("systemId");
				   licensename.add("validity");
				   System.out.println(licensevalue);
				   System.out.println(licensename);
	        	HashMap<String, String> pair=new HashMap<String, String>();
	        	Class.forName("org.postgresql.Driver");
	        	System.out.println("Driver version: " + org.postgresql.Driver.getVersion());
	        	    Connection conn = null;
	        	        conn = DriverManager.getConnection(url, user, pass);
	        	        System.out.println("Connected to the PostgreSQL server successfully.");
	        	   
	        	    	PreparedStatement st = conn.prepareStatement("Update licensedetails  SET value=?  WHERE name=? ");
	        	    	for (int i = 0; i < licensename.size(); i++) {
							
		        	    	st.setString(1,licensevalue.get(i));
		        	    	st.setString(2,licensename.get(i));
							st.addBatch();
		                }
						
		        	    	 int[] affectedRows =st.executeBatch();
		        	            if (affectedRows.length > 0) {
		        	            	st.close();
	        	    	autherization.put("status","success");
		                logger.info("All the functions worked properly, exiting the function with an array object");
		               this.expiredatevalid=endDateDecoded;
		               /* new ScheduledExecutor().scheduleRegularly(() -> {
	        	    		  LocalDateTime expireCheck=LocalDateTime.parse(endDateDecoded);
	   					   System.out.println("end:"+expireCheck);
	   					   int ret=LocalDateTime.now().compareTo(expireCheck);
	   					   try {
	   						 if(ret>0) {
							updateLicenseValidity();
	   						 }
						} catch (ClassNotFoundException e) {
							e.printStackTrace();
						}
	        	    		}, LocalDateTime.now(), thisTime -> thisTime.plusHours(10));*/
		        		return Response.status(200).entity(autherization).build();	
		        	            }else {
		        	            	return Response.status(400).entity(autherization).build();
		        	            }
	      }catch(PSQLException ex) {
	    	  ex.printStackTrace();
	    	  ex.getMessage();
	        	ex.getErrorCode();
	        	logger.error("There was Problems while adding user"+ex.getMessage());
	        	autherization.put("status", "usernameexist");
				   return Response.status(409).entity(autherization).build();
	      }
	       catch (SQLException ex) {
	        	ex.printStackTrace();
	        	ex.getMessage();
	        	ex.getErrorCode();
	        	logger.error("There was Problems while adding user");
	        	autherization.put("status", "Unable to update user information");
				   return Response.status(500).entity(autherization).build();
	        } 
	}
	
	@POST
	@Path("/updateLicenseValidity")
	public boolean updateLicenseValidity() throws ClassNotFoundException {
		init();
		Base64.Decoder decoder1 = Base64.getMimeDecoder(); 
		   String keydecoded1=new String(decoder1.decode(authString));
		   //System.out.println("keydecoded1:"+keydecoded1);
		   String[] arrOfStr1 = keydecoded1.split("!q4Ev");
		   //System.out.println("arrOfStr:"+arrOfStr1[arrOfStr1.length-1]);
		   String pass=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-1]));
		   //System.out.println("arrOfStr:"+arrOfStr1[arrOfStr1.length-2]);
		   String user=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-2]));

	        try {
	        	Class.forName("org.postgresql.Driver");
	        	System.out.println("Driver version: " + org.postgresql.Driver.getVersion());
	        	    Connection conn = null;
	        	        conn = DriverManager.getConnection(url, user, pass);
	        	        System.out.println("Connected to the PostgreSQL server successfully.");
	        	        ArrayList<String> licensevalue=new ArrayList<String>(6);
	 				   ArrayList<String> licensename=new ArrayList<String>(6);
					   licensevalue.add("noServerSecurity");
					   licensevalue.add("noDeployment");
					   licensevalue.add("false");
					   licensename.add("security");
					   licensename.add("deployment");
					   licensename.add("validity");
	        	    	PreparedStatement st = conn.prepareStatement("Update licensedetails  SET value=?  WHERE name=? ");
	        	    	for (int i = 0; i < licensename.size(); i++) {
		        	    	st.setString(1,licensevalue.get(i));
		        	    	st.setString(2,licensename.get(i));
							st.addBatch();
		                }
						
		        	    	 int[] affectedRows =st.executeBatch();
		        	            if (affectedRows.length > 0) {
		        	            	st.close();
		        	            	return true;
		        	            }else {
		        	            	return false;
		        	            }
	      }catch(PSQLException ex) {
	    	  ex.printStackTrace();
	    	  ex.getMessage();
	        	ex.getErrorCode();
	        	logger.error("There was Problems while adding user"+ex.getMessage());
				   return false;
	      }
	       catch (SQLException ex) {
	        	ex.printStackTrace();
	        	ex.getMessage();
	        	ex.getErrorCode();
	        	logger.error("There was Problems while adding user");
				   return false;
	        } 
	}
	
	public LocalDateTime getExpireDate() throws ClassNotFoundException{
		init();
		JSONArray array=new JSONArray();
		JSONObject autherization=new JSONObject();
		 //String url = "jdbc:postgresql://localhost:5432/postgres";
		  Base64.Decoder decoder1 = Base64.getMimeDecoder(); 
		   String keydecoded1=new String(decoder1.decode(authString));
		   String[] arrOfStr1 = keydecoded1.split("!q4Ev");
		   String pass=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-1]));
		   String user=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-2]));
		   
	        try {
	        	HashMap<String, String> pair=new HashMap<String, String>();
	        	Class.forName("org.postgresql.Driver");
	        	System.out.println("Driver version: " + org.postgresql.Driver.getVersion());
	        	    Connection conn = null;
	        	        conn = DriverManager.getConnection(url, user, pass);
	        	        System.out.println("Connected to the PostgreSQL server successfully.");
	        	   
	        	    	PreparedStatement pst = conn.prepareStatement("SELECT * FROM licensedetails ");
	        	    	 ResultSet rs = pst.executeQuery();
	        	    	 //if(rs.getRow()>0) {
	     	            while (rs.next()) {
	     	            	pair.put(rs.getString(1),rs.getString(2));
	     	            	}
	        	    	if(pair.isEmpty()) {
		        		pair.put("status","expire date not found");
       	    		 	logger.info("No data found");
		        		return null;	
	        	    	}else {
	        	    		if(pair.get("expiredate")!=null) {
	        	    			LocalDateTime end=LocalDateTime.parse(pair.get("expiredate"));
		        	    		logger.info("All the functions worked properly, exiting the function with an array object");
				        		return end;
	        	    		}else {
	        	    			return null;
	        	    		}
	        	    		  
	        	    	 }
	        } catch (SQLException ex) {
	        	ex.printStackTrace();
				   return null;
	        }
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject getModuleData() throws ClassNotFoundException{
		init();
		JSONObject licenseData=new JSONObject();
		  Base64.Decoder decoder1 = Base64.getMimeDecoder(); 
		   String keydecoded1=new String(decoder1.decode(authString));
		   String[] arrOfStr1 = keydecoded1.split("!q4Ev");
		   String pass=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-1]));
		   String user=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-2]));
		   
	        try {
	        	HashMap<String, String> pair=new HashMap<String, String>();
	        	Class.forName("org.postgresql.Driver");
	        	System.out.println("Driver version: " + org.postgresql.Driver.getVersion());
	        	    Connection conn = null;
	        	        conn = DriverManager.getConnection(url, user, pass);
	        	        System.out.println("Connected to the PostgreSQL server successfully.");
	        	   
	        	    	PreparedStatement pst = conn.prepareStatement("SELECT * FROM licensedetails ");
	        	    	 ResultSet rs = pst.executeQuery();
	        	    	 //if(rs.getRow()>0) {
	     	            while (rs.next()) {
	     	            	licenseData.put(rs.getString(1),rs.getString(2));
	     	            	}
	        	    	if(licenseData.isEmpty()) {
	        	    		licenseData.put("status","expire date not found");
       	    		 	logger.info("No data found");
		        		return null;	
	        	    	}else {
	        	    		  return licenseData;
	        	    	 }
	        } catch (SQLException ex) {
	        	ex.printStackTrace();
				   return null;
	        }
	}
	
}
