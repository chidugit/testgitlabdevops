package com.cirrus.devops.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.cirrus.devops.util.PropertyUtility;


@Path("/example") 
public class GetGitlabGroups {	
	 private static final String JSONObject = null;
	private String name;
	private String path;
	private String gitBaseUrl;
	 private String projectUrl;
	 private String groupUrl;
	 private String gitProjectCreation;
	 public GetGitlabGroups() {}
	  String projectPath = null; 
	  String projectName = null; 
	  String groupID=null;
	 private HttpURLConnection con;
	 String inputLine;
	 JSONObject pair;
	 JSONArray array;

	
	 @GET 
	   @Path("/getGitlabGroups")
	   @Produces(MediaType.APPLICATION_JSON)
	   public Response getGroups( @QueryParam("accessToken") String accessToken, @QueryParam("name") String name, @QueryParam("path") String path ) throws ParseException, MalformedURLException, IOException{
		
		 array = new JSONArray();
		name="superman";
		path="pigeon";
		System.out.println("Project service is called");
		try{
		System.out.println("this is access token"+accessToken);
   con = (HttpURLConnection) ((new URL("192.168.137.100/api/v4/groups?access_token="+accessToken+"&search="+name+"&search="+path).openConnection()));
  
   
   con.setRequestMethod("GET");

   BufferedReader in2 = new BufferedReader(new InputStreamReader(con.getInputStream()));
   StringBuffer content2 = new StringBuffer();
   while ((inputLine = in2.readLine()) != null) {
	    content2.append(inputLine);
	    }
   System.out.println("Response Message after GET call for Group "+content2.toString());
   
  JSONParser parsers = new JSONParser(); 
  JSONArray jobj2 = (JSONArray)parsers.parse(content2.toString());
  System.out.println("why you came "+jobj2.size());
  
    
  for(int i=0;i<jobj2.size();i++) 
  {
	  pair= new JSONObject();
  JSONObject jobj3=(JSONObject) jobj2.get(i);
  	
  System.out.println(jobj3.get("id").toString());
  System.out.println(jobj3.get("web_url").toString());
  System.out.println(jobj3.get("name").toString());
  System.out.println(jobj3.get("path").toString());
  System.out.println(jobj3.get("visibility").toString());
  
  pair.put("id", jobj3.get("id").toString());
  pair.put("web_url", jobj3.get("web_url").toString());
  pair.put("name", jobj3.get("name").toString());
  pair.put("path", jobj3.get("path").toString());
  pair.put("visibility", jobj3.get("visibility").toString());
  
  array.add(pair);


		  					


  }    
  System.out.println(array);
  con.disconnect();
		}
  catch(Exception e){
			
			pair.put("Status", "Error gettign the projects");
			   return Response.status(500).entity(pair).build();
		}
		return Response.status(200).entity(array).build();
		
		
	}

	
	}
