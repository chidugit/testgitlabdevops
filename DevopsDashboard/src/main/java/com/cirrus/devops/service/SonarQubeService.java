package com.cirrus.devops.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;
import java.util.HashMap;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

//import org.apache.catalina.valves.rewrite.Substitution.StaticElement;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.util.HashMap;
import com.cirrus.devops.util.PropertyUtility;
import com.mongodb.util.JSON;


@SuppressWarnings("unused")
@Path("/SonarQubeService")
public class SonarQubeService {
	static final Logger logger = Logger.getLogger(SonarQubeService.class);
	String login;
	String check_login;
	String projects;
	String projectsMetrics;
	String projectStatus;
	String projectsIssues;
	@SuppressWarnings("unchecked")
	@GET
	@Path("/Projects")
	@Produces(MediaType.APPLICATION_JSON)
	public Response login() throws MalformedURLException, IOException, ParseException {
		logger.info("Starting the login service");
		init();
		JSONObject pair = new JSONObject();
		JSONArray array = new JSONArray();

		System.out.println(login);
		HttpURLConnection con = (HttpURLConnection) ((new URL(login).openConnection()));
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		con.connect();
		// 

		
		con = (HttpURLConnection) ((new URL(check_login).openConnection()));
		con.setDoOutput(true);
		con.connect();
		con.getResponseCode();
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

		String inputLine;
		StringBuffer content = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			content.append(inputLine);
		}
		logger.debug("Auth over parsing the result");
		JSONParser parser = new JSONParser();
		JSONObject jobj = (JSONObject) parser.parse(content.toString());
		Boolean value = (Boolean) jobj.get("valid");
		
		// con.disconnect();

		if (value) {
			
			try {
				con = (HttpURLConnection) ((new URL(projects).openConnection()));
				con.setRequestMethod("GET");
				// con.setDoOutput(true);
				con.connect();

			} catch (java.io.IOException e) {
				
				logger.error("There was something wrong with the call to sonarQube Server, Check Credentials");
				
			}

			con.getResponseCode();
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			content = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				content.append(inputLine);
			}

			parser = new JSONParser();
			JSONObject projectList = (JSONObject) parser.parse(content.toString());
			logger.info("JSON object is:"+projectList);
			JSONArray arr = (JSONArray) projectList.get("components");
			logger.debug("getting the components list");

			for (int i = 0; i < arr.size(); i++) {
				pair = new JSONObject();

				JSONObject jobj1 = (JSONObject) arr.get(i);
				
				pair.put("name", jobj1.get("name").toString());
				pair.put("id", jobj1.get("id").toString());
				// pair.put("project", jobj1.get("project").toString());
				pair.put("key", jobj1.get("key").toString());
				// array.add(pair);
				String projectName = jobj1.get("name").toString();
				con = (HttpURLConnection) ((new URL(projectsMetrics + pair.get("key")).openConnection()));
				con.connect();
				con.getResponseCode();
				in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				content = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
					content.append(inputLine);
				}
				parser = new JSONParser();
				JSONObject projectList2 = (JSONObject) parser.parse(content.toString());
				logger.info("Second JSON object is:"+projectList2);
				JSONObject arr2 = (JSONObject) projectList2.get("component");

				JSONArray arr3 = (JSONArray) arr2.get("measures");
				logger.info("third array:"+arr3);
				for (int j = 0; j < 6; j++) {
					JSONObject jobj2 = (JSONObject) arr3.get(j);
					pair.put(jobj2.get("metric").toString(), jobj2.get("value"));
				}

				con = (HttpURLConnection) ((new URL(projectStatus + pair.get("key")).openConnection()));
				con.connect();
				con.getResponseCode();
				in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				content = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
					content.append(inputLine);
				}
				parser = new JSONParser();
				JSONObject projectList3 = (JSONObject) parser.parse(content.toString());
				
				JSONObject arr4 = (JSONObject) projectList3.get("projectStatus");
				String arr5 = (String) arr4.get("status");
				pair.put("status", arr4.get("status"));
				array.add(pair);

			}

			logger.debug("Finished extracting all the details ");

			con.disconnect();

			return Response.status(200).entity(array).build();
		} else {
			logger.error("The value was not valid");
			JSONArray projectList = new JSONArray();
			return Response.status(500).entity(projectList).build();
		
		}

	}

	public static void main(String[] args) {
		SonarQubeService obj = new SonarQubeService();
		Response result;
		try {
			result = obj.login();
			System.out.println(result);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}


	public void init() {
		 BasicConfigurator.configure();
		 PropertyUtility props = PropertyUtility.getPropertyUtility();
		 this.login = props.getProperty("login");
		 this.check_login = props.getProperty("check_login");
		 this.projects = props.getProperty("projects");
		 this.projectsMetrics = props.getProperty("projectsMetrics");
		 this.projectStatus = props.getProperty("projectStatus");
		 this.projectsIssues=props.getProperty("projectsIssues");
	}

	
	//SERVICE TO GET SOURCE CODE DETAILS FROM SONARQUBE
	@SuppressWarnings("unchecked")
	@GET
	@Path("/sonar_details")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProjectDetails(@QueryParam("projectname") String projName) throws MalformedURLException, IOException, ParseException{
		logger.info("Starting the login service");
		init();
		JSONObject pair = new JSONObject();
		JSONArray array = new JSONArray();
		HttpURLConnection con = (HttpURLConnection) ((new URL(login).openConnection()));
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		con.connect();
		//LOGIN REQUEST TO SERVER
		con = (HttpURLConnection) ((new URL(check_login).openConnection()));
		con.setDoOutput(true);
		con.connect();
		con.getResponseCode();
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

		String inputLine;
		StringBuffer content = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			content.append(inputLine);
		}
		logger.debug("Auth over parsing the result");
		JSONParser parser = new JSONParser();
		JSONObject jobj = (JSONObject) parser.parse(content.toString());
		Boolean value = (Boolean) jobj.get("valid");
		
		// con.disconnect();

		if (value) {
			logger.info("login successfull and forwarding request to get projects and details from SONARQUBE");
			//SENDING REQUEST TO SERVER TO GET PROJECTS AND DETAILS
			try {
				con = (HttpURLConnection) ((new URL(projects).openConnection()));
				con.setRequestMethod("GET");
				// con.setDoOutput(true);
				con.connect();

			} catch (java.io.IOException e) {
				
				logger.error("There was something wrong with the call to sonarQube Server, Check Credentials");
				
			}

			con.getResponseCode();
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			content = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				content.append(inputLine);
			}

			parser = new JSONParser();
			JSONObject projectList = (JSONObject) parser.parse(content.toString());
			logger.info("JSON object is:"+projectList);
			JSONArray arr = (JSONArray) projectList.get("components");
			logger.debug("getting the components list");

			for (int i = 0; i <arr.size(); i++) {
				pair = new JSONObject();
				JSONObject jobj1 = (JSONObject) arr.get(i);
				pair.put("name", jobj1.get("name").toString());
				pair.put("id", jobj1.get("id").toString());
				// pair.put("project", jobj1.get("project").toString());
				pair.put("key", jobj1.get("key").toString());
				// array.add(pair);
				String projectName = jobj1.get("name").toString();
				String projectkey = jobj1.get("key").toString();
				logger.info("key:"+projectkey);
				logger.info("projName:"+projName);
		if(projectkey.equals(projName)) {		
		con = (HttpURLConnection) ((new URL(projectsMetrics + pair.get("key")).openConnection()));
		con.connect();
		con.getResponseCode();
		in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		content = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			content.append(inputLine);
		}
		parser = new JSONParser();
		JSONObject projectList2 = (JSONObject) parser.parse(content.toString());
		logger.info("Second JSON object is:"+projectList2);
		JSONObject arr2 = (JSONObject) projectList2.get("component");

		JSONArray arr3 = (JSONArray) arr2.get("measures");
		logger.info("third array:"+arr3);
		for (int j = 0; j < 6; j++) {
			JSONObject jobj2 = (JSONObject) arr3.get(j);
			pair.put(jobj2.get("metric").toString(), jobj2.get("value"));
		}
		logger.info("getting project key has successufully");
		logger.info("making server call to get details of each project");
		//REQUESTING SERVER TO GET EACH PROJECT DETAILS USING KEY OR PROJECTID 
		con = (HttpURLConnection) ((new URL(projectStatus + pair.get("key")).openConnection()));
		con.connect();
		con.getResponseCode();
		in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		content = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			content.append(inputLine);
		}
		parser = new JSONParser();
		JSONObject projectList3 = (JSONObject) parser.parse(content.toString());
		
		JSONObject arr4 = (JSONObject) projectList3.get("projectStatus");
		String arr5 = (String) arr4.get("status");
		pair.put("status", arr4.get("status"));

		array.add(pair);
		}

	}

			logger.debug("Finished extracting all the details ");
		
			con.disconnect();
		
			return Response.status(200).entity(array).build();
		} else {
			logger.error("The value was not valid");
			JSONArray projectList = new JSONArray();
			return Response.status(500).entity(projectList).build();
		}
	}
	
	@SuppressWarnings("unchecked")
	@GET
	@Path("/issues_details")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSeverityDetails(@QueryParam("projectname") String projName) throws MalformedURLException, IOException, ParseException{
		logger.info("Starting the login service");
		init();
		JSONObject pair=new JSONObject();
		JSONArray array = new JSONArray();
		
		HttpURLConnection con = (HttpURLConnection) ((new URL(login).openConnection()));
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		con.connect();
		//LOGIN REQUEST TO SERVER
		con = (HttpURLConnection) ((new URL(check_login).openConnection()));
		con.setDoOutput(true);
		con.connect();
		con.getResponseCode();
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

		String inputLine;
		StringBuffer content = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			content.append(inputLine);
		}
		logger.debug("Auth over parsing the result");
		JSONParser parser = new JSONParser();
		JSONObject jobj = (JSONObject) parser.parse(content.toString());
		Boolean value = (Boolean) jobj.get("valid");
		
		// con.disconnect();

		if (value) {
			logger.info("login successfull and forwarding request to get projects and details from SONARQUBE");
			//SENDING REQUEST TO SERVER TO GET PROJECTS AND DETAILS
			try {
				con = (HttpURLConnection) ((new URL(projects).openConnection()));
				con.setRequestMethod("GET");
				// con.setDoOutput(true);
				con.connect();

			} catch (java.io.IOException e) {
				
				logger.error("There was something wrong with the call to sonarQube Server, Check Credentials");
				
			}
			con.getResponseCode();
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			content = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				content.append(inputLine);
			}
			parser = new JSONParser();
			JSONObject projectList = (JSONObject) parser.parse(content.toString());
			logger.info("JSON object is:"+projectList);
			JSONArray arr = (JSONArray) projectList.get("components");
			logger.info("key array:"+arr);
			logger.debug("getting the components list");
			JSONArray values=null;	
			for (int i = 0; i <arr.size(); i++) {
				JSONObject jobj1 = (JSONObject) arr.get(i);
				String projectName = jobj1.get("name").toString();
				String projectkey = jobj1.get("key").toString();
				pair.put("key", jobj1.get("key").toString());
				logger.info("key:"+projectkey);
				logger.info("projName:"+projName);
		if(projectkey.equals(projName)) {
			int count=1;
			long total=0;
			for (int h = 0; h < count; h++) {
				
			System.out.println(projectsIssues+pair.get("key")+"&p="+(h+1));
		con = (HttpURLConnection) ((new URL(projectsIssues+pair.get("key")+"&p="+(h+1)).openConnection()));
		con.connect();
		con.getResponseCode();
		in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		content = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			content.append(inputLine);
		}
		parser = new JSONParser();
		JSONObject projectList2 = (JSONObject) parser.parse(content.toString());
		logger.info("Second JSON object is:"+projectList2);
		if(h==0) {
		 total = (long)projectList2.get("total");
		}
		System.out.println(total);
		JSONArray arr4 = (JSONArray) projectList2.get("issues");
		logger.info("issues:"+arr4);
		/*JSONArray arr2 = (JSONArray) projectList2.get("facets");
		JSONArray arr3=null;*/
		for (int j = 0; j < arr4.size(); j++) {
			JSONObject objvalues=(JSONObject)arr4.get(j);
			logger.info("issueObject:"+objvalues);
			logger.info("j:"+j);
			JSONObject pair1=new JSONObject();
			pair1.put("rule",objvalues.get("rule"));
			pair1.put("line",objvalues.get("line"));
			pair1.put("status",objvalues.get("status"));
			pair1.put("severity",objvalues.get("severity"));
			pair1.put("type",objvalues.get("type"));
			pair1.put("componentId",objvalues.get("componentId"));
			pair1.put("creationDate",objvalues.get("creationDate"));
			pair1.put("updateDate",objvalues.get("updateDate"));
			if(objvalues.get("updateDate")!=null) {
				pair1.put("closeDate",objvalues.get("closeDate"));	
			}
			
			array.add(pair1);
		}
		int temp=1;
		long tot=0;
		tot=total;
		long page=500;
		for (int k = 0; k < temp; k++) {
			if(tot>page) {
				System.out.println("tot:"+tot);
				System.out.println("total:"+total);
				tot=tot-page;
				total=tot;
				System.out.println("tot:"+tot);
				if(tot>page) {
				temp=temp+1;
				}
				System.out.println("temp:"+temp);
				count=count+1;
				System.out.println("count:"+count);
			}	
		}
		
	}
		/*for (int j = 0; j < arr2.size(); j++) {
			JSONObject objvalues=(JSONObject)arr2.get(j);
			arr3=(JSONArray)objvalues.get("values");
			System.out.println(arr3);
			values=arr3;
			//pair.put("values", arr3);
				
			logger.info("values:"+values);
			logger.info("arr3:"+arr3);
			
		}*/
		logger.info("getting project key has successufully");
		logger.info("making server call to get details of each project");
		
		}

	}
			/*for (int j = 0; j <values.size(); j++) {
				JSONObject obj1=new JSONObject();
				System.out.println("j:"+j);
				JSONObject obj=(JSONObject)values.get(j);
				obj1.put("value", obj.get("val").toString());
				obj1.put("count",obj.get("count").toString());
				array.add(obj1);
				System.out.println("array:"+array);
			}*/

			logger.debug("Finished extracting all the details ");
		
			con.disconnect();
		
			return Response.status(200).entity(array).build();
		} else {
			logger.error("The value was not valid");
			JSONArray projectList = new JSONArray();
			return Response.status(500).entity(projectList).build();
		}
	}
	
}
