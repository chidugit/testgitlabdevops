package com.cirrus.devops.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Base64;

import javax.validation.constraints.NotNull;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import com.cirrus.devops.util.PropertyUtility;

@Path("/Authentication") 
public class Authentication {
	static final Logger logger = Logger.getLogger(Authentication.class);
	 private String authString;
	 private String url;
	 
	@SuppressWarnings({ "deprecation", "unchecked" })
	@POST 
	@Path("/Autherization")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAutherization(@NotNull @QueryParam("username") String userName ,@NotNull @QueryParam("password") String password) throws ClassNotFoundException{
		
		JSONObject autherization=new JSONObject();
		init();
		 Base64.Decoder decoder1 = Base64.getMimeDecoder(); 
		   String keydecoded1=new String(decoder1.decode(authString));
		   String[] arrOfStr1 = keydecoded1.split("!q4Ev");
		   String pass=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-1]));
		   String user=new String(decoder1.decode(arrOfStr1[arrOfStr1.length-2]));

	        try {
	        	Class.forName("org.postgresql.Driver");
	        	System.out.println("Driver version: " + org.postgresql.Driver.getVersion());
	        	Connection con = DriverManager.getConnection(url, user, pass);
                PreparedStatement pst = con.prepareStatement("SELECT * FROM iqusers ");
                ResultSet rs = pst.executeQuery();
	            while (rs.next()) {
	            	if(rs.getString(1).equals(userName) && rs.getString(2).equals(password)) {
	                System.out.print(rs.getString(1));
	                System.out.print(": ");
	                System.out.println(rs.getString(2));
	                autherization.put("Status","success");
	                logger.info("All the functions worked properly, exiting the function with an array object");
	        		return Response.status(200).entity(autherization).build();	
	            	}
	            }

	        } catch (SQLException ex) {
	        	ex.printStackTrace();
	        	ex.getMessage();
	        	ex.getErrorCode();
	        	logger.error("There was Problems while generating licence key");
	        	autherization.put("Status", "Autherization error");
				   return Response.status(500).entity(autherization).build();
	            
	        }
	        autherization.put("Status","unautherised");
    		autherization.put("Status","username/password is wrong");
    		return Response.status(401).entity(autherization).build();
	}
	
	  public void init(){
			
		   BasicConfigurator.configure();
		   PropertyUtility props = PropertyUtility.getPropertyUtility();
		   this.authString=props.getProperty("privatToken");
		   this.url=props.getProperty("dburl");
		   
	   }	
	
}
