package com.cirrus.devops.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.HashMap;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.cirrus.devops.util.PropertyUtility;

@Path("/deployment")
public class Deployment {
	static final Logger logger = Logger.getLogger(Deployment.class);
	String linuxPath;
	
	@POST 
	@Path("/createOnpremDetails")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createFile(InputStream json){
		init();
		HashMap<String,String> pair=new HashMap<String,String>();
		JSONObject json1=new JSONObject();
	       try {
	    	   String osname = System.getProperty("os.name", "").toLowerCase();
	    	   logger.debug("osname::"+System.getProperty("os.name", ""));
	    	   String filePath=System.getProperty("user.home")+"/iq-dev/onprem";
	    	   String linuxFilePath=linuxPath+"/onprem";
	    	   boolean createDir;
	    	   if (osname.startsWith("windows")) {
	    	   logger.debug("userhome::"+System.getProperty("user.home"));
		    	  createDir=  new File(filePath).mkdirs();
		    	 System.out.println(createDir);
		    	 String filename="/onprem("+LocalDateTime.now().toString().substring(0, LocalDateTime.now().toString().indexOf("T"))+"["+LocalDateTime.now().getHour()+"-"+LocalDateTime.now().getMinute()+"-"+LocalDateTime.now().getSecond()+"]"+").json";
					//String filename="/onprem.json";
						System.out.println(filename);
						File createNew=new File(filePath+filename);
					 boolean createFile = createNew.createNewFile();
					 System.out.println(createFile);
	    	   BufferedReader br;
				StringBuffer sb;
				sb= new StringBuffer();
				br = new BufferedReader(new InputStreamReader(json));
				String line;
				while((line=br.readLine())!=null){
					sb.append(line);
				}
				JSONParser parser = new JSONParser(); 
				json1 = (JSONObject) parser.parse(sb.toString());
				System.out.println(json1);
				FileWriter fw=new FileWriter(filePath+filename);  
				   fw.write(json1.toJSONString());
		           fw.close();
	    	   }else if(osname.startsWith("linux")) {   
	    		    createDir=  new File(linuxFilePath).mkdirs();
	    		    String filename="/onprem("+LocalDateTime.now().toString().substring(0, LocalDateTime.now().toString().indexOf("T"))+"["+LocalDateTime.now().getHour()+"-"+LocalDateTime.now().getMinute()+"-"+LocalDateTime.now().getSecond()+"]"+").json";
					//String filename="/onprem.json";
						System.out.println(filename);
						File createNew=new File(linuxFilePath+filename);
					 boolean createFile = createNew.createNewFile();
					 System.out.println(createFile);
	    	   BufferedReader br;
				StringBuffer sb;
				sb= new StringBuffer();
				br = new BufferedReader(new InputStreamReader(json));
				String line;
				while((line=br.readLine())!=null){
					sb.append(line);
				}
				JSONParser parser = new JSONParser(); 
				json1 = (JSONObject) parser.parse(sb.toString());
				System.out.println(json1);
				FileWriter fw=new FileWriter(linuxFilePath+filename, true);
				  fw.write(json1.toJSONString());
		           fw.close();
	    	   }
					
	       }
	       catch (Exception ex) {
	    	   logger.debug("deployment", ex.fillInStackTrace());
	    	   pair.put("Fail", "Unable to write data check logs for issue details");
				   return Response.status(500).entity(pair).build();
	        }
	       return Response.status(200).entity(json1).build();
	}
	@POST 
	@Path("/createAWSDetails")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createAWSFile(InputStream json){
		init();
		HashMap<String,String> pair=new HashMap<String,String>();
		JSONObject json1=new JSONObject();
	       try {
	    	   String osname = System.getProperty("os.name", "").toLowerCase();
	    	   logger.debug("osname::"+osname);
	    	   String filePath=System.getProperty("user.home")+"/iq-dev/AWS";
	    	   String linuxFilePath=linuxPath+"/AWS";
	    	   boolean createDir;
	    	   if (osname.startsWith("windows")) {
	    	   logger.debug(System.getProperty("user.home"));
		    	  createDir=  new File(filePath).mkdirs();
		    	 System.out.println(createDir);
		    	 String filename="/aws("+LocalDateTime.now().toString().substring(0, LocalDateTime.now().toString().indexOf("T"))+"["+LocalDateTime.now().getHour()+"-"+LocalDateTime.now().getMinute()+"-"+LocalDateTime.now().getSecond()+"]"+").json";
					System.out.println(filename);
					//String filename="/aws.json";
					File createNew=new File(filePath+filename);
				 boolean createFile = createNew.createNewFile();
				 System.out.println(createFile);
 	   BufferedReader br;
			StringBuffer sb;
			sb= new StringBuffer();
			br = new BufferedReader(new InputStreamReader(json));
			String line;
			while((line=br.readLine())!=null){
				sb.append(line);
			}
			JSONParser parser = new JSONParser(); 
			json1 = (JSONObject) parser.parse(sb.toString());
			System.out.println(json1);
			FileWriter fw=new FileWriter(filePath+filename);  
			   fw.write(json1.toJSONString());
	           fw.close();
	    	   }else if(osname.startsWith("linux")) {   
	    		    createDir=  new File(linuxFilePath).mkdirs();
	    		    String filename="/aws("+LocalDateTime.now().toString().substring(0, LocalDateTime.now().toString().indexOf("T"))+"["+LocalDateTime.now().getHour()+"-"+LocalDateTime.now().getMinute()+"-"+LocalDateTime.now().getSecond()+"]"+").json";
					System.out.println(filename);
					//String filename="/aws.json";
					File createNew=new File(linuxFilePath+filename);
				 boolean createFile = createNew.createNewFile();
				 System.out.println(createFile);
    	   BufferedReader br;
			StringBuffer sb;
			sb= new StringBuffer();
			br = new BufferedReader(new InputStreamReader(json));
			String line;
			while((line=br.readLine())!=null){
				sb.append(line);
			}
			JSONParser parser = new JSONParser(); 
			json1 = (JSONObject) parser.parse(sb.toString());
			System.out.println(json1);
			FileWriter fw=new FileWriter(linuxFilePath+filename);  
			   fw.write(json1.toJSONString());
	           fw.close();
	    	   }
						
	       }
	       catch (Exception ex) {
	    	   pair.put("Fail", "Unable to write data check logs for issue details");
				   return Response.status(500).entity(pair).build();
	        }
	       return Response.status(200).entity(json1).build();
	}
	
	  public void init(){
			
		   BasicConfigurator.configure();
		   PropertyUtility props = PropertyUtility.getPropertyUtility();
		   this.linuxPath=props.getProperty("linuxFileDirectory");
		   
	   }	
}
