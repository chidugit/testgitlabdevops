package com.cirrus.devops.service;  


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.cirrus.devops.ControllerServlet;
import com.cirrus.devops.util.PropertyUtility;

@SuppressWarnings("unchecked")
@Path("/LoginService") 
public class LoginService {
	
	static final Logger logger = Logger.getLogger(LoginService.class);
	 private String gitBaseUrl;
	 private String urlSuffix;
	 private String urlparameters;
	 static String username;
	 static String password;
	 static String access_token;
	 
   @GET 
   @Path("/Login")
   @Produces(MediaType.APPLICATION_JSON)
   public Response login(
		   @QueryParam("name") String username,
		   @QueryParam("password") String pass) { 
	   logger.info("login function Executing");
	   init();
	   LoginService.username=username;
	   LoginService.password=pass;
	   urlparameters = urlparameters.replaceAll("@username@", username);
	   urlparameters = urlparameters.replaceAll("@password@", pass);
	   
	   
	   String accessToken = null;
	   JSONObject response = new JSONObject();
	   try{
	    accessToken = getAccessToken();
	    
	    if (accessToken.equals("invalid_grant")) {
	    	response.put("Status",accessToken);
	    	return Response.status(401).entity(response).build();}
	    	else  if (accessToken.equals("Error")){
	    		response.put("Status", "Something went Wrong while Accessing gitlab");
	    		return Response.status(500).entity(response).build();
	    	}
	    
	   }catch(Exception e) {
		   logger.error("Something went Wrong with the Parsing of accesstoken",e);
		   response.put("Status", "Error getting details from the gitlab results");
		   return Response.status(500).entity(response).build();
	   }
	   
	   response.put("accessToken", accessToken);
	   return Response.status(200).entity(response).build();
   }  
   
   public void init(){
	   BasicConfigurator.configure();  
	   PropertyUtility props = PropertyUtility.getPropertyUtility();
	   this.gitBaseUrl = props.getProperty("git_url");
	   this.urlSuffix =  props.getProperty("urlSuffix");
	   this.urlparameters = props.getProperty("urlparameters");
	   
   }
   
   public String getAccessToken() {
	   try {
	   logger.info("Starting the getAccessToken Function");
	   HttpURLConnection con = (HttpURLConnection) 
				 ((new URL(gitBaseUrl+urlSuffix+urlparameters).openConnection()));

		 con.setDoOutput(true);
       con.setRequestMethod("POST");
       con.connect();
       int responseCode =con.getResponseCode();
       if(responseCode == 401) {
       	   return "invalid_grant";
          }
       
       BufferedReader in = new BufferedReader(
      		  new InputStreamReader(con.getInputStream()));
       
     
       
       String inputLine;
       StringBuffer content = new StringBuffer();
       while ((inputLine = in.readLine()) != null) {
      	    content.append(inputLine);
       }
       logger.debug("Getting the token from Gitlab and sending it back to the function");
      JSONParser parser = new JSONParser();
      JSONObject jobj = (JSONObject)parser.parse(content.toString());   
      
      logger.info("Exiting the details");
      access_token=jobj.get("access_token").toString();
      ControllerServlet.session.setAttribute("access_token",jobj.get("access_token").toString());
    return jobj.get("access_token").toString();
   }catch(IOException | ParseException e) {
	   return "Error";}
   }
   
   
   @GET 
   @Path("/userDetails")
   @Produces(MediaType.APPLICATION_JSON)
   public Response userDetails(){
	  
		BasicConfigurator.configure();
		logger.info("Starting the userDetails function");
		PropertyUtility props=PropertyUtility.getPropertyUtility();
		
		JSONObject userInfo = new JSONObject();
		JSONArray array = new JSONArray();

		String url=props.getProperty("git_url");
		String suffix = props.getProperty("git_user_suffix");
		try{
			String actualUrl=url+suffix;
			logger.info("Requesting to gitlab to get user details : " + actualUrl);
				username=(String) ControllerServlet.session.getAttribute("username");
				password=(String) ControllerServlet.session.getAttribute("userpassword");
				access_token=(String) ControllerServlet.session.getAttribute("access_token");
						 URL urlObject = new URL(url+suffix+"?access_token="+access_token);
			    	HttpURLConnection con = (HttpURLConnection) urlObject.openConnection();
			    	con.setRequestMethod("GET");
			    	String authString =  username+ ":" + password;
			    	String authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes());
			    	con.setRequestProperty("Authorization", "Basic " + authStringEnc);
			BufferedReader in = new BufferedReader(
		      		  new InputStreamReader(con.getInputStream()));
			 StringBuffer response = new StringBuffer();
			 String inputLine;
		     while ((inputLine = in.readLine()) != null) {
		     	response.append(inputLine);
		     	
		     } 
		     logger.info("Data Received:" + response);
          in.close();		
     JSONParser parser = new JSONParser(); 
     JSONArray jobj = (JSONArray)parser.parse(response.toString());
    logger.info("Json Object is " + jobj);
    // JSONArray arr = (JSONArray)jobj.get("actions");
     logger.info("Array data:"+jobj);
     logger.debug("Getting the job details from JUnit");
     for(int i=0;i<jobj.size();i++) 
     {
    	 userInfo = new JSONObject();
 	    	JSONObject jobj1 =(JSONObject)jobj.get(i);
 	    	logger.info("jobj1:"+jobj1);
 	    try {
 	    	userInfo.put("username", jobj1.get("username").toString());
 	    	userInfo.put("external", jobj1.get("external").toString());
 	    	userInfo.put("is_admin", jobj1.get("is_admin").toString());
     		array.add(userInfo);
     		logger.info("Array response:"+array);
 	    }catch(Exception e) {
 	    	continue;
 	    }
     }
     con.disconnect();
		}
		 catch(Exception e){
		   	  e.printStackTrace();
					logger.error("There was Problems while getting the details from gitlab");
					userInfo.put("Status", "Error getting the user details");
				
					   return Response.status(500).entity(userInfo).build();
				}
				logger.info("All the functions worked properly, exiting the function with an array object");
				return Response.status(200).entity(array).build();	
	}
		
   }
   
