package com.cirrus.devops;

import java.io.IOException;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.cirrus.devops.service.LicenceKey;

import javax.faces.application.ResourceHandler;
/**
 * Servlet Filter implementation class NoCacheFilter
 */
@WebFilter("/NoCacheFilter")
public class NoCacheFilter implements Filter {
	final static Logger logger = Logger.getLogger(NoCacheFilter.class);
    /**
     * Default constructor. 
     */
	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		logger.info("cash filter called");
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        // Skip JSF resources (CSS/JS/Images/etc)
        if (!req.getRequestURI().startsWith(req.getContextPath() + ResourceHandler.RESOURCE_IDENTIFIER)) {
            /*// HTTP 1.1.
            res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            // HTTP 1.0.
            res.setHeader("Pragma", "no-cache");
            // Proxies.
            res.setDateHeader("Expires", 0);*/
        	 HttpServletResponse resp = (HttpServletResponse) response;
             resp.setHeader("Expires", "Tue, 03 Jul 2001 06:00:00 GMT");
             resp.setDateHeader("Last-Modified", new Date().getTime());
             resp.setHeader("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
             resp.setHeader("Pragma", "no-cache");

        }
        res.setHeader("Expires", "Tue, 03 Jul 2001 06:00:00 GMT");
        res.setDateHeader("Last-Modified", new Date().getTime());
        res.setHeader("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
        res.setHeader("Pragma", "no-cache");

/*        res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        res.setHeader("Pragma", "no-cache");
        res.setDateHeader("Expires", 0);*/
        chain.doFilter(request, response);
        logger.info("cashe filter ended");
    }
	
	
    public NoCacheFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}


	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
