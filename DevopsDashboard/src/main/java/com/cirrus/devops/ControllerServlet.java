 package com.cirrus.devops;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.cirrus.devops.projects.Projects;
import com.cirrus.devops.service.database.Licensing;
import com.cirrus.devops.service.database.createTable.CreateTable;
import com.cirrus.devops.service.schedulexecution.ScheduledExecutor;
import com.cirrus.devops.util.PropertyUtility;
import com.jcraft.jsch.JSchException;

public class ControllerServlet extends HttpServlet{

	/**

	 * 
	 */
	String name;
	String password;
	String serverUrl;
	static final Logger logger = Logger.getLogger(ControllerServlet.class);
	private static final long serialVersionUID = 1L;
	public static HttpSession session;
	ArrayList<Projects> ListofProjects;
	String Token ;
	HttpServletRequest request;
	HttpServletResponse response;
	HttpPost postRequest;
	int gitStat=0;
	double jenStat=0;
	double sonStat=0;
	double testStat=0;
	String deployDecoded;
	String secDecoded;
	String validity;
	
	@Override
	public void init() throws ServletException {
		super.init();
		CreateTable ct=new CreateTable();
		 new ScheduledExecutor().scheduleRegularly(() -> {
			 try {
				 ct.createTables();
				 	Licensing ls=new Licensing();
				 	LocalDateTime expireCheckdate=ls.getExpireDate();
				 	/*JSONObject licenseData=ls.getModuleData();
				 	LocalDateTime expireCheckdate1=LocalDateTime.parse(licenseData.get("expiredate").toString());
				 	String deploy=null;
				 	String security=null;
				 	if(request!=null) {
				 	deploy=licenseData.get("deployment").toString();
				 	}if(request!=null) {
				 	security=licenseData.get("security").toString();}
				 	System.out.println(expireCheckdate);
				 	System.out.println(expireCheckdate1);*/
				 	if(expireCheckdate!=null) {
				 		int ret=LocalDateTime.now().compareTo(expireCheckdate);
				 		if(ret>0) {
				 			boolean updatelicense=ls.updateLicenseValidity();
				 			if(updatelicense==true) {
				 				logger.info("License expired");	
				 			}
				 		}
					 }
			} catch (ClassNotFoundException | ParseException | IOException | JSchException e) {
				e.printStackTrace();
			}

   		}, LocalDateTime.now(), thisTime -> thisTime.plusHours(10));
	}
	
	private Integer getGitlabResults() {
		
		logger.info("getGitlabResults was called");
		Integer statusCode = null;
		StringBuilder result = new StringBuilder();
		
		PropertyUtility props = PropertyUtility.getPropertyUtility();
		String loginUrl = props.getProperty("loginUrl");
		String appurl =  props.getProperty("appUrl");
		loginUrl =loginUrl.replaceAll("@username@", name);
		loginUrl =loginUrl.replaceAll("@password@", password);
		//String ctxPath=this.getServletContext().getServletContextName(); //          getRealPath(this.getServletContext().getContextPath());
		 //logger.warn(ctxPath);
		 //logger.warn(appurl+loginUrl);
		 System.out.println(serverUrl);
		 System.out.println(appurl+loginUrl);
		 HttpGet getRequest = new HttpGet(serverUrl+appurl+loginUrl);
		 
		 JSONParser parser = new JSONParser();
		 CloseableHttpClient client = HttpClients.createDefault();
		 CloseableHttpResponse gitResponseToken;
		try {
			gitResponseToken = client.execute(getRequest);
			statusCode = gitResponseToken.getStatusLine().getStatusCode();
			if (statusCode == 500) {
				return 500;
			}else if (statusCode == 401) {
				return 401;
			}
			//InputStreamReader istReader=new InputStreamReader(gitResponseToken.getEntity().getContent());
			BufferedReader rd = new BufferedReader(new InputStreamReader(gitResponseToken.getEntity().getContent()));
			String line;
			logger.debug("Making the call to the LoginService function");
			try {
				while ((line = rd.readLine()) != null) {
					result.append(line);
				}
			}catch(Exception e){
				logger.error("Something with wrong with the parsing of result");
				logger.error(e);
				return 404;}
			finally {
				//istReader.close();
				rd.close();
			}
		
			if (result.toString() != null && !result.toString().isEmpty()) {
				try{
						
						JSONObject objResp = (JSONObject)parser.parse(result.toString());
						Token=objResp.get("accessToken").toString();
						//session.setAttribute("access_token",(Object)Token);
						session.setAttribute("access_token", Token);
						logger.debug("Getting the token from LoginService"+ Token);
					}catch(Exception e){
						logger.error("There was some issue with parsing the data",e);
						return 404;
					}
				
			}
		} catch (ClientProtocolException e) {
			logger.error("There was some issue with the execution of gitRespontoken URL");
			logger.error(e);
			return 500;
		} catch (IOException e) {
			logger.error("There was some issue with the execution of gitRespontoken URL");
			logger.error(e);
			return 500;
		}
		return getProjects(Token);
	}
	

	public Integer getProjects(String access_token) {
	logger.debug("After getting the token, now accessing the Projects");
	StringBuilder result = new StringBuilder();
	PropertyUtility props = PropertyUtility.getPropertyUtility();
	String projectUrl = props.getProperty("projectUrl");
	Integer statusCode = null;
	projectUrl = projectUrl.replaceAll("@accessToken@",access_token);
	String appurl =  props.getProperty("appUrl");

	 //appurl+projectUrl
	 //"http://localhost:8080/DevopsDashboard/rest/GitService/projects?accessToken=e45a837579c58f2af0e9bb9e77e28118deaf11d853de08896faf28613d2e653d");
	 HttpGet getRequest = new HttpGet(serverUrl+appurl+projectUrl);
	 JSONParser parser = new JSONParser();
	 
	//getRequest = new HttpGet(appurl+projectUrl);
	 parser = new JSONParser();
	 CloseableHttpClient client = HttpClients.createDefault();
	 CloseableHttpResponse gitResponseToken;
	 client = HttpClients.createDefault();
	 try {
		 
	 gitResponseToken = client.execute(getRequest);
	 statusCode = gitResponseToken.getStatusLine().getStatusCode();
		BufferedReader rd;
		//InputStreamReader istReader=new InputStreamReader(gitResponseToken.getEntity().getContent());
			rd = new BufferedReader(new InputStreamReader(gitResponseToken.getEntity().getContent()));
			String line;
			result=new StringBuilder();		
			try {
				while ((line = rd.readLine()) != null) {
					result.append(line);
				}
			}
			finally {
				//istReader.close();
				rd.close();
			}
		} catch (IllegalStateException | IOException e1) {
			logger.error("Error while Executing Parsing the data from git Projects");
			e1.printStackTrace();
			return 500;
		}
		
		
		if (result.toString() != null && !result.toString().isEmpty()) {
			if (statusCode.equals(200)) {
				try{	
				logger.debug("The url execution was successful, now parsing the result..");
				JSONArray objResp = (JSONArray)parser.parse(result.toString());
				
				Projects object;
				ListofProjects = new ArrayList<Projects>();
				gitStat=objResp.size();
				for(int i = 0;i<objResp.size();i++) {
					logger.debug("Now putting the data into the ListofProjects Object");
					object = new Projects();
					JSONObject gitlabiter = (JSONObject)objResp.get(i);
					
					object.setProjectName(gitlabiter.get("name").toString());
					
					
					object.setgitlabReturns(gitlabiter);
					this.ListofProjects.add(object);
				}
				}catch(Exception e) {
					logger.error("There was an exeception while making the call to GitService/projects",e);
					return 500;
				}
			}
		}	 	
		logger.info("Everything worked as expected, finished with getgitResults Function");
		return 200;
	}
	
	private boolean getJenkinsResults(String name,String password,HttpSession session ){
		/*String username=session.getAttribute("username").toString();
		String userpassword=session.getAttribute("userpassword").toString();
		System.out.println("Starting with getJenkinsResults function");
		System.out.println("name:"+username+"password:"+userpassword);*/
		logger.info("Starting with getJenkinsResults function ");
		PropertyUtility props = PropertyUtility.getPropertyUtility();
		String jenkinUrl = props.getProperty("jenkinProjectUrl");
		jenkinUrl=jenkinUrl.replaceAll("@username@", name);
		jenkinUrl=jenkinUrl.replaceAll("@password@", password);
		String appurl =  props.getProperty("appUrl");
		HttpGet GetRequest = new HttpGet(serverUrl+appurl+jenkinUrl);
		CloseableHttpClient client = HttpClients.createDefault();
		Integer jenkinstatusCode=null;
		CloseableHttpResponse jenkinresp;
		try {
			logger.debug("making a call to jenkinsService");
			jenkinresp = client.execute(GetRequest);
		
		 	jenkinstatusCode = jenkinresp.getStatusLine().getStatusCode();
		 	//InputStreamReader istReader=new InputStreamReader(jenkinresp.getEntity().getContent());
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(jenkinresp.getEntity().getContent()));
			String jenkinline;
			StringBuilder jenkinResult = new StringBuilder();
			jenkinstatusCode = jenkinresp.getStatusLine().getStatusCode();
			if (jenkinstatusCode != 200) {
				logger.error("There was something wrong with the jenkins call");
				return false;
			}
			try {
				while ((jenkinline = reader.readLine()) != null) {
					jenkinResult.append(jenkinline);
				}
			}
			catch(Exception e){
				logger.error("There was something wrong with the jenkins call");
				logger.error(e);
				return false;
			}
			finally {
				//istReader.close();
				reader.close();
			}
		
		JSONParser parser = new JSONParser();
		JSONArray jenkinresponse = (JSONArray)parser.parse(jenkinResult.toString());
		//this.jenStat=jenkinresponse.size();
		double jen=jenkinresponse.size();
		double jen1=jenkinresponse.size();
		for (int i = 0; i < jenkinresponse.size(); i++) {
			JSONObject jobj1 =(JSONObject)jenkinresponse.get(i);
			if(jobj1.get("failcount")!=null&&jobj1.get("skipcount")!=null) {
			if(jobj1.get("failcount").toString().equalsIgnoreCase("0")&&jobj1.get("skipcount").toString().equalsIgnoreCase("0")) {
			}else {
				System.out.println("befor"+jen1);
				jen1=jen1-1;
				System.out.println("after"+jen1);
				}
			}
			if(jobj1.get("color").equals("red")){
				jen=jen-1;
			}
		}
		this.testStat=(jen1/jenkinresponse.size())*100;
		this.jenStat=(jen/jenkinresponse.size())*100;
		for (int i=0;i<this.ListofProjects.size();i++) {
			Projects projectObject=(Projects)this.ListofProjects.get(i);
			for (int j=0;j<jenkinresponse.size();j++) {
				JSONObject jenkinsObject=(JSONObject)jenkinresponse.get(j);
				logger.debug("matching the project name and adding the jenkins result");
				if ((projectObject.getprojectNaame().toLowerCase()).contains((jenkinsObject.get("name").toString().toLowerCase()))){
					this.ListofProjects.get(i).setjenkinsReturns(jenkinsObject);
				}
			}
		}
	} catch (IOException |ParseException  e1) {
		logger.error("catching the possible errors that is IOexception and ParseException");
		}
		logger.info("Exiting the jenkins function" );
		return true;
	}

	private boolean getsonarResults() {
		logger.info("starting with getsonarResults function");
		PropertyUtility props = PropertyUtility.getPropertyUtility();
		String sonarUrl = props.getProperty("sonarProjecturl");
		String appurl =  props.getProperty("appUrl");
		HttpGet GetRequest = new HttpGet(serverUrl+appurl+sonarUrl);
		CloseableHttpClient client = HttpClients.createDefault();
		Integer sonarStatusCode=null;
		
		CloseableHttpResponse sonarResponse;
		try {
			logger.debug("Making a call to the sonarQube Service");
			sonarResponse = client.execute(GetRequest);
			sonarStatusCode = sonarResponse.getStatusLine().getStatusCode();
		 	logger.info("sonar status code:"+sonarStatusCode); 
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(sonarResponse.getEntity().getContent()));
			String sonarline;
			StringBuilder sonarResult = new StringBuilder();
			try {
				while ((sonarline = reader.readLine()) != null) {
					sonarResult.append(sonarline);
				}
			} finally {
				reader.close();
			}
		logger.info("Getting the results back from sonarQube");
		JSONParser parser= new JSONParser();
		JSONArray sonarArray=(JSONArray)parser.parse(sonarResult.toString());
		//this.sonStat=sonarArray.size();
		double son=sonarArray.size();
		for (int i = 0; i < sonarArray.size(); i++) {
			JSONObject sonar =(JSONObject)sonarArray.get(i);
			if(sonar.get("status").equals("ERROR")){
				son=son-1;
			}
		}
		this.sonStat=(son/sonarArray.size())*100;
		for (int i=0;i<this.ListofProjects.size();i++) {
			Projects projectObject=(Projects)this.ListofProjects.get(i);
			for (int j=0;j<sonarArray.size();j++) {
				JSONObject sonarIterator=(JSONObject)sonarArray.get(j);
				logger.debug("Matching the project name with the jenkins job name");
				if (projectObject.getprojectNaame().contains(sonarIterator.get("name").toString())) {
					this.ListofProjects.get(i).setsonarResults(sonarIterator);	
				}
			}
		}
		
		} catch (IOException | ParseException e) {
			e.printStackTrace();
			logger.error("There was an error with the sonarService call");
		}
		
		logger.info("Everthing went as expected, returning from the sonar Function ");
		return true;
	}
	
	@SuppressWarnings("unused")
	private boolean getTestResults() {
		logger.info("Starting with the getTestResults function");
		PropertyUtility props = PropertyUtility.getPropertyUtility();
		String testUrl = props.getProperty("testProjectUrl");
		String appurl =  props.getProperty("appUrl");
		HttpGet GetRequest = new HttpGet(serverUrl+appurl+testUrl);
		CloseableHttpClient client = HttpClients.createDefault();
		Integer testStatusCode=null;
		
		
		CloseableHttpResponse testServiceResponse;
		try {
			testServiceResponse = client.execute(GetRequest);
			testStatusCode = testServiceResponse.getStatusLine().getStatusCode();
			//InputStreamReader istReader=new InputStreamReader(testServiceResponse.getEntity().getContent());
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(testServiceResponse.getEntity().getContent()));
			String testline;
			StringBuilder testResult = new StringBuilder();
			try {
				while ((testline = reader.readLine()) != null) {
					testResult.append(testline);
				}
			}catch(Exception e) {return false;
			} finally {
				//istReader.close();
				reader.close();
			}
		logger.debug("getting the results from testService");
		JSONParser parser= new JSONParser();
		JSONArray testArray=(JSONArray)parser.parse(testResult.toString());
		
		
		for (int i=0;i<this.ListofProjects.size();i++) {
			Projects projectObject=(Projects)this.ListofProjects.get(i);
			for (int j=0;j<testArray.size();j++) {
				JSONObject testIterator=(JSONObject)testArray.get(j);
				if (projectObject.getprojectNaame().toLowerCase().contains(testIterator.get("ProjectName").toString().toLowerCase())) {
					logger.debug("matching the project name with the test result match");
					this.ListofProjects.get(i).settestResults(testIterator);	
				}
			}
		}
		
		} catch (IOException | ParseException e) {
			logger.error("There was some project with the execution of test results service ");
			return false;
		}
		
		logger.info("getting the proper results from testcases, exiting the test Function");
		return true;
	}
	
//Get License Details	
	@SuppressWarnings("unchecked")
	public JSONObject getLicenseDetails() {
		JSONObject objResp = new JSONObject();
		logger.debug("After getting the token, now accessing the Projects");
		StringBuilder result = new StringBuilder();
		PropertyUtility props = PropertyUtility.getPropertyUtility();
		String licenseDetails = props.getProperty("licenseDetails");
		Integer statusCode = null;
		String appurl =  props.getProperty("appUrl");

		 //appurl+projectUrl
		 //"http://localhost:8080/DevopsDashboard/rest/GitService/projects?accessToken=e45a837579c58f2af0e9bb9e77e28118deaf11d853de08896faf28613d2e653d");
		 HttpGet getRequest = new HttpGet(serverUrl+appurl+licenseDetails);
		 JSONParser parser = new JSONParser();
		 
		//getRequest = new HttpGet(appurl+projectUrl);
		 parser = new JSONParser();
		 CloseableHttpClient client = HttpClients.createDefault();
		 CloseableHttpResponse licenseResponse;
		 client = HttpClients.createDefault();
		 try {
			 
		 licenseResponse = client.execute(getRequest);
		 System.out.println("git response token:"+licenseResponse);
		 statusCode = licenseResponse.getStatusLine().getStatusCode();
		 System.out.println("git status code:"+statusCode);
		 if(statusCode==204) {
			 objResp.put("status", "license does not exist");
			 return objResp; 
		 }else {
			BufferedReader rd;
			//InputStreamReader istReader=new InputStreamReader(licenseResponse.getEntity().getContent());
				rd = new BufferedReader(new InputStreamReader(licenseResponse.getEntity().getContent()));
				String line;
				result=new StringBuilder();		
				try {
					while ((line = rd.readLine()) != null) {
						result.append(line);
					}
				}
				finally {
					//istReader.close();
					rd.close();
				}
		 }
			} catch (IllegalStateException | IOException e1) {
				logger.error("Error while Executing Parsing the data ");
				e1.printStackTrace();
				objResp.put("error", "error getting data");
				return objResp;
			}
			
			
			if (result.toString() != null && !result.toString().isEmpty()) {
				if (statusCode.equals(200)) {
					try{	
					logger.debug("The url execution was successful, now parsing the result..");
					objResp = (JSONObject) parser.parse(result.toString());
					}catch(Exception e) {
						logger.error("There was an exeception while making the call to database/",e);
						objResp.put("error", "error getting data");
						return objResp;
					}
				}
			}	 	
			
			logger.info("Everything worked as expected, finished with getgitResults Function");
			return objResp;
		}


	//Get access level from the gitlab
public String getAccessLevel(String username) {
	 PropertyUtility props = PropertyUtility.getPropertyUtility();
	 String projectUrl = props.getProperty("userAuth");
		String appurl =  props.getProperty("appUrl");
	  try {
			URL url = new URL(serverUrl+appurl+projectUrl+"?key="+username);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
			 StringBuffer response = new StringBuffer();
			 String inputLine;
		     while ((inputLine = br.readLine()) != null) {
		     	response.append(inputLine);
		     	
		     } 
		     logger.info("Data Received:" + response);
		     br.close();		
     JSONParser parser = new JSONParser(); 
     JSONObject jobj = (JSONObject)parser.parse(response.toString());
    logger.info("Json Object is " + jobj);
    	 try {
 	    		String access=jobj.get("access").toString();
 	    		if(access.equalsIgnoreCase("Admin")) {
 	    				return "admin";
 	    			}else if(access.equalsIgnoreCase("user")){
 	    				return "user";
 	    			}
 	    }catch(Exception e) {
 	    	e.printStackTrace();
 	    }
			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
			}

			conn.disconnect();

		  } catch (Exception e) {

			e.printStackTrace();

		  }
	  return "Access level not found";
}
//End of getAccesslevel method	

//first Licence updating
@SuppressWarnings({ "unchecked", "null" })
public JSONObject firstUpdateLicence(String licenceKey) {
	 PropertyUtility props = PropertyUtility.getPropertyUtility();
	 String licenceurl = props.getProperty("licenceUrl");
		String appurl =  props.getProperty("appUrl");
		JSONObject jobj = new JSONObject();
	  try {
		  String urlstring=serverUrl+appurl+licenceurl+"?licencekey="+licenceKey;
			URL url = new URL(urlstring);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() == 409) {
				jobj.put("status", "exist");
				System.out.println(jobj);
				return jobj; 
			}else if(conn.getResponseCode() == 500) {
				return (JSONObject) jobj.put("status", "fail");
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
			 StringBuffer response = new StringBuffer();
			 String inputLine;
		     while ((inputLine = br.readLine()) != null) {
		     	response.append(inputLine);
		     	
		     } 
		     br.close();		
    JSONParser parser = new JSONParser(); 
    jobj = (JSONObject)parser.parse(response.toString());
			conn.disconnect();

		  } catch (Exception e) {

			e.printStackTrace();

		  }
	return jobj;
}

//Update license
@SuppressWarnings({ "unchecked" })
public JSONObject updateLicence(String licenceKey) {
	 PropertyUtility props = PropertyUtility.getPropertyUtility();
	 String licenceurl = props.getProperty("updatelicenceUrl");
		String appurl =  props.getProperty("appUrl");
		JSONObject jobj = new JSONObject();
	  try {
		  String urlstring=serverUrl+appurl+licenceurl+"?licencekey="+licenceKey;
			URL url = new URL(urlstring);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("PUT");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() == 409) {
				/*throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());*/
				jobj.put("status", "exist");
				System.out.println(jobj);
				return jobj; 
			}else if(conn.getResponseCode() == 500) {
				return (JSONObject) jobj.put("status", "fail");
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
			 StringBuffer response = new StringBuffer();
			 String inputLine;
		     while ((inputLine = br.readLine()) != null) {
		     	response.append(inputLine);
		     	
		     } 
		     br.close();		
		     JSONParser parser = new JSONParser(); 
		     jobj = (JSONObject)parser.parse(response.toString());
			conn.disconnect();

		  } catch (Exception e) {

			e.printStackTrace();

		  }
	return jobj;
}

//LDAP authentication
private int ldapAuthentication(String username, String password) {
	logger.info("Starting with the ldap authentication");
	PropertyUtility props = PropertyUtility.getPropertyUtility();
	String ldap = props.getProperty("ldap");
	String appurl =  props.getProperty("appUrl");
	HttpPost postRequest = new HttpPost(serverUrl+appurl+ldap+"?username="+username+"&password="+password);
	CloseableHttpClient client = HttpClients.createDefault();
	Integer testStatusCode=null;
	
	
	CloseableHttpResponse testServiceResponse;
	try {
		testServiceResponse = client.execute(postRequest);
		testStatusCode = testServiceResponse.getStatusLine().getStatusCode();
	 	
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(testServiceResponse.getEntity().getContent()));
		String testline;
		StringBuilder testResult = new StringBuilder();
		try {
			while ((testline = reader.readLine()) != null) {
				testResult.append(testline);
			}
		}catch(Exception e) {return -2;
		} finally {
			reader.close();
		}
	logger.debug("getting the results from ldap");
	JSONParser parser= new JSONParser();
	JSONObject testArray=(JSONObject)parser.parse(testResult.toString());
	if(testArray.get("Status").toString().equalsIgnoreCase("success")) {
		return 1;
	}else if(testArray.get("Status").toString().equalsIgnoreCase("fail")){
		return 0;
	}
	} catch (IOException | ParseException e) {
		logger.error("There was some project with the execution of ldap service ");
		return -2;
	}
	return -1;
}

private boolean systemAddress(String sysId) {
	logger.info("Starting system id function");
	PropertyUtility props = PropertyUtility.getPropertyUtility();
	String sysAddress = props.getProperty("sysId");
	String appurl =  props.getProperty("appUrl");
	HttpGet getRequest = new HttpGet(serverUrl+appurl+sysAddress);
	CloseableHttpClient client = HttpClients.createDefault();
	Integer testStatusCode=null;
	
	CloseableHttpResponse testServiceResponse;
	try {
		testServiceResponse = client.execute(getRequest);
		testStatusCode = testServiceResponse.getStatusLine().getStatusCode();
	 	
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(testServiceResponse.getEntity().getContent()));
		String testline;
		StringBuilder testResult = new StringBuilder();
		try {
			while ((testline = reader.readLine()) != null) {
				testResult.append(testline);
			}
		}catch(Exception e) {return false;
		} finally {
			reader.close();
		}
	logger.debug("getting the results from testService");
	JSONParser parser= new JSONParser();
	JSONArray testArray=(JSONArray)parser.parse(testResult.toString());
	System.out.println(testArray);
	for (int i = 0; i < testArray.size(); i++) {
		System.out.println(testArray.get(i));
		if(testArray.get(i).toString().equalsIgnoreCase(sysId)) {
			return true;
		}
	}
	} catch (IOException | ParseException e) {
		logger.error("There was some project with the execution of test results service ");
		return false;
	}
	return false;
}

	@SuppressWarnings({ "deprecation", "unchecked" })
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{ 
		serverUrl = request.getRequestURL().toString();
		serverUrl = serverUrl.substring(0,serverUrl.indexOf("/web"));
		System.out.println(request.getContextPath());
		logger.info("starting the doPost Function");
		this.request=request;
		this.response=response;
		//CONDITION TO CREATE PROJECT WHERE ACTION IS "login"
		if(request.getParameter("action").equals("login"))
			{
			 name=request.getParameter("name");  
			 password=request.getParameter("password");
			 int ldapret=ldapAuthentication(name, password);
			 if(ldapret==1) {
					//LICENCING CHECK
				 JSONObject licensedet=getLicenseDetails();
				//System.out.println(licensedet);
			 if(licensedet!=null) {
				if(licensedet.get("deployment")!=null) {
					 deployDecoded=licensedet.get("deployment").toString();
				}else{
					deployDecoded="noDeploy";
				}if(licensedet.get("security")!=null){ secDecoded=licensedet.get("security").toString();
				}else{
					secDecoded="noSecurity";
				}if(licensedet.get("validity")!=null){ validity=licensedet.get("validity").toString();
				}else{validity="false";}
			}
					  // if(validity.equalsIgnoreCase("true")) {
					
					 String userid=password;
					 Base64.Encoder encoder = Base64.getMimeEncoder();  
					 userid = encoder.encodeToString(userid.toString().getBytes());
					 userid="!40!"+userid+"!q4Ev";
					 userid = encoder.encodeToString(userid.toString().getBytes());
					 session=request.getSession();
					 logger.debug("Starting the session 1");
					 session.setAttribute("username", name);
					 session.setAttribute("userpassword",password);
					 session.setAttribute("serverPath", serverUrl);
					 session.setAttribute("userid", userid);
					 logger.debug("Making a call to all the component functions");
				
				Integer gitlabFlag= this.getGitlabResults();
				if(gitlabFlag == 200) {
							//this.getJenkinsResults(name, password, session);
							session.setAttribute("Deploy", deployDecoded);
							session.setAttribute("security", secDecoded);
							session.setAttribute("validity", validity);
							logger.info("access level set successfully");
							String usernameencoded = encoder.encodeToString(request.getParameter("name").toString().getBytes());
							 String passwordencoded = encoder.encodeToString(request.getParameter("password").toString().getBytes());
							 String validityencoded = encoder.encodeToString(validity.toString().getBytes());
							String name1=getAccessLevel(name);
							 String accessencoded = encoder.encodeToString(name1.getBytes());
							 String sessionid=usernameencoded+"!q4Ev"+passwordencoded+"!q4Ev"+accessencoded+"!q4Ev"+validityencoded;
							 String sessionidencoded=encoder.encodeToString(sessionid.toString().getBytes());
							 String urlencodedsessionidencoded=URLEncoder.encode(sessionidencoded, StandardCharsets.UTF_8.toString());
							 Cookie IQSESSIONID=new Cookie("IQSESSIONID", urlencodedsessionidencoded);
							 //IQSESSIONID.setMaxAge(60*60);
							 IQSESSIONID.setPath(request.getContextPath());
								response.addCookie(IQSESSIONID);
								Cookie path=new Cookie("serverPath", serverUrl);
								path.setPath(request.getContextPath());
								response.addCookie(path);
							session.setAttribute("Access_level",name1);
							//System.out.println("ListofProjects:"+this.ListofProjects);
							session.setAttribute("ProjectsList", this.ListofProjects);
							request.getRequestDispatcher("../Home.jsp").forward(request, response);
							//request.getRequestDispatcher("../DashBoard.jsp").forward(request, response);
			}else if(gitlabFlag==401) {
				request.setAttribute("errorMessage", "Unable to get response from repository (check repository credentials)");
				request.getRequestDispatcher("/index.jsp").forward(request, response);
			}else if(gitlabFlag==500) {
				request.setAttribute("errorMessage", "Repository sever is not responding(500 error)");
				request.getRequestDispatcher("/index.jsp").forward(request, response);
			}
				}else if(ldapret==0){
					request.setAttribute("errorMessage", "Invalid username or password");
					request.getRequestDispatcher("/index.jsp").forward(request, response);
				}else {
					request.setAttribute("errorMessage", "Unable to connect to AD(check your connection)");
					request.getRequestDispatcher("/index.jsp").forward(request, response);
				}
					  /* }
				else{
					request.setAttribute("errorMessage", "license has expired");
					request.getRequestDispatcher("/index.jsp").forward(request, response);
				}*//*else if(ret==1) {
					request.setAttribute("errorMessage", "license has expired");
					request.getRequestDispatcher("/index.jsp").forward(request, response);
				}*/
			}
//ELSE CONDITION TO LOGIN WHERE ACTION IS "create_Project"
else if (request.getParameter("action").equals("create_Project")) {
	/*FileInputStream in1 = new FileInputStream(this.getClass().getClassLoader().getResource("config.properties").getFile());
	   Properties props1 = new Properties();
	   props1.load(in1);
	   in1.close();

	   FileOutputStream out1 = new FileOutputStream(this.getClass().getClassLoader().getResource("config.properties").getFile());
	   props1.setProperty("tomUrl", serverUrl);
	   props1.store(out1, null);
	   out1.close();*/
	logger.info("Inside create project");
	String project_name=request.getParameter("project_name");
	String description=request.getParameter("description");
	String groupname=request.getParameter("group_name");
	String grouppath=request.getParameter("group_path");
	String industry=request.getParameter("industry");
	String visibility=request.getParameter("visibility");
	description = URLEncoder.encode(description);
	project_name = URLEncoder.encode(project_name);
	
	groupname = URLEncoder.encode(groupname);
	grouppath = URLEncoder.encode(grouppath);
	
	
	PropertyUtility props = PropertyUtility.getPropertyUtility();
	String appurl =  props.getProperty("appUrl");
	String createUrl =  props.getProperty("createUrl");
	createUrl =createUrl.replaceAll("@accessToken@", (String)session.getAttribute("access_token"));
	if(!groupname.isEmpty()&&!grouppath.isEmpty()){
	    
	    postRequest = new HttpPost(serverUrl+appurl+createUrl+"&project_name="+project_name+"&industry="+industry+"&group_name="+groupname+"&group_path="+grouppath+"&description="+description+"&visibility="+visibility);

	}else{
		
		postRequest= new HttpPost(serverUrl+appurl+createUrl+"&project_name="+project_name+"&industry="+industry+"&description="+description+"&visibility="+visibility);
		
	}
	System.out.println(postRequest);
	 JSONParser parser = new JSONParser();
	 
	 CloseableHttpClient client = HttpClients.createDefault();
	 CloseableHttpResponse project;
	// java.net.URI location = new java.net.URI("../Home.jsp");
	try {
		 project = client.execute(postRequest);
		 logger.info("Testing");
		int statusCode = project.getStatusLine().getStatusCode();
		logger.info("status code "+statusCode);
		if (statusCode == 500) {
			logger.info("Error creating project");
			request.getRequestDispatcher("../error.jsp").forward(request, response);
		}else if (statusCode == 201) {
			logger.info("Project created succesfully");
			 Integer gitlabFlag= getProjects((String)session.getAttribute("access_token"));
			 if(gitlabFlag == 200) {
				 session.setAttribute("ProjectsList", this.ListofProjects);
				 request.getRequestDispatcher("../Home.jsp").forward(request, response);
			 }
			 
		}
	}catch(Exception e) {
		e.printStackTrace();
	}
	
	
	String createjoburl =  props.getProperty("createJobUrl");
	createjoburl= createjoburl.replaceAll("@username@", name);
	createjoburl =createjoburl.replaceAll("@password@", password);
	System.out.println("JOB URL"+appurl+createjoburl+"&JobName="+project_name+"_"+description);
	HttpPost post = new HttpPost(serverUrl+appurl+createjoburl+"&JobName="+project_name+"_"+industry);
	 
	 project = client.execute(post);
	 int statusCode = project.getStatusLine().getStatusCode();
	 
	}else if(request.getParameter("action").equals("jiranewproj")) {
		session=request.getSession(true);
		Cookie[] cookies = null;
		cookies = request.getCookies();
		if( cookies != null ) {
		   for (int i = 0; i < cookies.length; i++) {
		      if(cookies[i].getName().equalsIgnoreCase("IQSESSIONID")){
		    	  try{
		    	 String token= cookies[i].getValue();
		    	 String cookieurldecode=URLDecoder.decode(token, StandardCharsets.UTF_8.toString());
		    	 Base64.Decoder decoder = Base64.getMimeDecoder();
		    	 String tokendecoded=new String(decoder.decode(cookieurldecode));
		    	 String[] tokendecode=tokendecoded.split("!q4Ev");
		    	String ckusernameencode=new String(decoder.decode(tokendecode[0]));
		    	String ckpassencode=new String(decoder.decode(tokendecode[1]));
		    	name=ckusernameencode;
		    	password=ckpassencode;
		    	
		    	session.setAttribute("username", name);
				session.setAttribute("userpassword",password);
				
		    	  }catch(Exception e){
		    		  
		    	  }
		      }else if(cookies[i].getName().equalsIgnoreCase("serverPath")){
		    	  try{
		    		  serverUrl=cookies[i].getValue();
		    		  session.setAttribute("serverPath", serverUrl);
		    	  }catch(Exception e){
		    		  
		    	  }
		      }
		   }
		} 
		Integer gitlabFlag= this.getGitlabResults();
		if(gitlabFlag == 200) {
			response.setContentType("application/json");
		    response.setCharacterEncoding("UTF-8");
		    response.getWriter().write("{\"status\":\"success\"}");
	}else if(gitlabFlag==401) {
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write("{\"status\":\"fail\"}");
	}else if(gitlabFlag==500) {
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write("{\"status\":\"server not responding\"}");
	}
	
	}

else if(request.getParameter("action").equals("licenceupdate")) {
	JSONObject illegalKey=new JSONObject();
	String licenceKey=request.getParameter("licence_key");
	System.out.println(licenceKey);
	try {
		 Base64.Decoder decoder = Base64.getMimeDecoder(); 
		   String keydecoded=new String(decoder.decode(licenceKey));
		   String[] arrOfStr = keydecoded.split("!q4Ev"); 
		   String endDateDecoded=new String(decoder.decode(arrOfStr[arrOfStr.length-2]));
		   String secDecoded=new String(decoder.decode(arrOfStr[arrOfStr.length-3]));
		   String deployDecoded=new String(decoder.decode(arrOfStr[arrOfStr.length-4]));
		   String sysId=new String(decoder.decode(arrOfStr[arrOfStr.length-5]));
		   boolean sysid=systemAddress(sysId);
		   LocalDateTime end=LocalDateTime.parse(endDateDecoded);
		   HttpSession session = request.getSession(true);
		   session.setAttribute("Deploy", deployDecoded);
			session.setAttribute("security", secDecoded);
			session.setAttribute("validity", "true");
		   int ret=LocalDateTime.now().compareTo(end);
		   System.out.println(ret);
		   JSONObject sysidresponse=new JSONObject();
		   sysidresponse.put("status","invalid");
		 if(sysid==true) {  
		   if(ret<0) {
	JSONObject status=updateLicence(licenceKey);
	if(status!=null) {
	if(status.get("status").toString().equalsIgnoreCase("success")) {
		System.out.println("in admin license update"+status.get("status").toString());
		request.setAttribute("licenceupdate", "license has updated successfully");
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(status.toString());
		//request.getRequestDispatcher("/error.jsp").forward(request, response);
	}else if (status.get("status").toString().equalsIgnoreCase("exist")) {
		request.setAttribute("licenceupdatefail", "license key existed");
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(status.toString());
		//request.getRequestDispatcher("/Home.jsp").forward(request, response);
	}else if (status.get("status").toString().equalsIgnoreCase("fail")) {
		request.setAttribute("licenceupdatefail", "Failed to update license");
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(status.toString());
		//request.getRequestDispatcher("/Home.jsp").forward(request, response);
	}
	}
	
} else {
	request.setAttribute("licenceupdatefail", "License key is not valid");
	request.getRequestDispatcher("/license.jsp").forward(request, response);
		   }
	}else {
		request.setAttribute("licenceupdatefail", "License key is not valid/Used by other system");
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(sysidresponse.toString());
	}
	}catch(IllegalArgumentException | ArrayIndexOutOfBoundsException e){
		illegalKey.put("status", "illegalkey");
		request.setAttribute("licenceupdatefail", "Failed to update license");
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(illegalKey.toString());
	}
	catch(Exception e){
		e.printStackTrace();
		request.setAttribute("licenceupdatefail", "Failed to update license/enter valid license");
		request.getRequestDispatcher("/license.jsp").forward(request, response);
	}
}else if(request.getParameter("action").equals("firstlicenseupdate")) {
	String licenceKey=request.getParameter("licence_key1");
	System.out.println(licenceKey);
	//licence validation
	JSONObject invalidresponse=new JSONObject();
	   invalidresponse.put("status","invalidkey");
	try {
		 Base64.Decoder decoder = Base64.getMimeDecoder(); 
		   String keydecoded=new String(decoder.decode(licenceKey));
		   String[] arrOfStr = keydecoded.split("!q4Ev"); 
		   String endDateDecoded=new String(decoder.decode(arrOfStr[arrOfStr.length-2]));
		   String secDecoded=new String(decoder.decode(arrOfStr[arrOfStr.length-3]));
		   String deployDecoded=new String(decoder.decode(arrOfStr[arrOfStr.length-4]));
		   String sysId=new String(decoder.decode(arrOfStr[arrOfStr.length-5]));
		   LocalDateTime end=LocalDateTime.parse(endDateDecoded);
		   int ret=LocalDateTime.now().compareTo(end);
		   boolean sysid=systemAddress(sysId);
		   HttpSession session = request.getSession(true);
		   session.setAttribute("Deploy", deployDecoded);
			session.setAttribute("security", secDecoded);
			session.setAttribute("validity", "true");
		   JSONObject sysidresponse=new JSONObject();
		   sysidresponse.put("status","invalid");
		   
		   if(sysid==true) { 
		   if(ret<0) {
	JSONObject status=firstUpdateLicence(licenceKey);
	System.out.println("in admin lisence"+status);
	if(status!=null) {
	if(status.get("status").toString().equalsIgnoreCase("success")) {
		request.setAttribute("licenceupdate", "license has updated successfully");
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(status.toString());
		//request.getRequestDispatcher("/error.jsp").forward(request, response);
	}else if (status.get("status").toString().equalsIgnoreCase("exist")) {
		request.setAttribute("licenceupdatefail", "license key existed");
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(status.toString());
		//request.getRequestDispatcher("/Home.jsp").forward(request, response);
	}else if (status.get("status").toString().equalsIgnoreCase("fail")) {
		request.setAttribute("licenceupdatefail", "Failed to update license");
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(status.toString());
		//request.getRequestDispatcher("/Home.jsp").forward(request, response);
	}
	}
	
}
	}else {
		request.setAttribute("licenceupdatefail", "License key is not valid/Used by other system");
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(sysidresponse.toString());
	}
	}catch(Exception e){
		request.setAttribute("licenceupdatefail", "Failed to update license");
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(invalidresponse.toString());
	}
}
 }
 
 
	@SuppressWarnings({ "unchecked", "null" })
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info("starting the get request function"); 
	if (request.getParameter("action").equals("dashboard")){
		Cookie[] cookies = null;
		cookies = request.getCookies();
		if( cookies != null ) {
		   for (int i = 0; i < cookies.length; i++) {
		      if(cookies[i].getName().equalsIgnoreCase("IQSESSIONID")){
		    	  try{
		    	 String token= cookies[i].getValue();
		    	 String cookieurldecode=URLDecoder.decode(token, StandardCharsets.UTF_8.toString());
		    	 Base64.Decoder decoder = Base64.getMimeDecoder();
		    	 String tokendecoded=new String(decoder.decode(cookieurldecode));
		    	 String[] tokendecode=tokendecoded.split("!q4Ev");
		    	String ckusernameencode=new String(decoder.decode(tokendecode[0]));
		    	String ckpassencode=new String(decoder.decode(tokendecode[1]));
		    	name=ckusernameencode;
		    	password=ckpassencode;
		    	  }catch(Exception e){
		    		  
		    	  }
		      }else if(cookies[i].getName().equalsIgnoreCase("serverPath")){
		    	  try{
		    		  serverUrl=cookies[i].getValue();
		    	  }catch(Exception e){
		    		  
		    	  }
		      }
		   }
		} 
		/*name=request.getParameter("username");  
		password=request.getParameter("password");*/
		//serverUrl=request.getParameter("serverPath");
		 session=request.getSession(true);
		logger.debug("Starting the session 1");
		session.setAttribute("username", name);
		session.setAttribute("userpassword",password);
	logger.debug("Making a call to all the component functions");

	Integer gitlabFlag= this.getGitlabResults();
	System.out.println("gitlabFlag:"+gitlabFlag);
	if(gitlabFlag == 200) {
				//this.getJenkinsResults(name, password, session);
				String name1=getAccessLevel(name);
				session.setAttribute("Access_level",name1);
				logger.info("access level set successfully");
				session.setAttribute("ProjectsList", this.ListofProjects);
				//request.getRequestDispatcher("../Home.jsp").forward(request, response);
				//request.getRequestDispatcher("../DashBoard.jsp").forward(request, response);
	}else if(gitlabFlag==401) {
		request.setAttribute("errorMessage", "Invalid username or password");
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}
		
		logger.debug("Starting the session 2");
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject json = new JSONObject();
		PrintWriter out = response.getWriter();
		int gitStat=ListofProjects.size();
		try {
					logger.debug("Making a call to all the component functions");
//		 Integer gitlabFlag = obj.getGitlabResults();
					//ControllerServlet obj = new ControllerServlet();		
  		            logger.debug("Making the call to all the compoent functions ");
  		          boolean jenkinflag = this.getJenkinsResults(name, password, session);
					boolean sonarFlag = this.getsonarResults();
					if (sonarFlag) {
					//this.getTestResults();
					session.setAttribute("ProjectsList", this.ListofProjects);
					//request.getRequestDispatcher("../DashBoard.jsp").forward(request, response);
					}
					else {
						session.setAttribute("message", "Something Wrong with SonarQube");
						request.getRequestDispatcher("../error.jsp").forward(request, response);
					}
//					if(gitlabFlag == 200) {
					 /*boolean jenkinflag = obj.getJenkinsResults(name, password, session);
					 System.out.println("jenkins flag:"+jenkinflag);*/
					 if (jenkinflag) {
						 boolean sonarqubeflag = this.getsonarResults();
						 if(sonarqubeflag) {
							//obj.getTestResults();
						 }
					 }
			
				int gitlabcount=this.gitStat;
				double jenkinscount=this.jenStat;
				double sonarcount=this.sonStat;
				int testcount=0;
				for (int i=0;i<this.ListofProjects.size();i++) {
					logger.debug("Checking to see the results in  ListofProjects");
					Projects object = (Projects) this.ListofProjects.get(i);
					if (object.getgitlabReturns() != null) {
						gitlabcount+=1;
						}
					if(object.getjenkinsReturns() !=null) {
						JSONObject jenkinsresult= object.getjenkinsReturns();
						if (jenkinsresult.get("color").equals("blue")){
							jenkinscount+=1;
						}
					}
					
					if(object.getsonarReturns() != null) {
						JSONObject sonarresult = object.getsonarReturns();
						if(sonarresult.get("status").equals("OK")) {
							sonarcount+=1;
						}
					}
					
					if(object.gettestReturns() != null) {
						testcount+=1;
					}
					json.put("gitlab", gitlabcount);
					/*json.put("jenkins", jenkinscount);
					json.put("sonar", sonarcount);*/
					//json.put("testcase", testcount);
				}
	  }
	catch(Exception e){
		e.printStackTrace();
		logger.error("There was an error with the compoents");
			}
		 	response.addHeader("Access-Control-Allow-Origin", "*"); 
		    response.addHeader("Access-Control-Allow-Methods", "GET, OPTIONS, PUT, DELETE, HEAD");
		    response.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept");
		    response.addHeader("Access-Control-Max-Age", "1728000");
		    JSONArray array = new JSONArray();
		    array.add(json);
		    System.out.println("json array for chart:"+array);
		    System.out.println("gitStat : "+gitStat);
		    System.out.println("jenStat : "+jenStat);
		    System.out.println("sonStat : "+sonStat);
		    out.print(json);
		    System.out.println("teststat:"+testStat);
		    session.setAttribute("gitCount", gitStat);
		    session.setAttribute("jenkinCount",jenStat);
		    session.setAttribute("sonarCount",sonStat);
		    session.setAttribute("testCount",testStat);
		response.sendRedirect(request.getContextPath()+"/DashBoard.jsp");
	}
	else if(request.getParameter("action").toString().equals("project")){
		System.out.println("redirecting to create project");
		session=request.getSession(true);
		Cookie[] cookies = null;
		cookies = request.getCookies();
		if( cookies != null ) {
		   for (int i = 0; i < cookies.length; i++) {
		      if(cookies[i].getName().equalsIgnoreCase("IQSESSIONID")){
		    	  try{
		    	 String token= cookies[i].getValue();
		    	 String cookieurldecode=URLDecoder.decode(token, StandardCharsets.UTF_8.toString());
		    	 Base64.Decoder decoder = Base64.getMimeDecoder();
		    	 String tokendecoded=new String(decoder.decode(cookieurldecode));
		    	 String[] tokendecode=tokendecoded.split("!q4Ev");
		    	String ckusernameencode=new String(decoder.decode(tokendecode[0]));
		    	String ckpassencode=new String(decoder.decode(tokendecode[1]));
		    	name=ckusernameencode;
		    	password=ckpassencode;
		    	
		    	session.setAttribute("username", name);
				session.setAttribute("userpassword",password);
				
		    	  }catch(Exception e){
		    		  
		    	  }
		      }else if(cookies[i].getName().equalsIgnoreCase("serverPath")){
		    	  try{
		    		  serverUrl=cookies[i].getValue();
		    		  session.setAttribute("serverPath", serverUrl);
		    	  }catch(Exception e){
		    		  
		    	  }
		      }
		   }
		} 
		Integer gitlabFlag= this.getGitlabResults();
		if(gitlabFlag == 200) {
			response.sendRedirect(request.getContextPath()+"/project_create.jsp");
	}else if(gitlabFlag==401) {
		response.sendRedirect(request.getContextPath()+"/project_create.jsp");
	}else if(gitlabFlag==500) {
		response.sendRedirect(request.getContextPath()+"/project_create.jsp");
	}
	}else if(request.getParameter("action").toString().equals("logout")){
		//HttpSession session=request.getSession();
		/*request.getSession().setAttribute("user", null);
		session.setAttribute("user", null);
		session.removeAttribute("username");
		session.invalidate();
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		response.sendRedirect(request.getContextPath()+"/index.jsp");*/
	}else if(request.getParameter("action").toString().equals("projectDetails")){
		request.getRequestDispatcher("/projectDetails.jsp").forward(request, response);
	}
	 	}

 
	public static void loggingFunction() {
	 BasicConfigurator.configure();
	}
}