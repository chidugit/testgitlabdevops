package com.cirrus.devops.projects;

import java.io.Serializable;

import org.json.simple.JSONObject;

public class Projects implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String projectName;
	JSONObject gitlabReturns;
	JSONObject jenkinsReturns;
	JSONObject sonarResults;
	JSONObject testResults;
	JSONObject serverResults;
	String project_name;
	String description;
	
	
	//setters
	public void setProjectName(String name) {
		this.projectName=name;
	}
	
	public void setgitlabReturns(JSONObject response) {
		this.gitlabReturns = response;
	}
	
	public void setjenkinsReturns(JSONObject response) {
		this.jenkinsReturns = response;
	}
	
	public void setsonarResults(JSONObject response) {
		this.sonarResults=response;
	}
	
	public void settestResults(JSONObject response) {
		this.testResults=response;
	}
	
	
	public void setproject_name(String response) {
		this.project_name=response;
	}
	
	public void setdescription(String response) {
		this.description=response;
	}
	
public void setserverresults(JSONObject response) {
	this.serverResults=response;
}
	
	//getters
	public String getprojectNaame() {
		return this.projectName;
	}
	
	public JSONObject getgitlabReturns() {
		return this.gitlabReturns;
	}
	
	public JSONObject getjenkinsReturns() {
		return this.jenkinsReturns;
	}
	
	public JSONObject getsonarReturns() {
		return this.sonarResults;
	}
	
	public JSONObject gettestReturns() {
		return this.testResults;
	}
	public String getdescription() {
		return this.description;
	}
	
	public String getproject_name() {
		return this.project_name;
	}
	
	public JSONObject getserverReturns( ) {
		return this.serverResults;
	}
}
