package com.cirrus.devops.util;

import java.io.InputStream;
import java.util.Properties;

public class PropertyUtility {

	Properties props= new Properties();
	private static PropertyUtility propertyUtility = null;
	
	private PropertyUtility(){
		InputStream fin = this.getClass().getClassLoader().getResourceAsStream("config.properties");
		InputStream fin1 = this.getClass().getClassLoader().getResourceAsStream("Licence.properties");
		try {
			if (fin != null || fin1!=null) {
		props.load(fin);
		props.load(fin1);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	public static PropertyUtility getPropertyUtility(){
		if (propertyUtility == null)
			propertyUtility = new PropertyUtility();
		return propertyUtility;
	}
	
	public  String getProperty(String key) {
//		props.get(key);
		
		return props.get(key).toString();
		
	}
	
	 public static void main(String[] args) {
	PropertyUtility ex = PropertyUtility.getPropertyUtility(); 
	
}	
	
}
