package com.cirrus.devops;

import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 * Servlet implementation class LogOutServlet
 */
@WebServlet("/LogOutServlet")
public class LogOutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(LogOutServlet.class);
   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info("logging out");
		HttpSession session=request.getSession(false);
		logger.info("request:"+request);
		//request.getSession().setAttribute("user", null);
		logger.info("response:"+response);
		/*Cookie IQSESSIONIDeRemove = new Cookie("IQSESSIONID", "");
		IQSESSIONIDeRemove.setPath("/");
		IQSESSIONIDeRemove.setMaxAge(0);
		response.addCookie(IQSESSIONIDeRemove);*/
		Cookie[] ck=request.getCookies();
		  for (int i = 0; i < ck.length; i++) {
			  if(ck[i].getName().equalsIgnoreCase("IQSESSIONID")) {
				  ck[i].setValue("");
				  ck[i].setMaxAge(0);  
				  response.addCookie(ck[i]);
			  }else if(ck[i].getName().equalsIgnoreCase("serverPath")) {
				  ck[i].setValue("");
				  ck[i].setMaxAge(0);  
				  response.addCookie(ck[i]);
			  }
          }
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.setHeader("Pragma", "no-cache");
		//response.setDateHeader("Expires", 0);
		response.setHeader("Expires", "Tue, 03 Jul 2001 06:00:00 GMT");
		response.setDateHeader("Last-Modified", new Date().getTime());
		response.sendRedirect(request.getContextPath()+"/logout.jsp");
		logger.info(session);
		if(session != null) {
			session.setAttribute("user", null);
			session.setAttribute("username", null);
			session.getServletContext().setAttribute("user",null);
			session.getServletContext().setAttribute("username",null);
			logger.info(session.getAttribute("username"));
			session.invalidate();
		}
	}

	

}
