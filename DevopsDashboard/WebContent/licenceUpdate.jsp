<!doctype html>
<%@page import="java.util.Base64"%>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/Logo.png">
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="${pageContext.request.contextPath}/assets/images/icon/favicon.ico">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/themify-icons.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/metisMenu.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/slicknav.min.css">
    <!-- amchart css -->
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <!-- others css -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/typography.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/default-css.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/responsive.css">
    <!-- modernizr css -->
    <script src="${pageContext.request.contextPath}/assets/js/vendor/modernizr-2.8.3.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/sweetalert.min.js"></script>
    <script>
    function Toggle() { 
        var temp = document.getElementById("login-password"); 
        if (temp.type === "password") { 
            temp.type = "text"; 
        } 
        else { 
            temp.type = "password"; 
        } 
    } 
    </script>
    <style>
   #loginbox{
   background-image: url("${pageContext.request.contextPath}/assets/img/DevOps.png");
   } 
  #loader {
  background-image:url("${pageContext.request.contextPath}/assets/img/Logo.png");
  background-size:100%;
  position: absolute;
  left: 50%;
  top: 50%;
  z-index:1;
  width: 40px;
  height: 60px;
  margin: -75px 0 0 -75px;
  width: 40px;
  height: 40px;
  -webkit-animation: animate 8s linear infinite;
  animation: animate 8s linear infinite;
	}
	
	@keyframes animate{
	0%{
			transform:rotateY(0deg);
	}
	100%{
			transform:rotateY(360deg);
	}
	} 
	@-webkit-keyframes animate {
	  0% { -webkit-transform:rotate(0deg); }
	  100% { -webkit-transform:rotate(360deg); }
	}
	
	.parent {
  position: relative;
  top: 3px;
    left: 103px;
}
#cirruslogobg {
  position: relative;
  top: -17px;
    left: -33px;
}
#cirruslogo {
  position: absolute;
  top: 0px;
  left: 0px;
}

.form-gp label{
color:black;
}

.form-gp i {
color: black;
}
.submit-btn-area button{
color: black;
}
.login-form-body {
    padding-bottom: 25px;
    padding-left: 40px;
    padding-right: 40px;
    padding-top: 20px;
}
.swal-modal{
	width: 306px;
    background-color: #d1ecf1;
    border-color: #bee5eb;
}
.swal-text{
    color: #0c5460;
}
.swal-text:last-child{
    margin-bottom: 15px;
}
.swal-text:first-child{
	margin-top: 15px;
}
.swal-overlay {
  background-color: transparent;
  top:-35rem;
  right: -52rem;
}
    </style>
    
</head>

<body oncontextmenu="return false;">
<% if (session.getAttribute("ProjectsList") != null){
		response.sendRedirect("Home.jsp");
		return;
	}
String ckusername=null;
String ckpass=null;
String ckaccess=session.getAttribute("Access_level").toString();
String ckvalid=session.getAttribute("validity").toString();
Cookie cookie = null;
Cookie[] cookies = null;
cookies = request.getCookies();
if( cookies != null ) {
   for (int i = 0; i < cookies.length; i++) {
      if(cookies[i].getName().equals("IQSESSIONID")){
    	  try{
    	 String token= cookies[i].getValue();
    	 Base64.Decoder decoder = Base64.getMimeDecoder();
    	 String tokendecoded=new String(decoder.decode(token));
    	 String[] tokendecode=tokendecoded.split("!q4Ev");
    	String ckusernameencode=new String(decoder.decode(tokendecode[0]));
    	String ckpassencode=new String(decoder.decode(tokendecode[1]));
    	String ckaccessencode=new String(decoder.decode(tokendecode[2]));
    	String ckvalidencode=new String(decoder.decode(tokendecode[3]));
    	ckusername=ckusernameencode;
    	ckpass=ckpassencode;
    	ckaccess=ckaccessencode;
    	ckvalid=ckvalidencode;
    	  }catch(Exception e){
    		  
    	  }
      }
   }
} 
 %>
  <% try
 {String message = (String)request.getAttribute("message");
%>
    <script>
    	var msg ="<%=message%>";
        if (msg != null){
        	if (msg == "null"){
        	}else{
        	alert(msg);}
        }
        function loadingFunction(){
            var loaderElement = document.getElementById("loader");
            loaderElement.style.visibility = "visible";
            document.getElementById("loginbox").style.visibility = "hidden";
            document.getElementById("loginbox1").style.visibility = "hidden";
            document.getElementById("loginform").style.visibility = "hidden";
            document.getElementById("cirruslogo").style.visibility = "hidden";
            document.getElementById("errorMassage").style.visibility = "hidden";
        }
 <% 
 }catch(Exception e){
	 e.printStackTrace();
	 
 }
 		
 %>   
</script>
 <%if(request.getAttribute("licencekeyinvalid")!=null){ %> 
<div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: auto;margin-bottom: auto;padding: 0.75rem 1.25rem;padding-right: 4rem;">
                                                <%-- <div id="errorMassage" style="color:red;font-size: small;padding-bottom:5px;">${errorMessage}</div> --%>
            <div id="licenceupdate">${licencekeyinvalid}</div>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="font-size: larger;padding: 0.6rem 1.25rem;">
                                                <span class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        <%} %>
    <!-- preloader area end -->
    <!-- login area start -->
    <div class="login-area login-bg" id="loginbox">
        <div class="container">
            <div class="login-box ptb--100" id="loginbox1" style="/* padding-top: 30px; */border-radius: 5px;">
                <form id="loginform" class="form-horizontal" role="form" action="web?action=licenceupdate" method="POST" style="opacity: 0.7;">
                    <div class="login-form-body" style="width: 70vh;">
                     <div class="parent" style="padding-bottom: 90px;">
  						<img id="cirruslogobg" src="${pageContext.request.contextPath}/assets/img/DevopsBg.png" style="float: left; height: 91px;">
  						<img id="cirruslogo" alt="" src="${pageContext.request.contextPath}/assets/img/RevisedHomeCirrusDigitalLogo-01.png" style="float: left;height: 50px;">
					</div>
                        <div class="form-gp">
                            <label for="login-username">Licence Key</label>
                            <input id="login-username" type="text" class="form-control" name="licence_key" value="" style="color: black;">
                            <i class="ti-key"></i>
                        </div>
                        
                        <div id="errorMassage" style="color:red;font-size: small;padding-bottom:5px;">${errorMessage}</div>
                        <div class="submit-btn-area">
                            <button id="form_submit" type="submit" onclick="loadingFunction()" disabled="disabled" style="width: 32%;">Update<i class="ti-arrow-right"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="loader" style="visibility:hidden"></div> 
    <!-- jquery latest version -->
    <script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-2.2.4.min.js"></script>
    <!-- bootstrap 4 js -->
    <script src="${pageContext.request.contextPath}/assets/js/popper.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/owl.carousel.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/metisMenu.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/jquery.slimscroll.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/jquery.slicknav.min.js"></script>
    
    <!-- others plugins -->
    <script src="${pageContext.request.contextPath}/assets/js/plugins.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/scripts.js"></script>
    
    <script type="text/javascript">
    swal({
		 //icon: "success",
		  //title: "Succcess",
		  position: 'top-end',
		  text: "license key updated successfully",
		  button:false
		});
   
    
    $(function(){
    	let inp = $('[type=text]'),
        btn = $('[type=submit]')
    	btn.prop('disabled', true)

      inp.on('input', () => {
      let empty = []
      inp.map(v => empty.push(inp.eq(v).val()))
      btn.prop('disabled', empty.includes(''))
    })
  	  }) 
    </script>
    
</body>

</html>