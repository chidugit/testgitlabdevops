<!DOCTYPE html>
<%@page import="org.jsoup.helper.HttpConnection.Response"%>
<%@page import="org.apache.catalina.authenticator.FormAuthenticator"%>
<%@page import="javax.swing.text.html.FormSubmitEvent"%>
<html>
<head>
  <meta charset="ISO-8859-1">
  <meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="0">
  <title>Logout</title>
 <script type="text/javascript">
 if (typeof(Storage) !== "undefined") {  
 	localStorage.clear();
 	} else {  
 	} 
 </script>
</head>
<body >

 <% 
 response.setHeader("Cashe-Control", "no-cache, no-store, must-revalidate");//HTTP 1.1
 response.setHeader("Pragma", "no-cache");//HTTP 1.0
 response.setDateHeader("Expires", 0);//Proxies
 if (session.getAttribute("ProjectsList")== null){
		response.sendRedirect("index.jsp");
		return;
	}
 String userName = (String) session.getAttribute("username");
 if (null == userName) {
     request.setAttribute("Error", "Session has ended.  Please login.");
     RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
     rd.forward(request, response);
 }
 
 %>

   
	
  </body>
  </html>
