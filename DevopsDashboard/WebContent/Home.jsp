<!DOCTYPE html>
<%@page import="java.lang.reflect.Array"%>
<%@page import="javax.ws.rs.core.Response"%>
<%@page import="com.cirrus.devops.service.JenkinsService"%>
<%@page import="com.cirrus.devops.service.GetGitlabGroups"%>
<%@page import="java.util.Base64"%>
<%@ page import="org.json.simple.*"
import="com.cirrus.devops.projects.*"
import="com.cirrus.devops.Modules.*"
import="java.util.ArrayList"
import="java.util.Iterator" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
    <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/Logo.png">
    <title>Home</title>
     <script>var ctx = "${pageContext.request.contextPath}"</script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="${pageContext.request.contextPath}/assets/images/icon/favicon.ico">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/themify-icons.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/metisMenu.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/owl.carousel.min.css">
   
     <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/default-css.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/styles.css">
   <link rel="stylesheet" href="${pageContext.request.contextPath}/css/loader.css">
   
  <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/jquery.3.2.1.min.js"></script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>
   <script src="${pageContext.request.contextPath}/assets/js/popper.min.js"></script>
  
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/DataTables-1.10.18/css/dataTables.bootstrap4.min.css"/>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/DataTables-1.10.18/js/dataTables.bootstrap4.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/sweetalert.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/loadingoverlay.js"></script>
	<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
<style>
	thead, tr, th, td {
	    text-align: center;
	    padding: 1%;
	}
	    	.parent {
	  position: relative;
	  top: 0;
	  left: 0;
	}
	#cirruslogobg {
	  position: relative;
	  top: -14px;
    left: -24px;
	}
	#cirruslogo {
	  position: absolute;
	  top: 0px;
	  left: 0px;
	}
	label{
	display:inline-block;
	margin-right:30px;
	width:200px;
	}
	span{
	font-size:small;
	}
	
	div.dataTables_wrapper div.dataTables_length select {
    width: 60px;
    margin: 0 10px;
}
.card-body {
    padding: 1.6rem;
}

.form-control, .form-control:focus{
background-color: white!important;
box-shadow: 0 1px 3px 0 rgba(0,0,0,.2);
}
.mr-5, .mx-5 {
    margin-right: 0em!important;
}
.main-content-inner {
    padding: 0 30px 0px;
}
table.dataTable{
border-collapse:collapse !important;
}
.table > thead > tr > th{
padding-bottom:10px;
}
table.dataTable thead>tr>th.sorting_asc, table.dataTable thead>tr>th.sorting_desc, table.dataTable thead>tr>th.sorting, table.dataTable thead>tr>td.sorting_asc, table.dataTable thead>tr>td.sorting_desc, table.dataTable thead>tr>td.sorting{
color: black;
background-color: #DEE9EB;
}
thead, tr, th, td{
color: gray;
font-size: small;
}
.modal-header{
	border-bottom: none;
}
.modal-footer{
	border-top: none;
}
.repoertckeckUnckeck{
	width: 18px;
    margin-top: -4px;
    margin-right: 4px;
}
 #loader {
  background-image:url("${pageContext.request.contextPath}/assets/img/Logo.png");
  background-size:100%;
  position: absolute;
  left: 50%;
  top: 50%;
  z-index:1;
  width: 40px;
  height: 60px;
  margin: -75px 0 0 -75px;
 /* border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;*/
  width: 40px;
  height: 40px;
  -webkit-animation: animate 8s linear infinite;
  animation: animate 8s linear infinite;
	}
	
	@keyframes animate{
	0%{
			transform:rotateY(0deg);
	}
	100%{
			transform:rotateY(360deg);
	}
	} 
	@-webkit-keyframes animate {
	  0% { -webkit-transform:rotate(0deg); }
	  100% { -webkit-transform:rotate(360deg); }
	}
</style>
</head>  
<body>
<%
response.setHeader("Cashe-Control", "no-cache, no-store, must-revalidate");//HTTP 1.1
response.setHeader("Pragma", "no-cache");//HTTP 1.0
response.setDateHeader("Expires", 0);//Proxies
if (session.getAttribute("ProjectsList") == null ||  session.getAttribute("username")==null){
		response.sendRedirect("index.jsp");
		return;
	}
String ckusername=null;
String ckpass=null;
String ckaccess=session.getAttribute("Access_level").toString();
String ckvalid=session.getAttribute("validity").toString();
Cookie cookie = null;
Cookie[] cookies = null;
cookies = request.getCookies();
if( cookies != null ) {
   for (int i = 0; i < cookies.length; i++) {
      if(cookies[i].getName().equals("IQSESSIONID")){
    	  try{
    	 String token= cookies[i].getValue();
    	 Base64.Decoder decoder = Base64.getMimeDecoder();
    	 String tokendecoded=new String(decoder.decode(token));
    	 String[] tokendecode=tokendecoded.split("!q4Ev");
    	String ckusernameencode=new String(decoder.decode(tokendecode[0]));
    	String ckpassencode=new String(decoder.decode(tokendecode[1]));
    	String ckaccessencode=new String(decoder.decode(tokendecode[2]));
    	String ckvalidencode=new String(decoder.decode(tokendecode[3]));
    	ckusername=ckusernameencode;
    	ckpass=ckpassencode;
    	ckaccess=ckaccessencode;
    	ckvalid=ckvalidencode;
    	  }catch(Exception e){
    		  
    	  }
      }
   }
} 
%>


  <!-- <div id="target" class="loading">
  <div class="loading-overlay">
    <p class="loading-spinner">
      <span class="loading-icon"></span>
      <span class="loading-text">loading</span>
    </p>
  </div>
</div> -->
    <!-- page container area start -->
    <div class="page-container">
        <!-- sidebar menu area start -->
        <div class="sidebar-menu">
            <div class="sidebar-header">
                <div class="logo">
                 <div class="parent">
  						<img id="cirruslogobg" src="${pageContext.request.contextPath}/assets/img/DevopsBg.png" style="float: left;height: 110px;">
  						<img id="cirruslogo" alt="" src="${pageContext.request.contextPath}/assets/img/RevisedHomeCirrusDigitalLogo-01.png" style="float: left;height: 70px;padding-top: 10px;">
					</div>
                </div>
            </div>
            <div class="main-menu">
                <div class="menu-inner">
                    <nav>
                        <ul class="metismenu" id="menu">
                        <li><a class="iqloader" href="${pageContext.request.contextPath}/Home.jsp"><i class="ti-home"></i> <span>Home</span></a></li>
                        <li><a class="iqloader" href="${pageContext.request.contextPath}/web?action=dashboard"><i class="ti-dashboard"></i> <span>Dashboard</span></a></li>
                        <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-folder"></i><span>Projects</span></a>
                                <ul class="collapse">
                                    <!-- <li><a href="#" onclick="hide();"><i class="ti-view-list-alt" style="padding-right:10px; "></i><span>List of Projects</span></a></li> -->
                                     <%if (ckvalid!=null){
                                     if (ckvalid.equals("true")){%>
                                    <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-package"></i><span>Join Project</span></a>
                                <ul class="collapse">
                                    <li><table>
    				<tr>
    				<tr>
    				<td>
    				<div class="form-group has-search">
                 <input class="form-control" type="text" placeholder="Search.." id="myInput" list="projectList" onkeyup="filterFunction()" style="background-color: aliceblue;"></div>
					    <datalist id="projectList" ></datalist></td></tr>
					    
					    <tr>
                       <td>
                       <button class="btn btn-primary btn-sm" id="btn" style="border-radius: 1rem;padding: 5px 13px;box-shadow: 0 1px 3px 0 rgba(0,0,0,.2);">Request Access</button>
							<p id="err" style="color:red; display:none"> Project/Group access is disabled </p>
							<p id="success" style="color:green; display:none"> </p></td></tr>
							</table></li>
                                </ul>
                            </li> 
                             <%}else{ %>
                              <li>
                                <a href="#"><i class="ti-package"></i><span>Join Project</span></a>
                            </li> 
                              <%}} %>
                            <%if (ckaccess!=null && ckvalid!=null){
                            if (ckaccess.equals("admin") && ckvalid.equals("true")){%>
                                    <li><a href="${pageContext.request.contextPath}/web?action=project" class="iqloader"><i class="ti-new-window"></i><span>Create project</span></a></li>
                                    <%}} %>
                                </ul>
                            </li>
                            <%if (ckaccess!=null){
                            if (ckaccess.equals("admin")){%>
                                    <!-- <li><a href="#" data-toggle="modal" data-target="#licenseupdate"><i class="ti-new-window"></i><span>License</span></a></li> -->
                                    <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-settings"></i><span>Admin</span></a>
                                <ul class="collapse">
                                    <li><a href="${pageContext.request.contextPath}/license.jsp" class="iqloader"><i class="fa fa-unlock-alt"></i><span>License</span></a></li>
                                    <%if (ckvalid!=null){
                                    if (ckvalid.equals("true")){%>
                                    <li><a href="${pageContext.request.contextPath}/UserManagement.jsp" class="iqloader"> <i class="ti-user"></i><span>User Management</span></a></li>
                                      <%}} %>
                                    <!-- <ul class="collapse">
                                    <li><a href="index.html"><i class="fa fa-user-plus"></i><span>Add User</span></a></li>
                                    <li><a href="index2.html"><i class="fa fa-edit"></i><span>Modify User</span></a></li>
                                    <li><a href="index3.html"><i class="ti-trash"></i><span>Delete User</span></a></li>
                               		 </ul> -->
                                    
                                </ul>
                            </li>
                                    <%} }%>
        						<li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-help"></i><span>Help</span></a>
                                <ul class="collapse">
                                    <li><a href="index.html"><span>Contact Us</span></a></li>
                                    <li><a href="index2.html"><span>FAQ</span></a></li>
                                    <li><a href="index3.html"><span>About</span></a></li>
                                </ul>
                            </li>
                            <%-- <li><a href="${pageContext.request.contextPath}/web?action=logout"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li> --%>
                            <li><a href="${pageContext.request.contextPath}/Logout"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>

        <!-- sidebar menu area end -->
        <!-- main content area start -->
         <!-- Breadcrumbs-->
            <!-- header area end -->
          
            <div id="homeinnerelement">
            <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item active">Overview</li>
              <%if(request.getAttribute("licenceupdatefail")!=null){ %> 
				<div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: auto;margin-bottom: auto;padding: 0.75rem 1.25rem;padding-right: 4rem;">
                                                <%-- <div id="errorMassage" style="color:red;font-size: small;padding-bottom:5px;">${errorMessage}</div> --%>
            <div id="licenceupdate">${licenceupdatefail}</div>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="font-size: larger;padding: 0.6rem 1.25rem;">
                                                <span class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        <%} %>
          </ol>
     
        <div class="row" style="padding:0px 0px 0px 10px;">
        <%if (session.getAttribute("Deploy").toString().equalsIgnoreCase("Deployment")){ %>
            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-primary o-hidden h-100" style="background-image: linear-gradient(to bottom right,#133c80, #69a8d4)!important;">
                <div class="card-body">
                  <div class="mr-5"><h4>Deployment</h4></div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="${pageContext.request.contextPath}/deployment.jsp" class="iqloader">
                  <span class="float-left">Start Deploy</span>
                </a>
              </div>
            </div>
            <!-- Deployment Disabled -->
            <%}else{ %>
            <div class="col-xl-3 col-sm-6 mb-3" data-toggle="tooltip" title="Need to buy licence to enable Deployment">
              <div class="card text-white bg-primary o-hidden h-100" style="opacity: 0.5; background-image: linear-gradient(to bottom right,#133c80, #69a8d4)!important;">
                <div class="card-body">
                  <div class="mr-5"><h4>Deployment</h4></div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="#">
                  <span class="float-left">Start Deploy</span>
                </a>
              </div>
            </div>
            <%} %>
            <!-- Deployment Disabled -->
            <%if (session.getAttribute("security").toString().equalsIgnoreCase("ServerSecurity")){ %>
            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-primary o-hidden h-100"  style="background-image: linear-gradient(to bottom right,#133c80, #69a8d4)!important;">
                <div class="card-body">
                  <div class="mr-5"><h4>Server Security</h4></div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="${pageContext.request.contextPath}/csmart.jsp" class="iqloader"><!--data-toggle="modal" data-target="${pageContext.request.contextPath}/csmart.jsp"  -->
                  <span class="float-left">Start Security</span>
                </a>
              </div>
            </div>
            <!--Server Security Disabled  -->
             <%}else{ %>
            <div class="col-xl-3 col-sm-6 mb-3" data-toggle="tooltip" title="Need to buy licence to enable Server Security">
              <div class="card text-white bg-primary o-hidden h-100" style="opacity: 0.5; background-image: linear-gradient(to bottom right,#133c80, #69a8d4)!important;">
                <div class="card-body">
                  <div class="mr-5"><h4>Server Security</h4></div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="#"><!--data-toggle="modal" data-target="${pageContext.request.contextPath}/csmart.jsp"  -->
                  <span class="float-left">Start Security</span>
                </a>
              </div>
            </div>
            <%} %>
            <!-- Server Security Disabled -->
            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-primary o-hidden h-100"  style="background-image: linear-gradient(to bottom right,#133c80, #69a8d4)!important;">
                <div class="card-body">
                  <div class="mr-5"><h4>Compliances</h4></div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="#">
                  <span class="float-left">View Details</span>
                </a>
              </div>
            </div>
            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-primary o-hidden h-100"  style="background-image: linear-gradient(to bottom right,#133c80, #69a8d4)!important;">
                <div class="card-body">
                  <div class="mr-5"><h4>IQ-Dev Env</h4></div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="#" onclick="hide();">
                  <span class="float-left">View Details</span>
                </a>
              </div>
            </div>
          </div>
          <div id="gitprojects">
          <ol class="breadcrumb" style="margin-bottom: 0px;">
            <li class="breadcrumb-item">
              <a href="#">Projects</a>
            </li>
            <li class="breadcrumb-item active">Overview</li>
          </ol>  
            <div class="main-content-inner">
                <!-- accroding start -->
                <div class="row" style="padding-top:4px; height: 56vh; overflow-y:scroll; ">
              <div class="table-responsive">
              <table id="homeTable" class="table table-bordered" border="1" >
  <thead  style="/* background-color: #007bff!important;color: white; */"><!--dataTables_wrapper dt-bootstrap4 no-footer  -->
  	<tr>
    	<th><div onclick="sortTable()">Project Name</div></th>   
    	<th>Created On</th>
    	<th>Owner</th>
    	<th>Visibility</th>
    	<th>Request Access</th>
    	<th>Commits</th>
    </tr> 
  </thead>
  <tbody id= "homeTable">
        <%
    try{
    ArrayList<Projects> ListofProjects = (ArrayList<Projects>)session.getAttribute("ProjectsList");
  for (int i=0;i<ListofProjects.size();i++){ 
  Projects object=ListofProjects.get(i);
  JSONObject gitlabReturns = object.getgitlabReturns();
  JSONObject jenkinsReturns = object.getjenkinsReturns();
  JSONObject sonarReturns = object.getsonarReturns();
  JSONObject testReturns = object.gettestReturns();
  %>
        <tr>
        
        <td>
        <%if(gitlabReturns!=null){ 
        	//if(gitlabReturns.get("name")!=null){
        %>
    <%String gitProjName=gitlabReturns.get("name").toString();
  
    System.out.println(gitProjName);
    if(gitProjName.contains("_none")||gitProjName.contains("_transport")||gitProjName.contains("_retail")||gitProjName.contains("_banking")||gitProjName.contains("_telecom")||gitProjName.contains("_ecommerce")){
    gitProjName=gitProjName.substring(0,gitProjName.indexOf("_"));
    }
    %>
<a href="<%=gitlabReturns.get("web_url").toString()%>"><%=gitProjName%></a>
<%-- <button id="button" type="button" data-project-id='<%=gitlabReturns.get("id")%>' style="background: transparent;border: none !important;font-size:14px !important;background: none;" data-toggle="modal" data-target="#myModal" onclick="editProject(this)"><i class="fa fa-edit"></i></button> --%>
        <%} //}%>
        </td>
        <td>
        <%if(gitlabReturns!=null){ 
        //if(gitlabReturns.get("created_at")!=null){
        %>
        <%String date=gitlabReturns.get("created_at").toString();
        date=date.substring(0, date.indexOf("T"));
        %>
        <p><%=date%></p>
        <%} //}%>
        </td>
        <td>
        <%if(gitlabReturns!=null){ 
        	//if(gitlabReturns.get("ownerName")!=null){
        %>
        <p><%=gitlabReturns.get("ownerName") %></p>
        <%}//} %>
        </td>
        <td>
        <%if(gitlabReturns!=null){ 
        	//if(gitlabReturns.get("visibility")!=null){
        %>
        <p><%=gitlabReturns.get("visibility")%></p>
        <%} //}%>
        </td>
        <td>
        <%if(gitlabReturns!=null){ 
        	if(gitlabReturns.get("requestEnabled")!=null){
        %>
        <%if(gitlabReturns.get("requestEnabled").toString().equalsIgnoreCase("true")){ %>
        <p>Enabled</p>
        <%}else if(gitlabReturns.get("requestEnabled").toString().equalsIgnoreCase("false")){ %>
        <p>Disabled</p>
        <%}}else{%>
        	<p>Not Found</p>
        <%}
        	} %>
        </td>
        <td>
        <%if(gitlabReturns!=null){ 
        	//if(gitlabReturns.get("commite_count")!=null){
        %>
        <p><%=gitlabReturns.get("commit_count")%></p>
        <%}//} %>
        </td>
        </tr>
      <%} %>
      <%}catch(Exception e){
    	  e.printStackTrace();
      }
    	  %>  
    	  
    	  </tbody>
        </table>
              </div>
              </div>
              </div>
                </div>
                
            <!--Server Details  -->    
                 <div id="vmdetails" style="visibility: hidden;">
          <ol class="breadcrumb" style="margin-bottom: 0px;">
            <li class="breadcrumb-item">
              <a href="#">Servers</a>
            </li>
            <li class="breadcrumb-item active">Overview</li>
            <li class="breadcrumb-item active"><a href="#" onclick="show();">Back</a></li>
          </ol>  
            <div class="main-content-inner" style="padding:0px 4px;">
                <!-- accroding start -->
                <div style="padding-top:4px; height: 56vh; overflow-y:scroll;width: 100%; overflow-x:hidden; ">
                  <table id="serverTable" class="table table-bordered" border="1" style="width: 98%;">
       	<thead>
            <tr>
                <th>Name</th>
                <th>VM</th>
                <th>RAM(in GB)</th>
                <th>Power State</th>
                <th>CPU Count</th>
            </tr>
       	 </thead>
       	 <tbody>
        </tbody>
       </table> 
              </div>
              </div>
              </div>
            <!--Server Details end  -->  
              </div>
                </div>
        <!-- main content area end -->
        
    <!-- page container area end -->
    
  <div class="container">
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
          <div class="modal-dialog">
          
            <!-- Modal content-->
            <div class="modal-content" style="opacity: 0.95;">
              <div class="modal-header">
              <h4 class="modal-title">Edit Project</h4>
               <button type="button" class="close" data-dismiss="modal">&times;</button>
                
              </div>
              <div class="modal-body">
              <form id="myform" action="" >
             <fieldset style="border:none">
             <label>Name:</label>  <input type="text" id="projectName" onfocus="orgVal(this)" onkeyup="newVal(this)" >
             <label>Description:</label> <textarea value="" id="projectDescription" onfocus="orgVal(this)" onkeyup="newVal(this)"></textarea>
             <label>path:</label> <input type="text" value="" id="projectPath" onfocus="orgVal(this)" onkeyup="newVal(this)">
             <label>issuesEnabled:</label> <input type="checkbox" id="issuesEnabled" onchange="document.getElementById('submit').removeAttribute('disabled')"><br>
             <label>mergeRequestsEnabled:</label> <input type="checkbox" id="mergeRequestsEnabled" onchange="document.getElementById('submit').removeAttribute('disabled')"><br>
             <label>owner:</label> <span id="owner"></span><br>
             <label>request_access_enabled:</label> <input type="checkbox" id="requestAccess" onchange="document.getElementById('submit').removeAttribute('disabled')">
             </fieldset>
			</form>
             </div>
              <div class="modal-footer">
              <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
              	<input id="submit" type="submit" class="btn btn-primary btn-sm" data-dismiss="modal" value='Update' onclick="update()" disabled></input>
              </div>
            </div>
            
          </div>
        </div>
        
      </div> 
    <!-- offset area end -->
<!--Edit user  -->
<div class="container">
        <!-- Modal -->
        <div class="modal fade" id="editUser" role="dialog" data-keyboard="false" data-backdrop="static">
          <div class="modal-dialog">
          
            <!-- Modal content-->
            <div class="modal-content" style="opacity: 0.95;">
              <div class="modal-header">
              <h4 class="modal-title">Edit User</h4>
               <button type="button" class="close" data-dismiss="modal">&times;</button>
                
              </div>
              <div class="modal-body">
              <form id="myform" action="" >
             <fieldset style="border:none">
             <label>Name:</label>  <input type="text" id="projectName" onfocus="orgVal(this)" onkeyup="newVal(this)" >
             <label>Description:</label> <textarea value="" id="projectDescription" onfocus="orgVal(this)" onkeyup="newVal(this)"></textarea>
             <label>path:</label> <input type="text" value="" id="projectPath" onfocus="orgVal(this)" onkeyup="newVal(this)">
             <label>issuesEnabled:</label> <input type="checkbox" id="issuesEnabled" onchange="document.getElementById('submit').removeAttribute('disabled')"><br>
             <label>mergeRequestsEnabled:</label> <input type="checkbox" id="mergeRequestsEnabled" onchange="document.getElementById('submit').removeAttribute('disabled')"><br>
             <label>owner:</label> <span id="owner"></span><br>
             <label>request_access_enabled:</label> <input type="checkbox" id="requestAccess" onchange="document.getElementById('submit').removeAttribute('disabled')">
             </fieldset>
			</form>
             </div>
              <div class="modal-footer">
              <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
              	<input id="submit" type="submit" class="btn btn-primary btn-sm" data-dismiss="modal" value='Update' onclick="update()" disabled></input>
              </div>
            </div>
            
          </div>
        </div>
        
      </div> 
<!--End Edit user  -->
    <script src="${pageContext.request.contextPath}/assets/js/owl.carousel.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/metisMenu.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/jquery.slimscroll.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/jquery.slicknav.min.js"></script>
    <!-- others plugins -->
    <script src="${pageContext.request.contextPath}/assets/js/plugins.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/scripts.js"></script> 
     <script>
    if('<%=session.getAttribute("username")%>'!=null){
  	   var username="<%=session.getAttribute("username").toString()%>";	
      }
     if('<%=session.getAttribute("userid")%>'!=null){
  	   var userid="<%=session.getAttribute("userid").toString()%>";	
      }
     if('<%=session.getAttribute("serverPath")%>'!=null){
  	   var ctxpath="<%=session.getAttribute("serverPath").toString()%>";
      }
   /*  if (typeof(Storage) !== "undefined") {  
    	localStorage.setItem("username", username);
    	localStorage.setItem("userid", userid);
    	localStorage.setItem("ctxpath", ctxpath);
    	} else {  
    	}   */
    </script>
    <script type="text/javascript">
    accessToken="<%=session.getAttribute("access_token")%>";
    </script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/joinProject.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/home.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/editProject.js"></script>
     <script type="text/javascript" src="${pageContext.request.contextPath}/js/server.js"></script>
</body>
</html>
