<!DOCTYPE html>
<%@page import="java.lang.reflect.Array"%>
<%@page import="javax.ws.rs.core.Response"%>
<%@page import="com.cirrus.devops.service.JenkinsService"%>
<%@page import="com.cirrus.devops.service.GetGitlabGroups"%>
<%@page import="java.util.Base64"%>
<%@ page import="org.json.simple.*"
import="com.cirrus.devops.projects.*"
import="com.cirrus.devops.Modules.*"
import="java.util.ArrayList"
import="java.util.Iterator" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
    <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/Logo.png">
    <title>License</title>
     <script>var ctx = "${pageContext.request.contextPath}"</script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="${pageContext.request.contextPath}/assets/images/icon/favicon.ico">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/themify-icons.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/metisMenu.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/owl.carousel.min.css">
   
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/default-css.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/styles.css">
   
 	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/jquery.3.2.1.min.js"></script>
  	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>
   	<script src="${pageContext.request.contextPath}/assets/js/popper.min.js"></script>
  
 	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/DataTables-1.10.18/css/dataTables.bootstrap4.min.css"/>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/DataTables-1.10.18/js/dataTables.bootstrap4.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/sweetalert.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/loadingoverlay.js"></script>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/loaderOverlay.css">
	<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
<style>
	thead, tr, th, td {
	    text-align: center;
	    padding: 1%;
	}
	    	.parent {
	  position: relative;
	  top: 0;
	  left: 0;
	}
	#cirruslogobg {
	  position: relative;
	  top: -14px;
    left: -24px;
	}
	#cirruslogo {
	  position: absolute;
	  top: 0px;
	  left: 0px;
	}/*#515354  */
	label{
	display:inline-block;
	margin-right:30px;
	width:200px;
	margin-left: 11px;
	}
	span{
	font-size:small;
	}
	
	div.dataTables_wrapper div.dataTables_length select {
    width: 60px;
    margin: 0 10px;
}
.card-body {
    padding: 1.6rem;
}

.form-control, .form-control:focus{
background-color: white!important;
}
.mr-5, .mx-5 {
    margin-right: 0em!important;
}
.main-content-inner {
    padding: 0 30px 0px;
}
table.dataTable{
border-collapse:collapse !important;
}
.table > thead > tr > th{
padding-bottom:10px;
}
table.dataTable thead>tr>th.sorting_asc, table.dataTable thead>tr>th.sorting_desc, table.dataTable thead>tr>th.sorting, table.dataTable thead>tr>td.sorting_asc, table.dataTable thead>tr>td.sorting_desc, table.dataTable thead>tr>td.sorting{
color: black;
background-color: #DEE9EB;
}
thead, tr, th, td{
color: gray;
font-size: small;
}
.card{
 border: 1px solid rgba(0,0,0,.125);
 margin: 0.5rem;
 box-shadow: 2px 0 32px rgba(0, 0, 0, 0.05);
}
fieldset {
    min-width: 0;
    padding: 0;
    margin: 0.4rem;
    border: 0;
}
.page-container {
    width: 100%;
    background-color: #77889963;
}
 #loader {
  background-image:url("${pageContext.request.contextPath}/assets/img/Logo.png");
  }
</style>
</head>  
<body>

<%
response.setHeader("Cashe-Control", "no-cache, no-store, must-revalidate");//HTTP 1.1
response.setHeader("Pragma", "no-cache");//HTTP 1.0
response.setDateHeader("Expires", 0);//Proxies
if (session.getAttribute("ProjectsList") == null || session.getAttribute("username")==null){
		response.sendRedirect("index.jsp");
		return;
	}
String ckusername=null;
String ckpass=null;
String ckaccess=null;
String ckvalid=null;
Cookie cookie = null;
Cookie[] cookies = null;
cookies = request.getCookies();
if( cookies != null ) {
   for (int i = 0; i < cookies.length; i++) {
      if(cookies[i].getName().equals("IQSESSIONID")){
    	  try{
    	 String token= cookies[i].getValue();
    	 Base64.Decoder decoder = Base64.getMimeDecoder();
    	 String tokendecoded=new String(decoder.decode(token));
    	 String[] tokendecode=tokendecoded.split("!q4Ev");
    	String ckusernameencode=new String(decoder.decode(tokendecode[0]));
    	String ckpassencode=new String(decoder.decode(tokendecode[1]));
    	String ckaccessencode=new String(decoder.decode(tokendecode[2]));
    	String ckvalidencode=new String(decoder.decode(tokendecode[3]));
    	ckusername=ckusernameencode;
    	ckpass=ckpassencode;
    	ckaccess=ckaccessencode;
    	ckvalid=ckvalidencode;
    	  }catch(Exception e){
    		  
    	  }
      }
   }
} 
%>

    <!-- page container area start -->
    <div class="page-container">
        <!-- sidebar menu area start -->
        <div class="sidebar-menu">
            <div class="sidebar-header">
                <div class="logo" style="/* width: 300px; */">
                 <div class="parent">
  						<img id="cirruslogobg" src="${pageContext.request.contextPath}/assets/img/DevopsBg.png" style="float: left;height: 110px;">
  						<img id="cirruslogo" alt="" src="${pageContext.request.contextPath}/assets/img/RevisedHomeCirrusDigitalLogo-01.png" style="float: left;height: 70px;padding-top: 10px;">
					</div>
                </div>
            </div>
            <div class="main-menu">
                <div class="menu-inner">
                    <nav>
                        <ul class="metismenu" id="menu">
                        <li><a class="iqloader" href="${pageContext.request.contextPath}/Home.jsp"><i class="ti-home"></i> <span>Home</span></a></li>
                        <li><a class="iqloader" href="${pageContext.request.contextPath}/web?action=dashboard"><i class="ti-dashboard"></i> <span>Dashboard</span></a></li>
                        <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-folder"></i><span>Projects</span></a>
                                <ul class="collapse">
                                    <!-- <li><a href="#" onclick="hide();"><i class="ti-view-list-alt" style="padding-right:10px; "></i><span>List of Projects</span></a></li> -->
                                     <%if (ckvalid!=null){
                                     if (ckvalid.equals("true")){%>
                                    <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-package"></i><span>Join Project</span></a>
                                <ul class="collapse">
                                    <li><table>
    				<tr>
    				<tr>
    				<td>
    				<div class="form-group has-search">
                 <input class="form-control" type="text" placeholder="Search.." id="myInput" list="projectList" onkeyup="filterFunction()" style="background-color: aliceblue;"></div>
					    <datalist id="projectList" ></datalist></td></tr>
					    
					    <tr>
                       <td>
                       <button class="btn btn-primary btn-sm" id="btn" style="border-radius: 1rem;padding: 5px 13px;box-shadow: 0 1px 3px 0 rgba(0,0,0,.2);">Request Access</button>
							<p id="err" style="color:red; display:none"> Project/Group access is disabled </p>
							<p id="success" style="color:green; display:none"> </p></td></tr>
							</table></li>
                                </ul>
                            </li> 
                             <%}else{ %>
                              <li>
                                <a href="#"><i class="ti-package"></i><span>Join Project</span></a>
                            </li> 
                              <%}} %>
                            <%if (ckaccess!=null && ckvalid!=null){
                            if (ckaccess.equals("admin") && ckvalid.equals("true")){%>
                                    <li><a href="${pageContext.request.contextPath}/web?action=project" class="iqloader"><i class="ti-new-window"></i><span>Create project</span></a></li>
                                    <%}} %>
                                </ul>
                            </li>
                            <%if (ckaccess!=null){
                            if (ckaccess.equals("admin")){%>
                                    <!-- <li><a href="#" data-toggle="modal" data-target="#licenseupdate"><i class="ti-new-window"></i><span>License</span></a></li> -->
                                    <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-settings"></i><span>Admin</span></a>
                                <ul class="collapse">
                                    <li><a href="${pageContext.request.contextPath}/license.jsp" class="iqloader"><i class="fa fa-unlock-alt"></i><span>License</span></a></li>
                                    <%if (ckvalid!=null){
                                    if (ckvalid.equals("true")){%>
                                    <li><a href="${pageContext.request.contextPath}/UserManagement.jsp" class="iqloader"> <i class="ti-user"></i><span>User Management</span></a></li>
                                      <%}} %>
                                    <!-- <ul class="collapse">
                                    <li><a href="index.html"><i class="fa fa-user-plus"></i><span>Add User</span></a></li>
                                    <li><a href="index2.html"><i class="fa fa-edit"></i><span>Modify User</span></a></li>
                                    <li><a href="index3.html"><i class="ti-trash"></i><span>Delete User</span></a></li>
                               		 </ul> -->
                                    
                                </ul>
                            </li>
                                    <%} }%>
        						<li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-help"></i><span>Help</span></a>
                                <ul class="collapse">
                                    <li><a href="index.html"><span>Contact Us</span></a></li>
                                    <li><a href="index2.html"><span>FAQ</span></a></li>
                                    <li><a href="index3.html"><span>About</span></a></li>
                                </ul>
                            </li>
                            <%-- <li><a href="${pageContext.request.contextPath}/web?action=logout"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li> --%>
                            <li><a href="${pageContext.request.contextPath}/Logout" class="iqloader"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>

        <!-- sidebar menu area end -->
        <!-- main content area start -->
         <!-- Breadcrumbs-->
            <!-- header area end -->
          <div id="loaderarea">
            <!-- page title area end -->
            <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">License</a>
            </li>
            <li class="breadcrumb-item active">Overview</li>
              <%if(request.getAttribute("licenceupdatefail")!=null){ %> 
				<div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: auto;margin-bottom: auto;padding: 0.75rem 1.25rem;padding-right: 4rem;">
                                                <%-- <div id="errorMassage" style="color:red;font-size: small;padding-bottom:5px;">${errorMessage}</div> --%>
            <div id="licenceupdate">${licenceupdatefail}</div>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="font-size: larger;padding: 0.6rem 1.25rem;">
                                                <span class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        <%} %>
          </ol>
               <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 mt-5" style="margin-top: 0rem!important;">
                        <div class="card">
                            <div class="card-body">
                                <div class="invoice-area">
                                    <div class="invoice-head">
                                        <div class="row">
                                            <div class="iv-left col-6">
                                                <span>IQ-DEV</span>
                                            </div>
                                            <!-- <div class="iv-right col-6 text-md-right">
                                                <span>#34445998</span>
                                            </div> -->
                                        </div>
                                    </div>
                                    <div class="row align-items-center">
                                        <div class="col-md-6">
                                        
                                            <div class="invoice-address" style="margin: 0.3rem;">
                                                <h3>License Details</h3>
                                                <div class="card">
                                                <div class="invoice-address" style="margin: 0.3rem;">
                                                <h5>Licensed Modules</h5>
                                                <p id="none"></p>
                                                <p id="deploy"></p>
                                                <p id="security"></p>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 text-md-right" style="text-align: left!important;font-weight: bold;">
                                        <div class="card">
                                                <div class="invoice-address" style="margin: 0.3rem;">
                                                <p id="purchasedate"></p>
                                                <p id="expiredate"></p>
                                            <!-- <ul class="invoice-date">
                                            <li id="purchasedate"></li>
                                            <li id="expiredate"></li> 
                                            </ul>-->
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row"> 
                                    <div class="col-lg-12 mt-5">
                                     <h3>License Update</h3><br>
                                     <form id="licenceform">
        				<div class="form-gp">
                            <label for="licensekey">Licence Key</label>
                            <input id="licensekey" type="text" class="form-control" name="licence_key" value="" style="color: black;background-color: aliceblue;">
                            <i class="ti-key"></i>
                        </div>
                        <div class="invoice-buttons text-right">
                                  <!--  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
                                    <button class="btn btn-primary" type="submit" id="updatelicense" disabled="disabled">Update</button><!--  onclick="doSubmit();"-->
                                </div>
                        </form>
                        </div>
                        </div>
     						 </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            
                <!-- main content area end -->
          <div class="container">
        <!-- Modal -->
        <div class="modal fade" id="firstLicense" role="dialog" data-keyboard="false" data-backdrop="static">
          <div class="modal-dialog">
          
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
              <h4 class="modal-title">Update License</h4>
              </div>
              <div class="modal-body">
            <form id="licenceformfirst">
        				<div class="form-gp">
                            <label for="firstlicensekey">Licence Key</label>
                            <input id="firstlicensekey" type="text" class="form-control" name="licence_key1" value="" style="color: black;background-color: aliceblue;">
                            <i class="ti-key"></i>
                        </div>
                        <div class="invoice-buttons text-right">
                                  <!--  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
                                    <button class="btn btn-primary" type="submit" id="updatelicense1" disabled="disabled">Update</button><!--  onclick="doSubmit();"-->
                                </div>
                        </form>
             </div>
            </div>
            
          </div>
        </div>
        
      </div> 
    
<!--End Edit user  -->
    <script src="${pageContext.request.contextPath}/assets/js/owl.carousel.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/metisMenu.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/jquery.slimscroll.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/jquery.slicknav.min.js"></script>
    <!-- others plugins -->
    <script src="${pageContext.request.contextPath}/assets/js/plugins.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/scripts.js"></script>
     <script>
    if('<%=session.getAttribute("username")%>'!=null){
  	   var username="<%=session.getAttribute("username").toString()%>";	
      }
     if('<%=session.getAttribute("userid")%>'!=null){
  	   var userid="<%=session.getAttribute("userid").toString()%>";	
      }
     if('<%=session.getAttribute("serverPath")%>'!=null){
  	   var ctxpath="<%=session.getAttribute("serverPath").toString()%>";
      }
    if (typeof(Storage) !== "undefined") {  
    	localStorage.setItem("username", username);
    	localStorage.setItem("userid", userid);
    	localStorage.setItem("ctxpath", ctxpath);
    	} else {  
    	}  
    </script> 
     <script type="text/javascript">
    accessToken="<%=session.getAttribute("access_token")%>";
    </script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/joinProject.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/licenseupdate.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/overlayLoader.js"></script>
</body>

</html>
