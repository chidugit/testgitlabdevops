<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
    <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/Logo.png">
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="${pageContext.request.contextPath}/assets/images/icon/favicon.ico">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/themify-icons.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/metisMenu.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/slicknav.min.css">
    <!-- amchart css -->
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <!-- others css -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/typography.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/default-css.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/responsive.css">
    <!-- modernizr css -->
    <script src="${pageContext.request.contextPath}/assets/js/vendor/modernizr-2.8.3.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/sweetalert.min.js"></script>
    <script>
    if (typeof(Storage) !== "undefined") {  
     	localStorage.clear();
     	} else {  
     	} 
    
   // history.forward();
    function Toggle() { 
        var temp = document.getElementById("login-password"); 
        if (temp.type === "password") { 
            temp.type = "text"; 
        } 
        else { 
            temp.type = "password"; 
        } 
    } 
    
    function changeHashOnLoad()
    {
       window.location.href += "#";
       setTimeout("changeHashAgain()", "50");
   }

   function changeHashAgain()
    {
       window.location.href += "1";
   }

   var storedHash = window.location.hash;
   window.setInterval(function ()
   {
       if (window.location.hash != storedHash)
        {
           window.location.hash = storedHash;
       }
   },
   50);
    </script>
    <style>
    .col, .col-1, .col-10, .col-11, .col-12, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-auto, .col-lg, .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-auto, .col-md, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-auto, .col-sm, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-auto, .col-xl, .col-xl-1, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-auto {
    padding-right: 0px;
    padding-left: 8px;
}
   #loginbox{
   background-image: url("${pageContext.request.contextPath}/assets/img/DevOps.png");
   } 
  #loader {
  background-image:url("${pageContext.request.contextPath}/assets/img/Logo.png");
  background-size:100%;
  position: absolute;
  left: 50%;
  top: 50%;
  z-index:1;
  width: 40px;
  height: 60px;
  margin: -75px 0 0 -75px;
 /* border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;*/
  width: 40px;
  height: 40px;
  -webkit-animation: animate 8s linear infinite;
  animation: animate 8s linear infinite;
	}
	
	@keyframes animate{
	0%{
			transform:rotateY(0deg);
	}
	100%{
			transform:rotateY(-360deg);
	}
	} 
	@-webkit-keyframes animate {
	  0% { -webkit-transform:rotate(0deg); }
	  100% { -webkit-transform:rotate(360deg); }
	}
	
	.parent {
  position: relative;
  top: 3px;
    left: 33px;
}
#cirruslogobg {
  position: relative;
  top: -17px;
    left: -26px;
}
#cirruslogo {
  position: absolute;
  top: 0px;
  left: 0px;
}

.form-gp label{
color:black;
padding-left: 10px;
}
.rmber-area {
    font-size: 11px;
}
.form-gp i {
color: black;
}
.submit-btn-area button{
color: black;
}
.login-form-body {
    padding-bottom: 40px;
    padding-left: 40px;
    padding-right: 40px;
    padding-top: 20px;
    box-shadow: 0 1px 3px 0 rgba(0,0,0,.2);
}
.custom-control-label::before{
top: 0rem;
}
.custom-control-label::after{
top:  0rem;
}
.modal-header{
	border-bottom: none;
}
.modal-footer{
	border-top: none;
}
.bg-primary {
    background-image: linear-gradient(to bottom right,#133c80, #69a8d4)!important;
}
.login-box form {
    width: 274px;
}
.submit-btn-area button {
    height: 35px;
    font-size: 10.5px;
}
.submit-btn-area button:hover {
    background: #292d2f96!important;
    color: #ffffff;
}
    </style>
    
</head>

<body onload="changeHashOnLoad();"><!--oncontextmenu="return false;"  --> 
<% 
response.setHeader("Cashe-Control", "no-cache, no-store, must-revalidate");//HTTP 1.1
response.setHeader("Pragma", "no-cache");//HTTP 1.0
response.setDateHeader("Expires", 0);//Proxies
if (session.getAttribute("ProjectsList") != null){
		response.sendRedirect("Home.jsp");
		return;
	}
 %>
 
  <% try
 {String message = (String)request.getAttribute("message");
 
%>
    <script>
    	var msg ="<%=message%>";
        if (msg != null){
        	if (msg == "null"){
        	}else{
        	alert(msg);}
        }
        function loadingFunction(){
            var loaderElement = document.getElementById("loader");
            loaderElement.style.visibility = "visible";
            document.getElementById("loginbox").style.visibility = "hidden";
            document.getElementById("loginbox1").style.visibility = "hidden";
            document.getElementById("loginform").style.visibility = "hidden";
            document.getElementById("cirruslogo").style.visibility = "hidden";
            document.getElementById("errorMassage").style.visibility = "hidden";
            document.getElementById("licensesucces").style.visibility = "hidden";
            document.getElementById("licensefail").style.visibility = "hidden";
            document.getElementById("usernamepass").style.visibility = "hidden";
        }
 <% 
 }catch(Exception e){
	 e.printStackTrace();
	 
 }
 		
 %>   
</script>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- preloader area start -->
    <!-- <div id="preloader">
        <div class="loader"></div>
    </div> -->
    <!-- preloader area end -->
    <!-- login area start -->
     <%if(request.getAttribute("licenceupdate")!=null){ %> 
<div id="licensesucces" class="alert alert-info alert-dismissible fade show" role="alert" style="margin-left: auto;margin-bottom: auto;padding: 0.75rem 1.25rem;padding-right: 4rem;">
                                                <%-- <div id="errorMassage" style="color:red;font-size: small;padding-bottom:5px;">${errorMessage}</div> --%>
            <div id="licenceupdate">${licenceupdate}</div>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="font-size: larger;padding: 0.6rem 1.25rem;">
                                                <span class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        <%} %>
                                        <%if(request.getAttribute("licenceupdatefail")!=null){ %> 
<div id="licensefail" class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: auto;margin-bottom: auto;padding: 0.75rem 1.25rem;padding-right: 4rem;">
                                                <%-- <div id="errorMassage" style="color:red;font-size: small;padding-bottom:5px;">${errorMessage}</div> --%>
            <div id="licenceupdate">${licenceupdatefail}</div>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="font-size: larger;padding: 0.6rem 1.25rem;">
                                                <span class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        <%} %>
                                        <%if(request.getAttribute("errorMessage")!=null){ %> 
<div id="usernamepass" class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: auto;margin-bottom: auto;padding: 0.75rem 1.25rem;padding-right: 4rem;">
                                               <div id="errorMassage">${errorMessage}</div> 
                                            <button type="button" onclick="usrpass();" class="close" data-dismiss="alert" aria-label="Close" style="font-size: larger;padding: 0.6rem 1.25rem;">
                                                <span class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        <%} %>
    <div class="login-area login-bg" id="loginbox">
        <div class="container">
            <div class="login-box ptb--100" id="loginbox1" style="/* padding-top: 30px; */border-radius: 5px;">
                <form id="loginform" class="form-horizontal" role="form" action="web/Login?action=login" method="POST" style="/* opacity: 0.7; */">
                    <!-- <div class="login-form-head" style="border-radius: 5px;"> -->
                   <%--  <div class="parent">
  						<img id="cirruslogobg" src="${pageContext.request.contextPath}/assets/img/DevopsBg.png" style="float: left;height: 70px;">
  						<img id="cirruslogo" alt="" src="${pageContext.request.contextPath}/assets/img/RevisedHomeCirrusDigitalLogo-01.png" style="float: left;height: 50px;">
					</div> --%>
                    <%-- <img id="cirruslogo" alt="" src="${pageContext.request.contextPath}/assets/img/RevisedHomeCirrusDigitalLogo-01.png" style="float: left;height: 50px;padding-top: 14px;padding-left: 10px;"> --%>
                        
                    <!-- </div> -->
                    <div class="login-form-body">
                     <div class="parent" style="padding-bottom: 90px;">
  						<img id="cirruslogobg" src="${pageContext.request.contextPath}/assets/img/DevopsBg.png" style="float: left; height: 91px;">
  						<img id="cirruslogo" alt="" src="${pageContext.request.contextPath}/assets/img/RevisedHomeCirrusDigitalLogo-01.png" style="float: left;height: 50px;">
					</div>
                  <!-- <h5 style="padding-left: 40%;padding-bottom:50px; ">Sign In</h5> --> 
                        <div class="form-gp">
                            <label for="login-username">UserName or Email</label>
                            <input id="login-username" type="text" class="form-control" name="name" value="" style="color: black;">
                            <i class="ti-user"></i>
                        </div>
                        <div class="form-gp">
                            <label for="login-password">Password</label>
                            <input type="password" id="login-password" class="form-control" name="password" data-toggle="password" style="color: black;">
                            <i class="ti-eye" id="pl" onclick="Toggle()"></i>
                        </div>
                        <%-- <div id="errorMassage" style="color:red;font-size: small;padding-bottom:5px;">${errorMessage}</div> --%>
                         <%-- <div id="licenceupdate" style="color:green;font-size: small;padding-bottom:5px;">${licenceupdate}</div> --%>
                        <div class="row mb-4 rmber-area">
                            <!-- <div class="col-6">
                                <div class="custom-control custom-checkbox mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="customControlAutosizing">
                                    <label class="custom-control-label" for="customControlAutosizing" style="color: black;">Remember Me</label>
                                </div>
                            </div> -->
                            <div class="col-6 text-right" style="margin-top: 1px;">
                                <a style="color: black;" href="#">Forgot Password?</a>
                            </div>
                        </div>
                        <div class="submit-btn-area" data-toggle="tooltip" title="Enter username and password to enable">
                            <button id="form_submit" type="submit" onclick="loadingFunction()" disabled="disabled" style="box-shadow: 0 1px 6px 0 rgb(8, 8, 8);border-color: rgba(223,225,229,0);">LogIn<i class="ti-arrow-right"></i></button>
                        </div>
                       <%--  <div class="form-footer text-center mt-5" style="margin-top: 1rem!important;">
                            <p class="text-muted" style="color: black!important;">Want to update License? <a href="#" data-toggle="modal" data-target="#licenseupdate" style="color: honeydew!important;">Update License</a></p>
                            ${pageContext.request.contextPath}/licenceUpdate.jsp
                        </div> --%>
                        
                        <!-- <div class="form-footer text-center mt-5">
                            <p class="text-muted">Don't have an account? <a href="register.html">Sign up</a></p>
                        </div> -->
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <!--  <div class="modal fade" id="licenseupdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="opacity: 0.95;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update license</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="font-size: inherit;">&times;</span>
        </button>
      </div>
    <form id="licenceform" class="form-horizontal" role="form" method="POST"> --><!-- action="web?action=adminlicenseupdate" method="POST"  
      <form id="licenceform">
      <div class="modal-body">
        
        <div class="form-gp">
                            <label for="licensekey">Licence Key</label>
                            <input id="licensekey" type="text" class="form-control" name="licence_key" value="" style="color: black;background-color: aliceblue;">
                            <i class="ti-key"></i>
                        </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button class="btn btn-primary" type="submit" id="updatelicense" disabled="disabled">Update</button> onclick="doSubmit();"
      </div>
      </form>
      </form> 
    </div>
  </div>
 
</div> -->
    <div id="loader" style="visibility:hidden"></div> 
    <!-- login area end -->

    <!-- jquery latest version -->
    <script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-2.2.4.min.js"></script>
    <!-- bootstrap 4 js -->
    <script src="${pageContext.request.contextPath}/assets/js/popper.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/owl.carousel.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/metisMenu.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/jquery.slimscroll.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/jquery.slicknav.min.js"></script>
    
    <!-- others plugins -->
    <script src="${pageContext.request.contextPath}/assets/js/plugins.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/scripts.js"></script>
    
    <script type="text/javascript">
    $("#login-username,#login-password").keyup(function() {
      	if($('#login-username').val()!="" && $('#login-password').val()!="" ){
      	$('#form_submit').prop('disabled', false);
      	}else{
      		$('#form_submit').prop('disabled', true);
      	}
      	});
 /*  $(function(){
  	let inp = $('#login-username,#login-password'),
    btn = $('#form_submit')
	btn.prop('disabled', true)
	inp.on('input', () => {
  	let empty = []
  	inp.map(v => empty.push(inp.eq(v).val()))
  	btn.prop('disabled', empty.includes(''))
	})
  }) 
  	 
$(function(){
  	let inp = $('#licensekey'),
    btn = $('#updatelicense')
	btn.prop('disabled', true)
	inp.on('input', () => {
  	let empty = []
  	inp.map(v => empty.push(inp.eq(v).val()))
  	btn.prop('disabled', empty.includes(''))
	})
  })  */
  	 
//Licensing
$('#licenceform').submit(function(e){
	    e.preventDefault();
	    $.ajax({
	        url:'<%=request.getContextPath()%>/web?action=licenceupdate',
	        type:'POST',
	        data:$('#licenceform').serialize(),
	        success:function(info){
	        	console.log(info.Status);
	        	 $('#licenseupdate').modal('hide');
	        	 if(info.Status=='Success'){
	        		 console.log(info.Status=='Success');
	        		 swal({
		        		 icon: "success",
		        		  title: "Succcess",
		        		  text: "license key updated successfully",
		        		  timer: 3000
		        		});
	        	 }else if(info.Status=='fail'){
	        		 swal("Failed", "Unable to update license", "error",{timer:3000});
	        	 }else{
	        		 swal("Failed", "Server responded 500 error", "error");
	        	 }
	        	
	        },
	        error: function(info){
	    	swal("Good job!", "You clicked the button!", "error");
           }
	    });
	}); 
 
 
    </script>
    
</body>

</html>