<!DOCTYPE html>
<%@page import="java.util.Base64"%>
<html lang='en'>
<head>
   <meta charset='utf-8'>
   <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/Logo.png">
   <title>Deployment</title>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>var ctx = "${pageContext.request.contextPath}"</script>
   <link rel="stylesheet" href="css/project_create.css"> 
   <link rel="icon" type="image/png" href="images/Logo.png">
   <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/font-awesome.min.css"/>
	<%-- <link href="${pageContext.request.contextPath}/assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/> --%>
	
	<script src="${pageContext.request.contextPath}/assets/js/jquery-3.3.1.min.js"></script>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/styles.css"/>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/default-css.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/themify-icons.css">
	 <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/metisMenu.css">
	 <script src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/popper.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/sweetalert.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/loadingoverlay.js"></script>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/loaderOverlay.css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.mask.min.js"></script>
<style>
body, .wrapper {
     min-height: 80vh;
     position: relative; 
}
/* span {
	float: left;
} */

#myform label {
	
    font-size: medium;
    
}
#myform span {
	
    font-size: medium;
    
}

tr{
border-style: hidden;  
}
    	.parent {
  position: relative;
  top: 0;
  left: 0;
}
span{
	font-size:small;
	}
#cirruslogobg {
  position: relative;
  top: -14px;
    left: -24px;
}
#cirruslogo {
  position: absolute;
  top: 0px;
  left: 0px;
}
.form-control, .form-control:focus{
background-color: var(--white)!important;
 box-shadow: 0 1px 3px 0 rgba(0,0,0,.2);
}
.card {
    border: aliceblue;
    background-color: azure;
    box-shadow: 0 1px 3px 0 rgba(0,0,0,.2);
}
.mt-5, .my-5 {
     margin-top: 1rem!important; 
}
#loader {
  background-image:url("${pageContext.request.contextPath}/assets/img/Logo.png");
  }
</style>
<script type="text/javascript">
$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
</script>
</head>
<body>
<% if (session.getAttribute("ProjectsList") == null || session.getAttribute("username")==null){
		response.sendRedirect("index.jsp");
		return;
	} 
if (session.getAttribute("validity").equals("true")== false && session.getAttribute("Deploy").toString().equalsIgnoreCase("Deployment")==false){
	 response.sendRedirect(request.getContextPath());
		return;
	}
String ckusername=null;
String ckpass=null;
String ckaccess=session.getAttribute("Access_level").toString();
String ckvalid=session.getAttribute("validity").toString();
Cookie cookie = null;
Cookie[] cookies = null;
cookies = request.getCookies();
if( cookies != null ) {
   for (int i = 0; i < cookies.length; i++) {
      if(cookies[i].getName().equals("IQSESSIONID")){
    	  try{
    	 String token= cookies[i].getValue();
    	 Base64.Decoder decoder = Base64.getMimeDecoder();
    	 String tokendecoded=new String(decoder.decode(token));
    	 String[] tokendecode=tokendecoded.split("!q4Ev");
    	String ckusernameencode=new String(decoder.decode(tokendecode[0]));
    	String ckpassencode=new String(decoder.decode(tokendecode[1]));
    	String ckaccessencode=new String(decoder.decode(tokendecode[2]));
    	String ckvalidencode=new String(decoder.decode(tokendecode[3]));
    	ckusername=ckusernameencode;
    	ckpass=ckpassencode;
    	ckaccess=ckaccessencode;
    	ckvalid=ckvalidencode;
    	  }catch(Exception e){
    		  
    	  }
      }
   }
} 
	%>
	 
        <!-- page container area start -->
        <div class="page-container">
        <!-- sidebar menu area start -->
               <div class="sidebar-menu">
            <div class="sidebar-header">
                <div class="logo" style="/* width: 300px; */">
                 <div class="parent">
  						<img id="cirruslogobg" src="${pageContext.request.contextPath}/assets/img/DevopsBg.png" style="float: left;height: 110px;">
  						<img id="cirruslogo" alt="" src="${pageContext.request.contextPath}/assets/img/RevisedHomeCirrusDigitalLogo-01.png" style="float: left;height: 70px;padding-top: 10px;">
					</div>
                </div>
            </div>
            <div class="main-menu">
                <div class="menu-inner">
                    <nav>
                        <ul class="metismenu" id="menu">
                        <li><a class="iqloader" href="${pageContext.request.contextPath}/Home.jsp"><i class="ti-home"></i> <span>Home</span></a></li>
                        <li><a class="iqloader" href="${pageContext.request.contextPath}/web?action=dashboard"><i class="ti-dashboard"></i> <span>Dashboard</span></a></li>
                        <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-folder"></i><span>Projects</span></a>
                                <ul class="collapse">
                                    <!-- <li><a href="#" onclick="hide();"><i class="ti-view-list-alt" style="padding-right:10px; "></i><span>List of Projects</span></a></li> -->
                                     <%if (ckvalid!=null){
                                     if (ckvalid.equals("true")){%>
                                    <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-package"></i><span>Join Project</span></a>
                                <ul class="collapse">
                                    <li><table>
    				<tr>
    				<tr>
    				<td>
    				<div class="form-group has-search">
                 <input class="form-control" type="text" placeholder="Search.." id="myInput" list="projectList" onkeyup="filterFunction()" style="background-color: aliceblue;"></div>
					    <datalist id="projectList" ></datalist></td></tr>
					    
					    <tr>
                       <td>
                       <button class="btn btn-primary btn-sm" id="btn" style="border-radius: 1rem;padding: 5px 13px;box-shadow: 0 1px 3px 0 rgba(0,0,0,.2);">Request Access</button>
							<p id="err" style="color:red; display:none"> Project/Group access is disabled </p>
							<p id="success" style="color:green; display:none"> </p></td></tr>
							</table></li>
                                </ul>
                            </li> 
                             <%}else{ %>
                              <li>
                                <a href="#"><i class="ti-package"></i><span>Join Project</span></a>
                            </li> 
                              <%}} %>
                            <%if (ckaccess!=null && ckvalid!=null){
                            if (ckaccess.equals("admin") && ckvalid.equals("true")){%>
                                    <li><a href="${pageContext.request.contextPath}/web?action=project" class="iqloader"><i class="ti-new-window"></i><span>Create project</span></a></li>
                                    <%}} %>
                                </ul>
                            </li>
                            <%if (ckaccess!=null){
                            if (ckaccess.equals("admin")){%>
                                    <!-- <li><a href="#" data-toggle="modal" data-target="#licenseupdate"><i class="ti-new-window"></i><span>License</span></a></li> -->
                                    <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-settings"></i><span>Admin</span></a>
                                <ul class="collapse">
                                    <li><a href="${pageContext.request.contextPath}/license.jsp" class="iqloader"><i class="fa fa-unlock-alt"></i><span>License</span></a></li>
                                    <%if (ckvalid!=null){
                                    if (ckvalid.equals("true")){%>
                                    <li><a href="${pageContext.request.contextPath}/UserManagement.jsp" class="iqloader"> <i class="ti-user"></i><span>User Management</span></a></li>
                                      <%}} %>
                                    <!-- <ul class="collapse">
                                    <li><a href="index.html"><i class="fa fa-user-plus"></i><span>Add User</span></a></li>
                                    <li><a href="index2.html"><i class="fa fa-edit"></i><span>Modify User</span></a></li>
                                    <li><a href="index3.html"><i class="ti-trash"></i><span>Delete User</span></a></li>
                               		 </ul> -->
                                    
                                </ul>
                            </li>
                                    <%} }%>
        						<li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-help"></i><span>Help</span></a>
                                <ul class="collapse">
                                    <li><a href="index.html"><span>Contact Us</span></a></li>
                                    <li><a href="index2.html"><span>FAQ</span></a></li>
                                    <li><a href="index3.html"><span>About</span></a></li>
                                </ul>
                            </li>
                            <%-- <li><a href="${pageContext.request.contextPath}/web?action=logout"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li> --%>
                            <li><a href="${pageContext.request.contextPath}/Logout" class="iqloader"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div> 
        
        
        <!-- sidebar menu area end -->
        <!-- main content area start -->
         <!-- Breadcrumbs-->
          <div id="loaderarea">
             
            <!-- header area end -->
          
            <!-- page title area end -->
            
            <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">Deployment</a>
            </li>
            <li class="breadcrumb-item active">Deploy new VM to the server</li>
          </ol>
          
             <div class="container">
        <div id="mainContent">
        <div class="main-content" style="background-color: white;">
         <div class="row">
                    <div class="col-lg-12 mt-5" >
                    <div id="deploycollapsehead" class="according accordion-s2 gradiant-bg">
                        <div class="card">
                        <div class="card-header">
                        <!-- <div class="row">
                                            <div class="iv-left col-6"> -->
                                                 <a class="card-link collapsed" data-toggle="collapse" href="#deploycollapse">On-Prem Deployment</a>
                                            <!-- </div>
                                            </div> -->
                        </div>
                         <div id="deploycollapse" class="collapse" data-parent="#deploycollapsehead">
                            <div class="card-body">
          <fieldset>
          <div class="form-row">
    <div class="col-md-4 mb-3">
      <label class="required-field">vCenter IP</label>
      <input type="text" class="form-control form-control-lg ip_address" id="vCenter_IP" name="vCenter_IP" placeholder="vCenter IP" autocomplete="off" required="required" autofocus >
    </div>
    <div class="col-md-4 mb-3">
      <label>vCenter Username</label>
      <input id="vCenter_Username" class="form-control form-control-lg" type="text" name="vCenter_Username" placeholder="vCenter Username" readonly onfocus="this.removeAttribute('readonly');" autocomplete="off" >
    </div>
    <div class="col-md-4 mb-3">
      <label>vCenter Password</label>
         <input class="form-control form-control-lg" id="vCenter_Password"  type="password" name="vCenter_Password" placeholder="vCenter Password" autocomplete="off"> 
      </div>
    </div>
    <br>
  <div class="form-row">
    <div class="col-md-3 mb-3">
      <label>VM Hostname</label>
         <input class="form-control form-control-lg" id="VM_Hostname"  type="text" name="VM_Hostname" placeholder="VM Hostname"> 
    </div>
    <div class="col-md-3 mb-3">
      <label>VM IP</label>
         <input class="form-control form-control-lg ip_address1" id="VM_IP"  type="text" name="VM_IP" placeholder="VM IP"> 
    </div>
    <div class="col-md-3 mb-3">
     <label>VM Subnet</label>
         <input class="form-control form-control-lg" id="VM_Subnet"  type="text" name="VM_Subnet" placeholder="VM Subnet"> 
    </div>
     <div class="col-md-3 mb-3">
     <label>VM Gateway</label>
         <input class="form-control form-control-lg" id="VM_Gateway"  type="password" name="VM_Gateway" placeholder="VM Gateway"> 
    </div>
  </div>
  <div class="form-row">
  <div style="padding-left: 43%;">
  <span data-toggle="tooltip" title="All fileds are required">
  <button id="onPrembtn" type="submit" class="btn btn-primary" disabled="disabled">submit</button>
  </span>
  </div>
  </div>
  <p style="font-size: 12px;">(Once the VM is deployed, Tomcat software will be installed on the VM with all default configurations)</p>
          </fieldset>
         </div>
         </div>
         </div>
         </div>
         </div>
         </div>
          <div class="row">
                    <div class="col-lg-12 mt-5" >
                     <div id="awscollapsehead" class="according accordion-s2 gradiant-bg">
                        <div class="card">
                        <div class="card-header">
                       <!--  <div class="row">
                                            <div class="iv-left col-6"> -->
                                                 <a class="card-link collapsed" data-toggle="collapse" href="#awscollapse">AWS Deployment</a>
                                           <!--  </div>
                                            </div> -->
                        </div>
                        <div id="awscollapse" class="collapse" data-parent="#awscollapsehead">
                            <div class="card-body">
          <fieldset>
          <div class="form-row">
    <div class="col-md-4 mb-3">
      <label>Access key</label>
      <input type="text" class="form-control form-control-lg" id="aws_access_key_id" name="aws_access_key_id" placeholder="AWS access key" autocomplete="off" required="required" autofocus >
    </div>
    <div class="col-md-4 mb-3">
      <label>Secret access key</label>
      <input id="aws_secret_access_key" class="form-control form-control-lg" type="password" name="aws_secret_access_key" placeholder="AWS secret access key" >
    </div>
    <div class="col-md-4 mb-3">
      <label>Role Name</label>
         <input class="form-control form-control-lg" id="role_name"  type="text" name="role_name" placeholder="IAM (Identity Access Management) Role Name"> 
      </div>
    </div>
    <br>
  <div class="form-row">
    <div class="col-md-3 mb-3">
      <label for="region">Region(Select AWS region)</label>
  <Select class="form-control" id="region" style="height:2.6rem;">
			                    <option value="us-east-2" selected>US East (Ohio)</option>
			                    <option value="us-east-1">US East (N. Virginia)</option>
			                    <option value="us-west-1">US West (N. California)</option>
			                    <option value="us-west-2">US West (Oregon)</option>
			                    <option value="ap-east-1">Asia Pacific (Hong Kong)</option>
			                    <option value="ap-south-1">Asia Pacific (Mumbai)</option>
			                    <option value="ap-northeast-3">Asia Pacific (Osaka-Local)</option>
			                    <option value="ap-northeast-2">Asia Pacific (Seoul)</option>
			                    <option value="ap-southeast-1">Asia Pacific (Singapore)</option>
			                    <option value="ap-southeast-2">Asia Pacific (Sydney)</option>
			                    <option value="ap-northeast-1">Asia Pacific (Tokyo)</option>
			                    <option value="ca-central-1">Canada (Central)</option>
			                    <option value="cn-north-1">China (Beijing)</option>
			                    <option value="cn-northwest-1">China (Ningxia)</option>
			                    <option value="eu-central-1">EU (Frankfurt)</option>
			                    <option value="eu-west-1">EU (Ireland)</option>
			                    <option value="eu-west-2">EU (London)</option>
			                    <option value="eu-west-3">EU (Paris)</option>
			                    <option value="eu-north-1">EU (Stockholm)</option>
			                    <option value="sa-east-1">South America (S�o Paulo)</option>
			                    <option value="us-gov-east-1">AWS GovCloud (US-East)</option>
			                    <option value="us-gov-west-1">AWS GovCloud (US)</option>
			                    </Select> 
    </div>
    <div class="col-md-3 mb-3">
      <label>Application Name</label>
         <input class="form-control form-control-lg" id="app_name"  type="text" name="app_name" placeholder="Application Name"> 
    </div>
    <div class="col-md-3 mb-3">
     <label>Number of Environments</label>
     <select id="rowNumber">
    <option value="0">0</option>
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
</select>
<div id="container"></div>
    </div>
     <div class="col-md-3 mb-3">
      <label for="stack">Platform Version</label>
  <Select class="form-control" id="stack" style="height:2.6rem;">
  								<option value="none" selected>Select...</option>
			                    <option value="64bit Amazon Linux 2018.03 v2.8.4 running Java 8">Java 8 version 2.8.4</option>
			                    <option value="64bit Amazon Linux 2018.03 v2.8.4 running Java 7">Java 7 version 2.8.4</option>
			                    <option value="64bit Amazon Linux 2018.03 v3.1.4 running Tomcat 8.5 Java 8">Java 8 with Tomcat 8.5 version 3.1.4</option>
			                    <option value="64bit Amazon Linux 2018.03 v3.1.4 running Tomcat 7 Java 7">Java 7 with Tomcat 7 version 3.1.4</option>
			                    <option value="64bit Windows Server 2016 v2.0.4 running IIS 10.0">Windows Server 2016 with IIS 10.0 version 2.0.4</option>
			                    <option value="64bit Windows Server Core 2016 v2.0.4 running IIS 10.0">Windows Server Core 2016 with IIS 10.0 version 2.0.4</option>
			                    <option value="64bit Windows Server 2012 R2 v2.0.4 running IIS 8.5">Windows Server 2012 R2 with IIS 8.5 version 2.0.4</option>
			                    <option value="64bit Windows Server Core 2012 R2 v2.0.4 running IIS 8.5">Windows Server 2012 R2 Server Core with IIS 8.5 version 2.0.4</option>
			                    <option value="64bit Windows Server 2016 v1.2.0 running IIS 10.0">Windows Server 2016 with IIS 10.0 version 1.2.0</option>
			                    <option value="64bit Windows Server Core 2016 v1.2.0 running IIS 10.0">Windows Server Core 2016 with IIS 10.0 version 1.2.0</option>
			                    <option value="64bit Windows Server 2012 R2 v1.2.0 running IIS 8.5">Windows Server 2012 R2 with IIS 8.5 version 1.2.0</option>
			                    <option value="64bit Windows Server Core 2012 R2 v1.2.0 running IIS 8.5">Windows Server 2012 R2 Server Core with IIS 8.5 version 1.2.0</option>
			                    <option value="64bit Windows Server 2012 v1.2.0 running IIS 8">Windows Server 2012 with IIS 8 version 1.2.0</option>
			                    <option value="64bit Windows Server 2012 R2 running IIS 8.5">Windows Server 2012 R2 with IIS 8.5</option>
			                    <option value="64bit Windows Server Core 2012 R2 running IIS 8.5">Windows Server 2012 R2 Server Core with IIS 8.5</option>
			                    <option value="64bit Windows Server 2012 running IIS 8">Windows Server 2012 with IIS 8</option>
			                    </Select> 
    </div>
  </div>
  <div class="form-row">
  <div style="padding-left: 43%;">
  <span data-toggle="tooltip" title="All fileds are required">
  <button id="awsbtn" type="submit" class="btn btn-primary" disabled="disabled">submit</button>
  </span>
  </div>
  </div>
          </fieldset>
         </div>
         </div>
         </div>
         </div>
         </div>
         </div>
         </div>
         </div>
         </div>
         </div>
         </div>
          <script type="text/javascript">
    accessToken="<%=session.getAttribute("access_token")%>";
    </script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/joinProject.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/deployment.js"></script>
  <script src="${pageContext.request.contextPath}/assets/js/owl.carousel.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/metisMenu.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/jquery.slimscroll.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/jquery.slicknav.min.js"></script>
    <!-- others plugins -->
    <script src="${pageContext.request.contextPath}/assets/js/plugins.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/scripts.js"></script> 
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/overlayLoader.js"></script>
 
</body>
</html>