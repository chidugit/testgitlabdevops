//JAVASCRIPT FUNCTION TO GET BUGS DETAILS FROM JIRA USING BUGS_DETAILS 
$(document).ready(function() {	
var releasedata=new Array();
	var issuedata=new Array();
	var createddate=new Array();
	var commitDate=new Array();
	var gitcommitsdata  =  new Array();
	var jiradatacount = new Array();
	var tempDate;
	var Issuedata;
	var Releasedata;
	var Inprogressdate = new Array();
	var jiratemp;
   var jiradates=new Array();
	var issuesinjira=new Array();
	var tododate = new Array();
	var closeddate = new Array();
	var statusdate = new Array();
	var gitnames = new Array();
	var bugscount = new Array();
	var finalstorename=new Array();
	var totalcreationdate=new Array();
	var olddatecount = new Array();
	var ccount=new Array();
	var latestbugsdetails=new Array();
	var tododate1=new Array();
	var latestbugsdate=new Array();
	var storedata = new Array();
	var projectKey;
	document.getElementById("name").innerHTML= projectName;
		$.ajax({
				type:'GET',
				url:ctx+'/rest/JiraService/projects',
				contentType:'application/json',
				dataType:'json',
				success:function(info){
		          	for (var j = 0;j<=info.length-1; j++) 
					{
		          		var object=info[j];
		          		if(object.name===projectName){
		          			projectKey=info[j].key;
		          		}
					}
		        },
		        error: function(info){
		    	swal("Error", "Unable to send request", "error");
		       }
		})
	$.when(
			$.ajax({
				url:ctx+"/rest/GitService/tags?access_token="+accesstoken+"&id="+projectId,
				type:"Get",
				success:function(returnhtml){
				Releasedata=returnhtml;
				releasedata.push(Releasedata);
				}
			
			}),
			
			$.ajax({
				url:ctx+"/rest/JiraService/jiraIssues?projectKey="+projectKey,
				type:"Get",
				datatype:"Json",
				crossDomain:true,
				 async: false,
				 
			/*	 beforeSend: function(xhr, settings) {
						xhr.setRequestHeader('Authorization',"Basic U3VjaGl0aHJhOldlbGNvbWUxMjMh");
						xhr.setRequestHeader('Content-Type', "application/json");
						xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
	     			xhr.setRequestHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
				},
				 */
				success:function(returnhtml){
					Issuedata=returnhtml;
				   issuedata.push(Issuedata);
				}
			})
)
	.done(function(){
		$('#Releasedata').html(Releasedata);
		$('#Issuedata').html(Issuedata);
	
		if(Releasedata.length=="0" || Issuedata.length=="0"){
			document.getElementById("jiraIssueTracking").style.visibility='hidden';
	    	document.getElementById("jiraIssuesTracking").style.visibility='visible';
         }
		else{
			document.getElementById("jiraIssueTracking").style.visibility='visible';
	        //to fetch the array of array objects 
	         $.each(Issuedata.issues, function(k, v) {
	       	  if (!issuesinjira.hasOwnProperty(v.created))
	       		issuesinjira.push({	
	       	      "fields": v.fields,
	       	      "sent": false
	       	    });
	         });

	     	for (var j = 0;j<=issuesinjira.length-1; j++) 
			{
	     			if(issuesinjira[j].fields.status.name === "In Progress")
	     			{
	     			   var jiratemp = issuesinjira[j].fields.created.split("T");  
	     			   Inprogressdate.push(jiratemp[0]);
	     			}
	     			else if(issuesinjira[j].fields.status.name === "To Do")
	     				{
	     				var jiratemp = issuesinjira[j].fields.created.split("T");
	     				tododate.push(jiratemp[0]);
	     				}
	     			else if(issuesinjira[j].fields.status.name ==="Done")
	     				{
	     				var jiratemp = issuesinjira[j].fields.updated.split("T");
	     				if(jiratemp!=null){
	     				closeddate.push(jiratemp[0]);
	     				}
	     				}
			}
              
	     totalcreationdate=Inprogressdate.concat(tododate);
					for (var i = 0;i<=Releasedata.length-1; i++) {
				 	    var object = Releasedata[i];
				 	    for (var property in object.commit)
				 	     {
				 	    	if(property === "committed_date")
				 	    		{
				 	    	     var datatemp = object.commit[property].split("T");
					 	        	    gitcommitsdata.push(datatemp[0]);
				 	    		} 
				 	      }
					}
					for (var k = 0;k<=Releasedata.length-1; k++) {
				 	    var object = Releasedata[k];
				 	   for (var property in object)
				 	     {
				 	         if(property === "name")
				 	        	 {
				 	        	  var tempdata = object[property];
				 	        	 gitnames.push(tempdata)
				 	        	 }
				 	     }
					}
	         
	                for(var i=0;i<=gitcommitsdata.length-1;i++) 
            		{ 
	                	 var count=0;
		                	   for(var j=0;j<=totalcreationdate.length-1;j++)
		                		   {
		                	              if(gitcommitsdata[i]!=null&&totalcreationdate[j]!=null)
		                	            	  {
						                		   if(totalcreationdate[j]<=gitcommitsdata[i])
					                			   {
					                			     count++;
					                			   }
		                	            	  }  
		                		     }
            		
            		
				                    for(var m=0;m<=closeddate.length-1;m++)
				                    {
				                    	for(var n=0;n<=totalcreationdate.length-1;n++)
						                	  { 
				                    		      if(closeddate[m]!=null&&totalcreationdate[n]!=null&&gitcommitsdata[i]!=null)
				                    		          {
							                		       if((totalcreationdate[n]>=gitcommitsdata[i])&&(closeddate[m]>=gitcommitsdata[i]))
							                			   {
							                			     count++;
							                			   }
				                    		          }   
				                              }
				                         }
				                    bugscount.push(count);
				                    } 
				                     	 
	                for(var i=0;i<=gitnames.length-1;i++)
	                	{
	                	finalstorename.push(gitnames[i]);
	                	}
	                
	               
	                var jiradates=totalcreationdate;
	                jiradates.sort();
	               latestbugsdate.push(jiradates[0]);
	               latestbugsdate.sort(function(a,b){
	 	     	      return new Date(a.latestbugsdate) - new Date(b.latestbugsdate);
	 	     	    });

	               var olddate = latestbugsdate[0];
	                
	                issuesinjira.filter(el =>{
	                	if(el.fields.status.name === "To Do" && "In Progress" && el.fields.created.split("T")[0]===olddate)
	                		{
	                		
	                		storedata.push(el);
	                		}
	                		
	                	
	                });
	                
	              //CHART TO SHOW NUMBER OF JIRABUGS IN A DAY   
					$(function(){
					Highcharts.chart('jiraIssueTracking', {
					    chart: {
					        type: 'column',
					        options3d: {
					            enabled: true,
					            alpha: 15,
					            beta: 15,
					            viewDistance: 25,
					            depth: 40
					        }
					    },
					    title: {
					        text: 'Bugs Tracking'
					    },
					    xAxis: {
					    	title:{
				        		text:'Releases'
				        	},
			    	        categories:finalstorename,
					    	 labels: {
						            skew3d: true,
						            style: {
						                fontSize: '10px'
						            },
						        }
			    	    },
					    yAxis: {
					        allowDecimals: false,
					        min: 0,
					        title: {
					            text: 'Number of Bugs',
					            skew3d: true
					        }
					    },
					    plotOptions: {
					    	 column: {
						            stacking: 'normal',
				            depth: 40
					    	 },
			    	        series: {
			    	            borderWidth: 0,
			    	        }
			    	    },
					    tooltip:{
					    	pointFormat:'Number of Bugs:<b>{point.y}</b>'
					    },
					    series:[{
					    	 name: 'Release Version',
   	                          data:bugscount,
					    }]
					});
					})
		}
		})
	.fail(function() {
		 document.getElementById("jiraIssueTracking").style.visibility='hidden';
		  	document.getElementById("jiraIssuesTracking").style.visibility='visible';
	})
});
