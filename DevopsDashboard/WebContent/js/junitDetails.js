	//JAVASCRIPT FUNCTION GET TEST DETAILS FROM JENKINS JUNIT USING JUNIT_DETAILS SERVICE
															//function JUnitDetails(projectName){
																$(document).ready(function() {
														    	document.getElementById("name").innerHTML= projectName;
														    	var skipCount=new Array();
														    	var totalCount=new Array();
														    	var failCount=new Array();
														    	var passCount=new Array();
														    	var num=new Array();
														    	//"http://localhost:8080/DevopsDashboard/rest/JenkinsService/JUnit_details?projectname="+projectName+"&"+"buildNumber="+len)
														    	//"http://localhost:8080/DevopsDashboard/rest/JenkinsService/JUnit_details?projectname=DevopsDashboard_ecommerce&buildNumber=67")
														    	//"/rest/JenkinsService/JUnit_details?projectname="+projectName+"&"+"buildNumber="+len
														    	 var jqxhr = $.ajax(ctx+"/rest/JenkinsService/JUnit_details?projectname="+projectName+"&name="+userName+"&password="+userPassword)
														         .done(function(data) {
														        	 Array.prototype.insert = function (index, items) { 
													        			 this.splice.apply(this, [index, 0].concat(items)); 
													        			 }
			
														        	 //console.log(data);
														        	 if(data.length!="0"){
														        		// alert("data present");
														        		 for (var i =data.length-1; i>=0 ; i--) {
														        			 console.log(data);
																		 	    var object = data[i];
																		 	    for (var property in object) {
																		 	    	if(property=="buildnumber"){
																		 	    		num.push(object[property]);
																		 	    }else if(property=="skipcount"){
																		 	        	skipCount.push(object[property]);
																		 	        	}else if(property=="totalcount"){
																		 	        		totalCount.push(object[property]);
																		 	        	} 
																		 	        	else{
																		 	        		failCount.push(object[property]);
																		 	        	}
																		 	          }
																		 	    }
														        		 
														        		 totalCount= totalCount.map(Number);
																			$.map(totalCount, function(value,index) { 
																				return parseInt(value); 
																				});
														        		 failCount= failCount.map(Number);
																			$.map(failCount, function(value,index) { 
																				return parseInt(value); 
																				});
																			skipCount= skipCount.map(Number);
																			$.map(skipCount, function(value,index) { 
																				return parseInt(value); 
																				});
														        		 for(var i=0;i<totalCount.length;i++){
														        			 var temp=0;
														        			 temp=totalCount[i]-skipCount[i]-failCount[i];
														        			 console.log(temp);
														        			 passCount.push(temp);
														        		 }
														        		 passCount= passCount.map(Number);
																			$.map(passCount, function(value,index) { 
																				return parseInt(value); 
																				});
																																						/*for (var i =0; i<data.length ; i++) {
															        			 console.log(data);
																			 	    var object = data[i];
																			 	    for (var property in object) {
																			 	    	 if(property=="skipcount"||property=="totalcount"||){
																			 	        	}else if(){
																			 	        		totalCount.push(object[property]);
																			 	        	} 
																			 	        	else{
																			 	        		failCount.push(object[property]);
																			 	        	}
																			 	          }
																			 	    }*/
														        		 console.log(passCount);
														        		 console.log(num);
														        		 console.log(skipCount);
														        		 console.log(totalCount);
														        		 console.log(failCount);
														        		 $(function(){
														        			 Highcharts.setOptions({
														        					credits:{
														        						enabled:false
														        					}
														        				});
																				Highcharts.chart('JUnitDet', {
																				    title: {
																				        text: 'Unit Test Results'
																				    },
																				
																				    subtitle: {
																				        text:null
																				    },
																				    xAxis: {
																				    	title:{
																			        		text:'Build Number'
																			        	},
																				        categories:num,
																				        labels: {
																				            skew3d: true,
																				            style: {
																				                fontSize: '10px'
																				            }
																				        }
																				    },
																				    yAxis: {
																				        title: {
																				            text:'Number of test cases'
																				        }
																				    },
																				    tooltip	:{
																				    	shared:true
																				    },
																				    legend: {
																			        	layout: 'horizontal',
																	                    align: 'center',
																	                    verticalAlign: 'bottom',
																				        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)'
																				    },
																				
																				    series: [{
																				        name: 'Number test passed',
																				        data: passCount
																				    },{
																				        name: 'Number test skipped',
																				        data: skipCount,
																				        color:'#FFC400'
																				    },{
																				        name: 'Number test failed',
																				        data: failCount,
																				        color:'#f45b5b'
																				    }],
																				    responsive: {
																				        rules: [{
																				            condition: {
																				                maxWidth: 500
																				            },
																				            chartOptions: {
																				                legend: {
																				                    layout: 'horizontal',
																				                    align: 'center',
																				                    verticalAlign: 'bottom'
																				                }
																				            }
																				        }]
																				    }
																				
																				});
																				})	 
														        		 
														        	 }else{
														        	 //alert("JUnit results not found");
														        		 document.getElementById("errmsg1").innerHTML="JUnit results not found";
														        		 document.getElementById("testnotfound").style.visibility='visible';
														        	 document.getElementById("JUnitDet").style.visibility='hidden';
														        	 }
														         })
														         .fail(function() { 
														        	 document.getElementById("errmsg1").innerHTML="JUnit results not found&";
														        	 document.getElementById("testnotfound").style.visibility='visible';
														        	 document.getElementById("JUnitDet").style.visibility='hidden';
														    		//alert("JUnit details not found 500 response please check log"); 
														        	 })
														   });											    
