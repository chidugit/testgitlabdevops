    var id=null;
    var iconok=ctx+"/assets/img/icons8-ok-500.png";
    var iconERROR=ctx+"/assets/img/icons8-box-important-100.png";
    $(document).ready(function() {
       var table= $('#assetsTable').DataTable( {
            ajax:  {
                url: ctx+'/rest/csmart/csmartassets',
                dataSrc: ''
            },
            columns: [
            	{ data: null,  defaultContent: ''  }, 
            	{ data: "ip" },
                { data: "username" },
                { data: "os" },
                { data: "online", render: function ( data, type, row ) {
                    if ( type === 'display' ) {
                    	/*  console.log(data);
                    	console.log($.fn.dataTable.render.number());  */
                    	if(data=="Online"){
                    		var numberRenderer = $.fn.dataTable.render.number( ',', '.', 0, '$' ).display;
                            return '<p style="color:#28D634;"> <img class="repoertckeckUnckeck" style="color:#28D634;" src='+iconok+'>'+numberRenderer( data )+ '</p>';	
                    	}else if(data=="Offline"){
                    		var numberRenderer = $.fn.dataTable.render.number( ',', '.', 0, '$' ).display;
                            return '<p style="color:#FF3E30;"><img class="repoertckeckUnckeck" style="color:#FF3E30;" src='+iconERROR+'>'+numberRenderer( data )+ '</p>';
                    	}
                        
                    }
                    return data;
                } },
                { data: "hostname" },
                { data: "compliance" },
            ],
            aLengthMenu: [[5, 10, 15, -1], [5, 10, 15, "All"]],
            iDisplayLength: 5,
            columnDefs: [ {
                orderable: false,
                className: 'select-checkbox',
                targets:   0
            } ],
            dom: 'Bfrtip',
            select: {
                style:    'os',
                blurable: true,
                info: true
               /*  selector: 'td:first-child' */
            },
            order: [[ 1, 'asc' ]]
          
        } ); 
       $.fn.dataTable.ext.errMode = 'none';
       $('#assetsTable').on( 'error.dt', function ( e, settings, techNote, message ) {
    	    console.log( 'An error has been reported by DataTables: ', message );
    	    } ) ;
      /*  var idArray=new Array();
       table.on('select', function (e, dt, type, indexes) {
                id = dt.row({selected: true}).data().id;
                console.log(id);
         }) */
         /* table.rows().every( function () {
   			if(this.data()[0].includes('checked="checked"')){
   		  this.select();
   		  console.log("selected");
   		} else {
    		 this.deselect();
   		}
 		}); */
     
          $('#assetsTable tbody').on( 'click', 'tr', function () {
        	  id=null;
        	  if (id != null) {
                  $('#deleteasset').prop('disabled', false);
              }
              else {
                  $('#deleteasset').prop('disabled', true);
              }//audit
              if (id != null) {
                  $('#audit').prop('disabled', false);
              }
              else {
                  $('#audit').prop('disabled', true);
              }
             $(this).toggleClass('selected');
             var d=table.rows('.selected').data().toArray();
             console.log(d[0].id);
             id=d[0].id;
             console.log(id);
             if (id != null) {
                 $('#deleteasset').prop('disabled', false);
             }
             else {
                 $('#deleteasset').prop('disabled', true);
             }
             if (id != null) {
                 $('#audit').prop('disabled', false);
             }
             else {
                 $('#audit').prop('disabled', true);
             }
         } );
      
         $('#button').click( function () {
             alert( table.rows('.selected').data().length +' row(s) selected' );
         } ); 
    } );
    

    $(document).ready(function() {
        $('#reportsTable').DataTable( {
            ajax:  {
                url: ctx+'/rest/csmart/csmartreports',
                dataSrc: ''
            },
            columns: [
                { data: "user" },
                { data: "hostname" },
                { data: "status",  render: function ( data, type, row ) {
                    if ( type === 'display' ) {
                    	/* console.log(data);
                    	console.log($.fn.dataTable.render.number()); */
                    	if(data=="COMPLIANT"){
                    		var numberRenderer = $.fn.dataTable.render.number( ',', '.', 0, '$' ).display;
                    		 return '<p style="color:#28D634;"> <img class="repoertckeckUnckeck" style="color:#28D634;" src='+iconok+'>'+numberRenderer( data )+ '</p>';	
                    	}else if(data=="ERROR"){
                    		var numberRenderer = $.fn.dataTable.render.number( ',', '.', 0, '$' ).display;
                    		return '<p style="color:#FF3E30;"><img class="repoertckeckUnckeck" style="color:#FF3E30;" src='+iconERROR+'>'+numberRenderer( data )+ '</p>';
                    	}
                        
                    }
                    return data;
                } },
                { data: "policy" },
                { data: "startTime" },
                { data: "endTime" }
            ],
            aLengthMenu: [[5, 10, 15, -1], [5, 10, 15, "All"]],
            iDisplayLength: 5,
            
        } );
        $.fn.dataTable.ext.errMode = 'none';
        $('#reportsTable').on( 'error.dt', function ( e, settings, techNote, message ) {
     	    console.log( 'An error has been reported by DataTables: ', message );
     	    });
    });
    
    $(document).ready(function() {
        $('#remiditiontable').DataTable( {
            ajax:  {
                url: ctx+'/rest/csmart/csmartRemidiation',
                dataSrc: ''
            },
            columns: [
            	{ data: "hostname" },
                { data: "user" },
                { data: "status",  render: function ( data, type, row ) {
                    if ( type === 'display' ) {
                    	/*  console.log(data);
                    	console.log($.fn.dataTable.render.number());  */
                    		var numberRenderer = $.fn.dataTable.render.number( ',', '.', 0, '$' ).display;
                    		 return '<p style="color:#28D634;"> <img class="repoertckeckUnckeck" style="color:#28D634;" src='+iconok+'>'+numberRenderer( data )+ '</p>';	
                    }
                    return data;
                }  },
                { data: "policy" },
                { data: "startTime" },
                { data: "endTime" }
            ],
            aLengthMenu: [[5, 10, 15, -1], [5, 10, 15, "All"]],
            iDisplayLength: 5,
            
        } );
        $.fn.dataTable.ext.errMode = 'none';
        $('#remiditiontable').on( 'error.dt', function ( e, settings, techNote, message ) {
     	    console.log( 'An error has been reported by DataTables: ', message );
     	    } ) ;
    } );
    
     $(document).ready(function(){
  	  $("#mySearch").on("keyup", function() {
  	    var value = $(this).val().toLowerCase();
  	    $("#homeTable tr").filter(function() {
  	      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
  	    });
  	  });
  	}); 
  	
  
    window.onload = function(){
    $("#enrol").fadeOut(500, function() {
        $("#assets").fadeIn(500, function() {
        });
    });
    }
    function show(){
    	 $("#enrol").fadeOut(500, function() {
    	        $("#assets").fadeIn(500, function() {
    	        });
    	    });
   }
    function hide(){
    	document.getElementById("enrol").style.visibility="visible"
    	 $("#assets").fadeOut(500, function() {
    	        $("#enrol").fadeIn(500, function() {
    	        });
    	    });
    }
    
    