 $("#firstlicensekey").keyup(function() {
  	if($('#firstlicensekey').val()!=""){
  	$('#updatelicense1').prop('disabled', false);
  	}else{
  	}
  	});
  
  $(document).ready(function() {
	  $.ajax({
			asyn:false,
			type:"get",
			url:ctx+"/rest/Licence/AllData",
			success: function(data, textStatus, xhr){
				console.log(xhr.status);
				console.log(textStatus);
				if(xhr.status==200){
				var i=1;
					//$('#projectList').append($('<option></option>').attr('id',v.id).attr('value',v.name).text(v.name));
					var purchasedate = data.purchasedate.split("T");
					var expiredate = data.expiredate.split("T");
					$("#purchasedate").html( "Purchased Date :"+purchasedate[0]);
					$("#expiredate").html( "Expire Date :"+expiredate[0]);
					if(data.deployment==="Deployment"){
						$("#deploy").html( "<p>"+i+". Deployment</p>" );
						i+=1;
					}
					if(data.security==="ServerSecurity"){
						$("#security").html( "<p>"+i+". Server Security</p>" );
					}
					if(data.deploy==="Deployment" && data.security==="ServerSecurity" ){
						$("#none").html( "<p style='color=red;'>No modules licensed</p>" );
					}
				}else if(xhr.status==204){
					$('#firstLicense').modal('show');
				}
			},
		  	error:function(error){
				alert(error);
			},
		});
	 });
  
  
  $(function(){
  	let inp = $('#licensekey'),
    btn = $('#updatelicense')
    btn.prop('disabled', true)

  inp.on('input', () => {
  let empty = []
  inp.map(v => empty.push(inp.eq(v).val()))
  btn.prop('disabled', empty.includes(''))
})
  	  }) 
 

$('#licenceformfirst').submit(function(e){
	    e.preventDefault();
	    $.ajax({
	        url:ctx+'/web?action=firstlicenseupdate',
	        type:'POST',
	        data:$('#licenceformfirst').serialize(),
	        success:function(info){
	        	console.log(info);
	        	 $('#firstLicense').modal('hide');
	        	 if(info.status=='success'){
	        		 console.log(info.status=='success');
	        		 swal({
		        		 icon: "success",
		        		  title: "Succcess",
		        		  text: "license key updated successfully",
		        		  timer: 3000
		        		}).then(function(isConfirm) {
		        			  if (isConfirm) {
		        				    location.reload();
		        				  } else {
		        					  location.reload();
		        				  }
		        				});
	        	 }else if(info.status=='fail'){
	        		 swal("Failed", "Unable to update license", "error",{timer:3000}).then(function(isConfirm){
                   	  if (isConfirm) {
      				    location.reload();
      				  } else {
      					  location.reload();
      				  }
                });
	        	 }else if(info.status=='exist'){
	        		 swal("Warning", "License key exist", "warning");
	        	 }else if(info.status=='invalid'){
	        		 swal("Warning", "License key is invalid for this system(license key is already in use)", "warning").then(function(isConfirm){
                   	  if (isConfirm) {
      				    location.reload();
      				  } else {
      					  location.reload();
      				  }
                });
	        	 }else if(info.status=='invalidkey'){
	        		 swal("Warning", "License key is invalid ", "warning").then(function(isConfirm){
                   	  if (isConfirm) {
      				    location.reload();
      				  } else {
      					  location.reload();
      				  }
                });
	        	 }else{
	        		 swal("Failed", "Server responded 500 error", "error").then(function(isConfirm){
                   	  if (isConfirm) {
      				    location.reload();
      				  } else {
      					  location.reload();
      				  }
                });
	        	 }
	        },
	        error: function(info){
	    	swal("Error", "Unable to send request", "error").then(function(isConfirm){
          	  if (isConfirm) {
				    location.reload();
				  } else {
					  location.reload();
				  }
        });
           }
	    });
	}); 
 
  
  $('#licenceform').submit(function(e){
	    e.preventDefault();
	    $.ajax({
	        url:ctx+'/web?action=licenceupdate',
	        type:'POST',
	        data:$('#licenceform').serialize(),
	        success:function(info){
	        	console.log(info);
	        	// $('#firstLicense').modal('hide');
	        	 if(info.status=='success'){
	        		 console.log(info.status=='success');
	        		 swal({
		        		 icon: "success",
		        		  title: "Succcess",
		        		  text: "license key updated successfully",
		        		  timer: 3000
		        		}).then(function(isConfirm) {
		        			  if (isConfirm) {
		        				    location.reload();
		        				  } else {
		        					  location.reload();
		        				  }
		        				});
	        	 }else if(info.status=='fail'){
	        		 swal("Failed", "Unable to update license", "error",{timer:3000});
	        	 }else if(info.status=='exist'){
	        		 swal("Warning", "License key exist", "warning");
	        	 }else if(info.status=='invalid'){
	        		 swal("Warning", "License key is invalid for this system(license key is already in use)", "warning");
	        	 }else if(info.status=='illegalkey'){
	        		 swal("Warning", "License key is invalid(illegal key)", "warning");
	        	 }else{
	        		 swal("Failed", "Server responded 500 error", "error");
	        	 }
	        },
	        error: function(info){
	    	swal("Error", "Unable to send request", "error");
         }
	    });
	});
  
  $(".iqloader").click(function(){ 
  	$.LoadingOverlaySetup({
  		background : "rgba(0, 0, 0, 0.5)",
  		image : "images/logo.png",
  		imageAnimation : "2s fadein",
  		imageColor : "#ffcc00"
  		});
  	var customElement = $('<div id="loader"></div>');
  	$("#licenseinner").LoadingOverlay("show", {
  	image :"",
  	custom : customElement,
  	text : "Gathering information"
  	});

  	setTimeout(function(){
  	$("#licenseinner").LoadingOverlay("text", "Just a momemt");
  	}, 3000);
  	});