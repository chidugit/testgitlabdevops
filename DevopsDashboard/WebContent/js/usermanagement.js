  function editUser(){
    	$.ajax({
    		type:'get',
    		url: ctx+"/rest/DataBaseOperations/SingleData?key="+$('#edituser').val(),
    		success: function(info){
    			$('#firstname').attr("value",info.firstname);
    			$('#lastname').attr("value",info.lastname);
    			$('#username').attr("value",info.username);
    			$('#passwordappend').attr("value",info.password);
    			$('#emailaddress').attr("value",info.emailid);
    			if(info.access==="Admin"){
    				$('#adminaccess').attr('checked','checked');
    				 $('#useraccess').attr('disabled', true);
    			}else if(info.access==="user"){
    				$('#useraccess').attr('checked','checked');
    				$('#adminaccess').attr('disabled', true);
    			}
    		}
    	});
    }
    
    function update(){
    	if($("#adminaccess").is(':checked')){
    	  var enrol={
					 firstname:$('#firstname').val(),
					 lastname:$('#lastname').val(),
					 username:$('#username').val(),
					 password:$('#passwordappend').val(),
			   		emailid:$('#emailaddress').val(),
			   		access:"Admin",
			   		oldpmr:$('#edituser').val()
			   		};
				}else{
					var enrol={
							 firstname:$('#firstname').val(),
							 lastname:$('#lastname').val(),
							 username:$('#username').val(),
							 password:$('#passwordappend').val(),
					   		emailid:$('#emailaddress').val(),
					   		access:"user",
					   		oldpmr:$('#edituser').val()
					   		};
				}
    	console.log(enrol);
    	$.ajax({
    		type:'PUT',
    		url: ctx+"/rest/DataBaseOperations/updateUser",
    		data:JSON.stringify(enrol),
			contentType:'application/json',
			dataType:'json',
			success:function(info){
	        	console.log(info);
	        		 swal({
		        		 icon: "success",
		        		  title: "Succcess",
		        		  text: "User Updated Successfully",
		        		  timer: 3000
		        		}).then(function(isConfirm) {
		        			  if (isConfirm) {
		        				    location.reload();
		        				  } else {
		        					  location.reload();
		        				  }
		        				});
	        	
	        },
	        error: function(info){
	        	if(info.status==409){
	        		swal("Warning", "User with username already exist", "warning");		
	        	}else if(info.status==500){
	        		swal("Error"+(info.status), "Unable Update user information", "error");
	        	}
	       }
    	});
    } 
    
    $("#validationCustom01, #validationCustom02, #validationCustomUsername,#validationCustom03 , #validationCustom031, #validationCustom04, #validationCustom05").keyup(function() {
    	console.log( $('#validationCustom031').val());
    	if($('#validationCustom01').val()!="" && $('#validationCustom02').val()!="" && $('#validationCustomUsername').val()!="" && $('#validationCustom03').val()!="" && $('#validationCustom031').val()!="" && $('#validationCustom04').val()!="" && $('#validationCustom05').val()!=""){
    		//$('#adduserform').bootstrapValidator();
    		$('input').keyup(function() {
        if ($('#validationCustom03').val() == $('#validationCustom031').val()) {
        	if($('#validationCustom03').val()!=""){
        		$('#validationCustom03').css({ 'border-color': 'green' });
           	 $('#validationCustom031').css({ 'border-color': 'green' });
           	 $('#adduserbtn').prop('disabled', false);
           	console.log("true");	
        	}
        	 
        } else if($('#validationCustom03').val() != $('#validationCustom031').val()){
        	$('#validationCustom03').css({ 'border-color': 'red' });
        	$('#validationCustom031').css({ 'border-color': 'red' });
        	$('#adduserbtn').prop('disabled', true);
        	console.log("false");
        	}
    });
    		console.log($('#adduserform').bootstrapValidator());
    	$('#adduserbtn').prop('disabled', false);
    	}else{
    		$('#adduserform').bootstrapValidator();
    		$('#adduserbtn').prop('disabled', true);
    	}
    	});
    
    
		  	$('#adduserbtn').on("click",function(ev){
		  		$.ajax({
					type:'POST',
					url:ctx+'/rest/ldaputhentication/authenticate?username='+$('#validationCustomUsername').val()+'&password='+$('#validationCustom031').val(),
					contentType:'application/json',
					dataType:'json',
					success:function(info){
						  var enrol={
									 firstname:$('#validationCustom01').val(),
									 lastname:$('#validationCustom02').val(),
									 username:$('#validationCustomUsername').val(),
									 password:$('#validationCustom031').val(),
							   		access:$('#validationCustom04').val(),
							   		emailid:$('#validationCustom05').val()
								};
					$.ajax({
							type:'POST',
							url:ctx+'/rest/DataBaseOperations/writeData',
							data:JSON.stringify(enrol),
							contentType:'application/json',
							dataType:'json',
							success:function(info){
					        	console.log(info);
					        		 swal({
						        		 icon: "success",
						        		  title: "Succcess",
						        		  text: "User added Successfully",
						        		  timer: 3000
						        		}).then(function(isConfirm) {
						        			  if (isConfirm) {
						        				    location.reload();
						        				  } else {
						        					  location.reload();
						        				  }
						        				});
					        	
					        },
					        error: function(info){
					        	if(info.status==409){
					        		swal("Warning", "User with username already exist", "warning");		
					        	}else if(info.status==500){
					        		swal("Error"+(info.status), "Unable add user", "error");
					        	}
					    	
					       }
					})
			        	
			        },
			        error: function(info){
			        	if(info.status==401){
			        		swal("Warning", "Not valid user(check AD for correct username and password)", "warning");		
			        	}else if(info.status==500){
			        		swal("Error"+(info.status), "Unable add user", "error");
			        	}
			    	
			       }
			})
			 
		})
		
var usernames=[];
 $(function() {
	 $.ajax({
			type:"get",
			url:ctx+"/rest/DataBaseOperations/AllData",
			success: function(data){
				$.each(data,function(k,v){
					console.log(k);
					console.log(v);
					usernames.push(v.name);
				})
			},
			error:function(error){
				//alert(error);
			},
		});
  }); 
  
 $(document).ready(function() {
	    $('#adduserform').bootstrapValidator();
	});  
	
    $('#deleteuser , #edituser').each(function () {
        var txt = $(this);
        txt.autocomplete({
            source: usernames,
            delay: 0,
            minLength: 0,
            select: function (event, ui) {$(this).val(ui.item.data); return true},
            change: function (event, ui) {$(this).trigger("change")}
        }).bind('focus', function () {
            $(this).autocomplete("search");
        })
        });
    
    $(document).ready(function(){
        $('#deleteuserbtn').on('click', function(e){
            e.preventDefault(); //cancel default action

            var message = $(this).data('confirm');

            //pop up
            swal({
                title: "Are you sure ??",
                text: message, 
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete)=>{
              if (willDelete) {
            	  $.ajax({
                      url: ctx+"/rest/DataBaseOperations/deleteData?key="+$('#deleteuser').val(),
                      type: "DELETE",
                      success: function () {
                          swal("Done!", $('#deleteuser').val()+" was succesfully deleted!", "success").then(function(isConfirm){
                        	  if (isConfirm) {
		        				    location.reload();
		        				  } else {
		        					  location.reload();
		        				  }
                          });
                      },
                      error: function (xhr, ajaxOptions, thrownError) {
                          swal("Error deleting!", "Please try again", "error");
                      }
                });
               // window.location.href = href;
              } else {
                swal("User data is safe!");
              }
            });
        });
    });
    
 /*   $("#deleteuser").keyup(function() {
    	if($('#deleteuser').val()!="" ){
    	$('#deleteuserbtn').prop('disabled', false);
    	}else{
    		$('#deleteuserbtn').prop('disabled', true);
    	}
    	}); 
    $("#deleteuser").on('change',function() {
    	console.log($('#deleteuser').val());
    	if($('#deleteuser').val()!="" ){
    	$('#deleteuserbtn').prop('disabled', false);
    	}else{
    		$('#deleteuserbtn').prop('disabled', true);
    	}
    	});  */
   

  $(function(){
  	let inp = $('#licensekey'),
    btn = $('#updatelicense')
    btn.prop('disabled', true)

  inp.on('input', () => {
  let empty = []
  inp.map(v => empty.push(inp.eq(v).val()))
  btn.prop('disabled', empty.includes(''))
})
  	  }) 
  	  
  	  
 