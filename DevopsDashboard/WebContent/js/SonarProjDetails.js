//JAVASCRIPT FUNCTION GET DETAILS FROM SONARQUBE USING SONAR_DETAILS SERVICE
//function sonarDetails(projectName){
$(document).ready(function() {
	document.getElementById("name").innerHTML= projectName;
	var sonarDet=new Array();
	var sonarDetName=new Array();
	var sonarData=new Array();
	var issuename=new Array();
	var bugs=0;
	var codesmell=0;
	var duplicated_lines=0;
	var violations=0;
	var ncloc=0;
	var vulnerabilities=0;
	var info=0;
	var bugblocker=0;
	var bugcritical=0;
	var bugminor=0;
	var bugmajor=0;
	var codeblocker=0;
	var codecritical=0;
	var codeminor=0;
	var codemajor=0;
	var vulblocker=0;
	var vulcritical=0;
	var vulminor=0;
	var vulmajor=0;
	var val=new Array();
	 console.log(ctx+"/rest/SonarQubeService/issues_details?projectname="+projectName);
	//"http://localhost:8080/DevopsDashboard/rest/SonarQubeService/issues_details?projectname=DevopsDashboard_sonar")
	//"http://localhost:8080/DevopsDashboard/rest/SonarQubeService/issues_details?projectname="+projectName)
	 var arrayOfStrings = projectName.split("_");
	 var str=arrayOfStrings[0];
	 var strName=str+"_sonar";
	 console.log(ctx+"/rest/SonarQubeService/issues_details?projectname="+strName);
   var jqxhr = $.ajax(ctx+"/rest/SonarQubeService/issues_details?projectname="+strName)
     .done(function(data) { //document.getElementById("projectDetails").style.visibility='visible'; alert("success");
     console.log(data);
     if(data.length=="0"){
    	 //alert("sonar data not found");
    	// document.getElementById("errmsg").innerHTML="Sonar data not found";
    	 document.getElementById("qualitynotfound").style.visibility='visible';
    	 document.getElementById("sonarDetailsChart").style.visibility='hidden';
     }else{
     for (var i =data.length-1; i>=0 ; i--){
    	 var object = data[i];
	 	    for (var property in object) {
	 	    	if(property=="type"){
	 	    		issuename.push(object[property]);
	 	    	}else if(property=="severity"){
	 	    		val.push(object[property]);
	 	    	}
	 	    }
	 	    
     }
     for (var i =data.length-1; i>=0 ; i--){
    	 var object = data[i];
	 	    for (var property in object) {
	 	    	if(property=="type"){
	 	    	if(object[property]=="BUG"){
	 	    		for (var property in object) {
	 	    		if(object[property]=="BLOCKER"){
	 	    			bugblocker=bugblocker+1;
		 	    	}else if(object[property]=="CRITICAL"){
		 	    		bugcritical=bugcritical+1;
		 	    	}else if(object[property]=="MAJOR"){
		 	    		bugmajor=bugmajor+1;
		 	    	}else if(object[property]=="MINOR"){
		 	    		bugminor=bugminor+1;
		 	    	}
	 	    		}
	 	    	}else if(object[property]=="CODE_SMELL"){
	 	    		for (var property in object) {
	 	    		if(object[property]=="BLOCKER"){
	 	    			codeblocker=codeblocker+1;
		 	    	}else if(object[property]=="CRITICAL"){
		 	    		codecritical=codecritical+1;
		 	    	}else if(object[property]=="MAJOR"){
		 	    		codemajor=codemajor+1;
		 	    	}else if(object[property]=="MINOR"){
		 	    		codeminor=codeminor+1;
		 	    	}
	 	    		}
	 	    	}else if(object[property]=="VULNERABILITY"){
	 	    		for (var property in object) {
	 	    		if(object[property]=="BLOCKER"){
	 	    			vulblocker=vulblocker+1;
		 	    	}else if(object[property]=="CRITICAL"){
		 	    		vulcritical=vulcritical+1;
		 	    	}else if(object[property]=="MAJOR"){
		 	    		vulmajor=vulmajor+1;
		 	    	}else if(object[property]=="MINOR"){
		 	    		vulminor=vulminor+1;
		 	    	}
	 	    		}
	 	    	}
	 	    	}
	 	    	
	 	    }
	 	    
     }
     console.log(bugmajor,bugminor,bugblocker,bugcritical);
     for(var i=0;i<issuename.length;i++){
    	 if(issuename[i]=="BUG"){
    		bugs=bugs+1; 
    	 }else if(issuename[i]=="CODE_SMELL"){
    		 codesmell=codesmell+1;
    	 }else if(issuename[i]=="VULNERABILITY"){
    		 vulnerabilities=vulnerabilities+1;
    	 }else if(issuename[i]=="VIOLATIONS"){
    		 violations=violations+1;
    	 }else if(issuename[i]=="DUPLICATED_LINES"){
    		 duplicated_lines=duplicated_lines+1;
    	 }
     }
     
     //CHART TO SHOW SONARQUBE DETAILS
     $(function(){
    	 Highcharts.setOptions({
    			credits:{
    				enabled:false
    			}
    		});
     Highcharts.chart('sonarDetailsChart', {
    	 
    	 chart: {
    	        type: 'column'
    	    },
    	    title: {
    	        text: 'Code Quality Details'
    	    },
    	    subtitle: {
    	        text: 'Click on any columns to view details'
    	    },
    	    xAxis: {
    	        type: 'category',
    	        title: {
    	            text:'Issues'
    	        }
    	    },
    	    yAxis: {
    	        title: {
    	            text:'Issue Number'
    	        }

    	    },
    	    legend: {
    	        enabled: false
    	    },
    	    plotOptions: {
    	        series: {
    	            borderWidth: 0,
    	            dataLabels: {
    	                enabled: true,
    	                format: '{point.y}'
    	            }
    	        }
    	    },

    	    tooltip: {
    	        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    	        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b>',
    	        shared:true
    	    },

    	    "series": [
    	        {
    	            //"name": "Quality Stat",
    	            "colorByPoint": true,
    	            "data": [
    	                {
    	                    "name": "Bugs",
    	                    "y": bugs,
    	                    "color":'#D50000',
    	                    "drilldown": "bugs"
    	                },
    	                {
    	                    "name": "Code_smell",
    	                    "y": codesmell,
    	                    "color":'#607D8B',
    	                    "drilldown": "Code_smell"
    	                },
    	                {
    	                    "name": "Vulnerabilities",
    	                    "y": vulnerabilities,
    	                    "color":'#FF6D00',
    	                    "drilldown": "Vulnerabilities"
    	                }
    	            ]
    	        }
    	    ],
    	    "drilldown": {
    	        "series": [
    	            {
    	                "name": "bugs",
    	                "id": "bugs",
    	                "data": [
    	                    ["blocker",bugblocker],
    	                    ["critical",bugcritical],
    	                    ["major",bugmajor],
    	                    ["minor",bugminor],
    	                ]
    	            },
    	            {
    	                "name": "Code_smell",
    	                "id": "Code_smell",
    	                "data": [
    	                    ["blocker",codeblocker],
    	                    ["critical",codecritical],
    	                    ["major",codemajor],
    	                    ["minor",codeminor]
    	                ]
    	            },
    	            {
    	                "name": "Vulnerabilities",
    	                "id": "Vulnerabilities",
    	                "data": [
    	                    ["blocker",vulblocker],
    	                    ["critical",vulcritical],
    	                    ["major",vulmajor],
    	                    ["minor",vulminor]
    	                ]
    	            }
    	        ]
    	    }
    	});
      })
     }})
     .fail(function() { 
    	 //document.getElementById("projectDetails").style.visibility='hidden';
    	 document.getElementById("qualitynotfound").style.visibility='visible';
    	 document.getElementById("errmsg").innerHTML="Severities data not found"; })
      
     });
     

