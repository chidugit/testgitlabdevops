$(document).ready(function() {
    $('#myTable').DataTable({
    	"aLengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
        "iDisplayLength": 5
    });
} );

$(function(){
    $(".dropdown").hover(            
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            });
    });
    
 
  $(document).ready(function() {
	    //set initial state.
	    var assignedTo =[]
	    $('input[name=filtertable]').on('change',function() {
	    	  assignedTo = $('input[name=filtertable]:checked').map(function() {
	    	        return this.id;
	    	    })
	    	    .get();
	    	 //console.log(assignedTo);
	    		    // Get the table rows
	    		    var rows = $("#myTable1").find("tr");
	    		    if (this.value == "") {
	    		        rows.show();
	    		        return;
	    		    }
	    		    
	    		    // Hide all the rows initially
	    		    rows.hide();

	    		    // Filter the rows; check each term in data
	    		    rows.filter(function (i, v) {
	    		        for (var d = 0; d < assignedTo.length; ++d) {
	    		            if ($(this).is(":contains('" + assignedTo[d] + "')")) {
	    		                return true;
	    		            }
	    		        }
	    		        return false;
	    		    })
	    		    // Show the rows that match.
	    		    .show();
	    });
	});
 
 