//console.log(accessToken);
var groups;
var projectsAccess;

function issues(data){
	var issues_enabled;
	if($(data).is(':checked'))
	{
			$(data).attr("data-type",true);
	}else{
		$(data).attr("data-type",false);
	}
	console.log(issues_enabled);
	}
$(document).ready(function() {
$.ajax({
	asyn:false,
	type:"get",
	global:false,
	url:ctx+"/rest/GitService/projects/all",
	data:{
		access_token: accessToken,
	},
	success:function(data){
		$.each(data,function(k,v){
			$('#projectList').append($('<option></option>').attr('id',v.id).attr('value',v.name).text(v.name));
		})
	},
	error:function(error){
		//alert(error);
	},
});
});


$('#myInput').change(function(){
	var project=$('#myInput').val();
	var projectID=$('#projectList option[value="'+ project+ '"]').attr('id');
	$.ajax({
	async:false,
	type:'get',
	url:ctx+'/rest/GitService/projects/'+projectID,
	dataType:'json',
	data:{
		access_token:accessToken,
		},
		success: function(data){
		JSON.stringify(data);
		$('#projectURL').html('');
		$('#projectURL').append(data.web_url);
		$('#projectURL').attr('href',data.web_url);
		if(data.hasOwnProperty('owner')){
		$('#Owner').append(data.owner.name).attr('href',data.owner.web_url);

		}	
		else{
		user(data.namespace.id);
		}
		
		if(data.request_access_enabled==true){
			req_access(projectsAccess,data.id);
		}else{
			if(data.hasOwnProperty('owner')==false&&data.namespace.kind=='group'){
				group(data.namespace.id);
				}else{
					$('#btn').attr('disabled',true);
					$('#err').show();
				}
		}	
		},
		error:function(error){
			//alert(error);
		},
	})
		console.log(projectID);
	});

function group(id){

	$.ajax({
		type:'get',
		async:false,
		url:ctx+'/rest/GitService/groups/'+id,
		data:{
		access_token:accessToken,
		},
		success: function(data){
		JSON.stringify(data);
		
		if(data.request_access_enabled==false){
		$('#btn').attr('disabled',true);
		$('#err').show();
		}
		else{
		req_access(groups,id);
		}

				
		}

	});

}

function user(id){
	$.ajax({
		type:'get',
		dataType:'json',
		url:ctx+'/rest/GitService/groups/'+id+'/members',
		data:{
		access_token:accessToken,
		},
		success: function(data){
		JSON.stringify(data);
		$('#Owner').append(data[0].name).attr('href',data[0].web_url);
		}
	})

}

function req_access(type,id){
var requestUrl;
	if(type==projectsAccess){
		this.id=id;
		var requestUrl=ctx+'/rest/GitService/projects/'+id+'/access_requests';
	}else if(type==groups){
		this.id=id;
		var requestUrl=ctx+'/rest/GitService/groups/'+id+'/access_requests';
	}

$('#btn').click(function(){
	
	$.ajax({
		type:'get',
		url: requestUrl,
		data:{
		access_token:accessToken,
		},
		success: function(data){
			
			$('#success').show().text(' Request Succesful');
			pass(data.requested_at);
			$('#btn').attr('disabled',true);
			
		}
	})
	})
}

function pass(date){
	var d = new Date(date);
	document.getElementById('success').innerHTML= "Request successful "+d.toLocaleString();
}
function filterFunction() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    div = document.getElementById("projectList");
    a = div.getElementsByTagName("option");
    for (i = 0; i < a.length; i++) {
        if (a[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
            a[i].style.display = "";
        } else {
            a[i].style.display = "none";
        }
    }
} 


