							//CHART TO DISPLAY SERVER UTILITY DETAILS					
												$(document).ready(function() {
													utility();
														 function utility(){
													    var cpudata=new Array();
													    var memdata=new Array();
													   var datetime=new Array();
													   var date="";
														 //'http://localhost:8080/DevopsDashboard/rest/ServerService/utilitydata'
														   var jqxhr = $.ajax(ctx+"/rest/ServerService/utilitydata")
													        .done(function(data) {  
													        	if(data.length=="0"){
													           	 alert("Details not found");
													           	 document.getElementById("projectDetails").style.visibility='hidden';
													            }else{
													        	//console.log(data);
													        	//document.getElementById("projectDetails").style.visibility='visible';// alert("success");
															for (var i =0; i<data.length ; i++) {
													 	    var object = data[i];
													 	    for (var property in object) {
													 	         if(object[property]=="cpu.util"){
													 	        	for (var property in object) {
													 	        	if(property=="data"){
													 	        	cpudata=object[property];}}
													 	        	}else if(object[property]=="mem.util"){
													 	        		for (var property in object) {
															 	        	if(property=="data"){
															 	        		memdata=object[property];}}
													 	        	}
													 	        	
													            }
															}
														/*console.log(cpudata);	
														console.log(memdata);*/	
														
														cpudata= cpudata.map(Number);
														$.map(cpudata, function(value,index) { 
															return parseInt(value); 
															});
														//console.log(cpudata);
														
														memdata= memdata.map(Number);
														$.map(memdata, function(value,index) { 
															return parseInt(value); 
															});
														//console.log(memdata);
														for(var i=0;i<memdata.length;i++){
															memdata[i]=(memdata[i]/(1024*1024));
														}
														for(var i=0;i<memdata.length;i++){
															memdata[i]=(Math.floor(memdata[i] * 100)) / 100;
														}
														for(var i=0;i<cpudata.length;i++){
															cpudata[i]=(Math.floor(cpudata[i] * 100)) / 100;
														}
														var currentDate=new Date($.now());
														//var date=currentDate.getDate();
														var hour=currentDate.getHours();
														var hour1=hour-1;
														var minutes=currentDate.getMinutes();
														//console.log(currentDate);
														date=currentDate.getDate()+'/'+(currentDate.getMonth()+1)+'/'+currentDate.getFullYear();
														//console.log("date:"+date);
														//console.log(date+'/'+currentDate.getMonth()+'/'+currentDate.getFullYear()+'T'+hour+':'+minutes);
														//datetime.push(date-1+'/'+currentDate.getMonth()+'/'+currentDate.getFullYear()+'T'+hour+':'+minutes);
														datetime.push(hour1+':'+minutes);
														for(var k=0;k<12;k++){
															minutes=minutes+5;
															if(minutes>=60){
																hour1=hour1+1;
																minutes=minutes%60;
																datetime.push(hour1+':'+minutes);
															}else{
																datetime.push(hour1+':'+minutes);	
															}
															
														}
													//CHART TO SHOW CPU UTILISATION   
													Highcharts.chart('vmcpuutility', {
													    chart: {
													        type: 'line',
													        options3d: {
													            enabled: true,
													            alpha: 15,
													            beta: 15,
													            viewDistance: 25,
													            depth: 40,
													            animation:false
													        }
													    },

													    title: {
													        text: 'CPU Usage('+date+')'
													    },

													    xAxis: {
													        categories:datetime,
													        labels: {
													            skew3d: true,
													            style: {
													                fontSize: '8px'
													            }
													        }
													    },

													    yAxis: {
													        allowDecimals: false,
													        min: 0,
													        title: {
													            text: 'CPU Usage(in %)',
													            skew3d: true
													        }
													    },

													    tooltip: {
													        headerFormat: '<b>'+date+'T{point.key}</b><br>',
													        pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}',
													        shared:true
													    },

													    plotOptions: {
													        column: {
													            stacking: 'normal',
													            depth: 40
													        },
													        series: {
													            animation: false
													        }
													    },

													    series: [{
													        name: 'CPU Usage(in %)',
													        data:cpudata
													    }]
													});
													//})
													
													//$(function(){
													Highcharts.chart('vmmemutility', {
													    chart: {
													        type: 'line',
													        options3d: {
													            enabled: true,
													            alpha: 15,
													            beta: 15,
													            viewDistance: 25,
													            depth: 40,
													            animation:false
													        },
													        events: {
													           /* load: function () {
													                var series = this.series[0];
													                setInterval(function () {
													                	function redraw() {
													                		 var chart = $('#vmutility').highcharts();
													                		        chart.yAxis[0].isDirty = true;
													                		        chart.redraw();
													                	    }
													                }, 1000);
													            }*/
													        }
													    },

													    title: {
													        text: 'Memory Usage('+date+')'
													    },

													    xAxis: {
													        categories:datetime,
													        labels: {
													            skew3d: true,
													            style: {
													                fontSize: '8px'
													            }
													        }
													    },

													    yAxis: {
													        allowDecimals: false,
													        min: 0,
													        title: {
													            text: 'Memory Usage(in GB)',
													            skew3d: true
													        }
													    },

													    tooltip: {
													        headerFormat: '<b>'+date+'T{point.key}</b><br>',
													        pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}',
													        shared:true
													    },

													    plotOptions: {
													        column: {
													            stacking: 'normal',
													            depth: 40
													        },
													        series: {
													            animation: false
													        }
													    },

													    series: [{
													    	name: 'Memory Usage(in GB)',
													        data:memdata
													    }]
													});
												//	})
															} })
														        .fail(function() { 
														        	document.getElementById("projectDetails").style.visibility='hidden';
														        	alert("No details found 500 response please check log"); 
														        
														        })
														 }
													setInterval(function(){
														utility();
													 }, 300000);
														       });