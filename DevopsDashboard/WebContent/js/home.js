 $(document).ready(function() {
        $('#homeTable').DataTable({
        	"aLengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
            "iDisplayLength": 5
        });
    });
    
     $(document).ready(function(){
  	  $("#mySearch").on("keyup", function() {
  	    var value = $(this).val().toLowerCase();
  	    $("#homeTable tr").filter(function() {
  	      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
  	    });
  	  });
  	}); 
    	 
  
    window.onload = function(){
    $("#vmdetails").fadeOut(500, function() {
        $("#gitprojects").fadeIn(500, function() {
        });
    });
    }
    function show(){
    	 $("#vmdetails").fadeOut(500, function() {
    	        $("#gitprojects").fadeIn(500, function() {
    	        });
    	    });
   }
    function hide(){
    	document.getElementById("vmdetails").style.visibility="visible"
    	 $("#gitprojects").fadeOut(500, function() {
    	        $("#vmdetails").fadeIn(500, function() {
    	        });
    	    });
    }
    var iconok=ctx+"/assets/img/icons8-ok-500.png";
    var iconERROR=ctx+"/assets/img/icons8-box-important-100.png";
    $(document).ready(function() {
        $('#serverTable').DataTable( {
            ajax:  {
                url: ctx+'/rest/ServerService/vmdetails',
                dataSrc: ''
            },
            columns: [
            	{ data: "name" },
                { data: "vm" },
                { data: "memory_size_MiB" },
                { data: "power_state",  render: function ( data, type, row ) {
                    if ( type === 'display' ) {
                    	if(data=="POWERED_ON"){
                    		var numberRenderer = $.fn.dataTable.render.number( ',', '.', 0, '$' ).display;
                    		 return '<p style="color:#28D634;"> <img class="repoertckeckUnckeck" style="color:#28D634;" src='+iconok+'>'+numberRenderer( data )+ '</p>';	
                    	}else if(data=="POWERED_OFF"){
                    		var numberRenderer = $.fn.dataTable.render.number( ',', '.', 0, '$' ).display;
                    		return '<p style="color:#FF3E30;"><img class="repoertckeckUnckeck" style="color:#FF3E30;" src='+iconERROR+'>'+numberRenderer( data )+ '</p>';
                    	}	
                    }
                    return data;
                } },
                { data: "cpu_count" }
            ],
            aLengthMenu: [[5, 10, 15, -1], [5, 10, 15, "All"]],
            iDisplayLength: 5
        });
        
        $.fn.dataTable.ext.errMode = 'none';
        $('#assetsTable').on( 'error.dt', function ( e, settings, techNote, message ) {
     	    console.log( 'An error has been reported by DataTables: ', message );
     	    } ) ; 
    }); 
    
    $(".iqloader").click(function(){ 
    	$.LoadingOverlaySetup({
    		background : "rgba(0, 0, 0, 0.5)",
    		image : "images/logo.png",
    		imageAnimation : "2s fadein",
    		imageColor : "#ffcc00"
    		});
    	var customElement = $('<div id="loader"></div>');
    	$("#homeinnerelement").LoadingOverlay("show", {
    	image :"",
    	custom : customElement,
    	text : "Gathering information"
    	});

    	setTimeout(function(){
    	$("#homeinnerelement").LoadingOverlay("text", "Just a momemt");
    	}, 3000);
    	});
    
   /* jQuery(function ($) {
  	  var target = $('#target');

  	  $('.toggle-loading').click(function () {
  	    if (target.hasClass('loading')) {
  	      target.loadingOverlay('remove');
  	    } else {
  	      target.loadingOverlay();
  	    };
  	  });
  	});*/
    
$(function(){
    	
    	$('input[value="jira"]').on("click",function(ev){
    		ev.preventDefault();
    			 var jira={
							key:$('#project_key').val(),
							name:$('#project_name').val(),
							lead:$('input[list="lead"]').val(),
							projectTypeKey:'business',
							description:$('#proj_desrciption').val(),
							assigneeType:'PROJECT_LEAD',
					};
    			 $.ajax({
 					type:'POST',
						url:ctx+'/rest/JiraService/create',
						data:JSON.stringify(jira),
						contentType:'application/json',
						dataType:'json',
 					success: function(){
 						swal({
				        		 icon: "success",
				        		  title: "Succcess",
				        		  text: "Project created at tracking level",
				        		  timer: 3000
				        		});
							},
		    				error: function(){
		    					 swal("Failed", "Problem creating project at tracking level", "error",{timer:3000});
		    				}
 						
 					})	
    	
    	})
    })