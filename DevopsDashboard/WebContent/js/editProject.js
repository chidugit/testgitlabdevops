var projectID;
function editProject(data){
	projectID=data.getAttribute("data-project-id");
	$.ajax({
		type:'get',
		url:ctx+'/rest/GitService/projects/'+data.getAttribute("data-project-id")+'',
		async:false,
		data:{
			access_token:accessToken,
			},
		success: function(info){
			$('#projectName').attr("value",info.name);
			$('#projectDescription').attr("value",info.description);
			$('#projectPath').attr("value",info.path);
			if(info.issues_enabled==true){
				$('#issuesEnabled').attr('checked','checked');
			}else if(info.merge_requests_enabled==true){
				$('#mergeRequestsEnabled').attr('checked','checked');
			}else if(info.request_access_enabled==true){
				$('#requestAccess').attr('checked','checked');
			}
				
			$('#owner').html(info.owner.name);
			
		}
	});
}


var original;
var orgProject;

function orgVal(item){

original=document.getElementById(item.id).value;
orgProject=document.getElementById("projectName").value;
}

console.log(original);

function newVal(item){

var current=document.getElementById(item.id).value;
if(original!=current){
document.getElementById("submit").removeAttribute("disabled");
}else{
document.getElementById("submit").setAttribute("disabled","disabled")
}


}

function update(){
	var issues_enabled;
	var request_access_enabled;
	var merge_requests_enabled;
	if($("#issuesEnabled").is(':checked')){
		issues_enabled=true;
	}else{
		issues_enabled=false;
	}
	if($("#mergeRequestsEnabled").is(':checked')){
		merge_requests_enabled=true;
	}else{
		merge_requests_enabled=false;
	}
	if($("#requestAccess").is(':checked')){
		request_access_enabled=true;
	}else{
		request_access_enabled=false;
	}
	var projName=document.getElementById("projectName").value;
	var projDescription=document.getElementById("projectDescription").value;
	var jsonData={
			
				name:projName,
				description:projDescription,
				path:document.getElementById("projectPath").value,
				issues_enabled:issues_enabled,
				merge_requests_enabled:merge_requests_enabled,
				request_access_enabled:request_access_enabled
				
			};
	
	$.ajax({
		
		type:'put',
		url:ctx+'/rest/GitService/update/projects/'+projectID+'/?access_token='+accessToken,
		async:false,
		dataType:"json",
		contentType:"application/json",
		data:JSON.stringify(jsonData),
		success: function(){
			console.log(orgProject);
			console.log(projName);
			$.ajax({
				type:'get',
				url:ctx+'/rest/JenkinsService/update/?oldJobName='+orgProject+'&newJobName='+projName+'',
				async:false,
				dataType:"json",
				contentType:"application/json",
			})
		}
	})
}
