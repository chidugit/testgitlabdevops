//JAVASCRIPT FUNCTION TO GET COMMIT DETAILS FROM GITLAB USING COMMIT_DETAILS SERVICE
											$(document).ready(function() {
													    var commitid=new Array();
													    var commitDate=new Array();
													    var commitsperDay=new Array();
													    var commitDatefiltered=new Array();
													    var dateforchart=new Array();
													    console.log(accesstoken);
													    console.log(projectId);
														 //'http://localhost:8080/DevopsDashboard/rest/GitService/commits?access_token=e26efc8c29dd996baaecd95245370656769d546a75a8da91c67d09e54596e298&id=21'
														   var jqxhr = $.ajax(ctx+"/rest/GitService/commits?access_token="+accesstoken+"&id="+projectId)
													        .done(function(data) {  
													        	if(data.length=="0"){
													           	 //alert("Details not found");
													           	 document.getElementById("commitsPerDay").style.visibility='hidden';
													           	document.getElementById("commitsnotfound").style.visibility='visible';
													            }else{
													        	document.getElementById("commitsPerDay").style.visibility='visible';// alert("success");
															for (var i =data.length-1; i>=0 ; i--) {
													 	    var object = data[i];
													 	    for (var property in object) {
													 	         if(property=="committed_date"){
													 	        	
													 	        	//var date = new Date(parseInt(object[property].substr(6)));
													 	        	 var tempDate=object[property].split("T");
													 	        	commitDate.push(tempDate[0]);
													 	        	}else if(property=="id"){
													 	        		commitid.push(object[property]);
													 	        	}
													 	        	
													            }
															}
														//console.log(commitDate);	
														//console.log(commitid);	
														
														commitDatefiltered.push(commitDate[0]);
														
														
														var tempArray=commitDate;
														commitDatefiltered.push(commitDate[0]);
														console.log(tempArray.length);
														for(var i=0; i<tempArray.length;i++){
															var temp=1;
															if(tempArray[i]!=null){
																if(i!=tempArray.length-1){
															for(var j=i+1; j<tempArray.length;j++){
															if(tempArray[i]!=null&&tempArray[j]!=null){	
															if(tempArray[i].localeCompare(tempArray[j])==0){
																tempArray[j]=null;
																temp++;
																//console.log(temp);
															}
														}
														}
																}
																commitsperDay.push(temp);
														}
														}	
														//console.log(tempArray);
														//console.log(commitDatefiltered);
														
													//console.log(commitsperDay);
													
													for(var i=0; i<tempArray.length;i++){
														if(tempArray[i]!=null){
															dateforchart.push(tempArray[i]);
														}
													}	
													//console.log(dateforchart);
													//CHART TO SHOW NUMBER OF COMMITS IN A DAY   
													$(function(){
													Highcharts.chart('commitsPerDay', {
													    chart: {
													        type: 'column',
													        zoomType: 'xy'
													        /*options3d: {
													            enabled: true,
													            alpha: 15,
													            beta: 15,
													            viewDistance: 25,
													            depth: 40
													        }*/
													    },

													    title: {
													        text: 'Number of commits in a day'
													    },

													    xAxis: {
													    	title:{
												        		text:'Date'
												        	},
													        categories:dateforchart,
													        labels: {
													            skew3d: true,
													            style: {
													                fontSize: '8px'
													            }
													        }
													    },

													    yAxis: {
													        allowDecimals: false,
													        min: 0,
													        title: {
													            text: 'Number of Builds',
													            skew3d: true
													        }
													    },

													    tooltip: {
													        headerFormat: '<b>{point.key}</b><br>',
													        pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}',
													        shared:true
													    },

													    plotOptions: {
													        column: {
													            stacking: 'normal',
													            depth: 40
													        }
													    },

													    series: [{
													        name: 'commits in a day',
													        data:commitsperDay ,
													    }]
													});
													})
													
													
															} })
														        .fail(function() { 
														        	document.getElementById("commitsPerDay").style.visibility='hidden';
														        	 document.getElementById("commitsnotfound").style.visibility='visible';
														        	//alert("No details found 500 response please check log"); 
														        
														        })
														       });