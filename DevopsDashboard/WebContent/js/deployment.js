 var input  = '<input type="text" name="dynamicInput" class="form-control" placeholder="Enter Environment Name"/>'; //input html
   var select = '<select>'+ //select html
                       '<option value="0">0</option>'+
                       '<option value="1">1</option>'+
                       '<option value="2">2</option>'+
                       '<option value="3">3</option>'+
                       '<option value="4">4</option>'+
                 '</select>';

   var row = '<p>'+ input+ '</p>'; //row html
   var container = $("#container"); //cache selector for better performance
   var i = 0; // variable used in loop

   $("#rowNumber").change(function(){ //when the main select changes
       var numRows = $(this).val(); //get its value
       
       container.html(""); //empty the container
       
       for(i=1; i<= numRows; i++){ 
           container.append(row); //and append a row from 1 to the numRows
       }
   });

   container.delegate('p > select','change',function(){ //when a select inside
       //a row changes
       var numInputs = $(this).val(); //get its value
       var parent = $(this).parent(); //select its parent
       var width = 170/numInputs;
       parent.html(""); //empty it
       for(i=1; i<=numInputs; i++){
           parent.append(input); //append an input from 1 to the numINputs
       }
       parent.append(select); //and append a select
   });
   var assign=[];
   $(function(){
 	    $(document).on('keyup','#container', function(){
 	        assign=  $( "input[name=dynamicInput]" ).map(function() {
 	            return $( this ).val();
 	          })
 	          .get()
 	        console.log(assign);
 	    })
 	    }); 
 	$('#onPrembtn').on("click",function(ev){
				  var enrol={
						  vCenter_IP:$('#vCenter_IP').val(),
						  vCenter_Username:$('#vCenter_Username').val(),
						  vCenter_Password:$('#vCenter_Password').val(),
						  VM_Hostname:$('#VM_Hostname').val(),
						  VM_IP:$('#VM_IP').val(),
						  VM_Subnet:$('#VM_Subnet').val(),
						  VM_Gateway:$('#VM_Gateway').val()
						};
			$.ajax({
					type:'POST',
					url:ctx+'/rest/deployment/createOnpremDetails',
					data:JSON.stringify(enrol),
					contentType:'application/json',
					dataType:'json',
					success:function(info){
			        		 swal({
				        		 icon: "success",
				        		  title: "Succcess",
				        		  text: "File created Successfully",
				        		  timer: 3000
				        		}).then(function(isConfirm) {
				        			  if (isConfirm) {
				        				    location.reload();
				        				  } else {
				        					  location.reload();
				        				  }
				        				});
			        	
			        },
			        error: function(info){
			        		swal("Error"+(info.status), "Unable create file", "error");
			       }
			})
	        	
})

$('#awsbtn').on("click",function(ev){
				  var enrol={
						  aws_access_key_id:$('#aws_access_key_id').val(),
						  aws_secret_access_key:$('#aws_secret_access_key').val(),
						  role_name:$('#role_name').val(),
						  region:$('#region').val(),
						  app_name:$('#app_name').val(),
						  solution_stack:$('#stack').val(),
						  env_names:assign
						};
			$.ajax({
					type:'POST',
					url:ctx+'/rest/deployment/createAWSDetails',
					data:JSON.stringify(enrol),
					contentType:'application/json',
					dataType:'json',
					success:function(info){
			        		 swal({
				        		 icon: "success",
				        		  title: "Succcess",
				        		  text: "File created Successfully",
				        		  timer: 3000
				        		}).then(function(isConfirm) {
				        			  if (isConfirm) {
				        				    location.reload();
				        				  } else {
				        					  location.reload();
				        				  }
				        				});
			        	
			        },
			        error: function(info){
			        		swal("Error"+(info.status), "Unable create file", "error");
			       }
			})
	        	
})
      
      $("#vCenter_IP, #vCenter_Username, #vCenter_Password,#VM_Hostname , #VM_IP, #VM_Subnet, #VM_Gateway").keyup(function() {
      	if($('#vCenter_IP').val()!="" && $('#vCenter_Username').val()!="" && $('#vCenter_Password').val()!="" && $('#VM_Hostname').val()!="" && $('#VM_IP').val()!="" && $('#VM_Subnet').val()!="" && $('#VM_Gateway').val()!=""){
      	$('#onPrembtn').prop('disabled', false);
      	}else{
      		$('#onPrembtn').prop('disabled', true);
      	}
      	});
      $("#aws_access_key_id, #aws_secret_access_key, #role_name,#region , #app_name, #stack").keyup(function() {
        	if($('#aws_access_key_id').val()!="" && $('#aws_secret_access_key').val()!="" && $('#role_name').val()!="" && $('#region').val()!="" && $('#app_name').val()!="" && $('#stack').val()!=""){
        	$('#awsbtn').prop('disabled', false);
        	}else{
        		$('#awsbtn').prop('disabled', true);
        	}
        	});
    
   function handleSubmit(ev){
	 ev.preventDefault();
	 
 } 
    
//ip mask
   var options =  { 
		    onKeyPress: function(text, event, currentField, options){
		    	var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
		                    if (text){
		                        var ipArray = text.split(".");
		                        var lastValue = ipArray[ipArray.length-1];  	
		                        if (ipArray[3] !== undefined) {
			                        if (ipArray[3].length <= 3 && ipArray[3] <= 255) {
			                        	$('#vCenter_IP').css({ 'border-color': 'green' });
			                        	//alert("You have entered an valid IP address!");  
			                        }                   	
		                        }
		                        if(lastValue != "" && parseInt(lastValue) > 255){
		                            var resultingValue = ipArray.join(".");
				                    var inputF = document.getElementById("vCenter_IP");
		                            if(!resultingValue.match(ipformat))
				                    {
				                      	//alert("You have entered an invalid IP address!");
		                            	$('#vCenter_IP').css({ 'border-color': 'red' });
				                      	inputF.value = text.substring(0, text.length - 1);
				                    }
		                            currentField.text(resultingValue);
		                        }
		                    }             
		                }, 
		    translation: {
		            'Z': {
		                pattern: /[0-9]/, optional: true
		            }
		    }
		};
	/* $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {translation: {'Z': {pattern: /[0-9]/, optional: true}}}); */
	$(".ip_address").mask("0ZZ.0ZZ.0ZZ.0ZZ", options); 
	
	 var options1 =  { 
			    onKeyPress: function(text, event, currentField, options){
			    	var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
			                    if (text){
			                        var ipArray = text.split(".");
			                        var lastValue = ipArray[ipArray.length-1];  	
			                        if (ipArray[3] !== undefined) {
				                        if (ipArray[3].length <= 3 && ipArray[3] <= 255) {
				                        	$('#VM_IP').css({ 'border-color': 'green' });
				                        	//alert("You have entered an valid IP address!");  
				                        }                   	
			                        }
			                        if(lastValue != "" && parseInt(lastValue) > 255){
			                            var resultingValue = ipArray.join(".");
					                    var inputF = document.getElementById("VM_IP");
			                            if(!resultingValue.match(ipformat))
					                    {
					                      	//alert("You have entered an invalid IP address!");
			                            	$('#VM_IP').css({ 'border-color': 'red' });
					                      	inputF.value = text.substring(0, text.length - 1);
					                    }
			                            currentField.text(resultingValue);
			                        }
			                    }             
			                }, 
			    translation: {
			            'Z': {
			                pattern: /[0-9]/, optional: true
			            }
			    }
			};
		/* $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {translation: {'Z': {pattern: /[0-9]/, optional: true}}}); */
		$(".ip_address1").mask("0ZZ.0ZZ.0ZZ.0ZZ", options1); 