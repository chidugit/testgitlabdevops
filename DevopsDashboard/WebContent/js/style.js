$(document).ready(function(){
          $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
          });
        });
//window.location.protocol + "//" + window.location.host + window.location.pathname + window.location.search;
//var newURL = window.location.protocol + "//" + window.location.host;
//console.log("window url:"+newURL);
//console.log(newURL+ctx+"/web?action=dashboard&name=vignesh&password=welcome123");
jenFixed= parseInt(jenFixed);
sonFixed=parseInt(sonFixed);
testFixed=parseInt(testFixed);
console.log(total);
console.log(bluemix);
console.log(onprem);
console.log(aws);
var bluemixper=parseInt((bluemix/total)*100);
var onpremper=parseInt((onprem/total)*100);
var awsper=parseInt((aws/total)*100);
var notdeployed=100-(bluemixper+onpremper+awsper);
console.log(bluemixper);
console.log(onpremper);
console.log(awsper);

$(function(){
	Highcharts.setOptions({
		credits:{
			enabled:false
		}
	});
    Highcharts.chart('overallprojstat', {
    
    	chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        title: {
            text:'Overall Project Statistics' //'Projects<br>passed in<br>respective stage',
            /*align: 'center',
            verticalAlign: 'middle',
            y: 40*/
        },
        tooltip: {
            pointFormat: '{series.name}'
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: true,
                    distance: -60,
                    style: {
                    	fontFamily: '\'Lato\', sans-serif', 
                    	lineHeight: '18px', 
                    	fontSize: '13px',
                        color: 'white'
                    }
                },
                startAngle: 0,
                endAngle: 360,
                center: ['50%', '50%'],
                size: '80%',
                innerSize: '40%',
            }
        },
        series: [{
            type: 'pie',
            name: 'Success',
            innerSize: '50%',
            data:[{
                "name": "Unit Test"+testFixed+"%",
                "y": testFixed,
                "color":'#597db7'
            },{
                "name": "Build:"+jenFixed+"%",
                "y": jenFixed,
                "color":"#597db7db"
            },{
                "name": "Quality"+sonFixed+"%",
                "y": sonFixed,
                "color":'#597db7ba'
            }]
        }]
    });
    
    
    var colors = Highcharts.getOptions().colors,
    categories = [
        'Blumix',
        'AWS',
        'Onprem',
        'Not Deployed'
    ],
    data = [
        {
            y:bluemixper,
            color: colors[2]
        },
        {
            y: onpremper,
            color: colors[1]
        },
        {
            y: awsper,
            color: colors[0]
        },
        {
            y: notdeployed,
            color: colors[3]
        }
    ],
    browserData = [],
    versionsData = [],
    i,
    j,
    dataLen = data.length,
    drillDataLen,
    brightness;


// Build the data arrays
for (i = 0; i < dataLen; i += 1) {

    // add browser data
    browserData.push({
        name: categories[i],
        y: data[i].y,
        color: data[i].color
    });

    // add version data
    //drillDataLen = data[i].drilldown.data.length;
    /*for (j = 0; j < drillDataLen; j += 1) {
        brightness = 0.2 - (j / drillDataLen) / 5;
        versionsData.push({
            name: data[i].drilldown.categories[j],
            y: data[i].drilldown.data[j],
            color: Highcharts.Color(data[i].color).brighten(brightness).get()
        });
    }*/
}

// Create the chart
Highcharts.chart('overallprojstattest', {
    chart: {
        type: 'pie'
    },
    title: {
        text: 'Projects In Various Environments'
    },
    subtitle: {
        //text: 'Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
    },
    plotOptions: {
        pie: {
            shadow: false,
            center: ['50%', '50%']
        }
    },
    tooltip: {
        valueSuffix: '%'
    },
    series: [{
        name: 'Deployed projects(in %)',
        data: browserData,
        size: '80%',
        innerSize: '40%',
        dataLabels: {
            formatter: function () {
                return this.y > 5 ? this.point.name : null;
            },
            color: '#ffffff',
            distance: -30
        }
    }],
    responsive: {
        rules: [{
            condition: {
                maxWidth: 400
            },
            chartOptions: {
                series: [{
                }, {
                    id: 'versions',
                    dataLabels: {
                        enabled: false
                    }
                }]
            }
        }]
    }
});


function renderIcons() {

 if (!this.series[0].icon) {
        this.series[0].icon = this.renderer.path(
            ['M', -8, 0, 'L', 8, 0, 'M', 0, -8, 'L', 8, 0, 0, 8,
                'M', 8, -8, 'L', 16, 0, 8, 8]
        )
            .attr({
                stroke: '#ffffff',
                'stroke-linecap': 'round',
                'stroke-linejoin': 'round',
                'stroke-width': 2,
                zIndex: 10
            })
            .add(this.series[2].group);
    }
    this.series[0].icon.translate(
        this.chartWidth / 2 - 10,
        this.plotHeight / 2 - this.series[0].points[0].shapeArgs.innerR -
            (this.series[0].points[0].shapeArgs.r - this.series[0].points[0].shapeArgs.innerR) / 2
    );

    if (!this.series[1].icon) {
        this.series[1].icon = this.renderer.path(
            ['M', -8, 0, 'L', 8, 0, 'M', 0, -8, 'L', 8, 0, 0, 8,
                'M', 8, -8, 'L', 16, 0, 8, 8]
        )
            .attr({
                stroke: '#ffffff',
                'stroke-linecap': 'round',
                'stroke-linejoin': 'round',
                'stroke-width': 2,
                zIndex: 10
            })
            .add(this.series[2].group);
    }
    this.series[1].icon.translate(
        this.chartWidth / 2 - 10,
        this.plotHeight / 2 - this.series[1].points[0].shapeArgs.innerR -
            (this.series[1].points[0].shapeArgs.r - this.series[1].points[0].shapeArgs.innerR) / 2
    );

    if (!this.series[2].icon) {
        this.series[2].icon = this.renderer.path(
            ['M', -8, 0, 'L', 8, 0, 'M', 0, -8, 'L', 8, 0, 0, 8,
                'M', 8, -8, 'L', 16, 0, 8, 8]
        )
            .attr({
                stroke: '#ffffff',
                'stroke-linecap': 'round',
                'stroke-linejoin': 'round',
                'stroke-width': 2,
                zIndex: 10
            })
            .add(this.series[2].group);
    }
    this.series[2].icon.translate(
        this.chartWidth / 2 - 10,
        this.plotHeight / 2 - this.series[2].points[0].shapeArgs.innerR -
            (this.series[2].points[0].shapeArgs.r - this.series[2].points[0].shapeArgs.innerR) / 2
    );
}
Highcharts.chart('overallprojstattest1', {

    chart: {
        type: 'solidgauge',
       // height: '80%',
        events: {
          render: renderIcons
        }
    },

    title: {
        text: 'Overall Project Statistics',
        style: {
            //fontSize: '24px'
        }
    },

    tooltip: {
        borderWidth: 0,
        backgroundColor: 'none',
        shadow: false,
        style: {
            fontSize: '16px'
        },
        pointFormat: '{series.name} success<br><span style="font-size:2em; color: black; font-weight: bold">{point.y}%</span>',
        positioner: function (labelWidth) {
            return {
                x: (this.chart.chartWidth - labelWidth) / 2,
                y: (this.chart.plotHeight / 2) + 35
            };
        }
    },

    pane: {
        startAngle: 0,
        endAngle: 360,
        background: [{ // Track for Move
            outerRadius: '100%',
            innerRadius: '76%',
            backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[0])
                .setOpacity(0.3)
                .get(),
            borderWidth: 0
        }, { // Track for Exercise
            outerRadius: '75%',
            innerRadius: '51%',
            backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[1])
                .setOpacity(0.3)
                .get(),
            borderWidth: 0
        }, { // Track for Stand
            outerRadius: '50%',
            innerRadius: '26%',
            backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[2])
                .setOpacity(0.3)
                .get(),
            borderWidth: 0
        }]
    },

    yAxis: {
        min: 0,
        max: 100,
        lineWidth: 0,
        tickPositions: []
    },

    plotOptions: {
        solidgauge: {
            dataLabels: {
                enabled: false
            },
            linecap: 'round',
            stickyTracking: false,
            rounded: true
        }
    },

    series: [{
        name: 'Unit Test',
        data: [{
            color: '#597db7ba',
            radius: '100%',
            innerRadius: '76%',
            y: testFixed
        }]
    }, {
        name: 'Quality',
        data: [{
            color: '#597db7db',
            radius: '75%',
            innerRadius: '51%',
            y: sonFixed
        }]
    }, {
        name: 'Builds',
        data: [{
            color: '#597db7',
            radius: '50%',
            innerRadius: '26%',
            y:jenFixed 
        }]
    }]
});
    })   

      function filterthis(ele){
        var textdomain = ele.textContent;
        if(textdomain == "All"){
          textdomain = "";
        }
        $("#myTable1 tr").filter(function() {
              $(this).toggle($(this).text().toLowerCase().indexOf(textdomain) > -1)
            });
      }


      function sortTable() {
  var table, rows, switching, i, x, y, shouldSwitch;
  table = document.getElementById("myTable");
  switching = true;
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.getElementsByTagName("TR");
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 1; i < (rows.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[0];
      y = rows[i + 1].getElementsByTagName("TD")[0];
      //check if the two rows should switch place:
      if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
        //if so, mark as a switch and break the loop:
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
    }
  }
    }


  var tablehead = document.getElementById("sortProject");
 // alert(tablehead.textContent);

  