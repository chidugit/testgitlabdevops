//JAVASCRIPT FUNCTION TO GET BUGS DETAILS FROM SONARQUBE USING BUGS_DETAILS 
    $(document).ready(function() {              
var JenkinsData;
var SonarQubedata;
var Sonardata = new Array(); 
var jenkinsdata = new Array();
var SonarIssuescreationdate= new Array();
var JenkinsSuccessdate= new Array();
var jenkinsconvertdate = new Array();
var buildnumber = new Array();
var SonarcloseDate = new Array();
var dateforchart = new Array();
var countIssues=new Array();
var Storeresult = new Array();
var sonarcreation = new Array();
var totalcount = new Array();
var Tempdates= new Array();
var OldIssuess = new Array();
var dates  = new Array();
var store = new Array();
var sonardetails = new Array();
var sonarolddate = new Array();
var sonardates = new Array();
document.getElementById("name").innerHTML= projectName;
var arrayOfStrings = projectName.split("_");
var str=arrayOfStrings[0];
var strName=str+"_sonar";
$.when(
    $.ajax({ // First Request 
        url: ctx+"/rest/JenkinsService/project_details?projectname="+projectName+"&name="+userName+"&password="+userPassword,
        type: "Get",           
        success: function(returnhtml){     
        	JenkinsData = returnhtml; 
        	jenkinsdata.push(JenkinsData);
         }
    
    }),
    $.ajax({ //Seconds Request
        url: ctx+"/rest/SonarQubeService/issues_details?projectname="+strName, 
        type: "Get",         
        success: function(returnhtml){                          
        	SonarQubedata = returnhtml; 
        	Sonardata.push(SonarQubedata);
        		
        }           
    })
)
.done(function() {
    $('#JenkinsData').html(JenkinsData);
    $('#SonarQubedata').html(SonarQubedata);
    if(JenkinsData.length=="0" || SonarQubedata.length=="0")
    {
    	document.getElementById("issueTracking").style.visibility='hidden';
    	document.getElementById("issuestracking").style.visibility='visible';
     }
    
    else{
	     document.getElementById("issueTracking").style.visibility='visible';
		    for (var i = 0;i<=JenkinsData.length-1;i++)
			   {
		    	
				   if(JenkinsData[i].result === "SUCCESS")
				   { 
				        var tempdateformat = (JenkinsData[i].buildTimeformated);
				        var buildId=(JenkinsData[i].buildId);

				        	JenkinsSuccessdate.push(Date.parse(tempdateformat)); 
						     buildnumber.push(buildId);
				        
				   }				
			    
			   } 	
		   		    
		    for (var j=0;j<=SonarQubedata.length-1;j++)
				 {
		             if(SonarQubedata[j].status === "OPEN")
						 { 
							  var tempDateopen=(SonarQubedata[j].creationDate);
							  SonarIssuescreationdate.push(Date.parse(tempDateopen));
							  var tempdates =(SonarQubedata[j].creationDate.split("T"))
							  sonardates.push(tempdates[0]);  	 
					     }
						       	    
					   else if(SonarQubedata[j].status === "CLOSED")
						   {
						   var tempDateclosed=(SonarQubedata[j].closeDate);
						   var tempcreationdate =(SonarQubedata[j].creationDate)
						    SonarcloseDate.push(Date.parse(tempDateclosed));
						   sonarcreation.push(Date.parse(tempcreationdate));
						    	
						   }
		            
		             
				   }
		    
		   var  jenkinsdatesuccess=JenkinsSuccessdate;
		   var  jenkinsIdsuccessnumber=buildnumber;
		   var   sonaropendate = SonarIssuescreationdate;
		   var data = Tempdates.sort()
		   var  temp = SonarIssuescreationdate.sort();
		   var   sonarclosedcreationdate=SonarcloseDate;
	        var  Sonarcloseddate = sonarcreation;
		     	
	        for(var i=0;i<=jenkinsdatesuccess.length-1;i++)
			  {   
	        	            var count=0;
					  for(var j=0;j<=sonaropendate.length-1;j++)
						  {  
						           if(sonaropendate[j]!=null&&JenkinsSuccessdate[i]!=null) 
						        	   {
									          if(sonaropendate[j]<=JenkinsSuccessdate[i])
									    	  {
									    	    count++;
									    	    
									    	  }
						        	   }
						  }
			  
					  for(var m=0;m<=sonarclosedcreationdate.length-1;m++)
						  {
						      for(var k=0;k<=Sonarcloseddate.length-1;k++)
							   {
						    	    if(Sonarcloseddate[k]!=null&&JenkinsSuccessdate[i]!=null&&sonarclosedcreationdate[m]!=null)
						    	    	{
									       if((Sonarcloseddate[k]>=JenkinsSuccessdate[i])&&(sonarclosedcreationdate[m]<=JenkinsSuccessdate[i]))
									    	{
									    	   
									    	    count++;
									    	}
						    	    	}  
							        
							   }
						     
						  }
			  countIssues.push(count); 
			  
			  }
		         
		   for(var i=jenkinsIdsuccessnumber.length-1;i>=0;i--)
		   {
		     dateforchart.push(jenkinsIdsuccessnumber[i]);
		   }
		   
		   for(var j=countIssues.length-1;j>=0;j--)
			   {
			   totalcount.push(countIssues[j]); 
			   }
		   
		   sonardates.sort(function(a,b) {
	        	  return new Date(a.sonardates) - new Date(b.sonardates)
	        	})

	        	var oldDate= sonardates[0];

		   SonarQubedata.filter(el => {
			   if(el.status === "OPEN" && el.creationDate.split("T")[0] === oldDate){
				   sonardetails.push(el);
			   }
		   });

			//CHART TO SHOW NUMBER OF Issues count   
			$(function(){
			Highcharts.chart('issueTracking', {
			    chart: {
			    	 zoomType: 'xy',
			        type: 'column'
			    },
			    title: {
			        text: 'Issue Tracking'
			    },
			    xAxis: {
			    	title:{
		        		text:'Build Number'
		        	},
			        categories:dateforchart,
				    	 skew3d: true,
			             
				    	 style:{
				    		 fontSize: '8px',
				    	 }
			    },
			    yAxis: {
			        allowDecimals: false,
			        min: 0,
			        title: {
			            text: 'Number of Issues',
			            skew3d: true
			        }
			    },
			    plotOptions: {
			        column: {
			            stacking: 'normal',
			            depth: 40
			        }
			    },

			    tooltip:{
			    	  headerFormat:'<span style="font-size:11px">{point.key}</span><br>',
			    	  pointFormat:'Number of Issues(open):<b>{point.y}</b>'
		    	 },
			    series: [{
			    	
			    	     name:'successful Build number',
				    	 data:totalcount,
				    	     
			    }]
			});
			})
		}    
    })
		   	   
  .fail(function() { 
	  document.getElementById("issueTracking").style.visibility='hidden';
  	document.getElementById("issuestracking").style.visibility='visible';
});

    });


