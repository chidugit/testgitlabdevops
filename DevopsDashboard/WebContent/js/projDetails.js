							//JAVASCRIPT FUNCTION GET DETAILS FROM JENKINS USING PROJECT_DETAILS SERVICE
													$(document).ready(function() {	
													    var buildid=new Array();
														   var result=new Array();
														   var buildsuccess1=new Array();
														   var buildfailure1=new Array();
														   var buildabort1=new Array();
														   var buildDuration=new Array();
														   var durationInSec=new Array();
														   var avgTimeofBuilds=new Array();
														   document.getElementById("name").innerHTML= projectName;
														   console.log(ctx+"/rest/JenkinsService/project_details?projectname="+projectName);
														   var jqxhr = $.ajax( ctx+"/rest/JenkinsService/project_details?projectname="+projectName+"&name="+userName+"&password="+userPassword)
													        .done(function(data) {  
													        	if(data.length=="0"){
													           	 //alert("Details not found");
													           	 document.getElementById("avgTime").style.visibility='hidden';
													            }else{
													            	console.log(data);
													        	document.getElementById("avgTime").style.visibility='visible';// alert("success");
															for (var i =data.length-1; i>=0 ; i--) {
													 	    var object = data[i];
													 	    for (var property in object) {
													 	         if(property=="buildId"){
													 	        		buildid.push(object[property]);
													 	        	}else if(property=="duration"){
													 	        		buildDuration.push(object[property]);
													 	        	} 
													 	        	else if(property=="result"){
													 	        		result.push(object[property]);
													 	        	}
													 	          }
													 	    }
														
																//console.log(buildid);
																buildDuration= buildDuration.map(Number);
																$.map(buildDuration, function(value,index) { 
																	return parseInt(value); 
																	});
															//	console.log(buildDuration);
																for (var i = 0; i < buildDuration.length; i++) {
																	durationInSec.push((buildDuration[i]/1000));
																}
																//console.log(durationInSec)
																var temp=0;
																var count=0;
																for (var i = 0; i < durationInSec.length;i++) {
																	if(result[i]=="SUCCESS"){
																	count=count+1;
																	temp=temp+durationInSec[i];
																	avgTimeofBuilds.push(temp/count);
																	}else{
																		avgTimeofBuilds.push(avgTimeofBuilds[i-1]);
																	}
																	}
																for (var i = 0; i <avgTimeofBuilds.length; i++) {
																	avgTimeofBuilds[i]=parseInt(avgTimeofBuilds[i]);
																}
															//	console.log(avgTimeofBuilds);
																
																for(var i=0;i<result.length;i++){
																	if(result[i]=="SUCCESS"){
																		buildsuccess1.push(durationInSec[i]);
																		buildabort1.push(null);
																		buildfailure1.push(null);
																	}else if(result[i]=="ABORTED"){
																		buildsuccess1.push(null);
																		buildabort1.push(durationInSec[i]);
																		buildfailure1.push(null);
																	}
																	else{
																		buildfailure1.push(durationInSec[i]);
																		buildsuccess1.push(null);
																		buildabort1.push(null);
																	}
																}
																
																for (var i = 0; i <buildsuccess1.length; i++) {
																	buildsuccess1[i]=parseInt(buildsuccess1[i]);
																}
																for (var i = 0; i <buildfailure1.length; i++) {
																	buildfailure1[i]=parseInt(buildfailure1[i]);
																}
																for (var i = 0; i <buildabort1.length; i++) {
																	buildabort1[i]=parseInt(buildabort1[i]);
																}
																//console.log(buildid);
																
															//CHART TO SHOW AVERAGE TIME TAKEN BY EACH BUILD	
																$(function(){
																	Highcharts.setOptions({
																		credits:{
																			enabled:false
																		},
																        lang: {
																            drillUpText: 'Back'
																        }
																    });
																	Highcharts.chart('avgTime', {
																		 chart: {
																		        zoomType: 'xy'
																		    },
																		    title: {
																		        text: 'Overall Duration(Build to Deploy)'
																		    },
																		    subtitle: {
																		        text: null
																		    },
																		    xAxis:{
																		    	title:{
																	        		text:'Build Number'
																	        	},
																			        categories:buildid,
																			        labels: {
																			            skew3d: true,
																			            style: {
																			                fontSize: '10px'
																			            }
																			        },
																		    	crosshair: true
																		    	},
																		    yAxis: [{ // Primary yAxis
																		        labels: {
																		            style: {
																		                color: Highcharts.getOptions().colors[0]
																		            }
																		        },
																		        title: {
																		            text:null,
																		            style: {
																		                color: Highcharts.getOptions().colors[0]
																		            }
																		        }
																		    }, { // Secondary yAxis
																		        title: {
																		        	text: 'Time(in sec)',
																		            style: {
																		                color: Highcharts.getOptions().colors[0]
																		            }
																		        },
																		    }],
																		    plotOptions: {
																		        series: {
																		            point: {
																		                events: {
																		                    click: function () {
																		                    	//http://192.168.137.101:8080/job/DevopsDashboard_ecommerce/213/execution/node/3/log
																		                    	//location.href = 'http://192.168.137.101:8080/job/DevopsDashboard_ecommerce/213/execution/node/3/log';
																		                       // location.href = 'http://192.168.137.101:8080/job/'+projName+'/';//+
																		                            //this.options.key;
																		                    }
																		                }
																		            }
																		        }
																		    },
																		    tooltip:{
																		    	shared:true
																	        },
																	        legend: {
																	        	layout: 'horizontal',
															                    align: 'center',
															                    verticalAlign: 'bottom',
																		        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)'
																		    },
																		    series: [{
																		    	name: 'Time taken(Build Success)',
																		        type: 'column',
																		        data:buildsuccess1,
																		        color:'#2979FF',
																		        tooltip: {
																		            valueSuffix: 'sec'
																		        },
																		        stack: 'success',
																		        key:'time'

																		    },{
																		    	name: 'Time taken(Build Failed)',
																		        type: 'column',
																		        data:buildfailure1	,
																		        color:'#f45b5b',
																		        tooltip: {
																		            valueSuffix: 'sec'
																		        },
																		        stack: 'fail',
																		        key:'fail'

																		    },{
																		    	name: 'Time taken(Build Aborted)',
																		        type: 'column',
																		        data:buildabort1,
																		        color:'#455A64',
																		        tooltip: {
																		            valueSuffix: 'sec'
																		        },
																		        stack: 'abort',
																		        key:'abort'

																		    },
																		    {
																		        name: 'Average Time',
																		        type: 'spline',
																		        data:avgTimeofBuilds,
																		        color:'#00C853',
																		        tooltip: {
																		            valueSuffix: 'sec'
																		        }
																		    }],
																	});
																})	
													        } })
													        .fail(function() { 
													        	document.getElementById("avgTime").style.visibility='hidden';
													        	 document.getElementById("avgnotfound").style.visibility='visible';
													        	//alert("No details found 500 response please check log"); 
													        
													        })
													        
												  }); 

