   $("#project_name, #lead_name").keyup(function() {
    	  	if($('#project_name').val()!="" && $('#lead_name').val()!=""){
    	  	$('#submitbutton').prop('disabled', false);
    	  	}else{
    	  		$('#submitbutton').prop('disabled', true);
    	  	}
    	  	});
      
      function handleChange(event){
    	  const {max,min,value,name}=event;
    	  if(value>max){
    		 if(name=="minutes"){
    			 event.value=58
    		 }else if(name=="hours"){
    			 event.value=23
    		 }else if(name=="dow"){
    			 event.value=7
    		 }
    		 
    	  }
    	  if(value<min){
    		  event.value=0
    	  }
      }
      
      $(function(){
    	  $('input[name="buildType"]').on("click",function(){
    	  if($('input[name="buildType"]:checked').val()==="Build Periodically"){
    		  $('#minutes').removeAttr('disabled');
    		  $('#hours').removeAttr('disabled');
    		  $('#dow').removeAttr('disabled');
    		  $('#minutes').focus()
    	  }else{
            $('#minutes').attr('disabled',true);
      		 $('#hours').attr('disabled',true);
      		 $('#dow').attr('disabled',true);
            };
    	  });
      });
      
      
      $(function () {
    	    // when the modal is closed
    	    $('#myModal #myform').on('hidden.bs.modal', function () {
    	        // remove the bs.modal data attribute from it
    	        $(this).find('p').val('')
    	    });
    	});
      
      $(function(){
    	  
    	  var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];
    	  
    	  $('input[value="submit"]').on("click",function(){
    		  
    		  $('#myform #project_name').text($("#project_name").val());
    		  
    		  $('#myform #group_path').text($('input[name="group_path"]').val());
    		  
    		  $('#myform #group_name').text($("#group_name").val());
    		  
    		  $('#myform #description').text($('#proj_desrciption').val());
    		 
    		  $('#myform #visibility').text($("#visibility").val());
    		 
    		  $('#myform #industry').text($("#industry").val());
    		  
    		  $('#myform #buildType').text($('input[name="buildType"]:checked').val())
    		 
    		  $('#myform #project_key').text($("#project_key").val());
    		  
    		  $('#myform #lead_name').text($("#lead_name").val());
    		
    		  $('#myform #assignee').text($("#assignee").val());
    		  if($('input[name="buildType"]:checked').val()==="Build Periodically"){
    			  $('label[for="schedule"]').show();
    			  $('#myform #schedule').show();
    			  $('#myform #schedule').text(" Every week "+weekday[$("#dow").val()]+" at "+$("#hours").val()+"hours and "+$("#minutes").val()+"minutes.\n")
    		  }
    		  if($('input[name="buildType"]:checked').val()==="Tag Push"){
    			  $('#myform #schedule').hide();
    			  $('label[for="schedule"]').hide()
    		  }
    	  })
      })
     
   $(function(){
    	$('input[id=group_path]').keyup(function(){
    		var txtClone = $(this).val();
    		$('input[id=group_name]').val(txtClone);
    	});
    });
    
    function filterFunction(){
      	var input, filter
      	input = document.getElementById("group_path");
      	filter= input.value.toUpperCase();
      	div = document.getElementById("path");
      	a = div.document.getElementByTagName("option");
      	 for (i = 0; i < a.length; i++) {
             if (a[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
                 a[i].style.display = "";
             } else {
                 a[i].style.display = "none";
             }
         }
      }
 $.ajax({
    				type: "GET",
    				async: false,
    				url: ctx+"/rest/GitService/groups",
    				data: {
    					access_token :accessToken,
    							
    				},
    				dataType:"text",
    				success: function(data,status){
    					var result = jQuery.parseJSON(data);
    							$.each(result,function(k,v){
    							$('#grouppath').append($('<option></option>').attr('value',v.path));	
    							});
    					
    				}
    			})
    		
    $(function(){
    	$("#grouppath").change(function(){
    		$.ajax({
    	
    		type:"GET",
    		url:ctx+"/rest/GitService/groups",
    		data: {
				access_token :accessToken,
				search: $('input[id=grouppath]').val() ,		
			},
			dataType:"text",
			success: function(data){
				$.each(JSON.parse(data), function(k, v) {
	            	
	           $("#groupname").attr('value',v.name);
	            	
	            });
			},
			error: function(jqXHR, textStatus){
				//alert("error : "+textStatus)
				swal("Failed", "Unable to get groups from repository", "error",{timer:3000});
			}
    	})
    })
    })
    
   function handleSubmit(ev){
	 ev.preventDefault();
	 
 } 
    
  window.onload= (function(){
    	$.ajax({
    		type:"GET",
    		url:ctx+'/rest/JiraService/users',
    		contentType:'application/json',
    		success: function(data){
    			$.each(jQuery.parseJSON(JSON.stringify(data)),function(k,v){
    				
    				$("#lead").append($('<option></option>').attr('value',v.name));
    			})
    		},
    		error: function(jqXHR, textStatus){
				//alert("error : "+textStatus);
    			swal("Failed", "Unable to get users from tracking unit", "error",{timer:3000});
			},
			timeout:3000
    	})
    })
    
    $(function(){
    	$('#project_name').on("keyup",function(){
    		var name= $('#project_name').val().toUpperCase();
    		if(name.length==3){
    			$("#project_key").val(name);
    		}
    	})
    })
    
    
    $(function(){
    	
    	$('input[value="Confirm"]').on("click",function(ev){
    		ev.preventDefault();
    		var minutes,hours,dayOfWeek; 
        	if($('#minutes').val()==""){
        		minutes = "*"
        	} else {
        		minutes=$('#minutes').val();
        	};
        	
        	if($('#hours').val()==""){
        		hours = "*"
        	}else{
        		hours = $('#hours').val()
        	};
        	
        	if($('#dow').val()==""){
        		dayOfWeek = "*"
        	}else{
        		dayOfWeek = $('#dow').val()
        	};
    		var jsondata= {
    				 name:$("#project_name").val()+'_'+$("#industry").val(),
    				 description:$('#proj_desrciption').val(),
    				 visibility:$("#visibility").val(),
    				 groupName:$("#group_name").val(),
    				 groupPath:$("#group_path").val(),
    				 
    			 };
    			 var jira={
							key:$('#project_key').val(),
							name:$('#project_name').val(),
							lead:$('input[list="lead"]').val(),
							projectTypeKey:'business',
							description:$('#proj_desrciption').val(),
							assigneeType:'PROJECT_LEAD',
					};
    			 if($("#selenium").is(':checked')){
					var jenkinsdata={
    						name:$("#project_name").val(),
    						buildTriggerType:$('input[name="buildType"]:checked').val(),
    						selenium:"true",
    						schedule:minutes+" "+hours+" * * "+dayOfWeek
    				};
    			 }else{
    				 var jenkinsdata={
     						name:$("#project_name").val(),
     						buildTriggerType:$('input[name="buildType"]:checked').val(),
     						selenium:"false",
     						schedule:minutes+" "+hours+" * * "+dayOfWeek
     				};
			   		}
					var webhook={
							tag_push_events:'true',
							push_events: 'true',
							projectName: $("#project_name").val(),
					};
					var projectID;
					$.ajax({
		    			type:'POST',
		    			url:ctx+'/web?action=jiranewproj',
		    			contentType:'application/json',
		    			dataType:'json',
		    			success: function(info){			
		    				//console.log(info);
		    				//console.log(info.status);
		        		/* swal({
			        		 icon: "success",
			        		  title: "Succcess",
			        		  text: "license key updated successfully",
			        		  timer: 3000
			        		}).then(function(isConfirm) {
			        			  if (isConfirm) {
			        				    location.reload();
			        				  } else {
			        					  location.reload();
			        				  }
			        				});*/
									
					$.ajax({
    					type:'POST',
						url:ctx+'/rest/JiraService/create',
						data:JSON.stringify(jira),
						contentType:'application/json',
						dataType:'json',
    					success: function(){
    						swal({
				        		 icon: "success",
				        		  title: "Succcess",
				        		  text: "Project created at tracking level",
				        		  timer: 3000
				        		});
    					
    					$.ajax({
						type:'POST',
	    				url:ctx+'/rest/JenkinsService/createjob',
	    				data:JSON.stringify(jenkinsdata),
	    				contentType:'application/json',
	    				//dataType:'json',
						success: function(json){	
							swal({
				        		 icon: "success",
				        		  title: "Succcess",
				        		  text: "Job created at monitoring level",
				        		  timer: 3000
				        		});
    				$.ajax({
    			type:'POST',
    			url:ctx+'/rest/GitService/createProject1?access_token='+accessToken,
    			data:JSON.stringify(jsondata),
    			contentType:'application/json',
    			dataType:'json',
    			success: function(data){
    				projectID=data.id;
    				swal({
		        		 icon: "success",
		        		  title: "Succcess",
		        		  text: "Project Repository created",
		        		  timer: 3000
		        		});
    				 $.ajax({
    						type:'POST',
							url:ctx+'/rest/GitService/projects/'+projectID+'/hooks?access_token='+accessToken,
							data:JSON.stringify(webhook),
							contentType:'application/json',
							dataType:'json',
    						success: function(info){
    							console.log(info);
    							swal({
   				        		 icon: "success",
   				        		  title: "Succcess",
   				        		  text: "Project created successfully in all the levels with hooks",
   				        		  timer: 4000
   				        		}).then(function(isConfirm) {
   			        			  if (isConfirm) {
  		        				    location.reload();
  		        				  window.location.href=ctx+'/Home.jsp';
  		        				  } else {
  		        					  location.reload();
  		        					 window.location.href=ctx+'/Home.jsp';
  		        				  }
  		        				});
    									
    								},
    			    				error: function(){
    			    					 swal("Failed", "Problem creating hooks", "error",{timer:3000});
    			    				}
    							})
    			},
    			error: function(info){
    				//console.log(info.responseJSON);
    				var value=info.responseJSON.message.base[0];
    				swal("Failed", value, "error");
    			}
    			
    		})				
					
    				},
    				error: function(info){
    					var value=info.responseJSON.error;
    					swal("Failed", value, "error");
    				}
    				
    					})

							},
		    				error: function(info){
		    					var value=info.responseJSON.errors.projectKey;
		    					 swal("Failed", value+"**First three characters of project name is used as key**make sure first three characters of project name is unique**", "error");
		    				}
    						
    					})
    	},
		error: function(jqXHR,textStatus){
			console.log(textStatus);
			console.log(jqXHR);
			$("#errorMsg").val(textStatus)
		}
	})
    	})
    })
    
