<!DOCTYPE html>
<%@page import="java.util.Base64"%>
<html lang='en'>
<head>
   <meta charset='utf-8'>
   <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/Logo.png">
   <title>Creating New Project</title>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
  
   <link rel="stylesheet" href="css/project_create.css"> 
   <link rel="icon" type="image/png" href="images/Logo.png">
   <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/font-awesome.min.css"/>
	<%-- <link href="${pageContext.request.contextPath}/assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/> --%>
	<script>var ctx = "${pageContext.request.contextPath}"</script> 
	<script src="${pageContext.request.contextPath}/assets/js/jquery-3.3.1.min.js"></script>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/styles.css"/>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/default-css.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/themify-icons.css">
	 <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/metisMenu.css">
	 <script src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/popper.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/sweetalert.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/loadingoverlay.js"></script>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/loaderOverlay.css">
<style>
body, .wrapper {
     min-height: 80vh;
     position: relative; 
}
/* span {
	float: left;
} */

#myform label {
	
    font-size: medium;
    
}
#myform span {
	
    font-size: medium;
    
}

tr{
border-style: hidden;  
}
    	.parent {
  position: relative;
  top: 0;
  left: 0;
}
span{
	font-size:small;
	}
#cirruslogobg {
  position: relative;
  top: -14px;
    left: -24px;
}
#cirruslogo {
  position: absolute;
  top: 0px;
  left: 0px;
}
.form-control, .form-control:focus{
background-color:#ffffff!important;
box-shadow: 0 1px 3px 0 rgba(0,0,0,.2);
}
#loader {
  background-image:url("${pageContext.request.contextPath}/assets/img/Logo.png");
  }
</style>
<script type="text/javascript">
$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
</script>
</head>
<body>
<% if (session.getAttribute("ProjectsList") == null || session.getAttribute("username")==null){
		response.sendRedirect("index.jsp");
		return;
	} 
	
if (session.getAttribute("validity").equals("true")== false){
	 response.sendRedirect(request.getContextPath());
		return;
	}
String ckusername=null;
String ckpass=null;
String ckaccess=session.getAttribute("Access_level").toString();
String ckvalid=session.getAttribute("validity").toString();
Cookie cookie = null;
Cookie[] cookies = null;
cookies = request.getCookies();
if( cookies != null ) {
   for (int i = 0; i < cookies.length; i++) {
      if(cookies[i].getName().equals("IQSESSIONID")){
    	  try{
    	 String token= cookies[i].getValue();
    	 Base64.Decoder decoder = Base64.getMimeDecoder();
    	 String tokendecoded=new String(decoder.decode(token));
    	 String[] tokendecode=tokendecoded.split("!q4Ev");
    	String ckusernameencode=new String(decoder.decode(tokendecode[0]));
    	String ckpassencode=new String(decoder.decode(tokendecode[1]));
    	String ckaccessencode=new String(decoder.decode(tokendecode[2]));
    	String ckvalidencode=new String(decoder.decode(tokendecode[3]));
    	ckusername=ckusernameencode;
    	ckpass=ckpassencode;
    	ckaccess=ckaccessencode;
    	ckvalid=ckvalidencode;
    	  }catch(Exception e){
    		  
    	  }
      }
   }
} 
	%>
	 
        <!-- page container area start -->
        <div class="page-container">
        <!-- sidebar menu area start -->
               <div class="sidebar-menu">
            <div class="sidebar-header">
                <div class="logo" style="/* width: 300px; */">
                 <div class="parent">
  						<img id="cirruslogobg" src="${pageContext.request.contextPath}/assets/img/DevopsBg.png" style="float: left;height: 110px;">
  						<img id="cirruslogo" alt="" src="${pageContext.request.contextPath}/assets/img/RevisedHomeCirrusDigitalLogo-01.png" style="float: left;height: 70px;padding-top: 10px;">
					</div>
                </div>
            </div>
            <div class="main-menu">
                <div class="menu-inner">
                    <nav>
                        <ul class="metismenu" id="menu">
                        <li><a class="iqloader" href="${pageContext.request.contextPath}/Home.jsp"><i class="ti-home"></i> <span>Home</span></a></li>
                        <li><a class="iqloader" href="${pageContext.request.contextPath}/web?action=dashboard"><i class="ti-dashboard"></i> <span>Dashboard</span></a></li>
                        <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-folder"></i><span>Projects</span></a>
                                <ul class="collapse">
                                    <!-- <li><a href="#" onclick="hide();"><i class="ti-view-list-alt" style="padding-right:10px; "></i><span>List of Projects</span></a></li> -->
                                     <%if (ckvalid!=null){
                                     if (ckvalid.equals("true")){%>
                                    <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-package"></i><span>Join Project</span></a>
                                <ul class="collapse">
                                    <li><table>
    				<tr>
    				<tr>
    				<td>
    				<div class="form-group has-search">
                 <input class="form-control" type="text" placeholder="Search.." id="myInput" list="projectList" onkeyup="filterFunction()" style="background-color: aliceblue;"></div>
					    <datalist id="projectList" ></datalist></td></tr>
					    
					    <tr>
                       <td>
                       <button class="btn btn-primary btn-sm" id="btn" style="border-radius: 1rem;padding: 5px 13px;box-shadow: 0 1px 3px 0 rgba(0,0,0,.2);">Request Access</button>
							<p id="err" style="color:red; display:none"> Project/Group access is disabled </p>
							<p id="success" style="color:green; display:none"> </p></td></tr>
							</table></li>
                                </ul>
                            </li> 
                             <%}else{ %>
                              <li>
                                <a href="#"><i class="ti-package"></i><span>Join Project</span></a>
                            </li> 
                              <%}} %>
                            <%if (ckaccess!=null && ckvalid!=null){
                            if (ckaccess.equals("admin") && ckvalid.equals("true")){%>
                                    <li><a href="${pageContext.request.contextPath}/web?action=project" class="iqloader"><i class="ti-new-window"></i><span>Create project</span></a></li>
                                    <%}} %>
                                </ul>
                            </li>
                            <%if (ckaccess!=null){
                            if (ckaccess.equals("admin")){%>
                                    <!-- <li><a href="#" data-toggle="modal" data-target="#licenseupdate"><i class="ti-new-window"></i><span>License</span></a></li> -->
                                    <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-settings"></i><span>Admin</span></a>
                                <ul class="collapse">
                                    <li><a href="${pageContext.request.contextPath}/license.jsp" class="iqloader"><i class="fa fa-unlock-alt"></i><span>License</span></a></li>
                                    <%if (ckvalid!=null){
                                    if (ckvalid.equals("true")){%>
                                    <li><a href="${pageContext.request.contextPath}/UserManagement.jsp" class="iqloader"> <i class="ti-user"></i><span>User Management</span></a></li>
                                      <%}} %>
                                    <!-- <ul class="collapse">
                                    <li><a href="index.html"><i class="fa fa-user-plus"></i><span>Add User</span></a></li>
                                    <li><a href="index2.html"><i class="fa fa-edit"></i><span>Modify User</span></a></li>
                                    <li><a href="index3.html"><i class="ti-trash"></i><span>Delete User</span></a></li>
                               		 </ul> -->
                                    
                                </ul>
                            </li>
                                    <%} }%>
        						<li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-help"></i><span>Help</span></a>
                                <ul class="collapse">
                                    <li><a href="index.html"><span>Contact Us</span></a></li>
                                    <li><a href="index2.html"><span>FAQ</span></a></li>
                                    <li><a href="index3.html"><span>About</span></a></li>
                                </ul>
                            </li>
                            <%-- <li><a href="${pageContext.request.contextPath}/web?action=logout"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li> --%>
                            <li><a href="${pageContext.request.contextPath}/Logout"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div> 
        
        
        <!-- sidebar menu area end -->
        <!-- main content area start -->
         <!-- Breadcrumbs-->
          
             
            <!-- header area end -->
          
            <!-- page title area end -->
            <div id="loaderarea">
            <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">Project</a>
            </li>
            <li class="breadcrumb-item active">Create new project</li>
          </ol>
             <div class="container">
        <div id="mainContent">
        <div class="main-content" style="background-color: white;">
          <fieldset>
          <div class="form-row">
    <div class="col-md-4 mb-3">
      <label>Project Name</label><span style="color:red;font-size: medium;font-weight: bold;">*</span>
      <input type="text" class="form-control form-control-lg" id="project_name" name="project_name" placeholder="Project name" autocomplete="off" required="required" style="/* font-size: medium;height:20%; */"autofocus >
    </div>
    <div class="col-md-4 mb-3">
      <label>Group path</label><i class="ti-info-alt" data-toggle="tooltip" title="(group path can contain only letters, digits, '_', '-' and should not start with '-' or end with '.' , '.git' , '.atom')" style="padding-left: inherit;color: #007bff!important;"></i>
      <input list="grouppath" id="group_path" class="form-control form-control-lg" type="text" name="group_path" placeholder="Project group path" style="/* font-size: medium;height:20%; */" >
			                     <datalist id="grouppath"></datalist>
    
    </div>
    <div class="col-md-4 mb-3">
      <label>Group name</label>
         <input class="form-control form-control-lg" id="group_name"  type="text" name="group_name" placeholder="Project group name" style="/*  font-size: medium;height:20%; */"> 
      </div>
    </div>
    <br>
  <div class="form-row">
    <div class="col-md-3 mb-3">
      <label for="visibility" style="/* font-size: medium; font-style:normal;font-weight:normal;color: #4a9afb;padding-top: 7px; */">Visibility</label>
      <select class="form-control" id="visibility" name="visibility" style="padding-top: 0px;padding-bottom: 0px; ">
									        <option value="private">private</option>
									        <option value="internal">internal</option>
									        <option value="public">public</option>
									      </select>
    </div>
    <div class="col-md-3 mb-3">
      <label for="industry" style="/* font-size: medium; font-style:normal;font-weight:normal;color: #4a9afb;padding-top: 7px; */">Industries</label>
      <select class="form-control" id="industry" name="industry" style="padding-top: 0px;padding-bottom: 0px;">
									        <option value="none">none</option>
									        <option value="transport">transport</option>
									        <option value="retail">retail</option>
									        <option value="banking">banking</option>
									        <option value="telecom">telecom</option>
									      </select>
    </div>
    <div class="col-md-6 mb-3">
      <label>Description</label>
      <textarea class="form-control" id="proj_desrciption" name="description" placeholder="Description" rows="3" cols="0"></textarea>
     <!-- <input type="text" class="form-control" name="description" placeholder="Description" style="height:100px;/* font-size: medium; */" autocomplete="off"> -->
    </div>
  </div>
     <br>
  <div class="form-row" style="padding-top:12px;">
   <!--  <div class="form-check"> -->
	
       <div class="col-md-4 mb-3">
       <label>Trigger Build On :&nbsp; &nbsp;</label>
  <input type="radio" name="buildType" id="Tag Push" value="Tag Push" checked><label style="font-size: medium.0.0;height:20%;font-weight:normal" for="Tag Push">Tag Push</label>
  </div>
  <div class="col-md-2 mb-3">			                    
<input type="radio" name="buildType" id="Build Periodically" value="Build Periodically"> <label style="font-size: medium;height:20%;font-weight:normal" for="Build Periodically">Periodic</label>
 </div>
<div class="col-md-2 mb-3">
	<label style="font-size: medium;height:20%;font-weight:normal">Minutes:</label> <input id="minutes" name="minutes" type="number" size="2" min="0" max="59" maxLength="2" onChange="handleChange(this)" disabled>
	</div>
	<div class="col-md-2 mb-3">
 <label style="font-size: medium;height:20%;font-weight:normal">Hours:</label> <input id="hours" name="hours" type="number" size="2" min="0" max="23" maxLength="2" onChange="handleChange(this)" disabled>
 </div>
 <div class="col-md-2 mb-3">
<label style="font-size: medium;height:20%;font-weight:normal">Days of Week:</label> <input id="dow" name="dow" type="number" size="2" min="0" max="7" maxLength="1" onChange="handleChange(this)" disabled>
	
	</div>
   
    
    <!-- </div> -->
  </div>
     <br>
  <div class="form-row"> 
  <div class="col-md-6 mb-3">
  <label style="/* font-size: medium;height:20%;font-weight:normal */" for="lead" >Project Lead Name</label><span style="color:red;font-size: medium;font-weight: bold;">*</span>
  <input class="form-control" style="/* font-size: medium;*/height:55%;" type="text" list="lead" id="lead_name">
			                    <datalist id="lead"></datalist>
  </div>
  <div class="col-md-6 mb-3">
  <label for="assignee" style="/* font-size: medium;height:20%;font-weight:normal */">Assignee for Issues Created </label>
  <Select class="form-control" id="assignee" style="/* font-size: medium;  font-weight:normal */ height:55%;">
			                    <option value="PROJECT_LEAD" selected>PROJECT_LEAD</option>
			                    <option value="COMPONENT_LEAD">COMPONENT_LEAD</option>
			                    <option value="UNASSIGNED">UNASSIGNED</option>
			                    <option value="PROJECT_DEFAULT">PROJECT_DEFAULT</option>
			                    </Select>
  </div>
  </div>
     <br>
     <div>
                                <div class="custom-control custom-checkbox mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="selenium">
                                    <label class="custom-control-label" for="selenium" style="color: black;">Selenium</label>
                                </div>
                            </div>
                            <br>
  <div class="form-row">
  <div style="padding-left: 43%;">
  <span data-toggle="tooltip" title="Project name and Project lead required">
  <input id="submitbutton" style="width: 100px;" type="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal" value="submit" disabled="disabled">
  </span>
  </div>
  </div>
  <!-- <button class="btn btn-primary" type="submit">Submit form</button> -->
          </fieldset>
         </div>
         </div>
         </div>
         </div>
</div>
			               
	 <div class="container">
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
          <div class="modal-dialog">
          
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header" style="border-bottom: none;">
              <h4 class="modal-title">Confirmation</h4>
               <button style="font-size:2.5rem;"type="button" class="close" data-dismiss="modal">&times;</button>
                
              </div>
              <div class="modal-body">
              <form id="myform" >
             <fieldset style="border:none">
             <label >Project Name:</label> <span id="project_name"></span><br>
             <label> Group Path:</label> <span id="group_path"> </span><br>
             <label>Group Name:</label><span id="group_name"></span><br>
             <label>Description:</label>  <span id="description"></span><br>
             <label>Visibility:</label> <span id="visibility"></span><br>
             <label>Industry:</label> <span id="industry"></span><br>
             <fieldset>
             <legend>Jenkins</legend>
             <label>Build Type:</label> <span id="buildType"></span><br>
             <label for="schedule">Runs on: </label><span id="schedule"></span>
             </fieldset>
             <fieldset>
             <legend>JIRA section</legend>
             <label>Project Key:</label> <span id="project_key"></span><br>
             <label>Project Lead Name:</label> <span id="lead_name"></span><br>
             <label>Assignee for issues created:</label> <span id="assignee"></span>
             </fieldset>
              </fieldset>
			</form>
             </div>
              <div class="modal-footer" style="border-top: none;">
              	<input id="submit" type="submit" class="btn btn-info" data-dismiss="modal" value='Confirm' ></input>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
            
          </div>
        </div>
        
      </div> 
    
  <script src="${pageContext.request.contextPath}/assets/js/owl.carousel.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/metisMenu.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/jquery.slimscroll.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/jquery.slicknav.min.js"></script>
    <!-- others plugins -->
    <script src="${pageContext.request.contextPath}/assets/js/plugins.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/scripts.js"></script> 
   <script type="text/javascript">
    accessToken="<%=session.getAttribute("access_token")%>";
    </script>
     <script>var ctx = "${pageContext.request.contextPath}"</script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/joinProject.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/create_project.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/overlayLoader.js"></script>
</body>
</html>