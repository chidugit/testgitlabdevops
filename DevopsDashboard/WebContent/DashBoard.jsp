<!DOCTYPE html>
<%@page import="javax.ws.rs.core.Response"%>
<%@page import="com.cirrus.devops.service.JenkinsService"%>
<%@page import="com.cirrus.devops.service.GetGitlabGroups"%>
<%@page import="java.util.Base64"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="org.json.simple.*"
import="com.cirrus.devops.projects.*"
import="java.util.ArrayList"
import="java.util.Iterator" %>
<html>
    <head>
        <meta charset="utf-8" />
	<link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/Logo.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>DashBoard</title>
	
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
   
    <link href="${pageContext.request.contextPath}/css/dataTable.css" rel="stylesheet" />
    
   <script>var ctx = "${pageContext.request.contextPath}"</script>
    <!--  Light Bootstrap Table core CSS    -->
    <link href="${pageContext.request.contextPath}/assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/assets/css/demo.css" rel="stylesheet" />
     <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/demo.css"/>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
     <script src="${pageContext.request.contextPath}/assets/js/jquery-3.3.1.min.js"></script>
	<link href="${pageContext.request.contextPath}/assets/css/googleicon.css" rel="stylesheet" />
	<script src="${pageContext.request.contextPath}/assets/js/loader-gstatic.js"></script>
    <!--     Fonts and icons     -->
    <link rel="javascript" type="text/javascript" href="${pageContext.request.contextPath}/togglefunction.js"/>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/projects.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css"/>
      <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/themify-icons.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/metisMenu.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/owl.carousel.min.css">
   
     <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/default-css.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/styles.css">
    <script src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/popper.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/Highcharts-7.1.0/code/highcharts.js"></script>
	<script src="${pageContext.request.contextPath}/assets/Highcharts-7.1.0/code/highcharts-more.js"></script> 
		<script src="${pageContext.request.contextPath}/assets/Highcharts-7.1.0/code/modules/series-label.js"></script>
	<script src="${pageContext.request.contextPath}/assets/Highcharts-7.1.0/code/modules/exporting.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/export-data.js"></script>  
	<script src="${pageContext.request.contextPath}/assets/Highcharts-7.1.0/code/modules/solid-gauge.js"></script>  
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/DataTables-1.10.18/css/dataTables.bootstrap4.min.css"/>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/DataTables-1.10.18/js/dataTables.bootstrap4.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/serverUtility.js"></script>
	<script src="${pageContext.request.contextPath}/js/dashboard.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/loadingoverlay.js"></script>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/loaderOverlay.css">
 <style>
 thead, tr, th, td {
	    text-align: center;
	    padding: 1%;
	}
   .parent {
	  position: relative;
	  top: 0;
	  left: 0;
	}
	#cirruslogobg {
	  position: relative;
	  top: -14px;
    left: -24px;
	}
	#cirruslogo {
	  position: absolute;
	  top: 0px;
	  left: 0px;
	}
	label{
	display:inline-block;
	margin-right:30px;
	/* width:200px; */
	}
	span{
	font-size:small;
	}
	
	div.dataTables_wrapper div.dataTables_length select {
    width: 60px;
    margin: 0 10px;
}
.card-body {
    padding: 1.6rem;
}

.form-control, .form-control:focus{
background-color: white!important;
box-shadow: 0 1px 3px 0 rgba(0,0,0,.2);
}
.mr-5, .mx-5 {
    margin-right: 0em!important;
}

.checkbox-menu li label {
    display: block;
    padding: 3px 10px;
    clear: both;
    font-weight: normal;
    line-height: 1.42857143;
    color: #333;
    white-space: nowrap;
    margin:0;
    transition: background-color .4s ease;
}
.checkbox-menu li input {
    margin: 0px 5px;
    top: 2px;
    position: relative;
}

.checkbox-menu li.active label {
    background-color: #cbcbff;
    font-weight:bold;
}

.checkbox-menu li label:hover,
.checkbox-menu li label:focus {
    background-color: #f5f5f5;
}

.checkbox-menu li.active label:hover,
.checkbox-menu li.active label:focus {
    background-color: #b8b8ff;
}
.main-content-inner {
    padding-top: 0px;
    padding-right:0px;
    padding-left: 30px;
    padding-bottom: 0px;
}

table.dataTable{
border-collapse:collapse !important;
}
.table > thead > tr > th{
padding-bottom:10px;
}
table.dataTable thead>tr>th.sorting_asc, table.dataTable thead>tr>th.sorting_desc, table.dataTable thead>tr>th.sorting, table.dataTable thead>tr>td.sorting_asc, table.dataTable thead>tr>td.sorting_desc, table.dataTable thead>tr>td.sorting{
color: black;
background-color: #DEE9EB;
}
thead, tr, th, td{
color: gray;
font-size: small;
}
.btn{
	padding: .375rem .75rem;
}
#loader {
  background-image:url("${pageContext.request.contextPath}/assets/img/Logo.png");
  }
 
    </style>
    </head>
    <body>
    <% if (session.getAttribute("ProjectsList") == null || session.getAttribute("username")==null){
		response.sendRedirect("index.jsp");
		return;
	}
    String ckusername=null;
	String ckpass=null;
	String ckaccess=null;
	String ckvalid=null;
	Cookie cookie = null;
	Cookie[] cookies = null;
	cookies = request.getCookies();
	if( cookies != null ) {
	   for (int i = 0; i < cookies.length; i++) {
	      if(cookies[i].getName().equals("IQSESSIONID")){
	    	  try{
	    	 String token= cookies[i].getValue();
	    	 Base64.Decoder decoder = Base64.getMimeDecoder();
	    	 String tokendecoded=new String(decoder.decode(token));
	    	 String[] tokendecode=tokendecoded.split("!q4Ev");
	    	String ckusernameencode=new String(decoder.decode(tokendecode[0]));
	    	String ckpassencode=new String(decoder.decode(tokendecode[1]));
	    	String ckaccessencode=new String(decoder.decode(tokendecode[2]));
	    	String ckvalidencode=new String(decoder.decode(tokendecode[3]));
	    	ckusername=ckusernameencode;
	    	ckpass=ckpassencode;
	    	ckaccess=ckaccessencode;
	    	ckvalid=ckvalidencode;
	    	  }catch(Exception e){
	    		  
	    	  }
	      }
	   }
	} 
	%>
    
     
        <div class="page-container">
        <!-- sidebar menu area start -->
        <div class="sidebar-menu">
            <div class="sidebar-header">
                <div class="logo" style="/* width: 300px; */">
                 <div class="parent">
  						<img id="cirruslogobg" src="${pageContext.request.contextPath}/assets/img/DevopsBg.png" style="float: left;height: 110px;">
  						<img id="cirruslogo" alt="" src="${pageContext.request.contextPath}/assets/img/RevisedHomeCirrusDigitalLogo-01.png" style="float: left;height: 70px;padding-top: 10px;">
					</div>
                   <%-- <img alt="" src="${pageContext.request.contextPath}/assets/img/RevisedHomeCirrusDigitalLogo-01.png" style="padding-top: 3px;"> --%>
                </div>
            </div>
            <div class="main-menu">
                <div class="menu-inner">
                   <nav>
                        <ul class="metismenu" id="menu">
                        <li><a class="iqloader" href="${pageContext.request.contextPath}/Home.jsp"><i class="ti-home"></i> <span>Home</span></a></li>
                        <li><a class="iqloader" href="${pageContext.request.contextPath}/web?action=dashboard"><i class="ti-dashboard"></i> <span>Dashboard</span></a></li>
                          <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-folder"></i><span>Projects</span></a>
                                <ul class="collapse">
                                    <!-- <li><a href="#" onclick="hide();"><i class="ti-view-list-alt" style="padding-right:10px; "></i><span>List of Projects</span></a></li> -->
                                     <%if (ckvalid!=null){
                                     if (ckvalid.equals("true")){%>
                                    <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-package"></i><span>Join Project</span></a>
                                <ul class="collapse">
                                    <li><table>
    				<tr>
    				<tr>
    				<td>
    				<div class="form-group has-search">
                 <input class="form-control" type="text" placeholder="Search.." id="myInput" list="projectList" onkeyup="filterFunction()" style="background-color: aliceblue;"></div>
					    <datalist id="projectList" ></datalist></td></tr>
					    
					    <tr>
                       <td>
                       <button class="btn btn-primary btn-sm" id="btn" style="border-radius: 1rem;padding: 5px 13px;box-shadow: 0 1px 3px 0 rgba(0,0,0,.2);">Request Access</button>
							<p id="err" style="color:red; display:none"> Project/Group access is disabled </p>
							<p id="success" style="color:green; display:none"> </p></td></tr>
							</table></li>
                                </ul>
                            </li> 
                             <%}else{ %>
                              <li>
                                <a href="#"><i class="ti-package"></i><span>Join Project</span></a>
                            </li> 
                              <%}} %>
                            <%if (ckaccess!=null && ckvalid!=null){
                            if (ckaccess.equals("admin") && ckvalid.equals("true")){%>
                                    <li><a href="${pageContext.request.contextPath}/web?action=project" class="iqloader"><i class="ti-new-window"></i><span>Create project</span></a></li>
                                    <%}} %>
                                </ul>
                            </li>
                            <%if (ckaccess!=null){
                            if (ckaccess.equals("admin")){%>
                                    <!-- <li><a href="#" data-toggle="modal" data-target="#licenseupdate"><i class="ti-new-window"></i><span>License</span></a></li> -->
                                    <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-settings"></i><span>Admin</span></a>
                                <ul class="collapse">
                                    <li><a href="${pageContext.request.contextPath}/license.jsp" class="iqloader"><i class="fa fa-unlock-alt"></i><span>License</span></a></li>
                                    <%if (ckvalid!=null){
                                    if (ckvalid.equals("true")){%>
                                    <li><a href="${pageContext.request.contextPath}/UserManagement.jsp" class="iqloader"> <i class="ti-user"></i><span>User Management</span></a></li>
                                      <%}} %>
                                    <!-- <ul class="collapse">
                                    <li><a href="index.html"><i class="fa fa-user-plus"></i><span>Add User</span></a></li>
                                    <li><a href="index2.html"><i class="fa fa-edit"></i><span>Modify User</span></a></li>
                                    <li><a href="index3.html"><i class="ti-trash"></i><span>Delete User</span></a></li>
                               		 </ul> -->
                                    
                                </ul>
                            </li>
                                    <%} }%>
        						<li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-help"></i><span>Help</span></a>
                                <ul class="collapse">
                                    <li><a href="index.html"><span>Contact Us</span></a></li>
                                    <li><a href="index2.html"><span>FAQ</span></a></li>
                                    <li><a href="index3.html"><span>About</span></a></li>
                                </ul>
                            </li>
                            <%-- <li><a href="${pageContext.request.contextPath}/web?action=logout"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li> --%>
                            <li><a href="${pageContext.request.contextPath}/Logout"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        
        <div id="loaderarea">
         <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Project Status</li>
            
          <div class="dropdown" style="float: right;margin-left: auto;margin-right: 6%;">
  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    <i class="glyphicon glyphicon-cog"></i>
    <span class="caret">LOB's</span>
  </button>
  <ul class="dropdown-menu checkbox-menu allow-focus" aria-labelledby="dropdownMenu1">
    <li>
      <label class="option">
			 <!-- <input type="radio" name="option"> -->
			   <input type="checkbox" id="" name="filtertable" value="">
			<span><i class="icon icon-size-fullscreen"style="color:navy;padding-right: 9px;"></i>All</span>
			</label>
    </li>
    
    <li>
      <label class="option">
			<!-- <input type="radio" name="option"> -->
        <input type="checkbox" id="transport" name="filtertable" value="transport">
			<span><i class="icon icon-plane"style="color:navy;padding-right: 9px;"></i>transport</span>
			</label>
    </li>
    <li>
     <label class="option">
			<!-- <input type="radio" name="option"> -->
        <input type="checkbox" id="retail" name="filtertable" value="retail">
			<span><i class="icon icon-basket-loaded" style="color:navy;padding-right: 9px;"></i>retail</span>
			</label>
    </li>
     <li>
       <label class="option">
			<!-- <input type="radio" name="option"> -->
       <input type="checkbox" name="filtertable" id="banking" value="banking">
			<span><i class="icon fa fa-bank" style="color:navy;padding-right: 9px;"></i>banking</span>
			</label>
    </li>
     <li >
      <label class="option">
			<!-- <input type="radio" name="option"> -->
         <input type="checkbox" id="telecom" name="filtertable" value="telecom">
			<span><i class="icon icon-phone"style="color:navy;padding-right: 9px;"></i>telecom</span>
			</label>
    </li>
  </ul>
</div>
          </ol>
          <script>
          var onprem=0;
          var aws=0;
          var bluemix=0;
          var onpremsucces=0;
          var awssucces=0;
          var bluemixsucces=0;
          </script>
        <div class="main-content-inner">
             
              <div class="table-responsive">
              
              <table id="myTable" class="table table-bordered" border="1" >
  <thead>
  	<tr>
    	<th><div onclick="sortTable()">Project Name</div></th>   
    	<th>Build</th>
    	<th>Quality Test</th>
    	<th>Unit Test</th>
    	<th>Pre-Prod</th>
    	<th>Prod</th>
    	<th>Env</th>
    </tr> 
  </thead>
  <tbody id= "myTable1">
  <% ArrayList<Projects> ListofProjects = (ArrayList<Projects>)session.getAttribute("ProjectsList");
  for (int i=0;i<ListofProjects.size();i++){ 
  Projects object=ListofProjects.get(i);
  JSONObject gitlabReturns = object.getgitlabReturns();
  JSONObject jenkinsReturns = object.getjenkinsReturns();
  JSONObject sonarReturns = object.getsonarReturns();
  JSONObject testReturns = object.gettestReturns();
 // JSONObject serverReturns = object.getserverReturns();
  %>
  <tr class="tablerow-<%=i%>">
    <td>
    <!--#detailsSection  onclick="details('<%-- <%=gitlabReturns.get("name")%> --%>');-->
    <%if(gitlabReturns!=null){ %>
    <%String gitProjName=gitlabReturns.get("name").toString();
    System.out.println(gitProjName);
    if(gitProjName.contains("_none")||gitProjName.contains("_transport")||gitProjName.contains("_retail")||gitProjName.contains("_banking")||gitProjName.contains("_telecom")||gitProjName.contains("_ecommerce")){
    gitProjName=gitProjName.substring(0,gitProjName.indexOf("_"));
    }
    System.out.println(gitProjName);
    %>
<a href="${pageContext.request.contextPath}/web?action=projectDetails&projName=<%=gitlabReturns.get("name").toString()%>&projId=<%=gitlabReturns.get("id").toString()%>"><%=gitProjName%></a>
<%-- <%if(serverReturns!=null){ %>
<p><%=serverReturns.get("vm") %></p>
<%} %> --%>
       <script type="text/javascript">
       var projName="<%=gitlabReturns.get("name")%>";
       </script> 
       <%} %>
    </td> 
    <td>
  <% if(jenkinsReturns!= null){%>
  
	  
 <% if(jenkinsReturns.get("color").equals("red")){%>
  <script>
  var color="<%=jenkinsReturns.get("color")%>";
  </script> 
  <%--  <button style="padding:1px;" type="button" class="btn btn-danger" data-toggle="collapse" data-target="#togglesection-<%=gitlabReturns.get("name")
     %> ">Failure
     data-toggle="collapse" data-target="#togglesection-<%=gitlabReturns.get("name")%>"
    </button> --%>
    <div class="checkMulti">
    <%-- <img alt="" src="${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png"> --%>
    <a class="btn" data-toggle="collapse" href="#jenkinDetails-<%=gitlabReturns.get("name").toString()%>" role="button" aria-expanded="false" aria-controls="collapseExample">
<img id="<%=i%>-jenkins-<%=(jenkinsReturns.get("color"))%>" alt="" src="${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png">   
  </a>
  </div>
    <%} else if(jenkinsReturns.get("color").equals("blue")){ %>
 <%-- <button  style="padding:1px;" type="button" class="btn btn-success" data-toggle="collapse" data-target="#togglesection-<%=gitlabReturns.get("name")
     %>">Success
     data-toggle="collapse" data-target="#togglesection-<%=gitlabReturns.get("name")%>"
</button> --%>
<script>
  var color="<%=jenkinsReturns.get("color")%>";
  </script>
<div class="checkMulti">
 
<%-- <img alt="" src="${pageContext.request.contextPath}/assets/img/icons8-checkmark-48.png" style="height:20px"> --%>
 <a class="btn" data-toggle="collapse" href="#jenkinDetails-<%=gitlabReturns.get("name").toString()%>" role="button" aria-expanded="false" aria-controls="collapseExample">
<img id="<%=i%>-jenkins-<%=(jenkinsReturns.get("color"))%>" alt="" src="${pageContext.request.contextPath}/assets/img/icons8-checkmark-48.png" style="height:20px">   
  </a>
</div>
<%-- <%	}else{
   %> 
   <div class="checkMulti">
<img alt="" src="${pageContext.request.contextPath}/assets/img/icons8-checkmark-48.png" style="height:20px">
 <a class="btn" data-toggle="collapse" href="#jenkinDetails-<%=gitlabReturns.get("name").toString()%>" role="button" aria-expanded="false" aria-controls="collapseExample">
<img style="height:20px;width:20px;" alt="" src="${pageContext.request.contextPath}/assets/img/noDetails.png">   
  </a>
</div> --%>
<%}else{ %>
<script>
  var color="<%=jenkinsReturns.get("color")%>";
  </script>
<div class="checkMulti"  data-toggle="tooltip" title="Build Aborted or data not found">
 <a class="btn" data-toggle="collapse" href="#jenkinDetails-<%=gitlabReturns.get("name").toString()%>" role="button" aria-expanded="false" aria-controls="collapseExample">
<img id="<%=i%>-jenkins-<%=(jenkinsReturns.get("color"))%>" alt="" src="${pageContext.request.contextPath}/assets/img/noDetails.png" style="height:20px">   
  </a>
</div>
<%} %>
  <div class="collapse" id="jenkinDetails-<%=gitlabReturns.get("name").toString()%>" style="font-size: small;">
    <br>
    <%if(jenkinsReturns.get("lastBuildNumber")!=null){ %>
    <p style="color: black;">Total Builds:<%=jenkinsReturns.get("lastBuildNumber")%></p>
    <%}else{ %>
    No Builds
    <%} %>
    <%if(jenkinsReturns.get("lastSuccessfulBuildUrl")!=null){ %>
   <a href=<%=jenkinsReturns.get("lastSuccessfulBuildUrl")
     %>>Success(Build_#<%=jenkinsReturns.get("lastSuccessfulBuildNumber") %>)</a>
     <%} %>
    <%-- <p style="color:black;">Build_#<%=jenkinsReturns.get("lastSuccessfulBuildNumber") %></p> --%>
  <% if(jenkinsReturns.get("lastFailedBuildUrl")==null){ %>
  No failed builds 
  <%} else{ %>
  <br/>  
      <a href=<%=jenkinsReturns.get("lastFailedBuildUrl")
     %>>last Failure(<%=jenkinsReturns.get("lastFailedBuildNumber")%>)</a> 
    <% }%>
    </div>
    <%}else{ %>
   <img id="jenkins-null" style="height:20px;width:20px;" alt="" src="${pageContext.request.contextPath}/assets/img/noDetails.png"  data-toggle="tooltip" title="data not found">
   <%} %>  
     </td>
     <td>
    <%if(sonarReturns != null){
    if(sonarReturns.get("status").equals("OK")) {%>
    <script>
    var status="<%=sonarReturns.get("status")%>";
    </script>
    <%-- <button  type="button" class="btn btn-outline-success" data-toggle="collapse" data-target="#sonarDetails-<%=gitlabReturns.get("name")
     %>">Passed
     data-toggle="collapse" data-target="#sonarDetails-<%=gitlabReturns.get("name")%>"
     </button> --%>
     <%-- <img alt="" src="${pageContext.request.contextPath}/assets/img/icons8-checkmark-48.png" style="height:20px"> --%>
     <div class="checkMulti">
     <a class="btn" data-toggle="collapse" href="#sonarDetails-<%=gitlabReturns.get("name").toString()%>" role="button" aria-expanded="false" aria-controls="collapseExample">
<img id="<%=i%>-sonar-<%=(sonarReturns.get("status"))%>" alt="" src="${pageContext.request.contextPath}/assets/img/icons8-checkmark-48.png" style="height:20px">   
  </a>
  </div>
    <%} else if ((sonarReturns.get("status").equals("ERROR"))){ %>
    <script>
    var status="<%=sonarReturns.get("status")%>";
    </script>
    <%-- <button type="button" class="btn btn-outline-danger" data-toggle="collapse" data-target="#sonarDetails-<%=gitlabReturns.get("name")
     %>">Failed
      data-toggle="collapse" data-target="#sonarDetails-<%=gitlabReturns.get("name")%>"
     </button> --%>
    <%-- <img alt="" src="${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png"> --%>
    <div class="checkMulti">
    <a class="btn" data-toggle="collapse" href="#sonarDetails-<%=gitlabReturns.get("name").toString()%>" role="button" aria-expanded="false" aria-controls="collapseExample">
<img id="<%=i%>-sonar-<%=(sonarReturns.get("status"))%>" alt="" src="${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png" style="height:20px">   
  </a></div>
    <%}else if((sonarReturns.get("status").equals("WARN"))){ %>
     <script>
    var status="<%=sonarReturns.get("status")%>";
    </script>
   <%-- <button type="button" class="btn btn-outline-danger" data-toggle="collapse" data-target="#sonarDetails-<%=gitlabReturns.get("name")
     %>">Failed</button> --%>
     <%-- <img alt="" src="${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png"> --%>
     <div class="checkMulti">
<a class="btn" data-toggle="collapse" href="#sonarDetails-<%=gitlabReturns.get("name").toString()%>" role="button" aria-expanded="false" aria-controls="collapseExample">
<img id="<%=i%>-sonar-<%=(sonarReturns.get("status"))%>" alt="" src="${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png" style="height:20px">   
  </a></div>
<%		}
    %>
	  <div class="collapse" id ="sonarDetails-<%=gitlabReturns.get("name").toString()%>">
		<%
		for (Object key : sonarReturns.keySet()) {
	        

	        if (key.equals("name") || 
	        		key.equals("id")||
	        		key.equals("key") ||
	        		key.equals("status")){
	        	continue;
	        }
	        String keyStr = (String)key;
	        Integer keyvalue =Integer.parseInt(sonarReturns.get(keyStr).toString());
	        if(keyvalue >5){%>
	        			<p class="text-danger"><%=keyStr %>:<%= keyvalue%></p>
	       <%}else if (keyvalue > 3 && keyvalue<=5){%>
	       				<p class="text-warning"><%=keyStr %>:<%= keyvalue%></p>
	    	<%} else {%> 
	    	   			<p class="text-success"><%=keyStr %>:<%= keyvalue%></p>
	    	   			<% }
		}
	    	   			 %> 
	<% 
    }	else{ %>
   <img style="height:20px;width:20px;" alt="" src="${pageContext.request.contextPath}/assets/img/noDetails.png"  data-toggle="tooltip" title="data not found">
   <%} %>
	</div>

    </td>
    <td>
   <%if(jenkinsReturns!=null){ %>
     <script>
   var testcase=null;
   </script>
   <%int totalcount=0;
   int passcount=0;
   int failcount=0;
   int skipcount=0;%>
   <%if(jenkinsReturns.get("totalcount")!=null&&jenkinsReturns.get("failcount")!=null&&jenkinsReturns.get("skipcount")!=null){ %>
   <%totalcount=Integer.valueOf(jenkinsReturns.get("totalcount").toString());
   failcount=Integer.valueOf(jenkinsReturns.get("failcount").toString());
   skipcount=Integer.valueOf(jenkinsReturns.get("skipcount").toString());
   passcount=totalcount-failcount-skipcount;%>
   <%if(failcount==0&&skipcount==0){ %>
   <script>
   testcase="<%=i%>-pass";
   </script>
   <div class="checkMulti">
     <a class="btn" data-toggle="collapse" href="#testDetails-<%=gitlabReturns.get("name").toString()%>" role="button" aria-expanded="false" aria-controls="collapseExample">
<img id="<%=i%>-pass" alt="" src="${pageContext.request.contextPath}/assets/img/icons8-checkmark-48.png" style="height:20px">   
  </a>
  </div>
   <%}else{%>
    <script>
  testcase="<%=i%>-fail";
   </script>
   <div class="checkMulti">
     <a class="btn" data-toggle="collapse" href="#testDetails-<%=gitlabReturns.get("name").toString()%>" role="button" aria-expanded="false" aria-controls="collapseExample">
<img id="<%=i%>-fail" alt="" src="${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png" style="height:20px">   
  </a>
  </div>
   <%}}else{ %>
   <div class="checkMulti"  data-toggle="tooltip" title="data not found">
     <a class="btn" data-toggle="collapse" href="#testDetails-<%=gitlabReturns.get("name").toString()%>" role="button" aria-expanded="false" aria-controls="collapseExample">
<img style="height:20px;width:20px;" alt="" src="${pageContext.request.contextPath}/assets/img/noDetails.png">   
  </a>
  </div>
   <%}%>
   <%-- <%}else if(jenkinsReturns.get("skipcount").toString().equalsIgnoreCase("blue")){ %>
   <img alt="" src="${pageContext.request.contextPath}/assets/img/icons8-checkmark-48.png" style="height:20px">
   <%}else{ %>
   <img alt="" src="${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png">
   <%} %> --%>
   <div class="collapse" id="testDetails-<%=gitlabReturns.get("name").toString()%>" style="font-size: small;">
   <%if(jenkinsReturns.get("totalcount")!=null&&jenkinsReturns.get("failcount")!=null&&jenkinsReturns.get("skipcount")!=null){ %>
   <%totalcount=Integer.valueOf(jenkinsReturns.get("totalcount").toString());
   failcount=Integer.valueOf(jenkinsReturns.get("failcount").toString());
   skipcount=Integer.valueOf(jenkinsReturns.get("skipcount").toString());
   passcount=totalcount-failcount-skipcount;%>
   <p style="color:black;">totalcount=<%=totalcount%></p>
   <p style="color:black;">passcount=<%=passcount%></p>
   <p style="color:black;">failcount=<%=failcount%></p>
   <p style="color:black;">skipcount=<%=skipcount%></p>
   <%}else{ %>
   <p style="color:black;">No test cases</p>
   <%} %>
   </div>
   <%}else{ %>
   <img style="height:20px;width:20px;" alt="" src="${pageContext.request.contextPath}/assets/img/noDetails.png"  data-toggle="tooltip" title="data not found">
   <%} %>
    </td>
   <td>
   <%if(jenkinsReturns!=null){ %>
   <%if(jenkinsReturns.get("stageName")==null){ %>
   <script>
   var stage="<%=jenkinsReturns.get("stageName")%>";
   </script>
   <img id="<%=i%>-stage-<%=jenkinsReturns.get("stageName")%>" alt="" src="${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png">
   <%}else if(jenkinsReturns.get("stageiconColor").toString().equalsIgnoreCase("blue")){ %>
   <script>
   var stage="<%=jenkinsReturns.get("stageiconColor")%>";
   </script>
   <img id="<%=i%>-stage-<%=jenkinsReturns.get("stageiconColor")%>" alt="" src="${pageContext.request.contextPath}/assets/img/icons8-checkmark-48.png" style="height:20px">
   <%}else{ %>
   <script>
   var stage="<%=jenkinsReturns.get("stageiconColor")%>";
   </script>
   <img id="<%=i%>-stage-<%=jenkinsReturns.get("stageiconColor")%>" alt="" src="${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png" style="height:20px">
   <%} %>
   <%}else{ %>
   <img style="height:20px;width:20px;" alt="" src="${pageContext.request.contextPath}/assets/img/noDetails.png"  data-toggle="tooltip" title="data not found">
   <%} %>
   <div id="<%=i%>-test-approve"></div>
   </td>
    <td>
    <%if(jenkinsReturns!=null){ %>
     
    <%if(jenkinsReturns.get("prodName")==null){ %>
    <script>
   var prod="<%=jenkinsReturns.get("prodName")%>";
   </script>
   <img id="<%=i%>-prod-<%=jenkinsReturns.get("prodName")%>" alt="" src="${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png">
   <%}else if(jenkinsReturns.get("prodiconColor").toString().equalsIgnoreCase("blue")){ %>
   <script>
   var prod="<%=jenkinsReturns.get("prodiconColor")%>";
   </script>
   <img id="<%=i%>-prod-<%=jenkinsReturns.get("prodiconColor")%>" alt="" src="${pageContext.request.contextPath}/assets/img/icons8-checkmark-48.png" style="height:20px">
   <%}else{ %>
   <script>
   var prod="<%=jenkinsReturns.get("prodiconColor")%>";
   </script>
   <img id="<%=i%>-prod-<%=jenkinsReturns.get("prodiconColor")%>" alt="" src="${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png">
   <%} %> 
   <%}else{ %>
   <script>
   var prod=1;
   </script>
   <img id="<%=i%>-prod-1" style="height:20px;width:20px;" alt="" src="${pageContext.request.contextPath}/assets/img/noDetails.png"  data-toggle="tooltip" title="data not found">
   <%} %>
   <div id="<%=i%>-stage-approve"></div>
   </td>
   <td>
   <script>
   total=<%=i%>;
   </script>
   <%if(jenkinsReturns!=null){ %>
   <%if(jenkinsReturns.get("server")!=null){ %>
   <%if(jenkinsReturns.get("server").toString().equalsIgnoreCase("scp")){ %>
   <script>
   onprem=onprem+1;
   </script>
   <p style="color: black;">On Prem</p>
   <%}else if(jenkinsReturns.get("server").toString().equalsIgnoreCase("aws")){ %>
   <script>
   aws =aws+1;
   </script>
   <p style="color: black;">AWS</p>
   <%}else if(jenkinsReturns.get("server").toString().equalsIgnoreCase("ibm")){ %>
   <script>
   bluemix=bluemix+1;
   </script>
   <p style="color: black;">Bluemix</p>
   <%}else if(jenkinsReturns.get("server").toString().isEmpty()){ %>
   <p style="color: black;">Not_Deployed</p>
   <%}}else{%>
   <p style="color: black;">Not_Deployed</p>
   <%}%>
   <%}%>
   </td>
  </tr>
  <%} %>
 </tbody>
</table> 
              
             </div>
              <div class="row">
           <div class="col-lg-4 mt-5">   
             <div class="card">
           <div id="overallprojstat" style="height:400px;"></div>
           </div>
           </div>
           <div class="col-lg-4 mt-5">  
           <div class="card">
           <div id="overallprojstattest" style="height:400px;"></div>
           </div>
           </div>
          <!--  <div class="col-lg-4 mt-5">  
           <div class="card">
            <div id="overallprojstattest1" style="height:400px;"></div>
            </div>
            </div> -->
             </div>
            <!--  <div class="row" style="margin-top: 10vh;">
             <div class="card" style="width:50%;">
           <div id="overallprojstat" style="height:400px;width:100%;"></div>
           </div>
           <div class="card" style="width:50%;">
           <div id="overallprojstattest" style="height:400px;width:100%;"></div>
           </div>
           <div class="card" style="width:50%;">
            <div id="overallprojstattest1" style="height:400px;width:100%;"></div>
            </div>
             <div class="card" style="width:50%;">
           <div id="vmcpuutility" style="height:400px;width:100%;"></div>
           </div>
           <div class="card" style="width:50%;">
           <div id="vmmemutility" style="height:400px;width:100%;"></div>
            </div>
             </div> -->
             </div>
             </div>
              </div>
           <script>
	  if('<%=session.getAttribute("username")%>'!=null){
			var userName1="<%=session.getAttribute("username")%>";
		    }
		   if('<%=session.getAttribute("userpassword")%>'!=null){
			   var userPassword1="<%=session.getAttribute("userpassword")%>";
		    }
		
   var gitStat=<%=session.getAttribute("gitCount") %>;
   var jenStat=<%=session.getAttribute("jenkinCount") %>;
   var sonStat=<%=session.getAttribute("sonarCount") %>;
   var testStat=<%=session.getAttribute("testCount") %>;
   var jenFixed=jenStat.toFixed(2);
   var sonFixed=sonStat.toFixed(2);
   var testFixed=testStat.toFixed(2);
  /*  var bluemix=0;
   var aws=0;
   var onprem=0;
   var total=0; */
   </script>
   <script src="${pageContext.request.contextPath}/js/style.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/owl.carousel.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/metisMenu.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/jquery.slimscroll.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/jquery.slicknav.min.js"></script>
    <!-- others plugins -->
    <script src="${pageContext.request.contextPath}/assets/js/plugins.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/scripts.js"></script> 
                  <script type="text/javascript">
    accessToken="<%=session.getAttribute("access_token")%>";
    </script>
     <script>var ctx = "${pageContext.request.contextPath}"</script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/joinProject.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/overlayLoader.js"></script>
              <script>
              //Ajax call for progress bar on dashboard
              
              $(document).ready(function() {
            	  setInterval(function(){
                 	 // ajaxCallFunction();
     				 }, 5000);
              });
              
            	function ajaxCallFunction(){
            		<% ArrayList<Projects> ListofProjects1 = (ArrayList<Projects>)session.getAttribute("ProjectsList");
            		  for (int i=0;i<ListofProjects1.size();i++){
            		  Projects object1=ListofProjects1.get(i);
            		  JSONObject gitlabReturns1 = object1.getgitlabReturns();
            		  JSONObject jenkinsReturns1 = object1.getjenkinsReturns();
            		  JSONObject sonarReturns1 = object1.getsonarReturns();
            		  JSONObject testReturns1 = object1.gettestReturns();%>
            		  <%if(gitlabReturns1!=null){%> 
            		  <%if(gitlabReturns1.get("name")!=null){%> 
            		  var projectnameforurl='<%=gitlabReturns1.get("name")%>';
            		var jqxhr = $.ajax(ctx+"/rest/JenkinsService/Staging_info?projectname="+projectnameforurl+"&name="+userName1+"&password="+userPassword1)
                    .done(function(data) {
                    	if(data.length!="0"){
                   		 for (var j =0; j<1; j++) {
            				 	    var object = data[j];
            				 	   // for (var property in object) {
            				 	    		var id1=<%=i%>+"-jenkins-"+color;
            				 	    		var sonar=<%=i%>+"-sonar-"+status;
            				 	    		//var test=testcase;
            				 	    		var stage1=<%=i%>+"-stage-"+stage;
            				 	    		var prod1=<%=i%>+"-prod-"+prod;
            					 	    	if(object["buildstatus"]=="SUCCESS"){
            					 	    		console.log("id1:"+id1);
            					 	    		document.getElementById(id1).src='${pageContext.request.contextPath}/assets/img/icons8-checkmark-48.png';
            					 	    		/* document.getElementById(test).src='${pageContext.request.contextPath}/assets/img/progress.gif';
            					 	    		document.getElementById(stage1).src='${pageContext.request.contextPath}/assets/img/progress.gif';
            					 	    		document.getElementById(prod1).src='${pageContext.request.contextPath}/assets/img/progress.gif'; */
            					 	    		//junit test
            					 	    		if(object["junitstatus"]=="SUCCESS"){
            					 	    			console.log("testcase:"+testcase);
                					 	    		document.getElementById(testcase).src='${pageContext.request.contextPath}/assets/img/icons8-checkmark-48.png';
                					 	    		//Test deploy
                					 	    		if(object["teststatus"]=="SUCCESS"){
                    					 	    		//stage deploy
                    					 	    		if(object["stagestatus"]=="SUCCESS"){
                    					 	    			console.log("stage1:"+stage1);
                        					 	    		document.getElementById(stage1).src='${pageContext.request.contextPath}/assets/img/icons8-checkmark-48.png';
                        					 	    		document.getElementById(<%=i%>+"-test-approve").style.visibility = "hidden";
                        					 	    		//prod deploy
                        					 	    		if(object["prodstatus"]=="SUCCESS"){
                        					 	    			console.log("prod1:"+prod1);
                        					 	    			document.getElementById(prod1).src='${pageContext.request.contextPath}/assets/img/icons8-checkmark-48.png';
                        					 	    			document.getElementById(<%=i%>+"-stage-approve").style.visibility = "hidden";
                            					 	    	}else if(object["prodstatus"]=="IN_PROGRESS"){
                            					 	    		document.getElementById(prod1).src='${pageContext.request.contextPath}/assets/img/progress.gif';
                            					 	    		}else if(object["prodstatus"]=="FAILED"){
                            					 	    			document.getElementById(prod1).src='${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png';
                            					 	    			}
                        					 	    		//end prod deploy
                        					 	    	}else if(object["stagestatus"]=="IN_PROGRESS"){
                        					 	    		document.getElementById(stage1).src='${pageContext.request.contextPath}/assets/img/progress.gif';
                        					 	    		document.getElementById(prod1).src='${pageContext.request.contextPath}/assets/img/progress.gif';
                        					 	    		}else if(object["stagestatus"]=="PAUSED_PENDING_INPUT"){
                        					 	    			document.getElementById(<%=i%>+"-stage-approve").style.visibility = "visible";
                        					 	    			<%-- document.getElementById("<%=i%>-stage-approve").innerHTML="<p style='font-size: x-small;color:black;'>waiting for approval to deploy on prod</p>"; --%>
                        					 	    			document.getElementById(stage1).src='${pageContext.request.contextPath}/assets/img/icons8-checkmark-48.png';
                        					 	    			}else if(object["stagestatus"]=="FAILED"){
                        					 	    			document.getElementById(stage1).src='${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png';
                        					 	    			document.getElementById(prod1).src='${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png';
                        					 	    			}else if(object["stagestatus"]=="ABORTED"){
                        					 	    				document.getElementById(stage1).src='${pageContext.request.contextPath}/assets/img/icons8-checkmark-48.png';
                            					 	    			document.getElementById(prod1).src='${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png';
                            					 	    			}
                    					 	    		//end of stage
                					 	    		}else if(object["teststatus"]=="IN_PROGRESS"){
                					 	    			document.getElementById(stage1).src='${pageContext.request.contextPath}/assets/img/progress.gif';
                    					 	    		document.getElementById(prod1).src='${pageContext.request.contextPath}/assets/img/progress.gif';
                    					 	    		}else if(object["teststatus"]=="PAUSED_PENDING_INPUT"){
                    					 	    			document.getElementById(<%=i%>+"-test-approve").style.visibility = "visible";
                    					 	    			<%-- document.getElementById("<%=i%>-test-approve").innerHTML="<p style='font-size: x-small;color:black;'>waiting for approval to deploy on staging</p>"; --%>
                    					 	    			}else if(object["teststatus"]=="FAILED"){
                    					 	    			document.getElementById(stage1).src='${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png';
                    					 	    			document.getElementById(prod1).src='${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png';
                    					 	    			}else if(object["teststatus"]=="ABORTED"){
                        					 	    			document.getElementById(stage1).src='${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png';
                        					 	    			document.getElementById(prod1).src='${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png';
                        					 	    			}
                					 	    		//end of test
                					 	    	}else if(object["junitstatus"]=="IN_PROGRESS"||object["junitstatus"]=="PAUSED_PENDING_INPUT"){
                					 	    		document.getElementById(testcase).src='${pageContext.request.contextPath}/assets/img/progress.gif';
                					 	    		document.getElementById(stage1).src='${pageContext.request.contextPath}/assets/img/progress.gif';
                					 	    		document.getElementById(prod1).src='${pageContext.request.contextPath}/assets/img/progress.gif';
                					 	    		}else if(object["junitstatus"]=="FAILED"){
                					 	    			document.getElementById(testcase).src='${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png';
                					 	    			document.getElementById(stage1).src='${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png';
                					 	    			document.getElementById(prod1).src='${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png';
                					 	    			}
            					 	    		//end of junit test
            					 	    	}else if(object["buildstatus"]=="IN_PROGRESS"||object["buildstatus"]=="PAUSED_PENDING_INPUT"){
            					 	    		document.getElementById(id1).src='${pageContext.request.contextPath}/assets/img/progress.gif';
            					 	    		document.getElementById(testcase).src='${pageContext.request.contextPath}/assets/img/progress.gif';
            					 	    		document.getElementById(stage1).src='${pageContext.request.contextPath}/assets/img/progress.gif';
            					 	    		document.getElementById(prod1).src='${pageContext.request.contextPath}/assets/img/progress.gif';
            					 	    		}else if(object["buildstatus"]=="FAILED"){
            					 	    			document.getElementById(id1).src='${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png';
            					 	    			document.getElementById(testcase).src='${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png';
                					 	    		document.getElementById(stage1).src='${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png';
                					 	    		document.getElementById(prod1).src='${pageContext.request.contextPath}/assets/img/icons8-multiply-16.png';
            					 	    			}
            				 	    //}
                   		 }
                    }
                    	}) 
                    <%}%>
            		  <%}%>
                    <%}%>
            	} 
             
              </script>
                  <!-- offset area end -->
                  
	 

        </body>

</html>