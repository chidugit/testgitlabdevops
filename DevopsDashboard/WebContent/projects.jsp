<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="org.json.simple.*"
import="com.cirrus.devops.projects.*"
import="java.util.ArrayList"
import="java.util.Iterator" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<Link rel="javascript" type="text/javascript" href="${pageContext.request.contextPath}/togglefunction.js"/>
<Link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/projects.css"/>
<title>Project Dashboard</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
<script src="https://d3js.org/d3.v5.min.js"></script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script>
function myFunction() {
	$("#togglesection").toggle();}
</script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<meta name="viewport" content="width=device-width, initial-scale=1">

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
</head>
<body ng-app="myApp" ng-controller="myCtrl" >

  <div id="chartContainer" style="height: 30%; width:30%"></div>
    
<div id="myWorkContent" class = "container-table" style=" height:60%; width:60%;float:right">
 <table style="width:100%;" class = "table table-hover table-bordered" border="1" align="center">
  <thead class="thead-dark">
  	<tr>
    	<th>Project Name</th>   
    	<th>Build</th>
    	<th>Sonar</th>
    	<th>JUnit</th>
    </tr> 
  </thead>
  <% ArrayList<Projects> ListofProjects = (ArrayList<Projects>)session.getAttribute("ProjectsList");
  for (int i=0;i<ListofProjects.size();i++){ 
  Projects object=ListofProjects.get(i);
  JSONObject gitlabReturns = object.getgitlabReturns();
  JSONObject jenkinsReturns = object.getjenkinsReturns();
  JSONObject sonarReturns = object.getsonarReturns();
  JSONObject testReturns = object.gettestReturns();%>
  <tr>
    <td>
<a href=<%=gitlabReturns.get("web_url")
	%>><%=gitlabReturns.get("name")
     %></a>
    </td> 
    
    
    <td>
    
  <% if(jenkinsReturns.get("color").equals("red")){ %>
 
   <%-- <button style="padding:1px;" type="button" class="btn btn-danger" data-toggle="collapse" data-target="#togglesection-<%=gitlabReturns.get("name")
     %> ">Failure
    </button> --%>
    <%} else{ %>
    
 <%-- <button  style="padding:1px;" type="button" class="btn btn-success" data-toggle="collapse" data-target="#togglesection-<%=gitlabReturns.get("name")
     %>">Success
</button> --%>
<%} %> 
  <div id="togglesection-<%=gitlabReturns.get("name")
     %>">
  <a href=<%=jenkinsReturns.get("url")
     %>><%=jenkinsReturns.get("name")
     %></a> 
    <br>
  
   <a href=<%=jenkinsReturns.get(" lastSuccessfulBuildUrl")
     %>>last Success</a>
    
  <% if(jenkinsReturns.get(" lastFailedBuildUrl")==null){ %>
  No failed builds 
  <%} else{ %>
  <br/>  
      <a href=<%=jenkinsReturns.get(" lastFailedBuildUrl")
     %>>last Failure</a> 
     <%}%>
    </div> 
     </td>
    
    
   <!--  <td  bgcolor="<%=jenkinsReturns.get("color")%>">
    <a href =<%=jenkinsReturns.get("url")%>>
    <%= jenkinsReturns.get("name")%></a>
    </td>
   --> 
   
 
     <td>
    <%if(sonarReturns.get("status").equals("OK")) {%>
    <button  type="button" class="btn btn-outline-success" data-toggle="collapse" data-target="#sonarDetails-<%=gitlabReturns.get("name")
     %>">Passed</button>
    <%} else if ((sonarReturns.get("status").equals("ERROR"))){ %>
    
    <button type="button" class="btn btn-outline-danger" data-toggle="collapse" data-target="#sonarDetails-<%=gitlabReturns.get("name")
     %>">Failed</button>
    
    <%}else if((sonarReturns.get("status").equals("WARN"))){ %>
   <button type="button" class="btn btn-danger" data-toggle="collapse" data-target="#sonarDetails-<%=gitlabReturns.get("name")
     %>">Failed</button>

<%} %>
	<div id = "sonarDetails-<%=gitlabReturns.get("name")
     %>" class="container">
		<%
		for (Object key : sonarReturns.keySet()) {
	        //based on you key types

	        if (key.equals("name") || 
	        		key.equals("id")||
	        		key.equals("key") ||
	        		key.equals("status")){
	        	continue;
	        }
	        String keyStr = (String)key;
	        Integer keyvalue =Integer.parseInt(sonarReturns.get(keyStr).toString());
	        if(keyvalue >5){%>
	        			<p class="text-danger"><%=keyStr %>:<%= keyvalue%></p>
	       <%}else if (keyvalue > 3 && keyvalue<=5){%>
	       				<p class="text-warning"><%=keyStr %>:<%= keyvalue%></p>
	    	<%} else {%> 
	    	   			<p class="text-success"><%=keyStr %>:<%= keyvalue%></p>
	    	   			<% }%>
	        
	       
		 	
		 
	<% } %>
		
	</div>

    </td>
    <td>
    <% if(testReturns==null){ %>
    <button type="button" class="btn btn-info">No TestCases</button> 
  <%} else{ %>  
      <button type="button" class="btn btn-primary">Total cases:<%=testReturns.get("totalCount")
     %></button>
     <button type="button" class="btn btn-success">passed:<%=testReturns.get("passCount")
     %></button>
     <button type="button" class="btn btn-danger">failed:<%=testReturns.get("failCount")
     %></button>
     <%}%>
    </td>
  </tr>
  
  
  
  
  <%} %>
</table border="1"> 
</div>

<script>
var obj;
var config = {headers: {
    'Access-Control-Allow-Origin':'*',
    'withCredentials':'true'
}
};
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope, $http) {
    $http.get("http://localhost:8080/DevopsDashboard/web?name=vignesh&password=welcome123",config)
    .then(function(response) {
    		obj = response.data;
    		var data_set = [];	
    		for (var i in obj){
    			data_set.push({y: (obj[i]/4)*100, label:i});}
    		console.log("-----------");
    		var chart = new CanvasJS.Chart("chartContainer", {
    			title: {
    				text: "DashBoard"
    			},
    			data: [{
    				type: "pie",
    				
    				yValueFormatString: "",
    				
    				dataPoints: data_set
    			}]
    		});
    		chart.render();
    		console.log(data);
    	
    	
        
    }).catch(function (err) {
        console.log(err)
    });
});


function makeCharts(Response){
		
	var data = []	
	for (var i in Response){
		data.push({y: Response[i], label:i});}
	console.log("-----------");
		console.log(data);
	
}
</script>








<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>


<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
</body>
</html>