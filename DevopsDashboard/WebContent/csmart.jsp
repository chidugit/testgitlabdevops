<!DOCTYPE html>
<%@page import="java.lang.reflect.Array"%>
<%@page import="javax.ws.rs.core.Response"%>
<%@page import="com.cirrus.devops.service.JenkinsService"%>
<%@page import="com.cirrus.devops.service.GetGitlabGroups"%>
<%@page import="java.util.Base64"%>
<%@ page import="org.json.simple.*"
import="com.cirrus.devops.projects.*"
import="com.cirrus.devops.Modules.*"
import="java.util.ArrayList"
import="java.util.Iterator" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/Logo.png">
    <title>Server Security</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="${pageContext.request.contextPath}/assets/images/icon/favicon.ico">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/themify-icons.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/metisMenu.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/owl.carousel.min.css">
   <script>var ctx = "${pageContext.request.contextPath}"</script>
     <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/default-css.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/styles.css">
   
  <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/jquery.3.2.1.min.js"></script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>
   <script src="${pageContext.request.contextPath}/assets/js/popper.min.js"></script>
  
	 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/DataTables-1.10.18/css/dataTables.bootstrap4.min.css"/>
	 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/DataTables-1.10.18/datatables.min.css"/>
	 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/DataTables-1.10.18/Select-1.3.0/css/select.bootstrap4.min.css"/>
	 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/DataTables-1.10.18/Select-1.3.0/css/select.dataTables.min.css"/>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/DataTables-1.10.18/js/dataTables.bootstrap4.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/DataTables-1.10.18/datatables.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/DataTables-1.10.18/Select-1.3.0/js/select.bootstrap4.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/DataTables-1.10.18/Select-1.3.0/js/dataTables.select.min.js"></script>
   <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/sweetalert.min.js"></script>
   <script type="text/javascript" src="${pageContext.request.contextPath}/js/loadingoverlay.js"></script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/js/csmart.js"></script>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/loaderOverlay.css">
 <!-- <script src="http://code.jquery.com/jquery-2.0.0.js"></script> -->
 <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.mask.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>  -->
<style>
	thead, tr, th, td {
	    text-align: center;
	    padding: 1%;
	}
	    	.parent {
	  position: relative;
	  top: 0;
	  left: 0;
	}
	#cirruslogobg {
	  position: relative;
	  top: -14px;
    left: -24px;
	}
	#cirruslogo {
	  position: absolute;
	  top: 0px;
	  left: 0px;
	}
	label{
	display:inline-block;
	margin-right:30px;
	width:200px;
	}
	span{
	font-size:small;
	}
	
	div.dataTables_wrapper div.dataTables_length select {
    width: 60px;
    margin: 0 10px;
}
.card-body {
    padding: 1.6rem;
}

.form-control, .form-control:focus{
background-color: white!important;
}
.mr-5, .mx-5 {
    margin-right: 0em!important;
}
.form-control{
padding: 0px 12.8px;
  padding: 0rem .8rem;
  box-shadow: 0 1px 3px 0 rgba(0,0,0,.2); 
}
table.dataTable{
border-collapse:collapse !important;
}
.table > thead > tr > th{
padding-bottom:10px;
}
table.dataTable thead>tr>th.sorting_asc, table.dataTable thead>tr>th.sorting_desc, table.dataTable thead>tr>th.sorting, table.dataTable thead>tr>td.sorting_asc, table.dataTable thead>tr>td.sorting_desc, table.dataTable thead>tr>td.sorting{
color: black;
background-color: #DEE9EB;
}
thead, tr, th, td{
color: gray;
font-size: small;
}
.btn-sm {
    padding: 5px 14px;
}
div.dataTables_wrapper div.dataTables_filter input{
margin-left: 0.1rem;
}
/* table.dataTable.select tbody tr,
table.dataTable thead th:first-child {
  cursor: pointer;
}
table.dataTable.select tbody tr,
table.dataTable{
  cursor: pointer;
} */
.repoertckeckUnckeck{
	width: 18px;
    margin-top: -4px;
    margin-right: 4px;
}
#loader {
  background-image:url("${pageContext.request.contextPath}/assets/img/Logo.png");
  }
</style>
</head>  
<body>
<% if (session.getAttribute("ProjectsList") == null || session.getAttribute("username")==null){
		response.sendRedirect("index.jsp");
		return;
	} 
if (session.getAttribute("validity").equals("true")== false && session.getAttribute("security").toString().equalsIgnoreCase("ServerSecurity") ==false){
	 response.sendRedirect(request.getContextPath());
		return;
	}
String ckusername=null;
String ckpass=null;
String ckaccess=session.getAttribute("Access_level").toString();
String ckvalid=session.getAttribute("validity").toString();
Cookie cookie = null;
Cookie[] cookies = null;
cookies = request.getCookies();
if( cookies != null ) {
   for (int i = 0; i < cookies.length; i++) {
      if(cookies[i].getName().equals("IQSESSIONID")){
    	  try{
    	 String token= cookies[i].getValue();
    	 Base64.Decoder decoder = Base64.getMimeDecoder();
    	 String tokendecoded=new String(decoder.decode(token));
    	 String[] tokendecode=tokendecoded.split("!q4Ev");
    	String ckusernameencode=new String(decoder.decode(tokendecode[0]));
    	String ckpassencode=new String(decoder.decode(tokendecode[1]));
    	String ckaccessencode=new String(decoder.decode(tokendecode[2]));
    	String ckvalidencode=new String(decoder.decode(tokendecode[3]));
    	ckusername=ckusernameencode;
    	ckpass=ckpassencode;
    	ckaccess=ckaccessencode;
    	ckvalid=ckvalidencode;
    	  }catch(Exception e){
    		  
    	  }
      }
   }
} 
	%>
 
    <!-- page container area start -->
    <div class="page-container">
        <!-- sidebar menu area start -->
        <div class="sidebar-menu">
            <div class="sidebar-header">
                <div class="logo" style="/* width: 300px; */">
                 <div class="parent">
  						<img id="cirruslogobg" src="${pageContext.request.contextPath}/assets/img/DevopsBg.png" style="float: left;height: 110px;">
  						<img id="cirruslogo" alt="" src="${pageContext.request.contextPath}/assets/img/RevisedHomeCirrusDigitalLogo-01.png" style="float: left;height: 70px;padding-top: 10px;">
					</div>
                </div>
            </div>
            <div class="main-menu">
                <div class="menu-inner">
                    <nav>
                        <ul class="metismenu" id="menu">
                        <li><a class="iqloader" href="${pageContext.request.contextPath}/Home.jsp"><i class="ti-home"></i> <span>Home</span></a></li>
                        <li><a class="iqloader" href="${pageContext.request.contextPath}/web?action=dashboard"><i class="ti-dashboard"></i> <span>Dashboard</span></a></li>
                        <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-folder"></i><span>Projects</span></a>
                                <ul class="collapse">
                                    <!-- <li><a href="#" onclick="hide();"><i class="ti-view-list-alt" style="padding-right:10px; "></i><span>List of Projects</span></a></li> -->
                                     <%if (ckvalid!=null){
                                     if (ckvalid.equals("true")){%>
                                    <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-package"></i><span>Join Project</span></a>
                                <ul class="collapse">
                                    <li><table>
    				<tr>
    				<tr>
    				<td>
    				<div class="form-group has-search">
                 <input class="form-control" type="text" placeholder="Search.." id="myInput" list="projectList" onkeyup="filterFunction()" style="background-color: aliceblue;"></div>
					    <datalist id="projectList" ></datalist></td></tr>
					    
					    <tr>
                       <td>
                       <button class="btn btn-primary btn-sm" id="btn" style="border-radius: 1rem;padding: 5px 13px;box-shadow: 0 1px 3px 0 rgba(0,0,0,.2);">Request Access</button>
							<p id="err" style="color:red; display:none"> Project/Group access is disabled </p>
							<p id="success" style="color:green; display:none"> </p></td></tr>
							</table></li>
                                </ul>
                            </li> 
                             <%}else{ %>
                              <li>
                                <a href="#"><i class="ti-package"></i><span>Join Project</span></a>
                            </li> 
                              <%}} %>
                            <%if (ckaccess!=null && ckvalid!=null){
                            if (ckaccess.equals("admin") && ckvalid.equals("true")){%>
                                    <li><a href="${pageContext.request.contextPath}/web?action=project" class="iqloader"><i class="ti-new-window"></i><span>Create project</span></a></li>
                                    <%}} %>
                                </ul>
                            </li>
                            <%if (ckaccess!=null){
                            if (ckaccess.equals("admin")){%>
                                    <!-- <li><a href="#" data-toggle="modal" data-target="#licenseupdate"><i class="ti-new-window"></i><span>License</span></a></li> -->
                                    <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-settings"></i><span>Admin</span></a>
                                <ul class="collapse">
                                    <li><a href="${pageContext.request.contextPath}/license.jsp" class="iqloader"><i class="fa fa-unlock-alt"></i><span>License</span></a></li>
                                    <%if (ckvalid!=null){
                                    if (ckvalid.equals("true")){%>
                                    <li><a href="${pageContext.request.contextPath}/UserManagement.jsp" class="iqloader"> <i class="ti-user"></i><span>User Management</span></a></li>
                                      <%}} %>
                                    <!-- <ul class="collapse">
                                    <li><a href="index.html"><i class="fa fa-user-plus"></i><span>Add User</span></a></li>
                                    <li><a href="index2.html"><i class="fa fa-edit"></i><span>Modify User</span></a></li>
                                    <li><a href="index3.html"><i class="ti-trash"></i><span>Delete User</span></a></li>
                               		 </ul> -->
                                    
                                </ul>
                            </li>
                                    <%} }%>
        						<li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-help"></i><span>Help</span></a>
                                <ul class="collapse">
                                    <li><a href="index.html"><span>Contact Us</span></a></li>
                                    <li><a href="index2.html"><span>FAQ</span></a></li>
                                    <li><a href="index3.html"><span>About</span></a></li>
                                </ul>
                            </li>
                            <%-- <li><a href="${pageContext.request.contextPath}/web?action=logout"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li> --%>
                            <li><a href="${pageContext.request.contextPath}/Logout"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>

        <!-- sidebar menu area end -->
        <!-- main content area start -->
         <!-- Breadcrumbs-->
          
             
            <!-- header area end -->
          <div id="loaderarea">
            <!-- page title area end -->
            <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">Server Security</a>
            </li>
            <li class="breadcrumb-item active">Overview</li>
          </ol>
            <div class="container">
       <nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Assets</a>
    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Remidiations</a>
    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Reports</a>
  </div>
</nav>
<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab"  style="padding-top: 15px;padding-left: 5px;">
  <div id="assets">
  <a href="#" class="btn btn-outline-info btn-sm" onclick="hide();" style="border-top-right-radius: 0;border-bottom-right-radius: 0;">Enrol</a><button class="btn btn-outline-info btn-sm" id="audit" onclick="doAudit();" data-toggle="tooltip" title="select any server first" style="border-radius: 0;border-left: none;border-right: none;" disabled="disabled">Audit</button><a href="" onclick="refreshPage();" class="btn btn-outline-info btn-sm" style="border-radius: 0">Refresh</a><button class="btn btn-outline-info btn-sm" id="deleteasset" onclick="deleteResgisteredHost();" data-toggle="tooltip" title="select asset first" style="border-left: none;border-bottom-left-radius: 0px;border-top-left-radius: 0px;" disabled="disabled">Delete</button>
  <div class="table-responsive" style="margin-top: 1rem!important;">
             
       
       <table id="assetsTable" class="table table-bordered table-hover" border="1" >
       <thead>
            <tr>
            <th></th>
                <th>Ip Address</th>
                <th>Username</th>
                <th>OS</th>
                <th>Status</th>
                <th>Hostname</th>
                <th>Compliance</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
       </table> 
       
     
             </div>
  </div>
  <div id="enrol" style="margin-top: 10px;visibility: hidden;">
  <fieldset>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="ipaddress">Ip Address</label>
      <input type="text" class="form-control ip_address" id="ipaddress" placeholder="______.______.______.______">
    </div>
    <div class="form-group col-md-2" style="margin-top: auto;">
     <button id="gethostname" class="btn btn-outline-info btn-sm" onclick="getHostname();" data-toggle="tooltip" title="Enter Ip address to get hostname" disabled="disabled">Get Hostname</button>
    </div>
    <div class="form-group col-md-4">
      <label for="hostname">Hostname</label>
      <input type="text" class="form-control" id="hostname" placeholder="Hostname">
    </div>
  </div>
  
 
  
  <div class="form-row">
    <div class="form-group col-md-4">
    <label for="username">Username</label>
    <input type="text" class="form-control" id="username" placeholder="Username">
  </div>
    <div class="form-group col-md-6">
      <label for="os">Operating System</label>
      <select id="os" class="form-control">
      <option value="">Choose..</option>
        <option value="LINUX_UBUNTU">Ubuntu</option>
									        <option value="WINDOWS">Windows 2012</option>
									        <option value="LINUX_ALPINE">Linux Alpine</option>
									        <option value="LINUX_CENTOS">Centos Linux 7</option>
									        <option value="LINUX_RHEL">Redhat Linux 7</option>
									        <option value="VMWARE_ESXI">VMware Esxi</option>
									        <option value="LINUX_SLES">SLES 12</option>
      </select>
    </div>
    
  </div>
  <!-- <div class="form-group"> -->
    <!-- <div class="form-check"> -->
    <!--   <div class="custom-control custom-checkbox mr-sm-2">
      <input class="custom-control-input" type="checkbox" id="critical">
      <label class="ustom-control-label" for="critical">
       Critical
      </label>
      </div> -->
      <div class="col-6">
                                <div class="custom-control custom-checkbox mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="critical">
                                    <label class="custom-control-label" for="critical" style="color: black;">Critical</label>
                                </div>
                            </div>
    <!-- </div> -->
  <!-- </div> -->
  <div class="form-row" style="padding-top: 1rem!important;padding-left: 1rem!important;">
   <a href="#" class="btn btn-outline-info btn-sm" onclick="show();" style="border-top-right-radius: 0;border-bottom-right-radius: 0;">Cancel</a><button class="btn btn-outline-info btn-sm" id="enrolsend" onclick="show();" disabled="disabled" data-toggle="tooltip" title="operating system is required" style="border-top-left-radius: 0;border-bottom-left-radius: 0; border-left: none;">Enrol</button>
  </div>
  </fieldset>
  
  </div>
  </div>
  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
<div class="table-responsive" style="margin-top: 1rem!important;">
       <table id="remiditiontable" class="table table-bordered" border="1" style="width: 100%;">
       <thead>
            <tr>
                <th>Hostname</th>
                <th>Started By</th>
                <th>Status</th>
                <th>Policy</th>
                <th>Start Time</th>
                <th>End Time</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
       </table> 
             </div>
</div>
  <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
  <div class="table-responsive" style="margin-top: 1rem!important;">
       <table id="reportsTable" class="table table-bordered" border="1" style="width: 100%;">
       <thead>
            <tr>
                <th>Started By</th>
                <th>Hostname</th>
                <th>Status</th>
                <th>Policy</th>
                <th>Start Time</th>
                <th>End Time</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
       </table> 
             </div>

</div>
</div>
</div>
</div>
</div>
    <!-- offset area end -->

    <script src="${pageContext.request.contextPath}/assets/js/owl.carousel.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/metisMenu.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/jquery.slimscroll.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/jquery.slicknav.min.js"></script>
    <!-- others plugins -->
    <script src="${pageContext.request.contextPath}/assets/js/plugins.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/scripts.js"></script> 
     <script type="text/javascript">
    accessToken="<%=session.getAttribute("access_token")%>";
    </script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/joinProject.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/csmartoperations.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/overlayLoader.js"></script>
<script >

</script>
</body>

</html>
