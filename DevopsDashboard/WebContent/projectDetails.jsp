<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="java.util.Base64"%>
<%@ page import="org.json.simple.*"
import="com.cirrus.devops.projects.*"
import="com.cirrus.devops.Modules.*"
import="java.util.ArrayList"
import="java.util.Iterator" %>
<html lang='en'>
<head>
    <meta charset="utf-8" />
	<link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/Logo.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Project_Details</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
   <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css"/>
    <link href="${pageContext.request.contextPath}/assets/css/googleicon.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/robotoFont.css"/>
   <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/styles.css">
  
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/themify-icons.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/default-css.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/metisMenu.css">
    <%-- <link href="${pageContext.request.contextPath}/assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/> --%>
    <link rel="javascript" type="text/javascript" href="${pageContext.request.contextPath}/togglefunction.js"/>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/projects.css"/>
   
    <script src="${pageContext.request.contextPath}/assets/js/loader-gstatic.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/popper.min.js"></script> 
	<script src="${pageContext.request.contextPath}/assets/Highcharts-7.1.0/code/highcharts.js"></script> 
	<!-- <script src="https://code.highcharts.com/stock/highstock.js"></script> -->
	<script src="${pageContext.request.contextPath}/assets/Highcharts-7.1.0/code/modules/drilldown.js"></script>
	<script src="${pageContext.request.contextPath}/assets/Highcharts-7.1.0/code/modules/series-label.js"></script>
	<script src="${pageContext.request.contextPath}/assets/Highcharts-7.1.0/code/modules/exporting.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/export-data.js"></script>
	 <script src="${pageContext.request.contextPath}/assets/js/jquery-3.3.1.min.js"></script>
	 <script type="text/javascript" src="${pageContext.request.contextPath}/js/loadingoverlay.js"></script>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/loaderOverlay.css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/sweetalert.min.js"></script>
 <style>
.dropdown-menu{
	background-color: #03A9F4;
}
 .dropdown-item a:hover{
	background-color:#03A9F4; 
}
    	.parent {
  position: relative;
  top: 0;
  left: 0;
}
#cirruslogobg {
  position: relative;
   top: -14px;
    left: -24px;
}
#cirruslogo {
  position: absolute;
  top: 0px;
  left: 0px;
}
h4{
font-weight: 500;
margin: 0px;
}
span{
	font-size:small;
	}
.card-body {
     padding: 0px; 
     padding: 0rem; 
}
.form-control, .form-control:focus{
background-color: white!important;
}
#loader {
  background-image:url("${pageContext.request.contextPath}/assets/img/Logo.png");
  }
 
</style> 
<script type="text/javascript">
$(function(){
    $(".dropdown").hover(            
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            });
    });
    
</script>
</head>
<body>
    <%
response.setHeader("Cashe-Control", "no-cache, no-store, must-revalidate");//HTTP 1.1
response.setHeader("Pragma", "no-cache");//HTTP 1.0
response.setDateHeader("Expires", 0);//Proxies
if (session.getAttribute("ProjectsList") == null || session.getAttribute("username") == null){
		response.sendRedirect("index.jsp");
		return;
	}
String ckusername=null;
String ckpass=null;
String ckaccess=null;
String ckvalid=null;
Cookie cookie = null;
Cookie[] cookies = null;
cookies = request.getCookies();
if( cookies != null ) {
   for (int i = 0; i < cookies.length; i++) {
      if(cookies[i].getName().equals("IQSESSIONID")){
    	  try{
    	 String token= cookies[i].getValue();
    	 Base64.Decoder decoder = Base64.getMimeDecoder();
    	 String tokendecoded=new String(decoder.decode(token));
    	 String[] tokendecode=tokendecoded.split("!q4Ev");
    	String ckusernameencode=new String(decoder.decode(tokendecode[0]));
    	String ckpassencode=new String(decoder.decode(tokendecode[1]));
    	String ckaccessencode=new String(decoder.decode(tokendecode[2]));
    	String ckvalidencode=new String(decoder.decode(tokendecode[3]));
    	ckusername=ckusernameencode;
    	ckpass=ckpassencode;
    	ckaccess=ckaccessencode;
    	ckvalid=ckvalidencode;
    	  }catch(Exception e){
    		  
    	  }
      }
   }
} 
%>
     <div class="page-container">
        <!-- sidebar menu area start -->
        <div class="sidebar-menu">
            <div class="sidebar-header">
                <div class="logo" style="/* width: 300px; */">
                 <div class="parent">
  						<img id="cirruslogobg" src="${pageContext.request.contextPath}/assets/img/DevopsBg.png" style="float: left;height: 110px;">
  						<img id="cirruslogo" alt="" src="${pageContext.request.contextPath}/assets/img/RevisedHomeCirrusDigitalLogo-01.png" style="float: left;height: 70px;padding-top: 10px;">
					</div>
                   <%-- <img alt="" src="${pageContext.request.contextPath}/assets/img/RevisedHomeCirrusDigitalLogo-01.png" style="padding-top: 3px;"> --%>
                </div>
            </div>
            <div class="main-menu">
                <div class="menu-inner">
                    <nav>
                        <ul class="metismenu" id="menu">
                        <li><a class="iqloader" href="${pageContext.request.contextPath}/Home.jsp"><i class="ti-home"></i> <span>Home</span></a></li>
                        <li><a class="iqloader" href="${pageContext.request.contextPath}/web?action=dashboard"><i class="ti-dashboard"></i> <span>Dashboard</span></a></li>
                        <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-folder"></i><span>Projects</span></a>
                                <ul class="collapse">
                                    <!-- <li><a href="#" onclick="hide();"><i class="ti-view-list-alt" style="padding-right:10px; "></i><span>List of Projects</span></a></li> -->
                                     <%if (ckvalid!=null){
                                     if (ckvalid.equals("true")){%>
                                    <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-package"></i><span>Join Project</span></a>
                                <ul class="collapse">
                                    <li><table>
    				<tr>
    				<tr>
    				<td>
    				<div class="form-group has-search">
                 <input class="form-control" type="text" placeholder="Search.." id="myInput" list="projectList" onkeyup="filterFunction()" style="background-color: aliceblue;"></div>
					    <datalist id="projectList" ></datalist></td></tr>
					    
					    <tr>
                       <td>
                       <button class="btn btn-primary btn-sm" id="btn" style="border-radius: 1rem;padding: 5px 13px;box-shadow: 0 1px 3px 0 rgba(0,0,0,.2);">Request Access</button>
							<p id="err" style="color:red; display:none"> Project/Group access is disabled </p>
							<p id="success" style="color:green; display:none"> </p></td></tr>
							</table></li>
                                </ul>
                            </li> 
                             <%}else{ %>
                              <li>
                                <a href="#"><i class="ti-package"></i><span>Join Project</span></a>
                            </li> 
                              <%}} %>
                            <%if (ckaccess!=null && ckvalid!=null){
                            if (ckaccess.equals("admin") && ckvalid.equals("true")){%>
                                    <li><a href="${pageContext.request.contextPath}/web?action=project" class="iqloader"><i class="ti-new-window"></i><span>Create project</span></a></li>
                                    <%}} %>
                                </ul>
                            </li>
                            <%if (ckaccess!=null){
                            if (ckaccess.equals("admin")){%>
                                    <!-- <li><a href="#" data-toggle="modal" data-target="#licenseupdate"><i class="ti-new-window"></i><span>License</span></a></li> -->
                                    <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-settings"></i><span>Admin</span></a>
                                <ul class="collapse">
                                    <li><a href="${pageContext.request.contextPath}/license.jsp" class="iqloader"><i class="fa fa-unlock-alt"></i><span>License</span></a></li>
                                    <%if (ckvalid!=null){
                                    if (ckvalid.equals("true")){%>
                                    <li><a href="${pageContext.request.contextPath}/UserManagement.jsp" class="iqloader"> <i class="ti-user"></i><span>User Management</span></a></li>
                                      <%}} %>
                                    <!-- <ul class="collapse">
                                    <li><a href="index.html"><i class="fa fa-user-plus"></i><span>Add User</span></a></li>
                                    <li><a href="index2.html"><i class="fa fa-edit"></i><span>Modify User</span></a></li>
                                    <li><a href="index3.html"><i class="ti-trash"></i><span>Delete User</span></a></li>
                               		 </ul> -->
                                    
                                </ul>
                            </li>
                                    <%} }%>
        						<li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-help"></i><span>Help</span></a>
                                <ul class="collapse">
                                    <li><a href="index.html"><span>Contact Us</span></a></li>
                                    <li><a href="index2.html"><span>FAQ</span></a></li>
                                    <li><a href="index3.html"><span>About</span></a></li>
                                </ul>
                            </li>
                            <%-- <li><a href="${pageContext.request.contextPath}/web?action=logout"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li> --%>
                            <li><a href="${pageContext.request.contextPath}/Logout"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        
          <!-- class="content" -->
    
         <div id="loaderarea">
         <div class="main-content">
         <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">PROJECT</a>
            </li>
            <li class="breadcrumb-item active"><%=request.getParameter("projName")%></li>
          </ol>
          <div id="errmsg1" style="color:red; font-size: small;float: right;"></div>
                                        <div id="errmsg2" style="color:red; font-size: small;float: right;"></div>
                                        <div id="name" style="font-size: medium; visibility: hidden;" ></div>
            <div class="main-content-inner">
                <!-- line chart start -->
                <div id="projectDetails" class="row" style="visibility:visible; ">
                    <div id="firstContainer" class="col-lg-6 mt-5" style="margin-top: 0rem !important;">
                        <div class="card">
                           <!--  <div class="card-body"> -->
                                <div id="avgTime" style="height:400px"><div id="avgnotfound" style="opacity: 0.4; visibility: hidden;padding-top: 30%;padding-left: 7%;"><h1>Average details not found</h1></div></div>
                                 <div id="container">
                                 
                                 </div>
									    <!-- show average time taken for each build -->
									    
                            <!-- </div> -->
                        </div>
                    </div>
                    <div class="col-lg-6 mt-5" style="margin-top: 0rem !important;">
                        <div class="card">
                            <div class="card-body">
                               <div id="stageTime" style="height:400px"><div id="stagenotfound" style="opacity: 0.4; visibility: hidden;padding-top: 30%;padding-left: 7%;"><h1>Stage details not found</h1></div></div>
                                 
						                <!-- show stage wise duration --> 
						                
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 mt-5">
                        <div class="card">
                            <div class="card-body">
                                <div id="sonarDetailsChart" style="height:400px;width:100%; margin: 0 auto"><div id="qualitynotfound" style="opacity: 0.4; visibility: hidden;padding-top: 30%;padding-left: 7%;"><h1>Quality details not found</h1></div></div>
                                
													<!-- show sonar details here -->
													
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 mt-5">
                        <div class="card">
                            <div class="card-body">
                                 <div id="JUnitDet" style="height:400px;width:100%;"><div id="testnotfound" style="opacity: 0.4; visibility: hidden;padding-top: 30%;padding-left: 7%;"><h1>Unit test details not found</h1></div></div>
						                          
													<!-- JUNit results -->
													
                            </div>
                        </div>
                    </div>
                    
                     <div class="col-lg-6 mt-5">
                        <div class="card">
                            <div class="card-body">
                                 <div id="commitsPerDay" style="height:400px;width:100%;"><div id="commitsnotfound" style="opacity: 0.4; visibility: hidden;padding-top: 30%;padding-left: 7%;"><h1>Commit details not found</h1></div></div>
						                          
													<!-- JUNit results -->
                            </div>
                        </div>
                    </div>
                    
                     <div class="col-lg-6 mt-5">
                        <div class="card">
                            <div class="card-body">
                                 <div id="issueTracking" style="height:400px;width:100%;"><div id="issuestracking" style="opacity: 0.4; visibility: hidden;padding-top: 30%;padding-left: 7%;"><h1>Issues tracking not found</h1></div></div>
						                          
													<!--Issue Tracking -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 mt-5">
                        <div class="card">
                            <div class="card-body">
                                 <div id="jiraIssueTracking" style="height:400px;width:100%;"><div id="jiraIssuesTracking" style="opacity: 0.4; visibility: hidden;padding-top: 30%;padding-left: 7%;"><h1>Jira issues tracking not found</h1></div></div>
						                          
													<!-- Jira Issue Tracking -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- line chart end -->
            </div>
        </div>
        </div>
        </div>
    
  
<%-- 
<div id="projectDetails"  style="visibility:visible; ">
                                <div class="card " >
                                    <div class="header">
                                    <div id="errmsg" style="color:red; font-size: small;float: right;"></div>
                                     	   <h4>PROJECT:<%=request.getParameter("projName")%></h4>
                                        <div id="errmsg1" style="color:red; font-size: small;float: right;"></div>
                                        <div id="errmsg2" style="color:red; font-size: small;float: right;"></div>
                                      <!-- <h5 id="name"></h5> -->
                                       <div id="name" style="font-size: medium; visibility: hidden;" ></div>
                                        <!-- <div class="content"> -->
						            <div class="container-fluid">
						                <div class="row">
						                <!-- <div class="col-md-4">
						                <div class="card" style="height:50%;width:100%; overflow: auto;">
						                 <div id="chartContainer"></div>
										<div id="container" style="height:100%; width:100%;"></div>
										
												    show number of build per day
												    
						                </div>
						                </div> -->
						                <div class="col-md-6">
						                <div id="avgTime" class="card">
						                 <div id="chartContainer"></div>
						                 <div id="avgTime" style="height:400px"></div>
						                 
									    <!-- show average time taken for each build -->
									    
						                </div>
						                </div>
						                
						                  <div class="col-md-6">
						                <div id="stageDuration" class="card"> 
						                 <div id="chartContainer"></div>
						                 <div id="stageTime" style="height:400px"></div>
						                 
						                <!-- show stage wise duration --> 
						                
						                 </div>
						                </div>
						                 <!-- <div class="col-md-4">
						                <div class="card" style="height:50%;width:100%; overflow: auto;">
						                 <div id="chartContainer"></div>
						                 <div id="succesFail" style="height:400px"></div>
						                 
						                show number of successful and failure builds per day 
						                
						                </div>
						                </div> -->
						                <!-- <div class="col-md-4">
						                        <div class="card" style="height:50%;width:100%; overflow: auto;">
						                         <div id="chartContainer"></div>
						                         <div id="allBuilds" style="height:400px"></div>
						                         
												show total builds with respect to date
													
						                            </div>
						                        </div> -->
						                        <div class="col-md-6">
						                      <div id="sonarResult" class="card">
						                         <div id="chartContainer"></div>
						                         <div id="sonarDetailsChart" style="height:400px;width:100%; margin: 0 auto"></div>
						                         
													<!-- show sonar details here -->
													
						                            <!-- </div> -->
						                        </div>
						                        </div>
						                        <div class="col-md-6">
						                        <div id="JUnitResult" class="card">
						                         <div id="chartContainer"></div>
						                          <div id="JUnitDet" style="height:400px;width:100%;"></div>
						                          
													<!-- JUNit results -->
													
						                        </div>
						                        </div>
						                        
						                    </div>
						                    </div>
						                </div>
                                    </div>
                                   </div> --%>
                             <!--  <div id="errmsg" style="color:red;font-size:small;"></div> -->
                              <div id="errmsg1" style="color:red;font-size:small;"></div>    
                           <!--  </div> -->
     <script src="${pageContext.request.contextPath}/assets/js/owl.carousel.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/metisMenu.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/jquery.slimscroll.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/jquery.slicknav.min.js"></script>
    <!-- others plugins -->
    <script src="${pageContext.request.contextPath}/assets/js/plugins.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/scripts.js"></script>
     <script>
var userName="<%=session.getAttribute("username")%>";
var userPassword="<%=session.getAttribute("userpassword")%>";
</script> 
<%String name=request.getParameter("projName"); 
    String accessToken=session.getAttribute("access_token").toString();
    %>
<script type="text/javascript">
var accesstoken="<%=session.getAttribute("access_token")%>";
var projectName="<%=request.getParameter("projName")%>";
var projectId="<%=request.getParameter("projId")%>";
console.log(projectId);
</script>
	 <script>var ctx = "${pageContext.request.contextPath}"</script>
	  <script src="${pageContext.request.contextPath}/js/projDetails.js"></script>
	  <script src="${pageContext.request.contextPath}/js/commits.js"></script>
	 <script src="${pageContext.request.contextPath}/js/junitDetails.js"></script>
       <script src="${pageContext.request.contextPath}/js/SonarProjDetails.js"></script> 
     <script src="${pageContext.request.contextPath}/js/stageDet.js"></script> 
     <script type="text/javascript" src="${pageContext.request.contextPath}/js/issueTracking.js"></script> 
     <script type="text/javascript" src="${pageContext.request.contextPath}/js/jiraIssueTracking.js"></script>
      <script type="text/javascript">
    accessToken="<%=session.getAttribute("access_token")%>";
    </script>
     <script>var ctx = "${pageContext.request.contextPath}"</script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/joinProject.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/overlayLoader.js"></script>
</body>

</html>
