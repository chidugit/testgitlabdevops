<!DOCTYPE html>
<%@page import="java.lang.reflect.Array"%>
<%@page import="javax.ws.rs.core.Response"%>
<%@page import="com.cirrus.devops.service.JenkinsService"%>
<%@page import="com.cirrus.devops.service.GetGitlabGroups"%>
<%@page import="java.util.Base64"%>
<%@ page import="org.json.simple.*"
import="com.cirrus.devops.projects.*"
import="com.cirrus.devops.Modules.*"
import="java.util.ArrayList"
import="java.util.Iterator" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
    <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/Logo.png">
    <title>Update User</title>
      <script>var ctx = "${pageContext.request.contextPath}"</script> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="${pageContext.request.contextPath}/assets/images/icon/favicon.ico">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/bootstrapValidator.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/themify-icons.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/metisMenu.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/owl.carousel.min.css">
   
     <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/default-css.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/styles.css">
   
  <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/jquery.3.2.1.min.js"></script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/bootstrapValidator.min.js"></script>
   <script src="${pageContext.request.contextPath}/assets/js/popper.min.js"></script>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/jquery-ui.min.css"/>
  <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/jquery-ui.min.js"></script>
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/DataTables-1.10.18/css/dataTables.bootstrap4.min.css"/>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/DataTables-1.10.18/js/dataTables.bootstrap4.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/sweetalert.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/loadingoverlay.js"></script>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/loaderOverlay.css">
	<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
<style>
	thead, tr, th, td {
	    text-align: center;
	    padding: 1%;
	}
	    	.parent {
	  position: relative;
	  top: 0;
	  left: 0;
	}
	#cirruslogobg {
	  position: relative;
	  top: -14px;
    left: -24px;
	}
	#cirruslogo {
	  position: absolute;
	  top: 0px;
	  left: 0px;
	}
	label{
	display:inline-block;
	margin-right:30px;
	width:200px;
	}
	span{
	font-size:small;
	}
	
	div.dataTables_wrapper div.dataTables_length select {
    width: 60px;
    margin: 0 10px;
}
.card-body {
    padding: 1.6rem;
}

.form-control, .form-control:focus{
background-color: white!important;
box-shadow: 0 1px 3px 0 rgba(0,0,0,.2);
}
.mr-5, .mx-5 {
    margin-right: 0em!important;
}
.main-content-inner {
    padding: 0 30px 0px;
}
table.dataTable{
border-collapse:collapse !important;
}
.table > thead > tr > th{
padding-bottom:10px;
}
table.dataTable thead>tr>th.sorting_asc, table.dataTable thead>tr>th.sorting_desc, table.dataTable thead>tr>th.sorting, table.dataTable thead>tr>td.sorting_asc, table.dataTable thead>tr>td.sorting_desc, table.dataTable thead>tr>td.sorting{
color: black;
background-color: #DEE9EB;
}
thead, tr, th, td{
color: gray;
font-size: small;
}
.modal-header{
	border-bottom: none;
}
.modal-footer{
	border-top: none;
}
.repoertckeckUnckeck{
	width: 18px;
    margin-top: -4px;
    margin-right: 4px;
}
.required-field::after {
    content: "*";
    color: red;
}
.card {
    box-shadow: 0 1px 3px 0 rgba(0,0,0,.2);
    margin-left: 1rem;
}
#loader {
  background-image:url("${pageContext.request.contextPath}/assets/img/Logo.png");
  }
</style>
</head>  
<body>

<%
response.setHeader("Cashe-Control", "no-cache, no-store, must-revalidate");//HTTP 1.1
response.setHeader("Pragma", "no-cache");//HTTP 1.0
response.setDateHeader("Expires", 0);//Proxies
 if (session.getAttribute("ProjectsList") == null || session.getAttribute("username")==null){
		response.sendRedirect("index.jsp");
		return;
	}

 if (session.getAttribute("validity").equals("true")== false){
	 response.sendRedirect(request.getContextPath());
		return;
	}
 String ckusername=null;
 String ckpass=null;
 String ckaccess=null;
 String ckvalid=null;
 Cookie cookie = null;
 Cookie[] cookies = null;
 cookies = request.getCookies();
 if( cookies != null ) {
    for (int i = 0; i < cookies.length; i++) {
       if(cookies[i].getName().equals("IQSESSIONID")){
     	  try{
     	 String token= cookies[i].getValue();
     	 Base64.Decoder decoder = Base64.getMimeDecoder();
     	 String tokendecoded=new String(decoder.decode(token));
     	 String[] tokendecode=tokendecoded.split("!q4Ev");
     	String ckusernameencode=new String(decoder.decode(tokendecode[0]));
     	String ckpassencode=new String(decoder.decode(tokendecode[1]));
     	String ckaccessencode=new String(decoder.decode(tokendecode[2]));
     	String ckvalidencode=new String(decoder.decode(tokendecode[3]));
     	ckusername=ckusernameencode;
     	ckpass=ckpassencode;
     	ckaccess=ckaccessencode;
     	ckvalid=ckvalidencode;
     	  }catch(Exception e){
     		  
     	  }
       }
    }
 } 
%>
  
    <!-- page container area start -->
    <div class="page-container">
        <!-- sidebar menu area start -->
        <div class="sidebar-menu">
            <div class="sidebar-header">
                <div class="logo" style="/* width: 300px; */">
                 <div class="parent">
  						<img id="cirruslogobg" src="${pageContext.request.contextPath}/assets/img/DevopsBg.png" style="float: left;height: 110px;">
  						<img id="cirruslogo" alt="" src="${pageContext.request.contextPath}/assets/img/RevisedHomeCirrusDigitalLogo-01.png" style="float: left;height: 70px;padding-top: 10px;">
					</div>
                </div>
            </div>
            <div class="main-menu">
                <div class="menu-inner">
                    <nav>
                        <ul class="metismenu" id="menu">
                        <li><a class="iqloader" href="${pageContext.request.contextPath}/Home.jsp"><i class="ti-home"></i> <span>Home</span></a></li>
                        <li><a class="iqloader" href="${pageContext.request.contextPath}/web?action=dashboard"><i class="ti-dashboard"></i> <span>Dashboard</span></a></li>
                        <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-folder"></i><span>Projects</span></a>
                                <ul class="collapse">
                                    <!-- <li><a href="#" onclick="hide();"><i class="ti-view-list-alt" style="padding-right:10px; "></i><span>List of Projects</span></a></li> -->
                                     <%if (ckvalid!=null){
                                     if (ckvalid.equals("true")){%>
                                    <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-package"></i><span>Join Project</span></a>
                                <ul class="collapse">
                                    <li><table>
    				<tr>
    				<tr>
    				<td>
    				<div class="form-group has-search">
                 <input class="form-control" type="text" placeholder="Search.." id="myInput" list="projectList" onkeyup="filterFunction()" style="background-color: aliceblue;"></div>
					    <datalist id="projectList" ></datalist></td></tr>
					    
					    <tr>
                       <td>
                       <button class="btn btn-primary btn-sm" id="btn" style="border-radius: 1rem;padding: 5px 13px;box-shadow: 0 1px 3px 0 rgba(0,0,0,.2);">Request Access</button>
							<p id="err" style="color:red; display:none"> Project/Group access is disabled </p>
							<p id="success" style="color:green; display:none"> </p></td></tr>
							</table></li>
                                </ul>
                            </li> 
                             <%}else{ %>
                              <li>
                                <a href="#"><i class="ti-package"></i><span>Join Project</span></a>
                            </li> 
                              <%}} %>
                            <%if (ckaccess!=null && ckvalid!=null){
                            if (ckaccess.equals("admin") && ckvalid.equals("true")){%>
                                    <li><a href="${pageContext.request.contextPath}/web?action=project" class="iqloader"><i class="ti-new-window"></i><span>Create project</span></a></li>
                                    <%}} %>
                                </ul>
                            </li>
                            <%if (ckaccess!=null){
                            if (ckaccess.equals("admin")){%>
                                    <!-- <li><a href="#" data-toggle="modal" data-target="#licenseupdate"><i class="ti-new-window"></i><span>License</span></a></li> -->
                                    <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-settings"></i><span>Admin</span></a>
                                <ul class="collapse">
                                    <li><a href="${pageContext.request.contextPath}/license.jsp" class="iqloader"><i class="fa fa-unlock-alt"></i><span>License</span></a></li>
                                    <%if (ckvalid!=null){
                                    if (ckvalid.equals("true")){%>
                                    <li><a href="${pageContext.request.contextPath}/UserManagement.jsp" class="iqloader"> <i class="ti-user"></i><span>User Management</span></a></li>
                                      <%}} %>
                                    <!-- <ul class="collapse">
                                    <li><a href="index.html"><i class="fa fa-user-plus"></i><span>Add User</span></a></li>
                                    <li><a href="index2.html"><i class="fa fa-edit"></i><span>Modify User</span></a></li>
                                    <li><a href="index3.html"><i class="ti-trash"></i><span>Delete User</span></a></li>
                               		 </ul> -->
                                    
                                </ul>
                            </li>
                                    <%} }%>
        						<li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-help"></i><span>Help</span></a>
                                <ul class="collapse">
                                    <li><a href="index.html"><span>Contact Us</span></a></li>
                                    <li><a href="index2.html"><span>FAQ</span></a></li>
                                    <li><a href="index3.html"><span>About</span></a></li>
                                </ul>
                            </li>
                            <%-- <li><a href="${pageContext.request.contextPath}/web?action=logout"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li> --%>
                            <li><a href="${pageContext.request.contextPath}/Logout"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>

        <!-- sidebar menu area end -->
        <!-- main content area start -->
         <!-- Breadcrumbs-->
          <div id="loaderarea">
            <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">User Management</a>
            </li>
            <li class="breadcrumb-item active">Overview</li>
              <%if(request.getAttribute("licenceupdatefail")!=null){ %> 
				<div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: auto;margin-bottom: auto;padding: 0.75rem 1.25rem;padding-right: 4rem;">
                                                <%-- <div id="errorMassage" style="color:red;font-size: small;padding-bottom:5px;">${errorMessage}</div> --%>
            <div id="licenceupdate">${licenceupdatefail}</div>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="font-size: larger;padding: 0.6rem 1.25rem;">
                                                <span class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        <%} %>
          </ol>
     <div class="card">
                            <div class="card-body">
                              <!--   <h4 class="header-title">Users</h4> -->
                                <div id="accordion4" class="according accordion-s2 gradiant-bg">
                                    <div class="card">
                                        <div class="card-header">
                                            <a class="card-link collapsed"  data-toggle="collapse"  href="#accordion41" >Add New User</a>
                                        </div>
                                        <div id="accordion41" class="collapse" data-parent="#accordion4">
                                           <div class="card-body">
                                          <!--  <form id="adduserform" class="needs-validation" novalidate  data-bv-feedbackicons-valid="glyphicon glyphicon-ok"
    data-bv-feedbackicons-invalid="glyphicon glyphicon-remove"
    data-bv-feedbackicons-validating="glyphicon glyphicon-refresh"> -->
                                           <fieldset id="adduserform">
  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="validationCustom01" class="required-field">First name</label>
      <input type="text" class="form-control" id="validationCustom01" placeholder="First name"  required>
      <div class="valid-feedback">
        Looks good!
      </div>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationCustom02" class="required-field">Last name</label>
      <input type="text" class="form-control" id="validationCustom02" placeholder="Last name"required>
      <div class="valid-feedback">
        Looks good!
      </div>
    </div>
   <div class="col-md-3 mb-3">
      <label for="validationCustom05" class="required-field">Email Id</label>
      <input type="email" class="form-control" id="validationCustom05" placeholder="email" readonly onfocus="this.removeAttribute('readonly');" required>
      <div class="invalid-feedback">
        Please provide a valid Email Id.
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-3 mb-3">
        <div class="form-group">
        <label for="validationCustom04" class="required-field">User Access</label>
    <select id="validationCustom04" class="custom-select" required style="box-shadow: 0 1px 3px 0 rgba(0,0,0,.2);">
      <option value="">Access</option>
      <option value="Admin">Admin</option>
      <option value="User">User</option>
    </select>
    <div class="invalid-feedback">Access value is required</div>
  </div>
    </div>
  <div class="col-md-3 mb-3">
      <label for="validationCustomUsername" class="required-field">Username</label>
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroupPrepend" style="padding: 0.0rem 0.75rem;">@</span>
        </div>
        <input type="text" class="form-control" id="validationCustomUsername" placeholder="Username" aria-describedby="inputGroupPrepend" required>
        <div class="invalid-feedback">
          Please choose a username.
        </div>
      </div>
    </div>
    <div class="col-md-3 mb-3">
      <label for="validationCustom03" class="required-field">Password</label>
      <input type="password" class="form-control" id="validationCustom03" placeholder="Password" required name="password" data-bv-identical="true"
                data-bv-identical-field="confirmPassword"
                data-bv-identical-message="The password and its confirm are not the same" />
      <div class="invalid-feedback">
        Please provide a valid Password.
      </div>
    </div>
      <div class="col-md-3 mb-3">
      <label for="validationCustom031" class="required-field">Confirm Password</label>
      <input type="password" class="form-control" name="confirmPassword" id="validationCustom031" placeholder="Confirm Password" required  data-bv-identical="true"
                data-bv-identical-field="password"
                data-bv-identical-message="The password and its confirm are not the same" />
      <div class="invalid-feedback">
        Please provide a valid Password.
      </div>
    </div>
  </div>
  <!-- <div class="form-group">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
      <label class="form-check-label" for="invalidCheck">
        Agree to terms and conditions
      </label>
      <div class="invalid-feedback">
        You must agree before submitting.
      </div>
    </div>
  </div> -->
  <button id="adduserbtn" class="btn btn-primary" type="submit" name="adduserval" disabled="disabled" data-toggle="tooltip" title="Fill all the required fields to enable submit" style="background-image: linear-gradient(to bottom right, #d8d6e6a8, #3c34985e);border-color: transparent;">Add User</button>
  </fieldset>
<!-- </form> -->

                                    </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            <a class="card-link collapsed" data-toggle="collapse" href="#accordion42">Delete User</a>
                                        </div>
                                        <div id="accordion42" class="collapse" data-parent="#accordion4">
                                            <div class="card-body" style="padding: 1.5rem 1rem!important;">
                                               <fieldset>
                                               		<div class="form-row">
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" id="deleteuser" placeholder="Enter username" style="margin-top: 0.4rem!important;">
                                                </div>
                                                <div class="col-auto my-1">
                                                    <button type="submit" id="deleteuserbtn" class="btn btn-outline-danger btn-sm" style="padding: 0.4rem 1rem!important; border-radius: 1rem!important; margin-top: inherit!important;box-shadow: 0 1px 3px 0 rgba(0,0,0,.2);">Delete</button>
                                                </div>
                                            </div>
                                               </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            <a class="card-link collapsed" data-toggle="collapse" href="#accordion43">Edit User Information</a>
                                        </div>
                                        <div id="accordion43" class="collapse" data-parent="#accordion4">
                                            <div class="card-body" style="padding: 1.5rem 1rem!important;">
                                                  <fieldset>
                                               		<div class="form-row">
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" id="edituser" placeholder="Enter username" style="margin-top: 0.4rem!important;">
                                                </div>
                                                <div class="col-auto my-1">
                                                    <button type="submit" id="edituserbtn" class="btn btn-outline-info btn-sm" data-toggle="modal" data-target="#myModal" onclick="editUser()" style="padding: 0.4rem 1rem!important; border-radius: 1rem!important; margin-top: inherit!important;box-shadow: 0 1px 3px 0 rgba(0,0,0,.2);">Edit</button>
                                                </div>
                                            </div>
                                               </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>	
                        </div>		
                </div>
        <!-- main content area end -->
          <div class="container">
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
          <div class="modal-dialog">
          
            <!-- Modal content-->
            <div class="modal-content" style="opacity: 0.95;">
              <div class="modal-header">
              <h4 class="modal-title">Edit User</h4>
               <button type="button" class="close" data-dismiss="modal">&times;</button>
                
              </div>
              <div class="modal-body">
              <form id="myform" action="" >
             <fieldset style="border:none">
             <label>First Name:</label>  <input type="text" id="firstname" class="form-control">
             <label>Last Name:</label> <input type="text" id="lastname" class="form-control">
             <label>Username:</label> <input type="text" id="username" class="form-control">
             <label>Password:</label> <input type="text" id="passwordappend" class="form-control"><br>
             <label>Email Address:</label> <input type="text" id="emailaddress" class="form-control"><br>
             <label>Access</label> <br>
             <hr>
             <label>Admin:</label><input type="checkbox" name="access" id="adminaccess" onclick="ckChange(this);"><br> 
             <label>User:</label><input type="checkbox" name="access" id="useraccess" onclick="ckChange(this);">
             </fieldset>
			</form>
             </div>
              <div class="modal-footer">
              <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
              	<input id="submit" type="submit" class="btn btn-primary btn-sm" data-dismiss="modal" value='Update' onclick="update()" ></input>
              </div>
            </div>
            
          </div>
        </div>
        
      </div> 
    
    <!-- offset area end -->
    <script src="${pageContext.request.contextPath}/assets/js/owl.carousel.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/metisMenu.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/jquery.slimscroll.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/jquery.slicknav.min.js"></script>
    <!-- others plugins -->
    <script src="${pageContext.request.contextPath}/assets/js/plugins.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/scripts.js"></script> 
     <script>
    if('<%=session.getAttribute("username")%>'!=null){
 	   var username="<%=session.getAttribute("username").toString()%>";	
     }
    if('<%=session.getAttribute("userid")%>'!=null){
 	   var userid="<%=session.getAttribute("userid").toString()%>";	
     }
    if('<%=session.getAttribute("serverPath")%>'!=null){
 	   var ctxpath="<%=session.getAttribute("serverPath").toString()%>";
     }
    if (typeof(Storage) !== "undefined") {  
    	localStorage.setItem("username", username);
    	localStorage.setItem("userid", userid);
    	localStorage.setItem("ctxpath", ctxpath);
    	} else {  
    	}   
    </script>
     <script type="text/javascript">
    accessToken="<%=session.getAttribute("access_token")%>";
    </script>
     <script>var ctx = "${pageContext.request.contextPath}"</script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/joinProject.js"></script>
     <script type="text/javascript" src="${pageContext.request.contextPath}/js/usermanagement.js"></script>
     <script type="text/javascript" src="${pageContext.request.contextPath}/js/overlayLoader.js"></script>
    <script>
  /*   $('#adminaccess, #useraccess').change(function () {
        if ($('#adminaccess').is(":checked")) {
            $('#useraccess').attr('disabled', true);
        }else if($('#adminaccess').not(":checked")) {
            $('#useraccess').attr('disabled', false);
        } else if($('#useraccess').is(":checked")) {
            $('#adminaccess').attr('disabled', true);
        }else if($('#useraccess').not(":checked")) {
            $('#adminaccess').attr('disabled', false);
        }
    }); */
    function ckChange(el) {
    	  var ckName = document.getElementsByName(el.name);
    	  for (var i = 0, c; c = ckName[i]; i++) {
    	   c.disabled = !(!el.checked || c === el);
    	  }
    	}
    
    </script>
</body>

</html>
